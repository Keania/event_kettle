import Vue from 'vue';
import Search from './components/admin/Search.vue';
import AdminList  from './components/admin/AdminList.vue';
import AdminForm  from './components/admin/AdminForm.vue';
import UsersPanel from './components/admin/UsersPanel.vue';
import AdvertPanel from './components/admin/UserAdvertPanel.vue';

window.App = new Vue({
	el : "#pt-container",

	components : {
		UsersPanel,
		AdvertPanel,
		'adminlist' : AdminList,
		'AdminForm' : AdminForm,
		'client-search' : Search
	},

	data: {
		userAction: 'adverts',
		canShowAdvert: false,
		advertsRecord: [],
		UserPanel: {
			UserList: {}
		},
		records: {},
	},

	methods: {
		setAdvertData(data) {
			console.log('Logging the returned data', data);
			// this.records[data.id] = data;
			// this.advertsRecord = this.records[data.id].adverts;
		}
	}
});
