
const Blog = new Vue({
	el: '#blog-app',
	data: {

	}
});

var BlogImage = (function () {
    function BlogImage(x) {
        this.props = {
            width: 0,
            height: 0,
        };
        this.className = "img-shorten";
        try {
            this.self = $(x);
            this.child = this.self.find('img');
            this.props = JSON.parse(this.self.attr(BlogImage.imageAttr));
            this.props = { width: this.self.width(), height: this.self.height() };
            this.startUp();
            console.log(this);
        }
        catch (b) {
            console.error('Image Error Occurred');
        }
    }
    BlogImage.prototype.startUp = function () {
        var _a = this, self = _a.self, props = _a.props, className = _a.className;
        if (props.height > BlogImage.maxHeight) {
            self.addClass(className);
            this.activateClick();
        }
    };
    BlogImage.prototype.activateClick = function () {
        var _a = this, self = _a.self, className = _a.className, child = _a.child;
        self.click(function () {
            if (self.hasClass(className)) {
                var height = child.height() + "px";
                self.animate({ height: height }, 400, 'swing', function () {
                });
                self.removeClass(className);
            }
            else {
                self.addClass(className);
                self.attr('style', '');
            }
        });
    };
    return BlogImage;
}());

BlogImage.maxHeight = 350;
BlogImage.imageAttr = "data-attr";
$(function () {
    configureImages();
    activeNavBarScrolling();
    var backTopButton = $('#back-2-top');
    backTopButton.click(function () {
        $('body, html').animate({ scrollTop: "5px" }, 700, 'swing');
    });
});
/**
 * configures the post images with the width/height size
 * @return null
 */
function configureImages() {
    var figures = $("figure[data-attr]");
    figures.each(function (x) {
        var figure = new BlogImage(this);
    });
}
/**
 * manipulates the blog page header-nav when scroll pass a limit.
 * @return void
 */
function activeNavBarScrolling() {
    var top = $(this).scrollTop();
    var navbrand = $('.navbar-brand');
    var mainNav = $('body > .navbar-default');
    var className = 'shift';
    if (top > 100) {
        if ($(this).width() > 768)
            mainNav.addClass('hideMenu');
        navbrand.addClass(className);
    }
    else {
        if ($(this).width() > 768)
            mainNav.removeClass('hideMenu');
        navbrand.removeClass(className);
    }
}
/**
 *shows or adds the back 2 top button on the page
 *
 *@return void
 */
function back2Top() {
    var win = $(window);
    var button = $('#back-2-top');
    if (win.scrollTop() > 300) {
        button.css({ right: "30px" });
        setTimeout(function () {
            button.removeClass('invisible');
        });
    }
    else {
        button.css({ right: "-100px" }, function () {
            button.addClass('invisible');
        });
    }
}

$(window).scroll(function () {
    back2Top.call(this);
    activeNavBarScrolling.call(this);
});
//# sourceMappingURL=blog.js.map