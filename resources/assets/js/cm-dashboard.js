import EventsPage from './components/client/EventsPage.vue';
import MerchandisePage from './components/client/MerchandisePage.vue';

window.cat = {
    state: 3
}
const App = new Vue({
    el: "#client",
    components: {EventsPage, MerchandisePage},
    data : {
        slideState: 0,
        showAddForm: false,
        shopCategories: _.toArray(window.ShopCategories)
    },
    methods: {
    }
});

const AppConfig = {
    Host: null
};
(function ($) {
    AppConfig.Host = "//" + window.location.host + "/cm-user/";
    load_dependencies();
})(jQuery);

function load_dependencies() {
    console.log('loaded');
    if($('.chosen-select')[0]) {
        $('.chosen-select').chosen({});
    }
}

//navbar-top menu button event
var menuButton = {
    init: function () {
        var _menuBar = this.self = $('#menu-button').first(),
            _sideNav = NavBar.self = $('#client-sidebar'), _parentCont = $('#main-col'), _parent = this;
        var state = _parent.state;
        state.active = parseInt(Cookie.getCookie('menuState'));
        _menuBar.on('click', function () {
            state.active = (state.active === 1 ? 0 : 1);
            if (state.active === 1) {
            	console.trace(_sideNav);
                _menuBar.addClass('active');
                _sideNav.addClass('col-slide-hide');
                _parentCont.removeClass('col-lg-offset-2');
            }
            else {
                _menuBar.removeClass('active');
                _sideNav.removeClass('col-slide-hide');
                _parentCont.addClass('col-lg-offset-2');
            }
            console.log(Cookie.getCookie('menuState'), state.active);
            $(this).toggleClass('active');
            document.cookie = "menuState=" + state.active + "";
        });
    },
    state: { active: false }
};
//
var NavBar = {
    self: null,
    init: function () {
        var Navigation = this.self;
        Navigation.children('ul').delegate('li', 'click', function () {
            var _subNav = $(this).next();
            if (_subNav[0].tagName === "UL") {
                _subNav.toggleClass('collapse');
            }
        });
    }
};

var Cookie = {
    loadCookie: function () {
        var DocCookie = document.cookie, CookieArray = [];
        this.cookies = DocCookie.split(/\s/g);
        for (var _i = 0, _a = this.cookies; _i < _a.length; _i++) {
            var cookie = _a[_i];
            var _b = cookie.split(/\=/), name = _b[0], value = _b[1];
            CookieArray.push({ "name": name, "value": value.split(/\;$/)[0] });
        }
        return CookieArray;
    },
    setCookie: function () {
        console.log('setting cookie');
    },
    getCookie: function (cookieString) {
        var cookies = this.loadCookie();
        for (var _i = 0, cookies_1 = cookies; _i < cookies_1.length; _i++) {
            var mi = cookies_1[_i];
            if (mi.name == cookieString) {
                return mi.value;
            }
        }
        return false;
    }
};
var Services = {
    init: function () {
        this.construct();
        var _a = this.props, list = _a.list, EditBox = _a.EditBox, ServiceForm = _a.ServiceForm, delLink = _a.delLink;
        var _hide = "collapse";
        var list_item = list.find(".list-group-item");
        function getParent(x) {
            return $(x).parents('.list-group-item');
        }
        list_item.map(function () {
            $(this).removeClass(_hide);
        });
        list.delegate("button[data-edit]", "click", function (evt) {
            var _parent = getParent(this), _xerox = _parent.attr("data-xerox");
            showLoader(evt);
            EditBox.load(AppConfig.Host + "services/" + _xerox, function (sonct, xhr) {
                removeLoader();
                var image = new ImageEdit('input[name=imageColl]', '.drag-drop');
                ServiceForm.addClass(_hide);
                EditBox.scrollTo(100);
            });
        });
        list.delegate("button[data-del]", "click", function () {
            var parentList = getParent(this), _serviceXerox = parentList.attr("data-xerox"), _confirm = confirm("Delete ?");
            if (_confirm)
                $.post(delLink, { _token: Laravel.csrfToken, activity: "services", serviceXerox: _serviceXerox }, function (xhr) {
                    if (xhr.code === true) {
                        alert("Deleted");
                        parentList.addClass(_hide);
                    }
                }, "json");
        });
    },
    construct: function () {
        this.props = {
            list: $("#service-list"),
            EditBox: $('#edit-services'),
            ServiceForm: $("#register__service"),
            delLink: AppConfig.Host + "services/delete"
        };
    }
};
var AdvertsPage = {
    init: function () {
        this.constructor();
        var _hide = "collapse";
        var _a = this.props, Form = _a.Form, EditPanel = _a.EditPanel, EditPage = _a.EditPage, AdvertBox = _a.AdvertBox, panelBody = _a.panelBody, AdvertBoxes = _a.AdvertBoxes;
        AdvertBox.find("button[data-collapse]").click(function () {
            var i = $(this).find("i"), rm = i.attr("data-toggle");
            if (Form.hasClass(_hide)) {
                i.removeClass(rm);
                Form.removeClass(_hide);
                if (window.innerWidth > 1200) {
                    AdvertBox.removeClass("expand");
                }
            }
            else {
                i.addClass(rm);
                Form.addClass(_hide);
                if (window.innerWidth > 1200) {
                    AdvertBox.addClass("expand");
                }
            }
        });
        AdvertBoxes.delegate('[data-edit]', 'click', function (evt) {
            if (evt.ctrlKey == true) {
                evt.preventDefault();
                evt.stopPropagation();
                var xerox = $(this).attr("data-edit");
                var link = $(this).attr("href");
                EditPanel.removeClass(_hide);
                EditPanel.load(link, showLoader);
                EditPanel.scrollTo(89);
            }
        });
        AdvertBox.delegate('[data-del]', 'click', function (evt) {
            var _self = $(this), _xerox = _self.attr("data-del"), _advertPreview = _self.parents(".adverts-preview");
            if (confirm("Delete?")) {
                showLoader(evt);
                $.post(EditPage + "/delete", { advertXerox: _xerox, deleteSubmit: 10, _token: window.Laravel.csrfToken }, function (xhr) {
                    if (xhr.code === 200) {
                        _advertPreview.addClass("collapse");
                        removeLoader();
                        EditPanel.addClass('_hide');
                    }
                });
            }
        });
        EditPanel.delegate("span[data-collapse]", "click", function () {
            EditPanel.addClass(_hide);
        });
    },
    constructor: function () {
        var Form = $("#add_advert"), AdvertBox = $("#advert-box"), editPanel = $("#edit-advert"), _a = window.location, pathname = _a.pathname, hash = _a.hash;
        this.props = {
            Form: Form,
            EditPage: pathname,
            AdvertBox: AdvertBox,
            EditPanel: editPanel,
            AdvertBoxes: AdvertBox.find(".panel"),
        };
    },
};
var Account = {
    init: function () {
        this.construct();
        var _a = this.props, BasicPanel = _a.BasicPanel, PPanel = _a.PPanel, UpdatePanel = _a.UpdatePanel, ModifyBtn = _a.ModifyBtn, ChangePass = _a.ChangePass;
        if (BasicPanel.length !== 0) {
            ModifyBtn.click(function () {
                UpdatePanel.toggleClass("collapse").scrollTo(100);
            });
            ChangePass.click(function () {
                PPanel.toggleClass("collapse").scrollTo(100);
            });
        }
        console.log('working on the Account panel');
    },
    construct: function () {
        this.props = {
            BasicPanel: $('#account__profile'),
            PPanel: $("#change__password"),
            UpdatePanel: $("#account__update"),
            ModifyBtn: $("#modify"),
            ChangePass: $("#ch-pwd"),
        };
    }
};

function showLoader(e, b) {
    if (b === void 0) { b = "failed"; }
    if (arguments.length < 2) {
        console.log('just a simple loader');
    }
    else {
        console.log('hide loader');
        if (b == "success") {
            console.log('hiding with success');
        }
        else {
            console.warn('hiding with error');
        }
    }
}

function removeLoader() {
    console.log("removing loader");
}

$(function () {
    menuButton.init();
    Services.init();
    AdvertsPage.init();
    NavBar.init();
    Account.init();
});
