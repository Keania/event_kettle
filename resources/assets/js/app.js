/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
import Vue from 'vue';
import EventCard from './components/events/EventCard.vue';


require('./bootstrap');
require('bootstrap-notify');
require('chosen-js');
require('bootstrap-validator');

var userDefaults = {
    placement: {
        from: "bottom",
        align: "right",
    },
    animate: {
        enter: "animated slideInRight",
        exit: "animated slideOutRight"
    },
    allow_dismiss: false,
    timer: 300,
    offset: 20,
    type: 'info',
    template: '<div data-notify="container" class="col-xs-11 col-sm-2 alert alert-{0}" role="alert"><button type="button" aria-hidden="true" class="close" data-notify="dismiss">&times;</button><span data-notify="icon"></span> <span data-notify="title">{1}</span> <span data-notify="message">{2}</span><div class="progress" data-notify="progressbar"><div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div></div><a href="{3}" target="{4}" data-notify="url"></a></div>'
};
$.notifyDefaults(userDefaults);

(function($) {
    $.fn.scrollTo = animateTo;
    return animateTo;
})(jQuery);

function animateTo(offset) {
    if (this !== window) {
        var _object = $(this),
            _off = (arguments.length == 0) ? 0 : offset,
            _selector = "body,html";
        if (0 != _object.length) {
            var _top = Math.ceil(_object.offset().top - _off) + "px";
            $(_selector).animate({ scrollTop: _top }, 1000, "swing");
        }
    }
}

if ($('#event_app')[0]) {
    const App = new Vue({
        el: "#event_app",
        components: { EventCard },
        mounted() {
            $(window).scroll(() => this.scrollTop = $(window).scrollTop());
        },
        data: {
            scrollTop: 0,
            state: false
        },
        computed: {
            event_cover_height() {
                return $('.event_cover').height();
            }
        },
        methods: {
            scrollUp() {
                console.log('scrolling up');
                $('body, html').animate({ scrollTop: '0px' }, 1000, 'swing');
            }
        }
    });
}