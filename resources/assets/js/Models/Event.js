import { Form, Errors } from './Form.js';
import { Validator } from './Validator.js';

export default {


    params: {
        title: "",
        time: "",
        venue: "",
        description: "",
        category: ""

    },
    events: [],
    find(id) {
        let response = {};
        this.events.map(function(x) {
            if (x.id == id) {
                Object.assign(response, x);
            }
        });
        return response;

    },
    all() {
        return new Promise((res, rej) => {
            axios.get('/cm-user/view-events').then((response) => {
                this.events = response.data;
                res(response);
            }).catch((error) => {
                console.log('there was an error getting the file');
                rej(error);
            });
        })
    },
    Deletor(eventID) {
        axios.post('/cm-user/delete/users', { 'id': eventID }).then(() => {
            this.all();
        }).catch((error) => {
            console.log('there was error deleting the event');
        });
    },
    Updator(data) {
        $this.validate().then(function(success) {
            if (success) {
                axios.post('/cm-user/events/update', data).then((res) => {

                }).catch((err) => {

                });
            }
        });
    },
    Creator(data) {


    },


}