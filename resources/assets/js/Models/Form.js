let image = null;

class Form {
	
	constructor(data){
		this.originalData = _.extend({}, data);
		_.map((value, field) => this[field] = value);
		this.errors = new FormErrors();
	}

	data() {
		let data = _.extend({}, this);
		let formData = new FormData('client-form');
			delete data.originalData;
			delete data.errors;
		_.map(data, (value, key) => formData.append(key, value));
		return formData;
	}

	persist(image) {
		this.image = image;
		let realImage = this.image;
		console.log(this.data());
	}

	reset() {

		for(let field in this.originalData){
			this[field] = '';
		}
		this.errors.clear();
	}

	post(url) { this.submit('post', url) }

	patch(url) { this.submit('patch', url) }

	delete(url) { this.submit('delete', url) }

	submit(requestType, url) {
		return new Promise((resolve,reject) => {
			axios[requestType](url, this.data())
				.then(response => {
					this.onSuccess(response.data);
					resolve(response);
				})
				.catch(error => {
					this.onFail(error.response.data);
					reject(error);
				})
		});
	}

	onSuccess(data) {
		
		this.reset();
		
	}

	onFail(errors) {
		this.errors.record(errors);

	}
}

class FormErrors {

    constructor() {
		this.errors = {};
    }

    get(field) {
        if(this.errors[field]){
            return this.errors[field][0];
        }
    }

    has(field) { return this.errors.hasOwnProperty(field) }

    any() { return Object.keys(this.errors).length > 0; }

    record(errors) { this.errors = errors }

    clear(field) {
       if(field){
        delete this.errors[field];
        return
       } 
       this.errors = {};
    }
}

export { Form, FormErrors }