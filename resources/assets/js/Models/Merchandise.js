import { Form, Errors } from './Form.js';

const defaults = {
    name: null,
    description: null,
    price: 0,
    category: null,
    image: null,
    quantity: 0,
}

export default {
    find(id) {
        return _.extend(this.form, this.merchs[_.findIndex(this.merchs, o => o.id == id)]);
    },
    merchs: [],
    form: new Form(defaults),
    all() {
        return new Promise((res, rej) => {
            this.form.submit('get', '/api/v1/merchandise')
                .then(response => {
                    this.merchs = [].slice.call(response.data);
                    res(response);
                })
                .catch(error => {
                    console.log('there was an error');
                    rej(error.response);
                })
        });
    },
    update(id) {
        console.log(id);
    },
    remove(merchId) {
        return axios.delete(`/api/v1/merchandise/${merchId}`);
        return axios.delete(`/api/v1/merchandise/${merchId}`);
    }
}