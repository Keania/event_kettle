class customValidator {

    constructor(labelkey, checkValiidities) {
        this.label = document.querySelector("label[for=" + labelkey + "]");
        this.inputNode = this.label.querySelector("input");
        this.elem_list = this.label.querySelector(".input-requirement li:nth-child(1)");
        this.validities = [];
        this.checkValiidities = checkValiidities;
        this.registersListeners();
    }
    addValidities(message) {
        this.validities.push(message);
    }
    getValiidities() {
        return this.valiidities;
    }
    checkValiidities() {
        for (var i = 0; i < this.checkValiidities.length; i++) {
            var isInvalid = this.checkValiidities[i].isEmpty(this.inputNode);
            if (isInvalid) {
                this.addValidities(this.checkValiidities[i].invalidityMessage);
                this.elem_list.classList.remove('valid');
                this.elem_list.classList.add('invalid');
            } else {
                this.elem_list.classList.remove('invalid');
                this.elem_list.classList.add('valid');
            }
        }
    }
    checkInput() {
        if (this.inputNode.value != '') {
            this.inputNode.setCustomValidity('');
        } else {
            var msg = this.getValiidities();
            this.inputNode.setCustomValidity(msg);
        }
        this.checkValiidities();
    }
    registersListeners() {
        var customValidator = this;
        this.inputNode.addEventListener('keyup', function() {
            customValidator.checkInput();
        });
    }
}