@section('styles')
    <link rel="stylesheet" href="{{ asset('css/cm-register.css') }}">
@endsection
@extends('_partials.authenticate',['title' => 'Sign Up - eCampusMarket'])


@section('container')
    <main class="container">

        <div class="row">

            <div class="col-sm-12 col-md-6 col-md-offset-0">

                <form name="registration" action="{{ route('register') }}" method="post" enctype="multipart/form-data" role="form" class="validator">
                    {{csrf_field()}}
                    
                    <div class="form-group text-center">

                        <h1>Sign Up</h1>
                        <span>Already Have an Account? <a href="{{ route('login') }}">Login</a></span>

                    </div>

                    @if (sizeof($errors) > 0)
                     @php
                     var_dump($errors->getMessages());

                     @endphp
                        <div class="alert alert-danger">
                            some fields are incorrect or missing.
                            {{ $errors->first('terms') }}
                        </div>
                    @endif

                    <div class="form-group">
                        <div class="row">
                            
                        <div class="input-label col-md-4">
                            <label>Username <span>*</span></label>                            
                        </div>
                        <div class="col-md-8">
                            <div class="row">
                                <input 
                                    type="text" 
                                    name="username" class="form-control" 
                                    value="{{ old('username') }}" placeholder="Enter Username.." required/>
                                <span class="under-text small"></span>
                                <div class="help-block with-errors"></div>
                                
                            </div>
                        </div>
                        </div>
                    </div> 
                    

                    <div class="form-group">
                        <div class="row">
                            
                        <div class="input-label col-md-4">
                            <label>Password <span>*</span></label>
                        </div>    
                        <div class="col-md-8">
                            <div class="row">
                            <input 
                                type="password" required 
                                class="form-control" name="password" 
                                placeholder="Enter Password.."/>
                                <div class="help-block with-errors"></div>
                                
                            </div> 
                        </div>
                        </div>
                    </div>

                    
                    <div class="form-group">
                        <div class="row">
                            
                        <div class="input-label col-md-4">
                            <label>Confirm <span>*</span></label>
                        </div>

                        <div class="col-md-8">
                            <div class="row">
                                <input type="password" required 
                                class="form-control" 
                                name="password_confirmation" placeholder="Confirm Password.."/>
                                @if ($errors->has('password'))
                                    <span class="text-danger">{{ $errors->first('password') }}</span>
                                @endif  
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        </div>
                    </div>

                    <hr class="col-xs-12" />                    

                    
                    <div class="form-group">
                        <div class="row">
                            
                        <div class="input-label col-md-4">
                            <label> Email <span>*</span></label>
                        </div>    
                        <div class="col-md-8">
                            <div class="row">
                            <input type="email" required class="form-control" name="email" value="{{ old('email') }}" placeholder="Enter email adress">
                            @if ($errors->has('email'))
                                       <span class="text-danger">{{ $errors->first('email') }}</span>
                                   @endif   
                            <div class="help-block with-errors"></div>    
                            </div>
                        </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            
                        <div class="input-label col-md-4">
                            <label>
                                Phone <span>*</span>
                            </label>
                        </div>

                        <div class="col-md-8">
                            <div class="row">
                                <input type="text"
                                    pattern="\d+" 
                                    required 
                                    class="form-control"
                                    name="phone"
                                    min="0"
                                    minlength="0" 
                                    value="{{ old('phone') }}" 
                                    placeholder="Enter phone number">
                            </div>                    
                            @if ($errors->has('phone'))
                               <span class="text-danger">{{ $errors->first('phone') }}</span>
                           @endif  
                           <div class="help-block with-errors"></div> 
                        </div>
                        </div>
                    </div>
                    

                                     
                    <div class="form-group text-center">
                        <div class="row">
                            
                        <input id="policy_check" required type="checkbox" name="terms" {{ old('terms') ? 'checked' : null }} />
                        <span>I agree to <a target="_blank" href="{{ route('shop.terms') }}">Terms and Condition</a>.</span>
                        </div>
                        <div class="help-block with-errors"></div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-6 col-sm-push-4">
                          
                            <button type="submit"  name="submit">
                                <i class="fa fa-send"></i> Sign Up
                            </button>
                            <div class="alert alert-info hide message-box">
                                agree to Terms and Condition.
                            </div>
                        </div>
                    </div>
                    
                </form>
                
            </div>
            
        </div>

    </main>
@endsection

