@extends('_partials.authenticate',['title' => 'Login - eCMARKET'])

@section('styles')
    <link rel="stylesheet" href="{{ asset('css/cm-register.css') }}"/>
@endsection

@section('container')
<div>
            <form class="login-form" role="form" method="POST" action="{{ route('login') }}">

                <div class="row">

                    <div class="col-md-8 col-md-push-2">
                        
                        
                                
                                <div class="form-group text-center">
                                    <h1 class="heading">Sign In</h1>
                                    <div>
                                        <small>No Account? <a href="{{ route('register') }}">Sign Up</a></small>
                                    </div>
                        
                                </div>

                                <div class="form-group input-group">
                                    <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
                                    <input type="text" name="email" class="form-control" 
                                            value="{{ old('email') }}" 
                                            placeholder="Enter Email..." autofocus />
                                     @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="form-group input-group">
                                    <span class="input-group-addon "><i class="zmdi zmdi-lock"></i></span>
                                    <input type="password" class="form-control" name="password" placeholder="Enter Password...">
                                </div>

                                <div class="form-group m-b-50">
                                    <div class="text-center">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                                            </label>
                                            <small class="pull-right">
                                                <a href="{{ route('password.request') }}">Forgot Password?</a>
                                            </small>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group text-center">
                                    <button class="submit btn btn-default" name="submit">
                                        <i class="zmdi zmdi-sign-in"></i> Login 
                                    </button>
                                    {{ csrf_field() }}
                                </div>

                    </div>
                </div>

            </form>
</div>
@endsection