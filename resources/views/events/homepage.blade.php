@extends('events.partials.base')

@section('content')

    <div class="row">

        <div class="jumbotron event_cover clearfix">

            <div class="text-center">
                <img width="200" src="{{ asset('/images/assets/logo-2x.png') }}" alt="events Logo">
            </div>

            <div   class="col-sm-12 col-md-6 col-md-push-3">
                <br><br>
                <div class="input-group event-search">
                    <span class="input-group-addon">
                        <i class="zmdi zmdi-search"></i>
                    </span>
                    <input type="text" placeholder="Search for an Event.." class="form-control">
                    <select name="state" class="form-control" id="">
                        <option value="rivers">Rivers State</option>
                        <option value="rivers">New Orleans</option>
                        <option value="rivers">Edo State</option>
                    </select>
                </div>

                <br><br><br><br>
            </div>


        </div>

    </div>

    <div class="row">

        @include('events.partials.sidebar')

        <div class="col-md-8">

            @foreach( range(1,15) as $item)

                <div class="col-lg-4 col-md-4 col-sm-3">
                    <div class="row">
                        @component('events.components.event_card') @endcomponent
                    </div>
                </div>

            @endforeach

        </div>

    </div>

@endsection
