@extends('events.partials.base')

@section('content')

    <div class="row">

        <div class="jumbotron event_cover clearfix" style="min-height: 350px">

            <div class="text-center">
                <img width="200" src="{{ asset('/images/assets/logo-2x.png') }}" alt="events Logo">
            </div>

        </div>

    </div>

    <div class="row">

        <div class="col-md-8 m-top-i-90">
            <div class="row">
                <div class="col-md-11 col-md-offset-1" style="-webkit-user-select: none">
                    <div class="panel event-page">
                        <div class="panel-heading">
                            <h1 class="m-top-5">THE GLO MUSIC TOUR AFRICA</h1>
                        </div>
                        <div class="panel-body">
                            <div class="event-detials m-b-30 clearfix">
                                <img class="pull-left m-r-15"  src="{{ asset('images/assets/location.svg') }}" width="30"/>
                                <div class="pull-left">
                                    KINGS PLAZA, MAIN-LAND <br> 
                                    <small><small>LAGOS, NIGERIA</small></small>
                                </div>
                            </div>
                            <small class="e-desc-heading small">DESCRIPTION</small>
                            <div class="e-desc">
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corrupti quia in harum earum, inventore autem molestias minus, tempore. Excepturi quod, quam. Id repellat, corporis expedita ex, mollitia debitis obcaecati error, quas amet deserunt fugiat consequatur! Modi nulla, error omnis aliquam molestiae suscipit, inventore magnam placeat. Sit accusantium est, ipsum aspernatur, perspiciatis ducimus soluta esse possimus quidem, ipsa ad fugiat maxime alias recusandae. Fuga cumque accusamus nemo corporis enim delectus nulla, repudiandae illum, et fugiat unde rerum at eius accusantium quasi perferendis nihil porro nesciunt asperiores aut totam. Voluptas eius nam ab hic atque, dicta nemo consequuntur repudiandae numquam! Ipsam similique molestias at necessitatibus consequuntur, sequi voluptates ipsa officiis distinctio quo dolore laudantium laborum perferendis id, odit eius quidem, fugiat, molestiae. Inventore sed culpa iusto doloribus numquam, architecto vel quam dolorum aperiam odio, excepturi praesentium nihil sint omnis voluptas, illum id in illo laborum quas? Explicabo culpa sunt doloremque dignissimos in, harum, eligendi hic id voluptatum dolor labore ullam itaque sit facere, laborum laboriosam nostrum! Eos perferendis iure earum officiis assumenda beatae non necessitatibus nulla tempore pariatur maiores eligendi labore eum mollitia qui architecto similique iste, sit aperiam sint maxime. Dolore neque voluptas modi natus ipsam facere quos aperiam unde ipsa, perferendis aut quam vero libero quis consequuntur eaque, doloribus vitae.
                                <p>At est rem velit adipisci dolorum delectus quod unde. Qui soluta placeat, numquam saepe! Assumenda inventore doloremque, at, corrupti aut minus maiores excepturi, unde harum aliquid alias odio? Eaque nulla ratione voluptates nesciunt dolore quis? Ea harum officiis esse saepe voluptas praesentium dolor commodi quam veritatis eaque enim aut tempore magni ullam voluptatem vel obcaecati magnam hic, accusantium adipisci omnis rem reprehenderit? Laborum aspernatur neque architecto modi expedita obcaecati minus, harum dolorem eos eaque non optio maiores qui nulla cumque ipsum distinctio! Accusantium voluptas in eaque, optio sint nisi saepe.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-3 clearfix">
            <div class="card col-sm-12 m-l-25 dark-bg">
                <div class="card-heading pull-left">
                    <b>TICKET SIGNUPS</b>
                </div>
                <div class="card-content pull-right">
                    <h1 class="ticket-info">
                        <span class="larger">76</span>
                        <span class="small">/ 100</span>
                    </h1>
                </div>
            </div>

            <div class="panel red-bg register-card" style="border-radius: 0">
                <form class="panel-body">
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Fullname"/>                        
                    </div>
                    <div class="form-group">
                        <input type="date" class="form-control" placeholder="Age">
                    </div>
                    <div class="form-group">
                        <select name="ticket-type" class="form-control">
                            <option value="" label="High Table">100,000</option>
                            <option value="VVIP">50,000</option>
                            <option value="">VIP</option>
                            <option value="">Regular</option>
                        </select>
                    </div>
                    <div class="text-center">
                        <button class="btn btn-group btn-sm">REGISTER</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
