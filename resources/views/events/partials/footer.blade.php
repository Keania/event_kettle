<footer class="container-fluid">

	<div class="row">
		
		<div class="col-sm-12">
			<div class="row">
				<div id="logos" class="col-sm-12 col-md-8 pull-left">
					<a href="#">
						<img class="z-depth-bottom-2" src="{{ asset('images/assets/event-logo.jpg') }}" width="80"/>
					</a>
				</div>
				<div class="col-sm-12 col-md-4 text-right pull-right">
					<div class="small">
						<a href="{{ route('shop.terms') }}">terms and conditions </a> 
						<br> for usuage. All rights Reserved.
					</div>
					<div>&copy; 2017 <b>Event Kettle</b> </div>
				</div>
			</div>
		</div>

	</div>
</footer>