<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    @stack('meta-data')
    <script>
        window.Laravel = {
            csrfToken : "{!! csrf_token() !!}"
        }
    </script>

    <title>{{ @$title }}</title>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet"/>
    <link href="{{ asset('css/cm.css') }}" rel="stylesheet"/>
    <link rel="stylesheet" href="{{ asset('css/generic.css') }}"/>
    <link rel="stylesheet" href="{{ asset('css/events.css') }}">
    @yield('styles')
</head>

<body>
<div id="event_app">
    <nav :class="{active: (scrollTop < event_cover_height) }" class="navbar navbar-fixed-top navbar-default">

        <div class="container">

            <div style="font-family: molle-italic;" class="navbar-header">
                <div class="navbar-brand">
                    <a href="#">
                        Event<b>Kettle</b>
                    </a>
                </div>
            </div>

            <nav class="navbar-right">
                <div class="nav navbar-nav">
                    <li>
                        <a href="{{ route('events.home') }}">Home</a>
                    </li>
                    <li>
                        <a href="{{ route('blog.home') }}">Blog</a>
                    </li>
                    <li>
                        <a href="{{ route('shop.home') }}">Shop</a>
                    </li>
                    @if (Auth::user())
                        <li>
                            <a href="{{ route('client.dashboard') }}">{{ Auth::user()->username }}</a>
                        </li>
                    @else 
                        <li>
                            <a href="{{route('login')}}">Login</a>
                        </li>
                    @endif

                </div>
            </nav>
        </div>
    </nav>
    <main id="main-col" class="container-fluid">
        @yield('content')
    </main>
    @include('events.partials.footer')
</div>

<script src="{{ asset('js/app.js') }}"  type="application/javascript"></script>
<script src="{{ asset('js/component.js') }}" type="application/javascript"></script>
@yield('js') @yield('scripts')
</body>
</html>

