
<!--navigation-->
<div id="event-sidebar" class="col-sm-3 m-top-i-80">

    <div class="panel dark-bg">
        <div class="panel-heading">
            <span class="panel-title">
                <span>Categories</span>
                <span class="pull-right">
                    <i class="zmdi zmdi-hc-2x zmdi-more-vert"></i>
                </span>
            </span>
        </div>

        <div class="panel-body">

            <ul class="nav nav-stacked collapse navbar-collapse row fancy-slide-down" id="t-menu">

                <li :class="{active: state}" @click.prevent="state = !state">
                    <a href="">
                        <span class="icon">
                            <span class="zmdi zmdi-accounts"></span>
                        </span>
                        <span>Conferences</span>
                    </a>
                </li>
                <li class="">
                    <a href="">
                        <span class="icon">
                            <span class="zmdi zmdi-audio"></span>
                        </span>
                        <span>Music Concerts</span>
                    </a>
                </li>
                <li class="">
                    <a href="">
                        <span class="icon">
                            <span class="zmdi zmdi-view-day"></span>
                        </span>
                        <span>Street Shows</span>
                    </a>
                </li>
                <li class="">
                    <a href="">
                        <span class="icon">
                            <span class="zmdi zmdi-cake"></span>
                        </span>
                        <span>Weddings</span>
                    </a>
                </li>
                <li class="">
                    <a href="">
                        <span class="icon">
                            <span class="zmdi zmdi-face"></span>
                        </span>
                        <span>Theatre</span>
                    </a>
                </li>

            </ul>
        </div>
    </div>

</div>
