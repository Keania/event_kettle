
<div class="card e-card p-b-5" is="event-card" inline-template>
    <div @click="getSelf">
        <figure class="card-image">
            <img src="{{ asset('images/boss-bear.jpg') }}" alt="">
        </figure>

        <div class="card-heading m-top-i-15">
            <span class="card-label">Crack your Ribs</span>
        </div>

        <div class="card-content m-top-15 text-left">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit.
            Amet doloremque dolorum hic id illo iste praesentium;
        </div>

        <div class="text-center clearfix m-top-5">
            <button class="btn col-sm-2 p-5" @click="like">
                <span :class="{'zmdi-favorite': liked }" class="zmdi zmdi-favorite-outline"></span>
            </button>
            <button class="btn col-sm-2 p-5 m-l-5">
                <span class="zmdi zmdi-thumb-up"></span>
            </button>

            <button class="btn col-sm-2 p-5 m-l-5">
                <span class="zmdi zmdi-thumb-down"></span>
            </button>

            <a href="{{ route('event.page',['club','the-happy-mark-event']) }}" class="btn col-sm-2 p-5 pull-right">
                <span class="zmdi zmdi-eye"></span>
            </a>
        </div>
    </div>

</div>
