@php
    use App\Helpers\Navigation as Nav;
@endphp
<nav class="navbar navbar-default col-sm-12">
	
			
	        <div class="navbar-header">
	            <div class="navbar-brand">

	                <a href="{{ route('shop.home') }}">
	                    <b>Event</b>KETTLE
	                </a>

	            </div>
	        </div>

	        <nav class="nav navbar-nav navbar-right">
	           @php

	                // Nav::layout('Home',[ route('shop.home') ]);
	                Nav::layout( Auth::user()->fname ,['id'=>'name']);

	            @endphp
	        </nav>

   
</nav>