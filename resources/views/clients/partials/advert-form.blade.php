@inject('cat', 'App\Market\Category')


<form 
    enctype="multipart/form-data"
    class="panel-body quick-form"
    action="{{ route('advert.add') }}" 
    method="post">
    
    @if (count($errors) > 0)
        <div class="alert alert-danger">
           Some fields are missing data.
           {{ var_dump($errors->getMessages()) }}
        </div>
    @endif

    <div class="form-group {{ $errors->has('advert__subject') ? 'has-error' : '' }}">
        <label>Label</label>
        <input class="form-control" type="text" 
        	required 
            name="advert__subject" 
            id="advert__subject" 
            value="{{ old('advert__subject') }}"
            placeholder="Name">

       @if($errors->has('advert__subject'))
        <div class="text-danger">
            <span>{{ $errors->first('advert__subject') }}</span>
        </div>
        @endif
    </div>

    <div class="form-group">
        <label>Taxonomy</label>
        <input class="form-control" type="text" name="advert__taxonomy" id="advert__taxonomy" value="{{ old('advert__taxonomy') }}">
        <small class="form-text text-muted">could be a specification, model. e.g <i>Samsung Galaxy, Tecno, Tablet, Watch, Android 5.0, Windows 10</i></small>
    </div> 

    <div class="form-group">
        <label for="add__category">Category</label>
        {{ $cat::selection('cat_selection') }}
    </div>

    <div class="form-group {{ $errors->has('advert__description') ? 'has-error' : '' }}">
        <label>Description</label>
        <textarea 
			required 
        	name="advert__description" 
            placeholder="Specification, Methods, Models"
            style="width:100%;min-height: 50px;font-size:smaller">{{ old('advert__description') }}</textarea>
        @if($errors->has('advert__description'))
            <div class="text-danger">
                <span>{{ $errors->first('advert__description') }}</span>
            </div>
        @endif
    </div>

    <div class="form-group">

        <label>Product Image</label>
        <label for="product__image" class="upload-button col-sm-12">Upload File <span
                    class="glyphicon glyphicon-cloud-upload"></span></label>
        <input type="file" name="product__image" id="product__image" value="">
    </div>

    <div class="form-group {{ $errors->has('product__pricing') ? 'has-error' : '' }}">
        <label>Pricing</label>
        <input required class="form-control" type="number" min="0.0" maxlength="9" name="product__pricing"
               id="product__pricing" value="{{ old('product__pricing') or "0.0" }}">

        @if($errors->has('product__pricing'))
            <div class="text-danger">
                <span>{{ $errors->first('product__pricing') }}</span>
            </div>
        @endif
    </div>

    {{ csrf_field() }}
    <input type="submit" name="upload__advert" value="Upload">
</form>