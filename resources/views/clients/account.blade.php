@extends('_partials._client_base', [ 'title' => 'Account Settings - ' . Auth::user()->username ])

@section('content')
    <div class="row">

        <div class="col-sm-12">
            <div id="account-top">
                <figure class="thumbnail">
                    @php

                        $imageName = Storage::disk('user_images')->url($user->photo);
                        $fileExist = Storage::disk('user_images')->exists($user->photo);

                    @endphp
                    <img src="{{ $fileExist ? $imageName : getAsset('user.svg') }}" alt="">
                </figure>
                <h3 class="name-box p-l-10 p-t-20">{{ $user->fname }} {{$user->lname}}</h3>
            </div>
            <hr>
        </div>

        <!-- user profile -->
        <div id="account__profile" class="col-sm-6 col-md-4">

            <div class="panel  panel-default">

                <div class="panel-heading">
                    <div class="panel-title">
                        <i class="zmdi zmdi-account"></i> Personal Detials
                    </div>
                </div>

                <fieldset class="panel-body">
                    <!--information table-->
                    <table border="0" class="table table-responsive table-sm">
                        <tbody>
                        <tr title="Username">
                            <td class="text-left">
                                <i class="zmdi zmdi-apple"></i>
                                {{--  Username  --}}
                            </td>
                            <td>{{ $user->username }}</td>
                        </tr>
                        <tr title="Phone Number">
                            <td class="text-left">
                                <span class="zmdi zmdi-phone"></span>
                                {{--  Phone  --}}
                            </td>
                            <td>{{ $user->phone }}</td>
                        </tr>
                        <tr class="Email Address">
                            <td class="text-left">
                                <i class="zmdi zmdi-email"></i>
                                {{--  Email  --}}
                            </td>
                            <td>{{ $user->email }}</td>
                        </tr>
                        <tr class="department">
                            <td class="text-left">
                                <i class="zmdi zmdi-accounts"></i>
                                {{--  Department  --}}
                            </td>
                            <td>{{ $user->department }}</td>
                        </tr>
                        </tbody>
                    </table>
                    <div class="btn-group-sm row text-center">
                        <button id="modify" class="md-btn btn btn-dezmdiult">
                            <i class="zmdi zmdi-edit"></i> Modify Profile &nbsp;</button>
                        <button id="ch-pwd" class="md-btn btn btn-dezmdiult">
                            <i class="zmdi zmdi-key"></i> Change Password</button>
                    </div>

                </fieldset>
            </div>
        </div>

        <!--account update form -->
        <div id="account__update" class="col-sm-6 {{ session('message') ? "" : "collapse" }}">
            <div class="panel">
                <div class="panel-heading panel-dezmdiult">
                    <i class="zmdi zmdi-edit"></i> Edit Account Details
                </div>
                <!--figure this guy out am going for lunch -->
                <fieldset class="panel-body">

                    @if (session('message'))
                        <div class="alert alert-success">
                            {{ session('message') }}
                        </div>
                    @endif

                    <form action="{{ route('profile.update') }}" enctype="multipart/form-data" method="POST">

                        <div class="row">
                            <div class="col-xs-12">

                                <label class="upload-button">
                                    {{
                                        ! Storage::disk('user_images')->exists($user->photo)
                                            ? 'Add a profile picture'
                                            : 'Pick a new profile picture' }}

                                    <input type="file" name="profile_pic" value=""/>
                                </label>

                            </div>

                        </div>

                        <div class="form-inline quick-form"> {{-- form inline start --}}

                            <div class="form-group">

                                <label>First Name</label>
                                <input type="text" name="fname" class="form-control" value="{{ $user->fname }}">

                            </div>

                            <div class="form-group">

                                <label>Last Name</label>
                                <input type="text" class="form-control" name="lname" value="{{ $user->lname }}">

                            </div>

                            <div class="form-group">

                                <label>Phone number</label>
                                <input type="text" class="form-control" name="phone" value="{{ $user->phone }}">

                            </div>

                            <div class="form-group">

                                <label>Email Address</label>
                                <input type="text" class="form-control" name="email" value="{{ $user->email }}">

                            </div>


                            <div class="form-group">

                                <label>Department</label>
                                <input type="text" class="form-control" name="department"
                                       value="{{ $user->department }}">

                            </div>
                        </div> {{-- inline ending --}}

                        <div class="form-group">
                            {{ csrf_field() }}
                            <input type="hidden" name="_method" value="PUT">
                        </div>

                        <div class="text-center">
                            <button class="btn btn-dezmdiult" name="submit" value="save">
                                <i class="zmdi zmdi-save"></i> Save
                            </button>
                            <a href="{{ route('client.truncate') }}">
                                <button class="btn btn-dezmdiult" name="submit" value="delete">
                                    <i class="zmdi zmdi-trash"></i> Delete
                                </button>
                            </a>
                        </div>

                    </form>
                </fieldset>

            </div>
        </div>

        <!--password changing form -->
        <div id="change__password" class="col-sm-4 {{ session('passMessage') ? "" : "collapse" }}">
            <div class="panel panel-dezmdiult">
                <div class="panel-heading">Edit Password...</div>
                <form method="post"
                      class="panel-body"
                      action="{{ route('profile.password') }}"
                      novalidate="true">

                    @if (session('passMessage'))
                        <div class="alert alert-info">
                            {{ session('passMessage') }}
                        </div>
                    @endif

                    <div class="form-group">
                        <label class="small"><i class="zmdi zmdi-lock"></i> Current Password</label>
                        <input class="form-control" name="cpass" type="password" placeholder="current password"/>
                    </div>

                    <div class="form-group">
                        <label class="small"><i class="zmdi zmdi-key"></i> New Passowrd </label>
                        <input class="form-control" type="password" name="pass" placeholder="new password"/>
                    </div>

                    <div class="form-group">
                        <label class="small" for="vpass"><i class="zmdi zmdi-key"></i> Verify Password</label>
                        <input class="form-control" type="password" name="vpass" placeholder="verify password"/>
                        <div id="password_indicator"></div>
                    </div>


                    <div class="form-group">

                        <div class="input-group">
                            <input type="hidden" name="_method" value="PUT"/>
                            {{ csrf_field() }}
                            <button name="ch-pwd" value="" class="btn btn-info col-sm-12">
                                Submit <span class="glyphicon glyphicon-chevron-right"></span>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>

@endsection
