@php
    use App\Helpers\Navigation as Nav ;

    if( Auth::user() !== null ):
        $menuState = (int) @$_COOKIE['menuState'];

@endphp
<nav id="client-sidebar" class="navbar col-md-2 {{ $menuState !== 1 ? "" : "col-slide-hide"  }}">

    <button class="pull-right p-15 " id="menu-button">
        <i class="zmdi zmdi-menu zmdi-hc-2x"></i>
    </button>

    <figure id="sidebar_image">
        <img widt="70%" src="{{ asset('/images/assets/logo-2x.png') }}" alt="">
    </figure>
    <ul>
        @php
            $username = Auth::user()->username;

            Nav::layout("Home (Shop)",[ route('shop.home'), 'zmdi-home']);
            Nav::layout('Dashboard',[ route('client.dashboard') ,'zmdi-view-dashboard']);
            Nav::layout('Merchandise',[ route('merchandise.index') ,'zmdi-view-list-alt']);
            Nav::layout('Events',[ route('client.events') ,'zmdi-view-list-alt']);
            Nav::layout('Services',[ route('client.services') ,'zmdi-wrench']);
            Nav::layout('Profile',[ route('client.profile') ,'zmdi-account']);
        @endphp
            <li>
                <a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout').submit();">
                    <span class="zmdi zmdi-power icon"></span>
                    <span>Logout</span>
                </a>
                 <form id="logout" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </li>
    </ul>

</nav>
@php
    endif;
@endphp
