@php
 
    use App\Shop\ServiceCategory as Services;

@endphp
<form method="post" 
    action="{{ route('service.add') }}" 
    class="panel-body quick-form"
    enctype="multipart/form-data">

    @if (sizeof($errors) > 0)
        {{ $errors->first('message') }}
        <div class="alert small alert-danger">Some fields are invalid.</div> 
        }
    @else
        <div class="alert small alert-info ">add a quick service.</div>
    @endif
    
    <label class="form-group">
        <label for='_coverImage' class="upload-button text-center">cover photo</label>
        <input type="file" id="_coverImage" name="cover_image" />
        <div class="small text-muted {{ $errors->has('cover_image') ? 'text-danger' : '' }}"> 
            {{ $errors->has('cover_image') 
                ?"a cover photo is need" 
                : "a cover photo for your service page" }}
        </div>
    </label>

    <div class="form-group">
        <label for="title">Title of Service</label>
        <input type="text" name='label' value="{{ old('title') }}">
        @if ($errors->has('title'))
            <span class="small text-danger">{{ $errors->first('title') }}</span>
        @endif
    </div>

    <div for="description">
        <label>Description</label>
        <textarea 
        name="description" 
        placeholder="service description..." 
        id="_description" class="form-control">{{ old('description') }}</textarea>
        @if ($errors->has('description'))
            <span class="small text-danger">{{ $errors->first('description') }}</span>
        @endif
    </div>

    <div class="form-group">
        <label>Service Category</label>
        {{ Services::selection('category','form-control') }}
        @if ($errors->has('category'))
            <span class="small text-danger">{{ $errors->first('category') }}</span>
        @endif
    </div>

    {{ csrf_field() }}
    <input type="submit" value="Add Service"/>

</form>
