@inject('cat', 'App\Shop\Category')
@inject('str', 'Illuminate\Support\Str')

@extends('_partials._client_base',['title' => 'Merchandise - ' . Auth::user()->fname ])

@section('content')
    <div is="merchandise-page" inline-template>
        <div>
            <h4>Merchandise</h4>
            <hr>
            <!-- confirm form container -->
            <!-- adverts content holder -->
            <div class="row">
                <!-- adverts list -->
                <div class="col-sm-12 col-lg-7">
                    <div id="advert-box" class="panel">
                        <div class="panel-heading">
                            <div class="clearfix">
                                <span class="panel-title">All Merchandise</span>
                                <button @click.prevent="toggleForm" class="btn btn-sm short-btn btn-default pull-right">
                                    <i class="zmdi zmdi-unfold-more zmdi-unfold-less" data-toggle="fa-unfold-more"></i>
                                </button>
                            </div>
                        </div>

                        <div class="panel-body">
                            <div v-if="merchs.length <= 0" class="alert alert-info">
                                <i v-show="loaded == false" class="zmdi zmdi-spinner zmdi-hc-spin"></i>
                                <span>@{{ load_message }}</span>
                            </div>
                            <transition-group name="flip-list" tag="div">
                                <div :key="key" v-for="(merchandise, key) in merchs" class="adverts-preview col-xs-12">
                                    <div class="row">
                                        <div class="col-xs-3 col-sm-3">
                                            <figure class="thumbnail">
                                                <img class="img-fluid" :src="merchandise.image"/>
                                            </figure>
                                        </div>

                                        <div class="advert-body p-b-10 col-xs-9 col-sm-9">
                                            <div class="advert-heading">
                                                <div class="row">
                                                    <div class="clearfix">
                                                        <a  @click.prevent="editMerchandise(merchandise.id)"
                                                            href="{{ route('merchandise.show',['id' => @$advert->id ]) }}#edit">
                                                            @{{ merchandise.name }}
                                                        </a>
                                                        <button @click.prevent="removeMerchandise(merchandise.id)" href="#delete" class="btn btn-sm m-l-15 btn-default">
                                                            <i class="zmdi zmdi-delete"></i>
                                                        </button>
                                                        <div><b class="text-muted">N @{{merchandise.price}} </b></div>
                                                  </div>
                                                </div>
                                            </div>
                                            <div class="advert-desc">
                                                @{{  merchandise.description }}
                                            </div>
                                            <div id="cat" class="text-muted text-left">
                                                <small>@{{ merchandise.category }}</small>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </transition-group>

                        </div>

                        <div class="panel-footer">
                            NoA : <b>@{{ merchs.length }}</b>
                        </div>
                    </div>

                </div>

                <!-- advert form panel-->
                <div class="col-sm-12 col-lg-5">
                    <div v-show="showForm" id="add_advert" class="panel">
                        <div class="panel-heading panel-default">
                            <span class="panel-title">Add Merchandise
                            </span>
                        </div>

                        <form enctype="multipart/form-data" class="panel-body quick-form" action="{{ route('merchandise.store') }}" method="post">
                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    Some fields are missing data.
                                </div>
                            @endif

                            <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                                <label>Name</label>
                                <input class="form-control" type="text"
                                       name="name"
                                       id="name"
                                       value="{{ old('name') }}"
                                       >
                                       {{-- placeholder="Name" --}}
                                @if($errors->has('name'))
                                    <div class="text-danger">
                                        <span>{{ $errors->first('name') }}</span>
                                    </div>
                                @endif
                            </div>

                            <div class="form-group">
                                <label>Taxonomy</label>
                                <select name="taxonomy" id="taxonomy" data-placeholder="Sort your merchandise..." multiple class="chosen-select form-control">
                                    <option value="">Lorem ipsum.</option>
                                    <option value="">Incidunt, eveniet!</option>
                                    <option value="">Placeat, inventore!</option>
                                    <option value="">Magni, error?</option>
                                    <option value="">Laudantium, veritatis.</option>
                                    <option value="">Tempora, tempore.</option>
                                    <option value="">Delectus, commodi.</option>
                                    <option value="">Dolorum, quas.</option>
                                    <option value="">Aspernatur, beatae.</option>
                                    <option value="">Sequi, deserunt.</option>
                                    <option value="">Eligendi, explicabo!</option>
                                    <option value="">Cumque, ipsa!</option>
                                    <option value="">Accusantium, ea!</option>
                                    <option value="">Fugiat, soluta.</option>
                                </select>
                                <small class="form-text text-muted">
                                    could be a specification, model. e.g
                                    <i>Samsung Galaxy, Tecno, Tablet, Watch, Android 5.0, Windows 10</i>
                                </small>
                            </div>

                            <div class="form-group">
                                <label for="add__category">Category</label>
                                {{ $cat::selection('category') }}
                            </div>

                            <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
                                <label>Description</label>
                                <textarea name="description"
                                          placeholder="Specification, Methods, Models"
                                          style="width:100%;min-height: 50px;font-size:smaller">{{ old('description') }}</textarea>
                                @if($errors->has('description'))
                                    <div class="text-danger">
                                        <span>{{ $errors->first('description') }}</span>
                                    </div>
                                @endif
                            </div>

                            <div class="form-group">

                                <label>Product Image</label>
                                <label for="image" class="upload-button col-sm-12">Upload File <span
                                            class="glyphicon glyphicon-cloud-upload"></span></label>
                                <input type="file" name="image" id="image" value="">
                            </div>

                            <div class="form-group {{ $errors->has('pricing') ? 'has-error' : '' }}">
                                <label>Pricing</label>
                                <input
                                        class="form-control" type="number" min="50" maxlength="9" name="pricing"
                                        id="pricing" value="{{ old('pricing') or "0.0" }}">

                                @if($errors->has('pricing'))
                                    <div class="text-danger">
                                        <span>{{ $errors->first('pricing') }}</span>
                                    </div>
                                @endif
                            </div>

                             <div class="form-group {{ $errors->has('pricing') ? 'has-error' : '' }}">
                                <label>Quantity Available</label>
                                <input class="form-control" type="number" min="1" name="quantity" value="{{ old('quantity') or "0" }}">

                                @if($errors->has('quantity'))
                                    <div class="text-danger">
                                        <span>{{ $errors->first('quantity') }}</span>
                                    </div>
                                @endif
                            </div>

                            {{ csrf_field() }}
                            <button type="submit" name="store">
                                <i class="zmdi zmdi-floppy"></i> Save
                            </button>
                        </form>
                    </div>
                    <!-- adverts edit form holder -->
                </div>
            </div>

            {{-- edit merchandise modal --}}
            <modal :show="showEdit" @closed="hideEdit">
                <template slot="modal-title">
                    <span class="pull-right" @click="showEdit = false"><i class="fa fa-remove"></i></span>
                    <span class="panel-title">
                        <b>Edit Merchandise</b>
                    </span>
                </template>
                <template slot="modal-body">
                    <form action="#" @submit.prevent="updateMerchandise($event.target)" enctype="multipart/form-data" method="POST">
                        <div class="row">
                            <div class="col-sm-4 col-md-5">
                                <figure class="img-thumbnail" style="width: 100%">
                                    <img class="img-fluid" :src="pickedMerchandise.image">
                                    <div class="form-group">
                                        <label for="productImage" class="upload-button col-sm-12">
                                            Upload File
                                            <span class="zmdi zmdi-cloud-upload"></span>
                                        </label>
                                        <input type="file" name="new_image" id="productImage"/>
                                    </div>
                                </figure>
                            </div>

                            <hr class="visible-xs">

                            <div class="col-sm-8 col-md-7">
                                <div class="col-sm-12">
                                    <div class="row quick-form">
                                        <div class="form-group">
                                            <label>Name</label>
                                            <input class="form-control" type="text" name="name" id="name" v-model:value="pickedMerchandise.name" placeholder="Name"/>
                                       </div>

                                       <div class="form-group">
                                            <label for="add__category">Category</label>
                                            <v-select v-model="pickedMerchandise.category" placeholder="Select Category.." :options="$root.shopCategories"></v-select>
                                       </div>

                                        <div class="form-group">
                                            <label for="">Quantity</label>
                                            <input type="number" class="form-control" name="quantity" min="1" v-model:value="pickedMerchandise.quantity" placeholder="Quantity"/>
                                        </div>

                                        <div class="form-group">
                                            <label>Description</label>
                                            <textarea v-model="pickedMerchandise.description" name="description" rows="7" class="form-control"
                                            placeholder="Specification, Methods, Models">@{{pickedMerchandise.description}}</textarea>
                                        </div>
                                        <div class="form-group">
                                            <label>Pricing</label>
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <span style="font-family: times new roman"> &#x20A6; </span>
                                                </span>
                                                <input v-model:value="pickedMerchandise.price" @input="updateValue($event.target.value)" class="form-control" type="text" min="0" name="price" id="price"/>
                                            </div>
                                        </div>
                                        <input type="hidden" name="image" :value="pickedMerchandise.image"/>
                                        <button type="submit"><span class="zmdi zmdi-floppy"></span> Save</button>
                                    </div>
                                </div>
                        </div>
                    </form>
                </template>
            </modal>
        </div>
    </div>
@endsection
