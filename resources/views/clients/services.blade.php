@php

@endphp


@extends('_partials._client_base',['title' => 'Services - ' . Auth::user()->username ])

@section('content')

    <h3 class="col-sm-12">
        <i class="glyphicon glyphicon-wrench"></i>
        Services
    </h3>

     <!-- add service -->
    <div class="collapse col-sm-6 col-md-6">
        <div class="panel">

            <div class="panel-heading panel-default">
                <div class="panel-title">
                    Register a Service
                </div>
            </div>

            @include('clients.service-form')            

        </div>
    </div>

    <!-- service list-->
    <div id="service-list" class="col-sm-6 col-md-6 ">
        <div class="panel">
            
            <div class="panel-heading panel-default">
                <div class="panel-title">

                    <i class="glyphicon glyphicon-send"></i>
                    service<b>collection</b>
                    
                </div>
            </div>

            @if (session('message') || session('error'))
                <div class="alert {{ session('error') ? 'alert-danger' : 'alert-success' }}">
                    {{ @session('message') }}
                    {{ @session('error')}}
                </div>
            @endif

            <ol class="panel-body list-group">

                @if (sizeof(Auth::user()->services) === 0)
                    <div>
                        <div class="text-primary text-center">No Service Uploaded. Go to the 
                            <a href="{{ route('client.dashboard') }}">
                                <span class="badge badge-primary">dashboard</span></a> to create one. 
                        </div>
                    </div>
                @endif

                @foreach( Auth::user()->services as $savi )

                    <li class='list-group-item collapse' data-xerox='{{ $savi->id }}'>
                        
                        <figure class='col-sm-12'>
                            <img src="{{ Storage::disk('cover_images')->url($savi->cover_image) }}"/>
                        </figure>

                        <div class='col-sm-12'>
                            
                            <div class='row'>

                                <div class='col-md-7'>
                                    <h5>
                                    {{ $savi->label }}
                                    </h5>
                                </div>

                                <div class='col-md-5'>

                                    <div class='text-right'>

                                        <button title='Edit' data-edit>
                                            <i class='fa fa-edit'></i>   
                                        </button>

                                        <button title='Delete' data-del class='no-style'>
                                            <i class='fa fa-trash-o'></i> 
                                        </button>

                                        <a href='{{ route("service.single",
                                            ['category' => str_slug($savi->category),
                                             'service_id' => $savi->id 
                                        ]) }}' target='_blank'>
                                            <button title='Preview in New Tab' data-edit class='no-style'>
                                                <i class='fa fa-external-link'></i>
                                            </button>
                                        </a>

                                    </div>

                                </div>
                            </div>
                        </div>
                    </li>

                @endforeach

            </ol>
            <div class="panel-footer text-right">
                NOA - #{{ sizeof(Auth::user()->services) }}
            </div>
        </div>
    </div>
    
    <div class="col-sm-6">

        @isset ($service)
            @include('clients.edit-service',['service' => $service, 'images' => @$images ])
        @endisset

        <!--    service edit panel-->
        <div id="edit-services">
        </div>
        
    </div>
@endsection