@push('advert-content')
    <div id="add__advert" class="col-sm-12 col-lg-8">
        <div class="row">
            <div class="panel">

                <div class="panel-heading panel-success">
                    @if (request()->ajax())

                        <span class="pull-right" data-collapse><i class="fa fa-remove"></i></span>

                    @else

                        <a class="btn btn-default" href="{{ route('client.adverts') }}">
                            <i class="fa fa-arrow-left"></i>
                        </a>

                    @endif
                    <span class="panel-title">
                        <b>{{ $advert->label }}</b>
                    </span>

                </div>

                <form
                    class="panel-body"
                    action="{{ route('advert.update') }}"
                    enctype="multipart/form-data"
                    method="POST">

                    @if (session('message'))
                        <div class="alert alert-success">
                            {{ session('message') }}
                        </div>
                    @endif

                    @if (sizeof($errors) > 0)
                        <div class=" alert alert-danger">
                            Please check the form, some data are missing.
                        </div>
                    @endif

                    <div class="row">

                        <div class="col-sm-4 col-md-5">

                            <figure class="img-thumbnail" style="width: 100%">
                                <img class="img-fluid" src="{{ getAdvertImage($advert->image) }}">
                                <div class="form-group">
                                    <label for="productImage" class="upload-button col-sm-12">
                                        Upload File
                                        <span class="glyphicon glyphicon-cloud-upload"></span>
                                    </label>
                                    <input type="file" name="product__image" id="productImage"/>

                                </div>
                            </figure>
                        </div>

                        <hr class="visible-xs">

                        <div class="col-sm-8 col-md-7">
                            <div class="col-sm-12">
                                <div class="row quick-form">

                                    <div class="form-group">

                                        <label>Label</label>
                                        <input class="form-control"
                                               type="text"
                                               name="advert__subject"
                                               id="advert__subject"
                                               value="{{ $advert->label }}"
                                               placeholder="Name">

                                        @if ($errors->has('advert__subject'))
                                            <span class="text-danger">
                                                {{ $errors->first('advert__subject') }}
                                            </span>
                                        @endif

                                   </div>

                                   <div class="form-group">
                                        <label>Taxonomy
                                            <input class="form-control" type="text" name="advert__taxonomy" id="advert__taxonomy"
                                               value="{{ $advert->taxanomy }}">
                                            <small class="form-text text-muted">
                                                could be a specification, model. e.g <i>Samsung Galaxy, Tecno, Tablet, Watch, Android 5.0, Windows 10</i>
                                            </small>
                                        </label>
                                    </div>

                                    <div class="form-group">

                                        <div class="col-sm-8">
                                            <div class="row">
                                                <label for="add__category">Category</label>

                                                {{ $catSelection }}

                                            </div>
                                        </div>

                                        <div class="col-sm-4">
                                            <label for="">Quantity</label>
                                            <input type="number"
                                                class="form-control"
                                                name="quantity"
                                                min="1"
                                                value="{{ $advert->quantity }}"
                                                placeholder="Quantity"/>

                                            @if ($errors->has('quantity'))
                                                <span class="text-danger">{{ $errors->first('quantity') }}</span>
                                            @endif
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <label>Description</label>
                                        <textarea name="advert__description" rows="7"
                                        class="form-control"
                                        placeholder="Specification, Methods, Models">{{ $advert->description }} </textarea>
                                        @if ($errors->has('advert__description'))
                                            <span class="text-danger">
                                                {{ $errors->first('advert__description') }}
                                            </span>
                                        @endif
                                    </div>


                                    <div class="form-group">

                                        <label>Pricing</label>

                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <span class="" style="font-family: times new roman"> &#x20A6; </span>
                                            </span>
                                            <input
                                                class="form-control" type="text" min="0.0"
                                                maxlength="9" name="product__pricing"
                                                id="product__pricing" value="{{ $advert->price }}"/>
                                        </div>

                                        @if ($errors->has('product__pricing'))
                                            <span class="text-danger">
                                                {{ $errors->first('product__pricing') }}
                                            </span>
                                        @endif

                                    </div>

                                    {{ csrf_field() }}
                                    <input type="hidden" name="_method" value="PUT" />
                                    <input type="hidden" name="advertXerox" value="{{ $advert->id }}"/>
                                    <input type="hidden" name="advert__image" value="{{ $advert->image }}"/>
                                    <input type="submit" name="update__advert" value="&#xf0c7; Save">
                                </div>
                            </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endpush

@if ( !(request()->ajax()) )

    @extends('_partials._client_base',['title' => "Edit : {$advert->label }"])

    @section('content')
        @stack('advert-content')
    @endsection


@else

  @stack('advert-content')
  {{ die() }}

@endif
