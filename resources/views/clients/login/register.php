<?php
/**
 * Created by PhpStorm.
 * User: Mobrad
 * Date: 12/20/2016
 * Time: 10:47 PM
 */
    extract(@$_POST);
    if(is_user_login()){
        redirect(Route\UserPage('dashboard'));
    }

    cm_enqueue_styles(['register.css']);
    set_title('Sign Up - '.APP_NAME);
    $this->view("../default-header");
?>
    <main class="container-fluid">
    <form class="col-lg-8 center-block" name ="registration" method="post">
        <h1>Sign Up</h1>
        <?php

            $default = 'Fields with asterisk (*) are <b>Mandatory</b>';
            $type = 'alert-success';

            $msg  = ( isset($erros) && sizeof($erros) > 0 ? 'fields' : @$msg);
            
            switch($msg){
                case 'terms': 
                    $msg = 'Agree to the Terms and Condition to complete registration';
                    $type = 'alert-danger';
                    break;

                case 'fields': 
                    $ling = 'Form incomplete or entries invalid';
                    if(isset($pass_mismatch)){
                        $ling = 'Password Mismatch';
                    }elseif (isset($username_empty)) {
                        $ling = "Username is Mandatory";
                    }

                    $msg  = '<b>Error:</b> '.$ling;
                    $type = "alert-danger";
                    break;

                case 'username' : 
                    $msg = '<b>Username</b> username is not available';
                    $type = "alert-info"; 

                default:
                    $msg = $default;
                    break;
            }
            print "<p id=\"messaging\" class='$type'>$msg</p>";
        ?>
        <br/>

        <label>Username </label>
        <input type="text" name="uname" value="<?php echo @$uname ?>" placeholder="Enter Username.."/>
        <span class="under-text small"><?php echo @$this->errorMessages['username']; ?></span>
        <br/>

        <label>Password </label>
        <input type="password" required name="upass" value="<?php echo @$upass ?>" placeholder="Enter Password.."/>
        <br/>

        <label>Confirm</label>
            <input type="password" required name="vpass" value="" placeholder="Confirm Password.."/>
        <hr/>

        <label>First Name *</label>
            <input type="text" required name="fname" value="<?php echo @$name ?>" placeholder="Enter name in full" >
        <br>
        <label>Last Name *</label>
            <input type="text" required name="lname" value="<?php echo @$name ?>" placeholder="Enter name in full" >
        <br/>

        <label> Email *</label>
        <input type="email" required name="email" value="<?php echo @$email ?>" placeholder="Enter email adress">
        <br>
        <label> Number * </label>
        <input type="number" required="true" name="number" value="<?php echo @$number ?>" placeholder="Enter phone number">
        <br>

        <label> Mat. No. * </label>
        <input type="text" required="true" name="matric" value="<?php echo @$matric ?>" placeholder="Enter Matriculation number">
        <span class="under-text small"><?php echo @$this->errorMessages['mat_no']; ?></span>
        <br>

        <label> School </label>
        <input type="text" name="school" value="<?php echo @$school ?>" placeholder="Enter University">
        <br>

        <label> Department </label>
        <input type="text" name="department"  value="<?php echo @$department ?>" placeholder="Enter Department">
        <br/> 
        <div>
            <input type="checkbox" name="terms" <?php echo isset($_POST['terms']) ? 'checked' : null ?> />
            I agree to <a href="#">Terms</a> and <a href="#">Condition</a>.
        </div>
        <input type="hidden" name="_token" value="<?php echo $this->token ?>"/>
        <input type="submit" name="submit" value="submit" />
    </form>

    </main>
<?php $this->view('../default-footer.php') ?>