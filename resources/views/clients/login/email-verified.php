<?php
    $this->setTitle("Email Verified");
    $this->view("../default-header");

    if($this::$user !== null){
        $link = Route\UserPage('dashboard');
        $link = "<a href='{$link}'>Dashboard</a>";
    }else{
        $link = Route\MainPage("homepage");
        $link = "<a href='{$link}'>Homepage</a>";
    }
?>
    <section class="col-sm-12 col-md-6 center-block panel">
        <div class="panel-body">
            <h4 class="title panel-default">EMAIL VERIFICATION SUCCESSFUL</h4>
            <br/>
            <div class="text-left"> Your Email has been successfully verified. </div>

            <div>
                In order to protect your account and also ensure that we keep record of all our users information safe from
                hackers and fraudsters, thanks for confirming that you are a person not a robot.<br/>
                <i class="fa fa-chevron-right"></i><?php echo @$link ?>
            </div>
        </div>

        <div class="text-right">Thank You.</div>
        <br/>
    </section>
<?php
?>