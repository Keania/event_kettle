<?php
    
    $forgetRoute = Route\site_info('login').'?forget&message=';
    
    if(isset($_POST['r_submit'])) {

        $r_email = (trim(@$_POST['recover_email']));
        $sql     = "SELECT id FROM users WHERE email = '{$r_email}' LIMIT 1";
        $stmt    = $this->model->getResource()->query($sql);
        
        if ($stmt->num_rows > 0) {
            $userID = $stmt->fetch_assoc()['id'];

            #encrypts the user id that matches the email
            $user = new User($userID);
            $userID = (new \Hashids\Hashids(RECOVERY_SALT, '40'))->encode($userID);
        
            #sends password recovery email to email
            $sendSuccess = $this->SendPasswordRecoveryEmail($userID,$user->getFullName(),$r_email);
            if(!$sendSuccess) echo $this->mailer->ErrorInfo;

            die();
            redirect( $forgetRoute .  ($sendSuccess ? 'passSend' : 'failSend') );


        }else{
            redirect( $forgetRoute . 'incorrect' );
        }
    }