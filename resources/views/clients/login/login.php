<?php
    @session_start();

    // start messaging script
    switch(@$_GET['message']){

        case 'incorrect':
            $message = "<span><b>Email Not Found</b></span><br/>
            <span>if seems your email was not found on our server, please check your email 
            and try again.</span>";

            $msg = [$message,'alert-danger'];
            break;

        case 'passSend':
            $message = '<span>Password Reset Mail Sent</span><br/>
            <span>An mail was been sent to your email address</span>';
            $msg = [$message,'alert-success'];
            break;

        case 'failSend':
            $message = 'Please Try again Later. ';
            $msg = [$message,'alert-danger'];
            break;

        default: 
            $message = 'A password reset email will be sent to your email address if valid.';
            $msg = [$message,'alert-info'];
            break;

    }
    // ending messaging script

    #if a form was sent, then send the email to the email address.
    $this->view('send-email');

    #if login form was sent then log in the user.
    include SCRIPTS . 'user_login.php';

    #if user is logged in just redirect
    #because logged in users are not to access the login page.
    if(is_user_login()){
        redirect(Route\UserPage("dashboard"));
        die();
    };


    #loads the login page.
    set_title('Campus Market - Login ');
    cm_enqueue_styles(['cm_login.css']);

    $this->view("../default-header");
    $token = makeToken('_token','_GENERATED');
?>
        <?php
        // var_dump($GLOBALS['route-reg']);
        // die();
        switch(isset($_GET['forget'])):
            case 1:
                ?>
                    <form method="POST" class="login-form center-block col-sm-5" action="<?php echo Route\MainPage('login')."/retrieveEmail" ?>">
                    <h1 class="heading">Forgot Password.</h1>
                    <div>
                        <span class="<?php echo @$msg[1] ?> alert text-left center-block">
                            <?php echo @$msg[0] ?>
                        </span>
                    </div>

                    <div class="cm-input-box col-sm-9 center-block">
                        <span class="icon"><i class="glyphicon glyphicon-user"></i></span>
                        <input name="recover_email" type="email" placeholder="Enter Email" />
                    </div>
                    <br/>
                    <button name="r_submit" class="submit center-block">SEND <i class="fa fa-arrow-right"></i></button>
                </form>
                
                <?php
                break;

            case 0:
            default:
            ?>
            <div class="row">
                <div class="col-md-6 col-md-push-3">
                    <form class="login-form" method="post" action="<?php echo Route\site_info("user_login") ?>">
                        <div class="row">
                            <div class="col-md-8 col-md-push-2">
                                
                            <h1 class="heading">Sign In</h1>
                            <div class="row form-group">
                                <div>
                                    <small>No Account? <a href="register">Sign Up</a></small>
                                </div>
                            </div>
                            <?php
                                $message = (is_numeric(@$msg) && isset($msg)
                                    ? (new Error($msg,0))->showMessage()
                                    : "");
                                print $message;
                             ?>

                            <div class="form-group input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                <input type="text" name="uname" class="form-control" value="<?php echo @$_POST['uname'] ?>" placeholder="Enter Username..." autofocus=""/>
                            </div>

                            <div class="form-group input-group">
                                <span class="input-group-addon "><i class="glyphicon glyphicon-lock"></i></span>
                                <input type="password" class="form-control" name="upass" placeholder="Enter Password...">
                            </div>

                            <div>
                                <button class="submit" name="submit">
                                    <i class="glyphicon glyphicon-arrow-"></i> login 
                                </button>
                                <input type="hidden" name="_token" value="<?php echo "$token" ?>"/>

                                <span>&nbsp;&nbsp;<small><a href="?forget">Forgot Password?</a></small></span>
                            </div>
                        </div>
                        </div>
                    </form>
                </div>
            </div>

            <?php
            break;
        endswitch;
        ?>
        </div>
    </body>
</html>