<?php
	$message = 'enter a new password for your account.';
	if(isset($_POST['r_submit'])) {
        extract($_POST);
	    if(strcasecmp($vpass,$pass) === 0){

            $this->changePassword($pass);
            //$this->removeToken($this->token);
            $result = $this->model->getResource()->query('SELECT username FROM users WHERE id = '.$this->userID);
            $username = $result->fetch_assoc()['username'];
            $this->user_login($username,$pass);

        }else{
	        $message = "Passwords Do Not Match";
        }
	}

    $this->setTitle("Password Recovery");
    $this->view("../default-header");
?>
<main>
	<form name="nP" class="panel col-md-5 col-sm-11 center-block" method="post" action="#">
		<h3 class="heading text-center">New Password</h3>
		<span class="alert-info msg-box col-sm-12 center-block text-center"><?php echo @$message ?></span>
		<div class="panel-body form-group col-sm-11 center-block">
			<label class="col-sm-12">
			<span class="text-muted small">New Password</span>
			<input class="form-control" type="password" name="pass" placeholder="enter password" />
			</label>

			<label class="col-sm-12">
			<span class="text-muted small">Verify Password</span>
			<input class="form-control" type="password" name="vpass" placeholder="enter password" />
			</label>
			<input class="form-control" type="hidden" value="<?php echo $this->token ?>"/>
			<label class="col-sm-12">
			<input class="btn btn-info" type="submit" name="r_submit" value="Change Password"/>
			</label>
		</div>
	</form>
    <script>
        $(function(){
            document.forms.nP.addEventListener('submit',function (e) {

                var pass1 = $('[name=pass]').val();
                var pass2 = $('[name=vpass]').val();

                if(btoa(pass2) === btoa(pass1)){
                    console.log(btoa(pass2) === btoa(pass1));

                }else{
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    $(this).find(".alert-info").addClass("alert-danger").removeClass("alert-info").text("Passwords do not Match");

                    $(this).addClass('panel-danger');
                }

                setTimeout(function(){
                    $(this).find(".alert-danger").addClass("alert-info").removeClass("alert-danger")
                        .text("Please re-enter a password");
                },1000);
            });
        });

    </script>
</main>
<?php $this->view("./default-footer") ?>