@inject('cat', 'App\Shop\Category')
@inject('str', 'Illuminate\Support\Str')
@php
@endphp

@extends('_partials._client_base',['title' => 'Active Events- ' . Auth::user()->fname ])

@section('content')
    <!-- events content holder -->
 
    <div is="events-page" inline-template>
        <div>
           

            <div class="row">
                <!-- events list -->
                <div class="col-sm-12 col-lg-6 col-md-7">
                    <div id="search-box" class="panel">
                        <input type="text" v-model="search" name="search" placeholder="Search...." class="form-control">
                    </div>
                    <div id="event-box" class="panel">
                      
                        <div class="panel-heading">
                            <div class="clearfix">
                                <span class="panel-title">Events</span>
                                <button @click.prevent="toggleForm" class="btn  btn-primary pull-right">
                                    Create Event
                                </button>
                            </div>
                        </div>
                        <div class="panel-body">
                           <div v-if="events.length <= 0">
                                <div class="alert alert-info">
                                    @{{load_message}}
                                </div>
                            </div>

                           <div v-for="event in filteredEvent" >
                                <div class="row no-gutters" >
                                    <div class="col-sm-6 col-md-6 col-lg-6 " >

                                       <img :src="url + event.cover_image" class="img-fluid" v-if="event.cover_image != null ">
                                       <img src="{{asset('images/avatar.jpg')}}" class="img-fluid" v-else>
                                    </div>
                                    <div class="col-sm-6 col-md-6 col-lg-6">

                                    <a href="#" @click.prevent="editEvent(event.id)">  @{{event.title}}</a><br>
                                    <i class="fa fa-diamond"></i>  @{{event.short_desc}}<br>
                                    <i class="fa fa-clock-o"></i> @{{event.time}}<br>
                                    <i class="fa fa-map-marker"></i> @{{event.venue}}
                                    </div>
                                    
                                </div>
                              
                               
                            </div>
                        </div>
                    </div>

                        <div class="panel-footer">
                          
                            NoA : @{{events.length}}</b>
                        </div>
                    </div>
                

                <!-- event form panel-->
                <div  class="col-sm-12 col-lg-5 col-md-5" v-show="showAddForm">
                    <div id="add_event" class="panel">
                        <div class="panel-heading panel-default">
                            <span class="panel-title">Create Event
                            </span>
                        </div>

                        <form  class="panel-body quick-form" @submit.prevent="saveEvent" method="post" id="addForm" role="form">

                            <div class="form-group">

                                <label>Event Title*</label>
                                <input class="form-control" type="text"
                                       name="title"
                                       id="title"
                                       v-model="addedEvent.title"
                                       placeholder="Name" required>
                                 <div class="help-block with-errors"></div>
                            </div>
                           
                            <div class="form-group">
                                <label for="">Date of Event *</label>
                                 <div class='input-group date ' id="datetimepicker_add">
                                    <input type='text' class="form-control" v-model="addedEvent.time" name="time" required/>
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                    
                                </div>
                                <div class="help-block with-errors">tap into the input after selection</div>
                            </div>

                            <div class="form-group">
                                <label for="">Venue*</label>
                                <input type="text" name="venue" class="form-control" v-model="addedEvent.venue" value="" required>
                                <span class="text-muted">the exact location of the event.</span>
                                 <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label for="category">Category</label>
                                {{ \App\Shop\Category::selection('name','addedEvent.category') }}
                                <div class="help-block with-errors"></div>
                            </div>


                            <div class="form-group">
                                <label>Description</label>
                                <textarea name="description"
                                          placeholder="Specification, Methods, Models"
                                          style="width:100%;min-height: 380px;font-size:smaller"
                                              v-model="addedEvent.description"
                                             required >
                                      
                                </textarea>
                                 <div class="help-block with-errors"></div>
                            </div>
                            {{ csrf_field() }}
                            <button type="submit" name="upload__event" class="btn btn-primary">Upload</button>
                        </form>
                    </div>

                </div>

                
            </div>
              <!-- events edit form holder -->
             <modal  :show="showEdit" @closed="hideEdit">
             <template slot="modal-title">
                    <span class="panel-title">
                        <b>Edit Event</b>
                    </span>
            </template>
              <template slot="modal-body">
                    <form action="update" @submit.prevent="update" enctype="multipart/form-data" method="POST"  role="form" id="editForm">
                        <div class="row">
                            <div class="col-sm-12 col-md-10 col-lg-10 text-center">
                                <figure class="img-thumbnail" style="width: 100%">

                                    <img class="img-fluid" :src="pickedEvent.cover_image" v-if="pickedEvent.cover_image"/> 
                                    <img class="img-fluid" :src="images" v-else/> 
                                </figure>
                            </div>
                        </div>
                            <hr class="visible-xs">
                        <div class="row">
                            <div class="col-sm-12 col-md-10 col-lg-10">
                                <div class="col-sm-12">
                                    <div class="row quick-form">
                                        <div class="form-group">
                                            <label for="title">Event Title*</label>
                                            <input class="form-control" type="text" name="title" id="etitle" v-model:value="pickedEvent.title" required/>
                                            <div class="help-block with-errors"></div>
                                       </div>
                                        
                                                                                                                                        

                                        <div class="form-group">
                                            <label for="category">Category</label>
                                            {{ \App\Shop\Category::selection('name','addedEvent.category',1) }}
                                            <div class="help-block with-errors"></div>
                                        </div>


                                        <div class="form-group">
                                            <label for="">Time*</label>
                                            <div class='input-group date'  id="datetimepicker_edit">
                                                <input type='text' class="form-control" :value="pickedEvent.time" required/>
                                                <span class="input-group-addon">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>

                                            </div>
                                            <div class="help-block with-errors"></div>
                                            
                                        </div>
                                         

                                        <div class="form-group">
                                            <label>Description*</label>
                                            <textarea  name="description" rows="7" class="form-control"
                                            placeholder="Specific Description" :value="pickedEvent.description" required id="edescription"></textarea>
                                            <div class="help-block with-errors"></div>
                                        </div>
                                        
                                        <div class="form-group">
                                            <label>Venue*</label>
                                            <div class="input-group">
                                                <input class="form-control" type="text" min="0" name="venue" id="venue" :value="pickedEvent.venue" required id="evenue"/>
                                                 <div class="help-block with-errors"></div>
                                            </div>
                                        </div>
                                         
                                        <input type="hidden" name="id" :value="pickedEvent.id" />
                                        <button type="submit" class="btn btn-primary"> Save</button>
                                    </div>
                                </div>
                        </div>
                    </form>
                    
                </template>
              <template slot="modal-image">
                 <form class="dropzone" action="{{route('event.images')}}">
                      {{csrf_field()}}
                     <input type="hidden" name="id" :value="pickedEvent.id">
                      
                 </form>
              </template>
             </modal>
            
        </div> 
         
    </div>
  
@endsection
