@extends('_partials._client_base', ['title' => Auth::user()->username ])

@section('content')

	<h3 class="col-sm-12">Dashboard</h3>
	<hr>
	<!--quick event-->
	<div class="row">
		<div class="col-sm-12">
			<div class="card">
				<div class="card-content">
					<h1>20%</h1>
					<h4 class="small">SOLD <br> MERCHANDISE</h4>
				</div>
			</div>

			<div class="card">
				<div class="card-content">
					<h1>25</h1>
					<h4 class="small">UPLOADED <br> MERCHANDISE</h4>
				</div>
			</div>
		</div>
	</div>
	<hr>
	<div class="row">
		<div class="col-sm-12">
			<div class="panel">
				<table class="table table-responsive table-bordered table-striped">
					<thead>
						<th>S/N</th>
						<th width="30%">EVENT NAME</th>
						<th>EVENT TYPE</th>
						<th>SIGNUP LIMIT</th>
						<th>SIGNUPS/SOLD TICKETS</th>
						<th>SIGNUPS PERCENTAGE</th>
					</thead>
					<tbody>
						<td>1</td>
						<td>AY LIVE PORT-HARCOURT</td>
						<td>PAID</td>
						<td>100</td>
						<td>30</td>
						<td>30%</td>
					</tbody>
				</table>
			</div>
		</div>
	</div>

@endsection
