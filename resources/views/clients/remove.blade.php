@extends('_partials._client_base',[ 'title' => 'Account Truncation'])

@section('content')

	<div class="col-xs-12">
		<div class="col-xs-6">
			<div class="row">

				<div class="panel">
					<div class="panel-body">
						
				
				<h3 class="block-left"><i>... Before Deleting Account</i></h3>
				<p> Let us know why you want to sign out from our platform, if there is any reason. Please give in the
				text field below. The following will happen once your account in deleted.
				<ul>
					<li>you will not be able to login into our system unless another account is made</li>
					<li>Your events all and all uploaded files will be automatically truncated</li>
					<li>Your Email will be removed from our newsletter and notification database, as well as other Bio-Data.</li>
				</ul>
				</p>


				<h3><i>Are you sure ?</i></h3>
				<h5>Do tell us why ...</h5>

				<form action="{{ route('client.truncate') }}" method="post">
					{{ csrf_field() }}
					<div class="form-group">
						<textarea name="reason" id="" class="form-control">{{old('reason')}}</textarea>
					</div>

					<button name="delete" class="btn btn-danger">Yes</button>
					<button name="delete" class="btn btn-success">No</button>
				</form>

					</div>
				</div>
			</div>
		</div>
	</div>

@endsection