@php

	use App\Shop\ServiceCategory as SC;
	$serviceCategories = SC::selection('service-category','form-control', $service->category);

@endphp

@if(isset($service))

	
	<div id="service-card">
		<form method="post" enctype="multipart/form-data" action="{{ route('service.add') }}">
			<figure>
				<img id="cover-image" src="{{ Storage::disk('cover_images')->url($service->cover_image) }} " />
				<label class='upload-button' for="coverImage">Change Cover Image
					<input type="file" name="cover_image" id="coverImage" />
				</label>
			</figure>

			<div class="col-sm-12">

				<div class="form-group">
					<input  class="form-control" type="text" name='label' value='{{ $service->label }}'><br>
				</div>				
				
				<div class="form-group">
					<label for='description'>Description</label>
					<textarea type='text' name='description'>{{ $service->description }}</textarea>
				</div>								
			
				<div class="form-group">
					<label>Categories</label>
					{{ $serviceCategories }}
				</div>

				<div class="form-group">
					<label>Images</label>
					
					<div>
						<div class="row">
							<div class="col-sm-4">
								<span for="product_image" class="upload-button">Upload Images</span>
								<input id="product_image" class="form-control" type="file" name="service_images[]" multiple >
							</div>

							<div class="col-sm-6" >
								<div class="row">
									<span class="col-sm-4"> OR </span>
									<i class="small btn btn-default">
										Drag Images in the Box below.
									</i>
								</div>
							</div>
						</div>

						
						<div class="drag-drop">
							<input type="hidden" name="imageColl" value='{!! $service->photos->toJson() !!}'>
							<div class="clearfix">
								{{ $images }}
							</div>
						</div>
					</div>
				</div>

	            <input type="hidden" name='_xerox' value='{{ $service->id }}'>
	            <input type="hidden" name="_method" value="PUT"/>
	            {{ csrf_field() }}

	            <div class="btn-group text-center col-sm-12">
		            <button class="btn" name='put-service' value="save">
		            	<i class="glyphicon glyphicon-floppy-disk"></i> Save
	            	</button>
		            <button class="btn" name='put-service' value="delete">
		            	<i class="glyphicon glyphicon-trash"></i> Delete
	            	</button>
	            </div>
            </div>
		</form>
	</div>

@else
	@php
		abort(403) 
	@endphp
@endif
