@php
    use App\Helpers\Navigation as Nav;
@endphp

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
        @stack('meta-data')
        <script>
            window.Laravel = {
                csrfToken : "{!! csrf_token() !!}"
            }
        </script>

        <title>{{ @$title }}</title>
        <link href="{{ asset('css/app.css') }}" rel="stylesheet"/>
        <link href="{{ asset('css/cm.css') }}" rel="stylesheet"/>
        <link rel="stylesheet" href="{{ asset('css/generic.css') }}"/>
        @yield('styles')

        <script src="{{ asset('js/app.js') }}"  type="application/javascript"></script>
        <script src="{{ asset('js/component.js') }}" type="application/javascript"></script>
        @yield('js') @yield('scripts')

    </head>

    <body>
    <div class="row">
            <div class="col-sm-6 col-md-6 col-lg-6">
              <div>
                <img src="{{asset('images/assets/event-logo.jpg')}}"  class="img-fluid">
              </div>
            </div>
            <div class="col-sm-6 col-md-6 col-lg-6">
            @yield('container')
            </div>
    </div>

        @include('_partials.footer')

        <script src="{{ asset('js/cm-home.js') }}"></script>
        <script type="text/javascript">
           $(function(){
                $('form').each(function(){
                     $(this).validator();
                 });
           });
        </script>
    </body>
</html>

