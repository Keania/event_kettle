@inject('cat', 'App\Shop\Category')
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

        <title>{{ @$title }}</title>
        <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
        window.ShopCategories = {!!
            $cat::all()->pluck('name', 'id')->toJson()
        !!}
        </script>
        <link href="{{ asset('font-awesome/css/font-awesome.min.css') }}" rel="stylesheet"/>
         <!--<link href="{{ asset('css/materialize.min.css') }}" rel="stylesheet"/> -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet"/>
        <link rel="stylesheet" href="{{ asset('css/generic.css') }}">
        <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
         <link rel="stylesheet" href="{{ asset('css/bootstrap-datetimepicker.min.css') }}">
        <link rel="stylesheet" href="{{ asset('css/cm-account.css') }}">
         <link rel="stylesheet" href="{{ asset('css/basic.min.css') }}">
          <link rel="stylesheet" href="{{ asset('css/dropzone.min.css') }}">

    
          <link href="{{ asset('css/mystyle.css') }}" rel="stylesheet"/>
        @yield('styles')
        
        <script src="{{ asset('js/app.js') }}"  type="application/javascript"></script>
        <script src="{{ asset('js/component.js') }}" type="application/javascript"></script>
        {{-- <script src="//cdnjs.cloudflare.com/ajax/libs/chosen/1.8.2/chosen.jquery.min.js"></script> --}}
    

    </head>

    <body>
        <div id="client">

            @include('clients.sidebar')
                <!-- container-start  -->
            <main id="main-col" class="col-lg-offset-2">
                <div class="container-fluid">

                    <div class="row">

                        @include('clients.partials.navbar')

                        <div id="pt-container" class="col-md-12" >

                            @yield('content')

                        </div>

                    </div>
                </div>
            </main>
        </div>
        <script src="{{ asset('js/cm-dashboard.js') }}" type="application/javascript"></script>
       
        <script>
           $(function () {
                
                 $('#datetimepicker_edit').datetimepicker({
                      format:'YYYY-MM-DD HH:mm:ss',
                       minDate: new Date()
                       
                 });
                 $('form').each(function(){
                     $(this).validator();
                 });
              
                
            });
        </script> 
        @yield('js') @yield('scripts')

    </body>
</html>

