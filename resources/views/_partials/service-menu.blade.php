
<h3 class="title">Services</h3>

<span class="text-muted small">pick a category.</span>

<hr/>

<ul class="nav">
    <li>
        <a href='{{ route('shop.services') }}'>
            <i class='fa fa-random fa-fw'></i>All Services
        </a>
    </li>

   @foreach( $categories as $category )
        @php
            $active = (request()->url() === $category->link ? 'active' : '');
        @endphp
            <li class="{{ $active }}">
                <a href='{{ $category->link }}'>
                    {{ $category->label }}
                </a>
            </li>

    @endforeach

</ul>
