<footer class="container-fluid">

	<div class="row">
		
		<div class="col-sm-12">
			<div class="row">
				<div id="logos" class="col-sm-12 col-md-8 pull-left">
					<a href="//www.gistmeust.com/">
						<img src="{{ asset('images/assets/gistme-logo.svg') }}" width="200"/>
					</a>
					<div class="fb-like" 
						data-width="50" 
						data-layout="button_count" 
						data-action="like" 
						data-size="large" 
						data-show-faces="false" 
						data-share="true">
					</div>
				</div>
				<div class="col-sm-12 col-md-4 text-right pull-right">
					<div>Copryight &copy; 2017 <b>eCAMPUS</b>MARKET </div>

					<div class="small">
						<a href="{{ route('shop.terms') }}">terms and conditions </a>
						for usuage. All rights Reserved.
					</div>
				</div>
			</div>
		</div>

	</div>
</footer>