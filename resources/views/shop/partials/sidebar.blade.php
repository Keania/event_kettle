@php

  use App\Shop\Category;
  use App\Shop\ServiceCategory as SC;

@endphp

<!--navigation-->
<div id="pd-menu" class="col-sm-3 shop-filter button-group">
    <div class="panel">
        <div class="panel-heading">
            <span class="panel-title">
                <span>Shop Categories</span>
                <button class="pd-menu-button pull-right">
                    <i class="zmdi zmdi-more-vert"></i>
                </button>
            </span>

        </div>

        <div class="panel-body">

            <ul class="nav nav-stacked collapse navbar-collapse row" style="margin-top: -20px" id="event-menu">

                @foreach( Category::all(['name','icon','position']) as $category)

                    @php

                        $slug = str_slug($category->name);
                        $link = route('shop.category',['category_name' => $slug ]);
                        $active = ( $link === request()->url()) ? "active" : "" ;

                    @endphp

                    <li class="{{$active}}">
                        <a href='{{$link}}' data-filter='.{{$slug}}'>
                            <span class="icon">{{ Category::pick_icon($category->icon) }}</span>
                            <span>{{ $category->name }}</span>
                        </a>
                    </li>

                @endforeach
            </ul>
        </div>
    </div>
</div>
