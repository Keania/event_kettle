<div id="pd-menu" class="panel">
    <div class="panel-heading">
        <span class="panel-title">
            <span>Sort Merchandise</span>
            <button class="pd-menu-button pull-right">
                <i class="zmdi zmdi-more-vert"></i>
            </button>
        </span>
    </div>

    <div class="panel-body">
        <ul class="nav nav-stacked collapse navbar-collapse" id="event-menu">
            <li>
                <b>PRICE</b> <input type="range" min="30" max="3000"/>
            </li>
        </ul>
    </div>
</div>
