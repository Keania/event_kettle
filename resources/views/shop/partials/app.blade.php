@php
    use App\Helpers\Navigation as Nav;
@endphp

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
        @stack('meta-data')
        <script>
            window.Laravel = {
                csrfToken : "{!! csrf_token() !!}"
            }
        </script>

        <title>{{ @$title }}</title>
        <link href="{{ asset('css/app.css') }}" rel="stylesheet"/>
        <link href="{{ asset('css/cm.css') }}" rel="stylesheet"/>
        <link href="{{ asset('css/generic.css') }}" rel="stylesheet"/>
        @yield('styles')

        <script src="{{ asset('js/app.js') }}"  type="application/javascript"></script>
        <script src="{{ asset('js/component.js') }}" type="application/javascript"></script>
        @yield('js') @yield('scripts')

    </head>

    <body>

        <nav class="navbar navbar-static-top navbar-default">

            <div class="container">

                <div class="navbar-header">
                    <div class="navbar-brand">
                        <a href="{{ "#" }}">
                            Event<b>Kettle</b>
                        </a>
                    </div>
                </div>

                <div class="nav-holder row">
                    <ul class="nav navbar-nav navbar-right">
                        @php

                            $genericNav = [
                                'Home' =>   [ route("shop.home"), 'zmdi-home' ],
                                'Login' => [ route('login'), 'zmdi-sign-in']
                            ];

                            $userNav = [
                                'Home' => $genericNav['Home'],
                                @Auth::user()->fname => [ route('client.profile'), '' ],
                            ];

                            $Nav = Auth::user() ? $userNav : $genericNav;

                            foreach($Nav as $navName => $navInfo ):
                                Nav::layout($navName,$navInfo);
                            endforeach;

                        @endphp
                    </ul>
                </div>

            </div>
        </nav>

        <main id="main-col" class="container-fluid">

            @yield('container')

        </main>

        @include('_partials.footer')

        <script src="{{ asset('js/cm-home.js') }}"></script>
    </body>
</html>

