 <div class="panel">
    <div class="panel-heading">
        <span class="panel-title">
            <span>Services Categories</span>
            <button class="pd-menu-button pull-right">
                <i class="glyphicon glyphicon-option-vertical"></i>
            </button>
        </span>

    </div>

    <div class="panel-body">

        <ul class="nav nav-stacked collapse navbar-collapse row" style="margin-top: -20px;" id="event-menu">

            @foreach( SC::all(['label']) as $service)

                @php

                    $slug = str_slug($service->label);
                    $link = route('service.category',['category' => $slug ]);
                    $active = ( $link === request()->url()) ? "active" : "" ;

                @endphp

                <li class="{{$active}}">
                    <a href='{{$link}}' data-filter='.{{$slug}}'>
                        <span class="icon">{{ icon('fa-circle-o') }}</span>
                        <span>{{ $service->label }}</span>
                    </a>
                </li>

            @endforeach
        </ul>
    </div>
</div>
