@extends('shop.partials.app',['title' => $owner->username ." <seller@cmarket.com> "])

@section('styles')
    <link rel="stylesheet" href="{{ asset('css/cm.css') }}">
@endsection

@section('js')
    <script src="{{ asset('js/cm-home.js') }}"></script>
@endsection

@section('container')

    <div id="userInfo" class="row">

        <div class="col-sm-4 col-md-3">
            
            <div class="panel">

                <div class="panel-body"> 
                    <div id="user-box" class="col-sm-12">

                        <p class="name-title">{{ fullName($owner) }}</p>

                        <figure>
                            <img class="thumbnail" src="{{ getImage('user_images',$owner->photo) }}"/>
                        </figure>

                        <ul class="list-group">

                            <li class="list-group-item">
                                <i class="fa fa-at"></i>
                                <span></span>
                                {{ $owner->email }}
                            </li>
                            <li class="list-group-item">

                                <i class="fa fa-phone fa-fw"></i>
                                <span>Mobile: </span> 
                                {{ $owner->phone }}

                            </li>

                            <li class="list-group-item">

                                <i class="fa fa-buysellads fa-fw"></i> 
                                <span>Adverts:</span>
                                {{ sizeof($owner->events) }}
                            </li>
                            <!--<li class="list-group-item">
                            <i class="fa fa-star fa-fw"></i>Rating:
                            {{ '5' }}</li>-->
                        </ul>
                    </div>
                    
                </div>
                
            </div>
        </div>

        <div class="col-xs-12 col-sm-6 col-md-9">
            <div class="row">

                <ul class="event-list">
                    <div class="col-xs-12">
                        <hgroup>
                            <h5 class="text-muted">&nbsp;
                                {{ icon('glyphicon-list-alt') }} ADVERTS</h5>
                        </hgroup>
                    </div>

                    <div class="col-xs-12">

                        @foreach ($owner->events as $event)

                            @include('market.event.card',['event' => $event ])

                        @endforeach

                    </div>
                </ul>
                
            </div>
        </div>
    </div>

    @include('market.event-modal');

@endsection