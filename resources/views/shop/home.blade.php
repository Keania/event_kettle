@extends('shop.partials.app', ['title' => "Shop | Home > Event Kettle "] )

@section('container')

	<div id="wrap" class="container m-top-30">
		<div class="row">
			@include('shop.partials.sidebar')
			<div id="slide-shower" class="col-md-9 panel" style="min-height: 300px">

			</div>
		</div>
		<hr>
		<div class="row m-top-30">
			<!--merchandise list -->
			<div class="col-sm-12 event-holder center-block">
			    <h3>Trending Stuff</h3>
			    <p>press the <kbd>ctrl</kbd> button and <kbd>click</kbd> for quick preview.</p>

			    <ul class="event-list row list-inline">
			        {{-- @foreach( $events as $event )

						@include('shop.event.card',['event' => $event ])

			        @endforeach --}}
			    </ul>
		        <a href="{{ route('shop.categories') }}">
					<button class="std-btn">
						See More
					</button>
				</a>

				<hr>
			</div>
		</div>
		{{--@include('shop.advert-modal') --}}
	</div>
@endsection
