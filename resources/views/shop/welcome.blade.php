@extends('shop.partials.app',['title' => $title ])

@section('styles')
    <link rel="stylesheet" href="{{ asset('css/cm.css') }}"/>
@endsection

@section('container')
    <script>
        $('.navbar-static-top')
            .removeClass('navbar-static-top')
            .addClass('navbar-fixed-top');
    </script>

    <section id="big-wall">
        
        <section id="page" class="row">
            <article id="desc" class="col-md-5 col-md-offset-1 col-xs-12 col-sm-8">
                <h1 class="heading">What's Campus Market?</h1>
                <p>
                    Instead of throwing out furniture at the end of the semester or bringing home textbooks and posters to gather dust, eCampusMarket users can sell what they don't need and buy what they do from members of their college community. Our website promotes sustainable reuse on campus by providing a simple and effective means of initiating exchange. In this way, items are given a second life and kept out of landfills they're not yet ready for. We also help schools reduce their carbon footprint and foster micro-economies on campus.</p>
            </article>
            <article id="image" class="col-md-5 visible-md visible-lg">
            </article>
        </section>

        <div id="card-holder" style="margin-top: -40px" class="row">
            <section class="text-center">
                    @php

                        $links = [
                            'Buy' => route('shop.home'),
                            'Sell' => route('login'),
                            'Hire' => route('shop.services')
                        ];

                    @endphp
                    
                    @foreach($links as $name => $link )
                        <a href="{{ $link }}" class="slide-ellipsis col-xs-12 col-md-3">
                            <div class="circle-behind"></div>
                            <span class="caption">{{ $name }}</span>
                        </a>
                    @endforeach
            </section>
        </div>
    </section>

    <script src="{{ asset('js/app.js') }}"></script>
@endsection