
@extends('shop.partials.app',['title' => 'TERMS OF USE :: eCampusMarket'])

@section('container')
	<div class="row">
		
		<div class="col-xs-11 col-md-12">
			<div id="terms-condition" class="col-md-8 col-md-offset-2">
				<div class="text-center text-muted">
					Terms of service
					
				</div>

<h3 class="heading"><i>1</i>. <b> Acceptance of Terms</b></h3>
The Campus Market provides online classified eventising (referred to hereafter as "the Service") subject to the following Terms of Use ("TOU"). By using the Service in any way, you are agreeing to comply with the TOU. In addition, when using particular The Campus Market services, you agree to abide by any applicable posted guidelines for all The Campus Market services, which may change from time to time. Should you object to any term or condition of the TOU, any guidelines, or any subsequent modifications thereto or become dissatisfied with The Campus Market in any way, your only recourse is to immediately discontinue use of The Campus Market. The Campus Market has the right, but is not obligated, to strictly enforce the TOU through self-help, community moderation, active investigation, litigation and prosecution.


<h3 class="heading"><i>2</i>. <b> Modification to This Agreement</b></h3>
We reserve the right, at our sole discretion, to change, modify or otherwise alter these terms and conditions at any time. Such modifications shall become effective immediately upon the posting thereof.


<h3 class="heading"><i>3</i>. <b> Content</b></h3>
<p>
You understand that all postings, messages, text, files, images, photos, video, sounds, or other materials ("Content") posted on, transmitted through, or linked from the Service, are the sole responsibility of the person from whom such Content originated. More specifically, you are entirely responsible for each individual item ("Item") of Content that you post, email or otherwise made available via the Service. You understand that The Campus Market does not control, and is not responsible for Content made available through the Service, and that by using the Service, you may be exposed to Content that is offensive, indecent, inaccurate, misleading, or otherwise objectionable. Furthermore, The Campus Market site and Content available through the Service may contain links to other websites, which are completely independent of The Campus Market. The Campus Market makes no representation or warranty as to the accuracy, completeness or authenticity of the information contained in any such site. Your linking to any other websites is at your own risk. You agree that you must evaluate, and bear all risks associated with, the use of any Content, that you may not rely on said Content, and that under no circumstances will The Campus Market be liable in any way for any Content or for any loss or damage of any kind incurred as a result of the use of any Content posted, emailed or otherwise made available via the Service. You acknowledge that The Campus Market does not pre-screen or approve Content, but that The Campus Market shall have the right (but not the obligation) in its sole discretion to refuse, delete or move any Content that is available via the Service, for violating the letter or spirit of the TOU or for any other reason.


<h3 class="heading"><i>4</i>. <b> Thirds Party Content, Sites, and Services</b></h3>
<p>
The Campus Market site and Content available through the Service may contain features and functionalities that may link you or provide you with access to third party content which is completely independent of The Campus Market, including web sites, directories, servers, networks, systems, information and databases, applications, software, programs, products or services, and the Internet as a whole.
</p>
<p>
Your interactions with organizations and/or individuals found on or through the Service, including payment and delivery of goods or services, and any other terms, conditions, warranties or representations associated with such dealings, are solely between you and such organizations and/or individuals. You should make whatever investigation you feel necessary or appropriate before proceeding with any online or offline transaction with any of these third parties. 
You agree that The Campus Market shall not be responsible or liable for any loss or damage of any sort incurred as the result of any such dealings. If there is a dispute between participants on this site, or between users and any third party, you understand and agree that The Campus Market is under no obligation to become involved. In the event that you have a dispute with one or more other users, you hereby release The Campus Market, its officers, employees, agents and successors in rights from claims, demands and damages (actual and consequential) of every kind or nature, known or unknown, suspected and unsuspected, disclosed and undisclosed, arising out of or in any way related to such disputes and / or our service. If you are a California resident, you waive California Civil Code Section 1542, which says: "A general release does not extend to claims which the creditor does not know or suspect to exist in his favor at the time of executing the release, which, if known by him must have materially affected his settlement with the debtor."</p>


<h3 class="heading"><i>5</i>. <b> Notification of Claims of Infringement</b></h3>
<p>
	
If you believe that your work has been copied in a way that constitutes copyright infringement, or your intellectual property rights have been otherwise violated, please notify The Campus Market's agent for notice of claims of copyright or other intellectual property infringement ("Agent") at info@thecampusmarket.com. The Campus Market will remove the infringing posting(s), subject to the procedures outlined in the Digital Millennium Copyright Act (DMCA).
</p>


<h3 class="heading"><i>6</i>. <b> Privacy AND Information Disclosure</b></h3>
<p>
	
Your use of The Campus Market website or the Service signifies acknowledgement of and agreement to our Privacy Policy. You further acknowledge and agree that The Campus Market may, in its sole discretion, preserve or disclose your Content, as well as your information, such as email addresses, IP addresses, timestamps, and other user information, if required to do so by law or in the good faith belief that such preservation or disclosure is reasonably necessary to: comply with legal process; enforce the TOU; respond to claims that any Content violates the rights of third-parties; respond to claims that contact information (e.g. phone number, street address) of a third-party has been posted or transmitted without their consent or as a form of harassment; protect the rights, property, or personal safety of The Campus Market, its users or the general public. In all other instances, collection of information on the signup page and elsewhere is used by the Service to place students in their correct marketplace.
</p>


<h3 class="heading"><i>7</i>. <b> Conduct</b></h3>
<p>
	
You agree not to post, email, or otherwise make available Content: 
<li>a) that is unlawful, harmful, threatening, abusive, harassing, defamatory, libelous, invasive of another's privacy, or is harmful to minors in any way;</li>
<li>b) that is pornographic;</li>
<li>c) that harasses, degrades, intimidates or is hateful toward an individual or group of individuals on the basis of religion, gender, sexual orientation, race, ethnicity, age, or disability;</li>
<li>d) that violates the Fair Housing Act by stating, in any notice or ad for the sale or rental of any dwelling, a discriminatory preference based on race, color, national origin, religion, sex, familial status or handicap (or violates any state or local law prohibiting discrimination on the basis of these or other characteristics);</li>
<li>e) that violates federal, state, or local equal employment opportunity laws, including but not limited to, stating in any eventisement for employment a preference or requirement based on race, color, religion, sex, national origin, age, or disability.</li>
<li>f) with respect to employers that employ four or more employees, that violates the anti-discrimination provision of the Immigration and Nationality Act, including requiring U.S. citizenship or lawful permanent residency (green card status) as a condition for employment, unless otherwise required in order to comply with law, regulation, executive order, or federal, state, or local government contract.</li>
<li>g) that impersonates any person or entity, including, but not limited to, a The Campus Market employee, or falsely states or otherwise misrepresents your affiliation with a person or entity (this provision does not apply to Content that constitutes lawful non-deceptive parody of public figures.);</li>
<li>h) that includes personal or identifying information about another person without that person's explicit consent;</li>
<li>i) that is false, deceptive, misleading, deceitful, or constitutes "bait and switch";</li>
<li>j) that infringes any patent, trademark, trade secret, copyright or other proprietary rights of any party, or Content that you do not have a right to make available under any law or under contractual or fiduciary relationships;</li>
<li>k) that constitutes or contains "affiliate marketing," "link referral code," "junk mail," "spam," "chain letters," "pyramid schemes," or unsolicited commercial eventisement;</li>
<li>l) that constitutes or contains any form of eventising or solicitation if: posted in areas of The Campus Market sites which are not designated for such purposes; or emailed to The Campus Market users who have not indicated in writing that it is ok to contact them about other services, products or commercial interests.</li>
<li>m) that includes links to commercial services or web sites, except as allowed in "services";</li>
<li>n) that eventises any illegal service or the sale of any items the sale of which is prohibited or restricted by any applicable law, including without limitation items the sale of which is prohibited or regulated by California law.</li>
<li>o) that contains software viruses or any other computer code, files or programs designed to interrupt, destroy or limit the functionality of any computer software or hardware or telecommunications equipment;</li>
<li>p) that disrupts the normal flow of dialogue with an excessive amount of Content (flooding attack) to the Service, or that otherwise negatively affects other users' ability to use the Service; or</li>
<li>q) that employs misleading email addresses, or forged headers or otherwise manipulated identifiers in order to disguise the origin of Content transmitted through the Service.<br>
Additionally, you agree not to:</li>
<li>r) contact anyone who has asked not to be contacted, or make unsolicited contact with anyone for any commercial purpose;</li>
<li>s) "stalk" or otherwise harass anyone;</li>
<li>t) collect personal data about other users for commercial or unlawful purposes;</li>
<li>u) use automated means, including spiders, robots, crawlers, data mining tools, or the like to download data from the Service - unless expressly permitted by The Campus Market;</li>
<li>v) post non-local or otherwise irrelevant Content, repeatedly post the same or similar Content or otherwise impose an unreasonable or disproportionately large load on our infrastructure;</li>
<li>w) attempt to gain unauthorized access to The Campus Market's computer systems or engage in any activity that disrupts, diminishes the quality of, interferes with the performance of, or impairs the functionality of, the Service or The Campus Market website; or</li>
<li>x) use any form of automated device or computer program that enables the submission of postings on The Campus Market without each posting being manually entered by the author thereof (an "automated posting device"), including without limitation, the use of any such automated posting device to submit postings in bulk, or for automatic submission of postings at regular intervals.</li>
<li>y) use any form of automated device or computer program ("flagging tool") that enables the use of The Campus Market's "flagging system" or other community moderation systems without each flag being manually entered by the person that initiates the flag (an "automated flagging device"), or use the flagging tool to remove posts of competitors, or to remove posts without a good faith belief that the post being flagged violates these TOU.</li>
</p>


<h3 class="heading"><i>8</i>. <b> Posting Agents</b></h3>
<p>
	
A "Posting Agent" is a third-party agent, service, or intermediary that offers to post Content to the Service on behalf of others. To moderate demands on The Campus Market's resources, you may not use a Posting Agent to post Content to the Service without express permission or license from The Campus Market. Correspondingly, Posting Agents are not permitted to post Content on behalf of others, to cause Content to be so posted, or otherwise access the Service to facilitate posting Content on behalf of others, except with express permission or license from The Campus Market.
</p>


<h3 class="heading"><i>9</i>. <b> No Spam Policy</b></h3>
<p>
	
You understand and agree that sending unsolicited email eventisements to The Campus Market email addresses or through The Campus Market computer systems is expressly prohibited by these Terms. Any unauthorized use of The Campus Market computer systems is a violation of these Terms and certain federal and state laws. Such violations may subject the sender and his or her agents to civil and criminal penalties.
</p>


<h3 class="heading"><i>10</i>. <b> Paid Postings</b></h3>
<p>
	
In the event a fee is established to post Content in some areas of the Service, the fee will be an access fee permitting Content to be posted in a designated area. Each party posting Content to the Service is responsible for said Content and compliance with the TOU. All fees paid will be non-refundable in the event that Content is removed from the Service for violating the TOU.
</p>


<h3 class="heading"><i>11</i>. <b> Limitations of Service</b></h3>
<p>
	
You acknowledge that The Campus Market may establish limits concerning use of the Service, including the maximum number of days that Content will be retained by the Service, the maximum number and size of postings, email messages, or other Content that may be transmitted or stored by the Service, and the frequency with which you may access the Service. You agree that The Campus Market has no responsibility or liability for the deletion or failure to store any Content maintained or transmitted by the Service. You acknowledge that The Campus Market reserves the right at any time to modify or discontinue the Service (or any part thereof) with or without notice, and that The Campus Market shall not be liable to you or to any third party for any modification, suspension or discontinuance of the Service.
</p>


<div class="section">	
<h3 class="heading"><i>12</i>. <b> Access to the Service</b></h3>
<p>
	
The Campus Market grants you a limited, revocable, nonexclusive license to access the Service for your own personal use. The Campus Market permits you to display on your website, or create a hyperlink on your website to, individual postings on the Service so long as such use is for noncommercial and/or news reporting purposes only (e.g., for use in personal web blogs or personal online media). You may also create a hyperlink to the home page of The Campus Market sites so long as the link does not portray The Campus Market, its employees, or its affiliates in a false, misleading, derogatory, or otherwise offensive matter.
</p>
<p>
	
The Campus Market offers various parts of the Service in RSS format so that users can embed individual feeds into a personal website or blog, or view postings through third party software news aggregators. The Campus Market permits you to display, excerpt from, and link to the RSS feeds on your personal website or personal web blog, provided that
<ul> 
	<li>(a).  your use of the RSS feed is for personal, non-commercial purposes only, </li>

	<li>(b).  each title is correctly linked back to the original post on the Service and redirects the user to the post when the user clicks on it, </li>

	<li>(c).  you provide, adjacent to the RSS feed, proper attribution to </li>
	"The Campus Market" as the source, 
	<li>(d).  your use or display does not suggest that The Campus Market promotes or endorses any third party causes, ideas, web sites, products or services, </li>

	<li>(e).  you do not redistribute the RSS feed, and </li>

	<li>(f).  your use does not overburden The Campus Market's systems. The Campus Market reserves all rights in the content of the RSS feeds and may terminate any RSS feed at any time.Use of the Service beyond the scope of authorized access granted to you by The Campus Market immediately terminates said permission or license. In order to collect, aggregate, copy, duplicate, display or make derivative use of the Service or any Content made available via the Service for other purposes (including commercial purposes) not stated herein, you must first obtain a license from The Campus Market.</li>
</ul>
</p>
</div>


<h3 class="heading"><i>13</i>. <b> Termination of Service</b></h3>
<p>	
You agree that The Campus Market, in its sole discretion, has the right (but not the obligation) to delete or deactivate your account, block your email or IP address, or otherwise terminate your access to or use of the Service (or any part thereof), immediately and without notice, and remove and discard any Content within the Service, for any reason, including, without limitation, if The Campus Market believes that you have acted inconsistently with the letter or spirit of the TOU. Further, you agree that The Campus Market shall not be liable to you or any third-party for any termination of your access to the Service. Further, you agree not to attempt to use the Service after said termination. Sections 2, 4, 6 and 10-16 shall survive termination of the TOU.
</p>


<h3 class="heading"><i>14</i>. <b> Proprietary Rights</b></h3>
<p>		
The Service is protected to the maximum extent permitted by copyright laws and international treaties. Content displayed on or through the Service is protected by copyright as a collective work and/or compilation, pursuant to copyrights laws, and international conventions. Any reproduction, modification, creation of derivative works from or redistribution of the site or the collective work, and/or copying or reproducing the sites or any portion thereof to any other server or location for further reproduction or redistribution is prohibited without the express written consent of The Campus Market. You further agree not to reproduce, duplicate or copy Content from the Service without the express written consent of The Campus Market, and agree to abide by any and all copyright notices displayed on the Service. You may not decompile or disassemble, reverse engineer or otherwise attempt to discover any source code contained in the Service. Without limiting the foregoing, you agree not to reproduce, duplicate, copy, sell, resell or exploit for any commercial purposes, any aspect of the Service. THE CAMPUS MARKET is a registered mark in the U.S. Patent and Trademark Office.
</p>
<p>
	
Although The Campus Market does not claim ownership of content that its users post, by posting Content to any public area of the Service, you automatically grant, and you represent and warrant that you have the right to grant, to The Campus Market an irrevocable, perpetual, non-exclusive, fully paid, worldwide license to use, copy, perform, display, and distribute said Content and to prepare derivative works of, or incorporate into other works, said Content, and to grant and authorize sublicenses (through multiple tiers) of the foregoing. Furthermore, by posting Content to any public area of the Service, you automatically grant The Campus Market all rights necessary to prohibit any subsequent aggregation, display, copying, duplication, reproduction, or exploitation of the Content on the Service by any party for any purpose.
</p>


<h3 class="heading"><i>15</i>. <b> Disclaimer of Warranties</b></h3>
<p>		
You agree that the use of The Campus Market site and the service is entirely at your own risk. The Campus Market site and the service are provided on an "as is" or "as available" basis, without any warranties of any kind. All express and implied warranties, including, without limitation, the warranties of merchantability, fitness for a particular purpose, and non-infringement of proprietary right are expressly disclaimed to the fullest extent permitted by law. To the fullest extent permitted by law, The Campus Market disclaims any warranties for the security, reliability, timeless, accuracy, and performance of The Campus Market site and the service. To the fullest extent permitted by law, The Campus Market disclaims any warranties for other services or good received through or eventised on The Campus Market site or the sties or service, or accessed through any links on The Campus Market site. To the fullest extent permitted by law, The Campus Market disclaims any warranties for viruses or other harmful components in connection with The Campus Market site or the service. Some jurisdictions do not allow the disclaimer of implied warranties. In such jurisdictions, some of the foregoing disclaimers may not apply to you insofar as they relate to implied warranties.
</p>


<h3 class="heading"><i>16</i>. <b> Limitations and Liability</b></h3>
<p>
	
Under no circumstances shall The Campus Market be liable for direct, indirect, incidental, special, consequential, or exemplary damages (even if The Campus Market has been advised of the possibility of such damages), resulting from any aspect of your use of The Campus Market site or the service, whether the damages arise from use or misuse of The Campus Market site or the service, from inability to use The Campus Market site or the service, or the interruption, suspension, modification, alteration, or termination of The Campus Market site or the Service. Such limitation shall also apply with respect to damages incurred by reason of other services or products received through or eventised in connection with The Campus Market site or the service or any links on The Campus Market site, as well as by reason of any information or advice received through or eventised in connection with The Campus Market site or the Service or any links on The Campus Market Site. These limitations shall apply to the fullest extent permitted by law. In some jurisdictions, limitations of liability are not permitted. In such jurisdictions, some of the foregoing limitation may not apply to you.
</p>


<h3 class="heading"><i>17</i>. <b> Indemnity</b></h3>
<p>
	
You agree to indemnify and hold The Campus Market, its officers, subsidiaries, affiliates, successors, assigns, directors, officers, agents, service providers, suppliers and employees, harmless from any claim or demand, including reasonable attorney fees and court costs, made by any third party due to or arising out of Content you submit, post or make available through the Service, your use of the Service, your violation of the TOU, your breach of any of the representations and warranties herein, or your violation of any rights of another.
</p>


<h3 class="heading"><i>18</i>. <b> General Information</b></h3>
<p>
	
The TOU constitute the entire agreement between you and The Campus Market and govern your use of the Service, superseding any prior agreements between you and The Campus Market.
</p>


<h3 class="heading"><i>19</i>. <b> Violation of Terms and Liquidated Damages</b></h3>
<p>
	
Please report any violations of the TOU by emailing to: info@thecampusmarket.com. Our failure to act with respect to a breach by you or others does not waive our right to act with respect to subsequent or similar breaches. 
</p>
<p>
	
You understand and agree that, because damages are often difficult to quantify, if it becomes necessary for The Campus Market to pursue legal action to enforce these Terms, you will be liable to pay The Campus Market's actual damages, to the extent such actual damages can be reasonably calculated. Notwithstanding any other provision of these Terms, The Campus Market retains the right to seek the remedy of specific performance of any term contained in these Terms, or a preliminary or permanent injunction against the breach of any such term or in aid of the exercise of any power granted in these Terms, or any combination thereof.
</p>
			</div>
		</div>

	</div>
@endsection