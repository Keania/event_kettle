@extends('shop.partials.app',['title' => 'Services - eCMARKET '])

@section('styles')
    <link rel="stylesheet" href="{{ asset('css/cm-services.css') }}">
@endsection

@section('container')
    
    <div>
        
        <div id="service-welcome-box">

            <h1 class="heading text-center">Main Services Categories</h1>
            <ul class="nav nav-pills event-list text-center">

                @foreach( $serviceCategories as $is)

                        @php
                            $link = route('service.category',['category' => str_slug($is->label) ]);
                        @endphp

                        <a class='' title='{{ $is->description }}' href='{{ $link }}'>
                            <span>{{ $is->label }}</span>
                        </a>

                @endforeach

            </ul>
        </div>
    </div>


    <div class="container">

        <div class="row">
            
            <h3 class="heading">TOP SERVICES</h3>

            <ul class="service-list list-inline">
                
                @foreach( $services as $service )

                   @include('shop.services.card',['service' => $service ])

                @endforeach

            </ul>

        </div>

    </div>

@endsection
