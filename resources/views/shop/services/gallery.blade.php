
<div class="col-xs-12" style="float:none">
	
<div class="row list-inline">
	
    @foreach( $images as $image )
        <li  class='sr-image-tb col-sm-5'>
        <figure>
            <a href='{{ getImage('service_images',$image->name) }}' data-lightbox="service-gallery">
                <img src="{{ getImage('service_images',$image->name) }}"/>
            </a>
        </figure>
        </li>
        
    @endforeach
</div>
</div>
