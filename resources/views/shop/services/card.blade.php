@isset ($service)

        <li class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <div class="thumb-box">
                <a href='{{ $service->link }}' class="col-xs-12">
                    <img class='thumbnail' src='{{ Storage::disk('cover_images')->url($service->cover_image) }}' />
                </a>

                <figcaption class="bottom-bar caption col-xs-12">
                    {{ $service->label }}
                </figcaption> 
            </div>
           <div class='top-bar' style="vertical-align: top">
                <figure class='user-thumb xs-small'> 
                    <img src='{{ Storage::disk('user_images')->url($service->user->photo) }}'>
                </figure>
                <span>{{ '@'.$service->user->username }}</span>
            </div>
        </li>

@endisset  