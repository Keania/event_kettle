@extends('shop.partials.app',['title' => $title ])

@section('scripts')
	<script src="{{ asset('js/cm-services.js') }}"></script>
@endsection

@section('styles')
	<link rel="stylesheet" href="{{ asset('css/cm.css') }}">
	<link rel="stylesheet" href="{{ asset('css/cm-services.css') }}">
	<link rel="stylesheet" href="{{ asset('css/lightbox.min.css') }}">
@endsection

@section('container')	
	

	
	<div class="col-sm-12 col-md-9 col-lg-7 center-block">
		 {{-- echo $this->getTrail() --}}
	</div>

	<div class="container">

		<div class="row">
			
			<div id="service-page" class="col-sm-12 col-md-9 col-lg-7">

				<div class="row">
					
				<!-- service cover photo -->
					<div class="col-xs-12">
						<div id="cover-photo">
							<figure class="cover-holder">
								<img src="{{  getImage('cover_images',$service->cover_image) }}">
							</figure>
						</div>
					</div>
 
				<!-- service content -->
					<div class="col-xs-12">

						<div class="row">

							<div class="col-sm-12">
								
								<div class="col-xs-12">
									
									<!-- service title -->
									<div id="title-bar" class="clearfix">
									
										<a href="{{ route('user.profile',['username' => $service->user->username ]) }}">
											<figure class="user-thumb circle">
												<img src="{{ getUserImage($service->user->photo) }}"/>
											</figure>
										</a>

										<div>
											
											<h1 class="heading">{{  $service->label }}</h1>
											<ul class="list-inline text-muted">
													
												<li class="under-text">
													<i class="glyphicon glyphicon-grain"></i>
													{{ $service->category }}
												</li>

						                        <li class="under-text">
						                            <i class="glyphicon glyphicon-phone-alt"></i>&nbsp;
						                            {{ $service->user->phone }}
						                        </li>
											</ul>

										</div>

									</div>

									<p>
										{{ $service->description }}
									</p>

									<p>
										
										<a href="{{ route('service.category',['category' => str_slug($service->category) ]) }}">
											<button class="back-button">
												<i class="fa fa-arrow-left"></i>
											</button>
										</a>	
									</p>
								</div>
							</div>
						</div>
					</div>



					<div id="review" class="col-xs-12">
						<!-- <h1>REVIEW RATING</h1> -->
					</div>

				</div>

			</div>

			<!-- service gallery -->
			<div class="col-sm-12 col-md-4">

				<div id="service-gallery">
					
					<h5 class="title">
						<span class="small">PHOTO GALLERY -:::- Previous Works</span>
					</h5>
					
					@if (sizeof($service->photos) == 0)
						<div>
							<h3><i>User has not Image.</i></h3>
						</div>
					@endif
						
					@include('shop.services.gallery',['images' => $service->photos ])

				</div>

			</div>
		</div>
		
	</div>

	
	<script src="{{asset("js/lightbox.min.js") }}" type="text/javascript"></script>

@endsection