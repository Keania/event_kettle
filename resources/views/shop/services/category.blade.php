@extends('shop.partials.app',['title' => $title ])

@section('styles')
	<link rel="stylesheet" href="{{ asset('css/cm.css') }}">
	<link rel="stylesheet" href="{{ asset('css/cm-services.css') }}">
@endsection


@section('container')
	
	<div  id="service-side-menu" class="col-sm-12 col-md-3">
		@include('_partials.service-menu',[ 'categories' => $categories ])
	</div>

	<div class="col-sm-12 col-md-9 row">

		<div class="col-md-12">
		<div>
		     {{-- echo $this->getTrail() --}}
		</div>


		@if ( sizeof($services) > 0 )

			<h1 class="heading">{{ $serviceCat->label }}</h1>
			<hr/>
			{{-- expr --}}
			<ul class="service-list list-inline row">

	            @foreach( $services as $service )

	               @include('shop.services.card',['service' => $service ])

	            @endforeach
	        </ul>
        @else

        	<div class="col-sm-12">
        		<h2>Am Sorry!</h2>
        		<h4>
        			<i>But i couldn't find any service in 
	        		<span class="badge badge-default">{{ $serviceCat->label }}</span>
        			</i>
        		</h4>
        	</div>

		@endif
			{{ $paginate->links() }}
		</div>
	</div>


@endsection