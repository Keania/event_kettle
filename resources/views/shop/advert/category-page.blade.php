@extends('shop.partials.app', ['title' => $category->name . ' | Shop Category' ])
@inject('categories','App\Shop\Category')

@section('styles')
	<link rel="stylesheet" href="{{ asset('css/cm_home.css') }}">
@endsection

@section('container')

	<div class="container-fluid m-b-30">
        <div class="row">
            <div class="jumbotron p-b-5 m-b-5">
                <div class="container">
                    <div class="panel text-center col-md-8 col-md-offset-2" style="display: table-cell;min-height: 300px">
                        <h3>ADD A SLIDE SHOW <br/> FOR THIS GROUP <h2> HERE </h2></h3>
                    </div>
                </div>
            </div>
        </div>
		<div class="row">
			<hgroup>
				<h1 class="text-center">{{ $category->name }}</h1>
			 	<h3 class="text-center">Pick a Category for a Quick Reference</h3>
			</hgroup>
		</div>
        <div class="row">
            <ul id="category-grid" class="list-group">

                @php
                    $x = 0;
                @endphp

                @foreach( $categories::all() as $category )

                    @php

                        $slug = str_slug($category->name);
                        $link = route('shop.category',['category_name' => $slug ]);
                        $active = ( $link === request()->Path()) ? "active" : "" ;

                    @endphp

                    <a href="{{ url($link) }}">
                        <li class="category-thumb list-group-item">

                            <figure>
                                <img src="{{ asset( 'images/assets/' .$x.".svg") }}"/>
                            </figure>

                            <div class="caption">
                                {{ $category->name }}
                            </div>

                        </li>
                    </a>

                    @php
                        $x++;
                    @endphp

                @endforeach
            </ul>
        </div>
	</div>

	{{-- @include('shop.partials.sidebar') --}}
	<div class="col-sm-3">
		<div class="row">
			@include('shop.partials.sorter')
		</div>
	</div>

	<div class="col-sm-9 mt-30">
		<div class="row">
		</div>

    	<div class="row">
			<div class="col-sm-11">

		    	<div class="">

			    		@if(sizeof($adverts) > 0)

				    		{{-- @foreach( $events as $event )

				    			@include('shop.advert.card',['event' => $event ,'shape' => 'wide'])

							@endforeach --}}

			    		@else

			    			<div class='block-quote alert alert-info col-md-6 small'>
			    				<h4 class="alert-heading">Category is Empty</h4>
			    				<p><i>There is current no <b>event</b> for this category at the moment.</i></p>
			    			</div>

			    		@endif
	    		</div>
			</div>
	    </div>

		<!-- pagination -->
		<div class="row">

			<div class="col-xs-12">
				{{ $paginate->links() }}
			</div>
		</div>

    </div>
@endsection
