@inject('str', 'Illuminate\Support\Str')
@php
	
	$shape = !@$shape ? 'thumb' : $shape;
	$image = getAdvertImage($event->image);
	$link = route('event.page',['id' => $event->id , 'category' => str_slug( $event->cat->name )]);

@endphp

@if ( $shape === 'thumb')

	<li data-filter="{$this->catSlug}" class="col-xs-12 col-md-3 {$this->catSlug}">

		<div class="thumbnail thumb-box">
			<div class="slider">
				
		    <a class="image" href="{{ $link }}" data-xerox="{{ $event->id }}">
		        <img src="{{ $image }}" class="thumbnail">
		    </a>

		    <figcaption title="{{ $event->label }}">
			    <div class="ad-name">{{ $str::limit($event->label,30) }}</div>
				<div class="ad-detail">
				    <span class="price">&#x20A6; {{ number_format($event->price) }} </span>

				    <span class="pull-right">
					    <span class="text-muted small">Quantity</span>
					    <span class="price"> {{ $event->quantity }} </span>
					</span>
				</div>
		    </figcaption>
			</div>
		</div>

	</li>
@endif

@if ($shape === 'wide')

	<div class="events-preview col-xs-12">

	    <figure class="col-xs-12 col-md-3">
	        <img class="" src="{{ $image }}"/>
	    </figure>

	    <div class="col-xs-12 col-md-9">

	        <h4 class="clearfix">
                <a href="{{ $link }}">
                	{{ $event->label }}
            	</a>
                <span class="pull-right">
                	&#x20A6; {{ number_format($event->price) }}
            	</span>

            </h4>

            <hr class="row">

            <p style="display: block">
        		{{ $event->description }}
        	</p>

    	</div>
	</div>

@endif