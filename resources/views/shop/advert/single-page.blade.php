@php
    
    use Illuminate\Support\Str;

    $params = [
        'id' => $event->id ,
        'category' => str_slug( $event->cat->name )
    ];
    $a_link = route('event.page', $params);
    $a_image = getAdvertImage($event->image);
    $a_desc = Str::words($event->description,150);

@endphp

@extends('shop.partials.app',['title' => $event->label . "- " . env('APP_NAME','Ecampus Market') ])


@push('meta-data')
    <meta name="og:title" content="{{ $event->label }}"/>
    <meta name="og:url" content="{{ $a_link }}"/>
    <meta name="og:image" content="{{ $a_image }}"/>
    <meta name="og:description" content="{{ $a_desc }}">
@endpush

@section('container')

    <div class="container">
        {{-- echo $this->getTrail() --}}
        <!--        heading-->
        <div id="owner-info" class="row">
            <div class="col-xs-12">
                <ul class="list-inline">

                    <li class="user-thumb">
                        <img src="{{ getUserImage($owner->photo) }}"/>
                    </li>

                    <li>                    
                        <a id="name" href="{{ $owner->address }}"> {{ $owner->fname . ' ' . $owner->lname }} </a>
                        <br/>
                        <small>
                            <i class="glyphicon glyphicon-calendar"></i>
                            {{ $event->created->toDayDateTimeString(' h:i d M, Y') }}
                        </small>
                    </li>   
                </ul>
            </div>
        </div>

        <div id="event-card" class="row">

            <figure class="col-sm-5 col-md-5">
                <img style="width: 100%" src="{{ getAdvertImage($event->image) }}"/>
            </figure>
            
            <div class="col-sm-7 col-md-7">
                <div class="row">
                    <h4 id="title" class="col-xs-12">
                        <div class="row">

                            <div id="label" class="col-xs-12 col-sm-9">
                                {{$event->label}}
                            </div>

                            <div class="col-xs-12 col-sm-2">
                                <div id="price" class="badge">
                                    &#x20A6; {{ number_format($event->price) }}
                                </div>
                            </div>

                        </div>
                    </h4>

                    <div id="description" class="col-xs-12">
                        <p class="text-muted small">DESCRIPTION</p>
                        <p class="smaller">
                            {{ $event->description }}
                        </p>
                    </div>

                </div>
                <div class="row">
                    <br>
                    <div class="col-md-9">
                        
                    </div>
                    <div class="col-md-3 text-center">
                        <a href="{{ $owner->address }}">
                            <button class="btn btn-default">
                                See More <i class="glyphicon glyphicon-arrow-right"></i>
                            </button>
                        </a>
                    </div>
                </div>

                <div class="row">
                    <div class="fb-share-button" 
                        data-href="{{ $a_link }}" 
                        data-layout="button_count" data-size="large" data-mobile-iframe="true">
                        <a class="fb-xfbml-parse-ignore"
                            target="_blank" 
                            href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse">
                            Share</a>
                    </div>
                    <div>
                        <button id="phone">
                            <i class="fa fa-phone"></i>
                        </button>

                        <div class="badge badge-default bagde-lg">
                            {{ $owner->phone }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection