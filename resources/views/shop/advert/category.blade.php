@extends('shop.partials.app',['title' => $title ])

@section('styles')
    <link rel="stylesheet" href="{{ asset('css/cm_home.css') }}">
@endsection

@section('container')


    <div class="col-sm-12 col-md-10 col-md-offset-1">
        <h3 class="text-center">Pick a Category for a Quick Reference</h3>
        <div class="row">
            <ul id="category-grid" class="list-group">

                @php
                    $x = 0;
                @endphp

                @foreach( $categories as $category )

                    @php

                        $slug = str_slug($category->name);
                        $link = route('shop.category',['category_name' => $slug ]);
                        $active = ( $link === request()->Path()) ? "active" : "" ;

                    @endphp

                    <a href="{{ url($link) }}">
                        <li class="category-thumb list-group-item">

                            <figure>
                                <img src="{{ asset( 'images/assets/' .$x.".svg") }}"/>
                            </figure>

                            <div class="caption">
                                {{ $category->name }}
                            </div>

                        </li>
                    </a>

                    @php
                        $x++;
                    @endphp

                @endforeach

            </ul>
        </div>

        <hr width="80%">

        <div class="container-fluid">
            <h3 class="heading">TOP TRENDS</h3>
            <ul class="event-list list-inline row">

            @foreach( $adverts as $event )

                @include('market.event.card',['event' => $event, 'shape' => 'thumb' ])

            @endforeach
            </ul>
        </div>

    </div>
@endsection
