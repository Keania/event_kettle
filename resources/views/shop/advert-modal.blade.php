
<!-- event preview modal-->
<div id="event-preview">

    <div class="col-md-8 center-block">

        <div class="row">

            <div id="event-owner" class="col-md-4">

                <figure class="image thumbnail">
                    <img id="ad_image" src="images/uploads/thumbs/1.jpg"/>
                </figure>

                <div id="event-owner-info" class="mobile col-sm-12">
                    <br>
                    <div class="btn-block">
                        <a id="userLink" href="#">

                            <button class="btn btn-success center-block submit">
                                See Seller 
                                <i class="glyphicon glyphicon-eye-open"></i>
                            </button>
                        </a>
                    </div>
                    <br/>
                    <div class="text-center">
                        <small>
                            |&nbsp; <i class="glyphicon glyphicon-phone"></i>&nbsp; MOBILE &nbsp|
                        </small>
                        <br>
                        <span id="phone">{PHONE NUMBER}</span>
                    </div>

                    <div class="user-rating text-center"></div>
                </div>
            </div>

            <div class="event-info col-md-8">
                <div class="row">
                    
                    <div class="col-sm-12">
                        <div class="row">
                            
                            <h4 id="label" class="col-xs-10">
                                <b>A Brand New Brook's Shoe.</b>
                            </h4>

                            <a href="#" data-close="true">
                                <i class="col-sm-2 text-right pull-left glyphicon glyphicon-remove-circle"></i>
                            </a>

                            <hr/>
                        <div class="col-sm-10">
                            <div class="row">
                                
                                <p id="text-muted">
                                    <i class="glyphicon glyphicon-calendar"></i>
                                    <span id="time">Today</span>
                                </p>

                                <p id="desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                    A aspernatur assumenda delectus dolore
                                    doloremque ea eaque eveniet ex neque, nisi obcaecati perspiciatis quae, quas reiciendis
                                    reprehenderit sunt totam? Repellat, veniam.</p>
                                </p>
                    

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>

    </div>

</div>