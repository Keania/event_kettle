@php

    $pageTitle = "{$category->name} | WaveBlog";
    
@endphp

@extends('blog.app',['title' => $pageTitle ])

@section('content')

    <div class="col-sm-offset-1">
        
    <h1 id="main-heading"><span class="{{ $category->icon }}"></span> {{ $category->name }}</h1>
    @foreach($posts as $post)
        @include('blog.partials._post-view',['post' => $post])
    @endforeach
    </div>

    <div class="col-sm-11 col-sm-offset-1">
        {{$posts->links()}}
    </div>

@endsection
