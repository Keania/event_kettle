@php
    $author = $post->getAuthor;
    // $comments = $post->comments()->where('post',$post->id)->orderByDesc('created_at')->get();
@endphp

@extends('blog.app',['title' => @$post->label ])

@push('meta-data')
    {!! @$metaData !!}
    <link rel="canonical" href="{{ request()->url() }}"/>    
@endpush

@section('scripts')
    <script type="text/javascript" src="{{ asset('js/postManager.js') }}"></script>
@endsection

@section('aside')
    <div class="panel">
        <div class="panel-heading">
            REDONEQUIOUS
        </div>
        <div class="panel-body">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quisquam iusto recusandae expedita suscipit minima fuga libero dignissimos amet, sit officiis aliquam error sed, eos velit, obcaecati tenetur. Dolorum, est, veniam!
        </div>
    </div>
    <div class="panel event-box">
        <div class="panel-body">
            Advertise Here!
        </div>
    </div>
@endsection
@section('content')

    <!-- post description -->
    <div id="post-page" class="col-sm-12 p-b-30">

        <div class="row">

            <div class="col-sm-12">

                <div class="row">
                    
                    <div class="col-sm-12">

                        {{-- heading --}}
                        <h1>{{$post->label}}</h1>

                        <p class="post-meta">
                            <span>
                                <span class="zmdi zmdi-calendar-note"></span>
                                Published: {{ date('M j, Y', strtotime($post->created_at)) }} </span>
                            <span>
                                <span class="zmdi zmdi-edit"> </span>
                                {{ $author->firstname }} {{ $author->lastname }}</span>
                            <span>
                                <span class="zmdi zmdi-bookmark"></span> 
                                    <a href="{{ route('blog.category',['category' => str_slug($category->name) ]) }}">
                                    {{ $category->name }}
                                    </a>
                                </span>
                        </p>
                        {{-- heading end --}}

                        {{-- post starting --}}

                        <div id="post-content">
                            {{ $post->content }}
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>

    <!-- author -->
    <div class="col-xs-12">
        <div class="row">
            <div id="post-author" class="panel">
                <div class="panel-body">

                    @php
                        $disk = Storage::disk('admin_photos');
                        $picture = $disk->exists($author->picture)
                            ? $disk->url($author->picture) 
                            : asset('images/assets/idea.svg');
                    @endphp

                    <figure>
                        <img style="border-radius:30px;overflow: hidden;" src="{{ $picture }}" alt="">
                    </figure>

                    <div class="text-center">
                        <h3 id="heading">{{ $author->firstname}} {{ $author->lastname }}</h3>
                        <p id="about_self">{!! $author->about_self !!}</p>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <!-- comment form -->
    <div id="comment-field" class="col-sm-12">
        
        <div>
            <h2>What do you think ? <i class="zmdi zmdi-comments" style="color: #ddd"></i></h2>
            <h4>let us know below. </h4>
        </div>

        <div class="row">

            <div class="col-sm-12 col-md-8">

                <form id="comment-form" method="post" action="{{ route('blog.comment') }}">

                    <div class="form-group">
                        <input type="text" class="form-control" name="name" placeholder="Name"/>
                    </div>

                    <div class="form-group">
                        <input type="email" class="form-control" name="email" placeholder="Email Address"/>
                    </div>

                    <div class="form-group">
                        <textarea required="required" placeholder="Enter comment here" name = "body" class="form-control"></textarea>
                    </div>

                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input id="on_post" type="hidden" name="on_post" value="{{ $post->id }}"/>
                    <input id="post_slug" type="hidden" name="slug" value="{{ $post->slug }}"/>
                    <button name='post_comment' class="btn comment-btn"/>
                        <span class="zmdi zmdi-mail-send"></span> Post
                    </button>

                </form>

            </div>

        </div>

    </div>

    <!-- comments -->
    <div id="comments" class="col-sm-11 col-md-offset-1">


     {{--    @for($i = 0; $i < count($comments);$i++)

            <div class="comments col-sm-12">
                
                <div class="row">
                    <div class="col-sm-2">

                        <figure>
                            <img src="{{ asset('images/uploads/img1.svg') }}" alt="">
                        </figure>

                    </div>

                    <div class="col-sm-10">
                     
                        <h4>{{$comments[$i]->email}} </h4>
                        <p>
                            {{$comments[$i]->comment}}<br>
                        </p>
                        
                    </div>
                    

                </div>

            </div>

        @endfor --}}
        
        <div id="sample" class="comments col-sm-12 hide">
                    
            <div class="row">

                <div class="col-sm-2">

                    <figure>
                        <img src="{{ asset('images/uploads/img1.svg') }}" alt="">
                    </figure>

                </div>

                <div class="col-sm-10">
                 
                    <h4>${ email } </h4>
                    <p>
                        ${ comment }<br>
                    </p>

                    <button data-delete=true class="btn btn-success pull-right">
                        <i class="zmdi zmdi-delete"></i>
                    </button>
                    
                </div>
                
            </div>

        </div>
    </div>
@endsection


