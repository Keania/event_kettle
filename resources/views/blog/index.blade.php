@php

    $pageTitle = 'Home | EventKettle.com.ng ';

@endphp

@extends('blog.app',['title' => $pageTitle ])

@section('content')
    
    <div class="col-sm-offset-1">

        <h3 id="main-heading"><i class="zmdi zmdi-trending-up"></i> <b>Trending</b> Posts</h3>

        @if (empty($posts))
            <blockquote>
                <h3>Am Sorry..But we have not post at the moment.</h3>
            </blockquote>
        @endif

        @foreach($posts as $post)
            @include('blog.partials._post-view',['post' => $post])
        @endforeach
    </div>
@endsection
