<!DOCTYPE html>
<html lang="en">
  <head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    @include('blog.partials._head')

    <title>{{ $title }}</title>

    <link rel="icon" href="{{ asset('favico.ico') }}" />
    <link href="{{ asset('/css/generic.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/blog.css') }}" rel="stylesheet"/>

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>

    <!-- Fonts -->
    {{-- <link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'> --}}
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->


  </head>

  <body>

    @include('blog.partials._nav')

    <div id="main-container" class="m-top-80 m-b-30 container">

        @if (Session::has('message'))
            <div class="flash alert-info">
            <p class="panel-body">
              {{ Session::get('message') }}
            </p>
            </div>
        @endif

        @if ($errors->any())
            <div class='flash alert-danger'>
                <ul class="panel-body">

                    @foreach ( $errors->all() as $error )
                        <li> {{ $error }} </li>
                    @endforeach

                </ul>
            </div>
        @endif

        <div class="row">

            <div class="col-sm-8">

              <div class="row">

                @yield('content')

              </div>

            </div>

            <div class="col-sm-12 col-md-4">
                <div class="container-fluid m-top-30">
                    @yield('aside')
                </div>
            </div>

        </div>

    </div>

    @include('blog.partials._footer')

    <script src="{{ asset('js/app.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/blog.js') }}" type="text/javascript"></script>

    @yield('stylesheet')
    {{-- <script id="dsq-count-scr" src="//wavetech.disqus.com/count.js" async></script> --}}

    <!-- add the disqus_thread to the end of a link
    <a href="http://foo.com/bar.html#disqus_thread">Link</a> -->
    @yield('scripts')

  </body>
</html>
