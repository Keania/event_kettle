@php
    use Illuminate\Support\Str;
    use Illuminate\Support\HtmlString;
    use App\Http\Controllers\Blog\PostController as Post;

    $disk = Storage::disk("postUploads");
    $postTitle   = $post->label;
    $authorName  = $post->getAuthor->firstname . " " . $post->getAuthor->lastname ;
    $postContent = Str::words(Post::shortDesc($post->content),30);
    $postPhoto   = $disk->exists( @$post->images->first()->name )
        ? $disk->url($post->images->first()->name) 
        : asset('images/assets/idea.svg');
    $link = route('blog') . "/" . str_slug($post->category->name);
    $postLink    = url("{$link}/{$post->slug}" );
    $postTime    = date('M j, Y ',strtotime($post->created_at));

@endphp

<div class="post-box col-sm-11 p-10 m-bottom-20">

    <div class="row">

        <div class="col-sm-12 col-md-3">
            <figure class="img-holder">
                <img class="img-thumbnail" src="{{ $postPhoto }}" alt="{{ $postTitle }} Main Photo"/>
            </figure>
        </div>

        <div class="col-sm-12 col-md-9">

            <h3 class="heading">
                <a href="{{ $postLink }}">
                    {{ $postTitle }}
                </a>
            </h3>

            <div class="text-muted small">
                <span class="zmdi zmdi-account"></span> {{ $authorName }} &nbsp;
                <span class="zmdi zmdi-calendar-note"></span>
                {{ $postTime }}
            </div>
            <p class="short-content">{{ $postContent }}</p>

        </div>

    </div>

</div>