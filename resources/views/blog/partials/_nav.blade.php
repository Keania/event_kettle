@php
	use Illuminate\Support\Str;
@endphp

@inject('cat', 'App\Blog\Category')

<nav class="nav navbar navbar-fixed-top navbar-default">

   <nav id="sub-nav" class="row">

		<div class="container">
			
			<div id="cat-menu" class="collapse navbar-collapse">

				<ul class="nav nav-pills">
					<li class="{{ request()->fullUrl() === route('blog.home') ? 'active' : null }}">
						<a href="{{ route('blog.home') }}">
							<i class="zmdi zmdi-home">
							</i>
						</a>
					</li>

					@foreach($cat::orderBy('position','ASC')->get(['icon','name']) as $category)
					  @php
						
						$catName = str_slug($category->name);
						$path =  route('blog.category',['category' => str_slug($catName) ]);
						$active = request()->is("blog/".str_slug($catName)."/*") 
						|| request()->path() === "blog/".str_slug($catName) ? 'active' : null ;

					  @endphp
						<li class="{{ $active }}">
							<a href="{{ $path }}">
								<i class="{{ $category->icon }}"></i>
								<span class="visible-md-inline visible-lg-inline"> {{$category->name}} </span>
							</a>
						</li>
					@endforeach

					<li>
						<a href="{{ route('blog') . "/popular" }}">Popular</a>
					</li>
				</ul>
			</div>
				
		</div>

	</nav>

	<div class="row">
		
		<div class="container">

			<div class="navbar-header">

			  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#cat-menu">
					<span class="sr-only">Toggle Navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
			  </button>

			<div class="navbar-brand">
				
				  <a href="{{ route('events.home') }}">
				  </a>
			</div>

			</div>

			<div class="collapse navbar-collapse" id="menu">
			   
			  <ul class="nav navbar-nav navbar-right">
				@if (Auth::guest())
					<li>
						<a href="{{ route('events.home') }}">
							<i class="zmdi zmdi-bookmark"></i>
							Events</a>
					</li>
					<li>
						<a href="#">
							<i class="zmdi zmdi-shopping-cart"></i>
							Shop</a>
					</li>
				@else

					<li>
					  <a href="#">{{ Auth::user()->name }} <span class="caret"></span></a>
					</li>

				@endif
			  </ul>
			</div>

		</div>

	</div>


</nav>