<div id="footer">
	
	<div id="back-2-top" @click="slideUp">
		<i class="zmdi zmdi-chevron-up"></i>
	</div>

	<div class="container">
		<div class="row">
			
			<div class="col-sm-12 col-md-11">
				<ul class="list-inline">
					
					<li class="col-xs-12 col-md-2">			
						<span id="top"> Top</span> 
						<span>
							<i class="zmdi zmdi-chevron-up"></i>
						</span>
					</li>

					<li class="">
						<a href="{{ route('events.home') }}">Events</a>
					</li>

					<li class="">
						<a href="{{ '#' }}">Advertise</a>
					</li>

					<li class="">
						<a href="{{ route('blog.home') }}">Blog</a>
					</li>

					<li class="">
						<a href="#">Terms</a>
					</li>
				</ul>
				
				<small id="copyright">
					copyright &copy; {{ date('Y') }} - eventkettle.com
				</small>
			</div>
		
			<div class="col-md-1 pull-right">
				<img src="{{ asset('images/assets/footer-logo.svg') }}" style="width:40px;height: 40px;" alt="">
			</div>
		</div>
	</div>
</div>