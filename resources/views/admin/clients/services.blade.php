<?xml version="1.0" encoding="UTF-8"?>

<user>
	<name>{{ $fullname }}</name>

	<image>{{ 
		Storage::disk("user_images")->exists($profilePic) 
		? Storage::disk("user_images")->url($profilePic)
		: asset('images/assets/user.svg')
	 }} 
	 </image>

	<services>
		@foreach ($services as $service)
			
			<service>
				{{ $service->label }}
			</service>

		@endforeach
	</services>

</user>