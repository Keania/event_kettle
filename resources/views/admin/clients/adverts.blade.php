<?xml version='1.0' encoding='UTF-8'?>
<user>

	<name>{{ $user->fname }} {{ $user->lname}}</name>
	<username>{{ $user->username}}</username>

	<profilePic>{{ 
		Storage::disk("user_images")->exists($user->photo)
		? Storage::disk("user_images")->url($user->photo)
		: asset("images/assets/user.svg")
	 }} </profilePic>

	<events>
		@foreach ($user->events as $event)
			<event data-xerox="{{ $event->id }}">
				<label>{{ $event->label }}</label>
				<price>{{ $event->price }}</price>
				<img href="{{ Storage::disk('event_images')->url($event->image) }}"/>
				<description>{{ $event->description }}</description>
			</event>
		@endforeach
	</events>
	
</user>