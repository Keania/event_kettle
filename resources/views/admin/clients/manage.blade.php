<div class="col-sm-12">

    <figure class="thumbnail col-sm-4">
        <image  src="{{ getImage('user_images', $user->photo) }}"/>
    </figure>

    <div id="details-list" class="col-sm-8 list-group">

        <li class="list-group-item" id="name">
            <h4>
                {{ fullName($user) }}
            </h4>
        </li>

        <li class="list-group-item">
                <i class="fa fa-at"></i>
                {{ $user->email }}
        </li>
        <li class="list-group-item">
                <i class="fa fa-hashtag"></i>
                {{ $user->username }}
        </li>
        <li class="list-group-item">
                <i class="fa fa-phone"></i>
                {{ $user->phone }}
        </li>
    </div>
</div>

<div class="col-sm-12">

    <div id="user-partials" class="pull-left col-sm-6">
        <small>
            <small>Account Type: <b>{{ $user->AT }}</b></small> |
            <small>Created: <b>{{ $user->created->toDateTimeString() }}</b></small>
        </small>
    </div>

    <div id="user-analytics" class="pull-right text-right col-sm-5">
        <small id="tpu">
            TPU: 
            <b>{{ sizeof($user->events) + sizeof($user->services) }}</b>
            &nbsp;&nbsp;
        </small>
        <small>
            NoS: <b>{{ sizeof($user->services) }}</b>
        </small>
        &nbsp;&nbsp;
        <small>NoA: <b>{{ sizeof($user->events) }}</b></small>
    </div>

</div>