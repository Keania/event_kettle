
@php
    use Illuminate\Support\Facades\DB;

    $serviceCount = DB::table('service_record')->count();
    $eventCount = DB::table('events')->count();
    $clientCount = DB::table('users')->count();
    
@endphp

@extends('admin.base',['title' => 'ECAMPUS DASHBOARD'])

@section('content')
    {{-- expr --}}

    <div id="dashbox" class="panel">
        <h3 class="heading">DASHBOARD</h3>
        <div class="clearfix"> 
            <div class="card-list animate">
                <div class="counter">
                    {{ $eventCount }}
                </div>

                <div class="other">
                    ADVERTS UPLOADED
                </div>
            </div>

            <div class="card-list">
                <div class="counter">
                    {{ $serviceCount }}
                </div>
                <div class="other">
                    No. of Services
                </div>
            </div>

            <div class="card-list">
                <div class="counter">
                    {{ $clientCount }}
                </div>
                <div class="other">
                    NO. of Users
                </div>
            </div>
        </div>

        <h3 class="heading">&nbsp;&nbsp;ANALYTICS</h3>
        <div class="clearfix">
            <div class="card-list">
                coming soon...
            </div>
        </div>
    </div>
    
@endsection
