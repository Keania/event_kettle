
@inject('category', 'App\Shop\Category')

@extends('admin.base',['title' => 'Category Manager :: Admin '])

@section('content')
<div class="row">
    
    <div class="col-sm-12">

        <h3 class="heading">Category Manager</h3>

    </div>

    <div class="col-sm-12"><br/></div>

    <div class="col-md-4">
        <div class="panel">
            
            <div class="panel-heading panel-default">Insert<b>Category</b></div>

            <form class="quick-form panel-body" method="post" action="{{ route('admin.event-add') }}">
                
                <div class="form-group">
                    <label>Name</label><br/>
                    <input name="cat_name" class="form-control" type="text" placeholder="eg. Laptops, Phones" required/>
                </div>
                
                <div class="form-group">
                    <label>Icon</label>
                    <input name="icon_name" class="form-control" type="text" placeholder="Icon"/>
                </div>

                <div class="form-group">
                    
                    <label>Parent</label>
                    <div class="form-group">
                        {{ $category::selection('form-control') }}
                        <div class="under-text small">select a parent category for this category to be under.</div>
                    </div>

                </div>

                <input name="add_category" class="button" type="submit" value="add category"/>
            </form>
        </div>
    </div> 

    <div id="manage-categories" class="col-md-4">
        <div class="panel">
            
            <div class="panel-heading">Manage<b>Categories</b></div>

            <div class="alert-info"> Click to Modify Entry... </div>

            <ul class="list-group">

                @foreach( $category::all() as $category )
                       
                       <li data-pos="{{ $category->position }}" data-xerox="{{ $category->cat_id}}" class="row list-group-item">
                            <div class="col-sm-8 text-nowrap">

                                {!! icon($category->icon) !!}
                                {{ $category->name }}

                            </div>

                            <div class="col-sm-1 pull-right">
                                <button data-del>
                                    <i class="fa fa-trash-o"></i>
                                </button>
                            </div>
                        </li>

                        <div class="list-group-item collapse">
                            <input data-name type="text" class="col-xs-12" 
                                value="{{ $category->name }}"/>
                            <input data-icon type="text" class="col-xs-9" 
                                value="{{ $category->icon }}"/>
                            <input data-pos type="number" class="col-xs-2 col-xs-offset-1" 
                                value="{{ $category->position }}"/>
                        </div>
                @endforeach
            </ul>

            <div class="panel-footer text-right">
                <span> Categories  <b class="badge">{{ sizeof($category::all()) }}</b> </span>
            </div>

        </div>
    </div>

</div>
@endsection