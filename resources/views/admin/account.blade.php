@php
/**
 * Created by PhpStorm.
 * User: Joseph
 * Date: 30/12/2016
 * Time: 19:07
 */
@endphp

@extends('admin.base',['title' => 'Account Settings'])

@section('content')
    
       
    <div class="col-xs-12">
        <h3>
            <i class="glyphicon glyphicon-cog"></i> General Settings
        </h3>
    </div> 

    <div class="col-sm-6 col-md-4">
        <div class="row">
            <div class="panel">
                <div class="panel-heading">
                    <span class="panel-title"><i class="glyphicon glyphicon-key"></i> Change<b> Password</b></span>
                </div>
                 <div class="panel-body">
                    <form method="post" action="{{ route('admin.account.update') }}">

                        @if (sizeof($errors) > 0)

                            <div class="alert alert-danger">
                                Some Errors Occurred.
                            </div>
                        @else
                            <div class="alert alert-info">
                                @if (session('passMessage'))
                                    {{ session('passMessage') }}
                                @else
                                    fill fields to change password.
                                @endif
                            </div>
                        @endif
                        
                        <div class="form-group">

                            <label for="">Old Password</label>
                            <input class="form-control" type="password" name="old_password" placeholder="Old Password" required>
                            @if ($errors->has('old_password'))
                                <span class="text-danger">{{ $errors->first('old_password') }}</span>
                            @endif

                        </div> 

                        <div class="form-group">
                            <label for="">New Password</label>
                            <input class="form-control" type="password" name="password" placeholder="New Password" required>
                            @if ($errors->has('password'))
                                <span class="text-danger">{{ $errors->first('password') }}</span>
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="">Verify Password</label>
                            <input class="form-control" type="password" name="password_confirmation" placeholder="Verify Password" required>
                            @if ($errors->has('password_confirmation'))
                                <span class="text-danger">{{ $errors->first('password_confirmation') }}</span>
                            @endif
                        </div>

                        <div class="form-group">
                            <input type="hidden" value="PUT" name="_method"/>
                            {{ csrf_field() }}
                            <button class="btn btn-default">
                                <i class="glyphicon glyphicon-save"></i> Save
                            </button>

                        </div>
                    </form>
                </div>
            
            </div>
        </div>
    </div>

    <div id="account__profile" class="col-sm-6 col-md-4 pull-right">
        <div class="panel">
            
            <div class="panel-heading panel-default">
                <span class="panel-title"><b>Account</b> Information</span>
            </div>

            <ul class="list-group">
                <li class="list-group-item">ID Number: #ECM-{{ $admin->id }} </li>
                <li class="list-group-item">Full Name : {{ fullName($admin) }} </li>
                <li class="list-group-item">Username : {{ ($admin->username) }} </li>
                <li class="list-group-item">IP Address: {{ request()->ip() }} </li>
            </ul>

        </div>
    </div>


@endsection
