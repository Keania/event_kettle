@extends('admin.base',['title' => 'User Interface'])

@section('content')
    <div>
        
        <div id="main-bar">
            <client-search list ="#users-list"></client-search>
        </div>

        <!-- users list -->
        @php         
            $list = [];
            $count = 1;
            $users = DB::table('users')->select('id','lname','fname')->orderBy('id')->paginate(5);
        @endphp

        <div is="UsersPanel" :users-list="{{ $users->toJson() }}">
            {{ $users->links() }}
        </div>     

        <!--info box-->
        <div class="col-sm-12 col-md-8">

            lv:if  

            <!-- user preview -->
            <div id="user-panel" class="collapse col-sm-12">
                <div class="row">
                    
                    <div class="panel panel-collapse">
                        
                        <h5 class="panel-heading panel-default">User<b>Information</b></h5>
                        <div class="panel-body">
                            <div id="main-info">
                                
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <!--for user services option-->
            <div id="user-services" class="panel col-sm-12 collapse">
                <div class="panel-heading"></div>
                <div class="panel-body row">
                    <div class="col-sm-4">
                        <figure class="thumbnail">
                            <img/>
                        </figure>
                    </div>

                    <div class="col-sm-7">
                        <div class="text-muted">name</div>
                        <h4 class="name-title"></h4>
                        <hr/>
                        <h5 class="service-title text-muted">services</h5>
                        <ul class="service-list list-group">
                        </ul>
                    </div>
                </div>
            </div>

            <!--for users events display-->
            <div v-if="canShowAdvert" :user="records" :events="records.events" is="AdvertPanel" id="user-events">
                <div class="row">
                    
                    <div class="panel">

                        <div class="panel-heading panel-default clearfix">
                            <figure class="thumbnail col-sm-1">
                                <img src=""/>
                            </figure>
                            <div class="col-sm-8 text-left">
                                <b><span class="name-title">Joseph Julius</span></b>
                            </div>
                            <div class="col-sm-3">
                                <span>ADVERTS: <span id="NOA"> <i class="fa fa-spinner fa-spin"></i> </span>
                            </div>
                        </div>

                        <div class="panel-body row">
                            <ul id="events-list" class="list-group">

                            </ul>
                        </div>

                    </div>
                </div>
            </div>
        </div>
            
    </div>

@endsection