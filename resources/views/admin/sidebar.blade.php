@php

  use App\Helpers\navigation as Nav;

@endphp
	<!--Sidebar Menu -->
	<ul>
	   @php
	   $userRoutes = (!request()->is('cm-admin/clients')) ? : ['Adverts','Services','Client Profile'];

	   $navMenu = [
		   'Dashboard' => ['admin.dashboard','fa-dashboard'],
		   'Services' => ['admin.services','glyphicon-list'],
		   'Categories' => ['admin.category','glyphicon-list-alt'],
		   'Users' => ['admin.users','fa-users', $userRoutes],
		   'Account Setting' => ['admin.account','glyphicon-user'],
	   ];

	   //adds the admin navigation
	   //to admin with the right privilege
	   if( Auth::guard('admin')->user()->privilege == 3 )
	   {
		   $navMenu['Admin'] = ['admin.manager','glyphicon-wrench',['Create','Manage']];
	   }

	   foreach($navMenu as $name => $props):

		   $min = @$props[2];
		   $props[0] = $link = route($props[0]);

		   print Nav::layout($name,$props);
		   if(is_array($min) && isset($min)):

			   print '<div><ul>';

			   foreach($min as $key => $name):
					$href = $link.'/'.$name;
					echo Nav::layout($name,[$href,'']);
			   endforeach;
			  

			   print '</ul></div>';

		   endif;

	   endforeach;

	   @endphp
	   
		<li>
			<a href="" onclick="event.preventDefault();document.getElementById('logout').submit()">
			  <span class="glyphicon glyphicon-log-out"></span> Logout</a>
			</li>
			<form id="logout" method="post" action="{{ route('admin.logout') }}">
				{{ csrf_field() }}
			</form>
		</li>
	</ul>
