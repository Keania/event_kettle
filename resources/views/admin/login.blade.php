@extends('_partials.base',['title' => 'Admin :: Login '])

@section('styles')
	<link rel="stylesheet" href="{{ asset('css/cm-register.css') }}">
@endsection

@section('container')

	 <div class="mt-30 col-md-4 col-md-offset-4">

			<div class="panel">
				<div class="panel-heading">
					<h1 class="panel-title">Admin Sign In</h1>
				</div>
					
				<div class="panel-body">
						
					<form action="{{ route('admin.login') }}" method="post" >

						@if (sizeof($errors) > 0)
							<div class="alert alert-danger">
								{{ $errors->first('username') }}
								{{ $errors->first('password') }}
								{{ $errors->first('privilege') }}
							</div>

						@else
							<div class="alert alert-info">this Login is made for just administrators</div>
						@endif

					    <div class="form-group">
					    	<div class="input-group">
						        <span class="input-group-addon"><i class="glyphicon glyphicon-knight"></i></span>
						        <input class="form-control" autofocus type="text" value="{{ old('username') }}" name="username" placeholder="Username..."/>
					    	</div>
					    </div>

					    <div class="form-group">
					    	<div class="input-group">
						        <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
						        <input  class="form-control" type="password" name="password" placeholder="Password..."/>
					    	</div>
					    </div>

						{{ csrf_field() }}
						<div class="form-group">
							<button class="btn btn-default" name="submit">login</button>
						</div>
						
					</form>

				</div>

			</div>
	 </div>

@endsection