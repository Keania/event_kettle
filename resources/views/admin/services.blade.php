@php

/**
 * Created by PhpStorm.
 * User: Joseph
 * Date: 01/01/2017
 * Time: 03:23
    
 */
@endphp
@inject('services', 'App\Shop\ServiceCategory')

@extends('admin.base',['title' => 'Services Manager :: Admin'])

@section('content')

    <div class="row">
        <div class="col-sm-12">
            <h3 class="heading">Service Manager</h3>
        </div>
    </div>
    <br>

    <div class="row">

        <div id="service-list" class="col-sm-10 col-md-4">

            <div class="panel">

                <div class="panel-heading">
                    <i class="glyphicon glyphicon-send"></i>
                    All Services
                </div>
                <div class="alert-info text-center">click the <b>dash</b> for a Service to delete</div>
                <ol class="list-group">
                                 
                    @foreach( $services::all() as $service)

                                
                        <li data-xerox='{{ $service->id }}' class='list-group-item'>
                            <span>{{ $service->label }}</span>
                            <small class='pull-right '>
                                <a href='#{{ $service->id }}'><i class='glyphicon glyphicon-minus'></i></a>
                            </small>

                        </li>
                        <form method="post" class="list-group-item collapse hide">

                            <div>
                                <input type="text" class="form-control" value="{{ $service->label }}" 
                                    name="service_name" maxlength="20" placeholder="Label.."/>
                                <textarea rows="5" name="service_desc" class="form-control" 
                                    placeholder="Description">{{ $service->description }}</textarea>
                            </div>
                            
                            {{ csrf_field() }}
                            <input type="hidden" name="serviceXerox" value="{{ $service->id }}"/>

                            <div class="btn-group-xs">
                                <button class="btn" name="update_service">
                                    <i class="glyphicon glyphicon-save"></i> Save</button>
                                <button class="btn" name="delete_service">
                                    <i class="glyphicon glyphicon-trash"></i> Delete</button>
                            </div>

                        </form>
                    @endforeach
                </ol>

                <div class="panel-footer text-right">
                    NOA - #{{ sizeof($service::all()) }}
                </div>

            </div>
        </div>

        <div id="add-service" class="col-md-4 col-sm-10">

            <div class="panel">

                <form method="post" class="quick-form" action="{{ route('admin.services.create') }}" >
                        
                    <div class="panel-heading">Add a Service</div>
                    <div class="panel-body">

                        @if (sizeof($errors) > 0)
                            <div class="alert alert-danger"> {{ $errors->first('error') }} </div>
                        @else
                            <div class="alert-info">fill the form to add a <b>Service</b> </div>
                        @endif 

                        <div class="form-group">
                            <input type="text" class="form-control" name="service_name" maxlength="20" placeholder="Label.."/>
                            <div class="small text-muted">e.g Painter, Artist, Graphic Desiginer, Web Designer....</div>
                        </div>

                        <div class="form-group">
                            <textarea rows="5" name="service_desc" class="form-control" placeholder="Description"></textarea>
                        </div>

                        {{ csrf_field() }}
                        <input type="submit" name="add_service" value="Add Service"/>
                    </div>
                    
                </form>
            </div>
            
        </div>

    </div>

@endsection
