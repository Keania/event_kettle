<div>
	@if ($errors->has('updateError'))
		<div class="alert alert-danger">
			{{$errors->first('updateError')}}
		</div>
	@else
		
	@endif

	<div 
		is="AdminForm" 
		admin_id="{{ $admin->id }}"
		username="{{ $admin->username }}"
		action="{{ route('admin.manager.update') }}"
		>
	</div>
	{{-- <div class="quick-form">
		<form method="post" @submit.prevent="" action="{{ route('admin.manager.update') }}">
			<div class="form-group">
				<label>Username</label>
				<input type="text" name="username" placeholder="enter username" class="form-control" value="{{ $admin->username }}"/>
				@if ($errors->has('username'))
					<span class="text-danger">{{ $errors->first('username') }}</span>
				@endif
			</div>

			<div class="form-group">		
				<label>Password</label>
				<input type="text" name="password" class="form-control" placeholder="enter password..." value=""/>
				@if ($errors->has('password'))
					<span class="text-danger">{{ $errors->first('password') }}</span>
				@endif
			</div>

			<div class="form-group">
				<label>Grant Privilege</label>
				<select id="priv" name="privilege" class="form-control">
	                <option value="3">Main</option>
	                <option value="2">Categories</option>
	                <option value="1">Analytics</option>
	                <option value="0">disabled</option>
	            </select>
	            @if ($errors->has('privilege'))
					<span class="text-danger">{{ $errors->first('privilege') }}</span>
				@endif
			</div>

			<div class="form-group">
				<input type="hidden" value="{{ $admin->id }}" name="xerox"/>
				<input type="hidden" value="PUT" name="_method">
				{{csrf_field()}}
				<input type="submit" name="update-admin" class="btn btn-default" value="update"/>
			</div>

		</form>
	</div> --}}
</div>