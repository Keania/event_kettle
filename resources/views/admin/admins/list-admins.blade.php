
@push('admin-details')

    @if (!is_null($selected))

        <div class="panel">
            
            <div class="panel-heading panel-default">
                <div class="panel-title">
                    
                    <i class="fa fa-user-md"></i> <b>Admin</b>Information

                </div>
            </div>
            
            <div id="preview-panel" class="panel-body">
                @include('admin.admins.preview-admin', ['admin' => $selected])
            </div>
        </div>

    @endif

@endpush


@if (request()->ajax())

    @stack('admin-details')
    @php
        die();
    @endphp

@elseif(request()->ajax() === false)

    @extends('admin.base',['title' => 'Manage Administrator - Admin '])

    @section('content')

        <div id="manage-admins" class="col-xs-12">
            
            <div class="row">
                <div class="col-sm-12">
                    <h3 class="heading">Manage Administrators</h3>                 
                </div>
            </div>

            <div class="row">

                <div id="manage-panel" class="col-md-5">
                    <div class="panel">
                        
                        <div class="panel-heading panel-default">
                            <i class="fa fa-users"></i> <b>Administators</b>
                        </div>

                        <div class="panel-body">

                            @if (session('message') || session('error'))
                                <div class="alert alert-{{ session('message') ? "success" : "danger" }}">
                                    {{ session('message') }}                                
                                    {{ session('error') }}                                
                                </div>
                            @else
                                <div class="alert alert-info">
                                    click to edit 
                                </div>
                            @endif
                        
                            <div class="list-group">

                                @foreach( $admins as $admin )

                                    <div is="adminlist"
                                        fullname="{{ fullName($admin) }}" 
                                        link-address="{{ route('admin.manager.get-admin',['id' => $admin->id]) }}" 
                                        admin_id="{{ $admin->id }}"
                                        privilege="{{ $admin->privilege }}">
                                    </div>

                                @endforeach

                            </div>
                        </div>

                    </div>
                </div>
            
                <div id="preview-column" class="col-md-4">

                        @stack('admin-details');
                </div>

            </div>
        </div>

    @endsection

@endif