@php


@endphp


@extends("admin.base",['title' => 'Create and Administrator - Admin '])


@section('content')

    <h4 class="heading">Add a new Administrator.</h4>

    {{-- create administrator's form --}}
    <div class="col-md-5 clearfix">
        <div class="panel">

            <div class="panel-heading panel-default">
                <i class="fa fa-user"></i> ADD<b>Administrator</b>
            </div>

            <form method="post" action="{{ route('admin.manager.create') }}" class="panel-body">
                @if (session('message'))
                    <div class="alert {{ session('message') ? "alert-success" : "alert-danger" }}">
                        {{ session('message') }}
                    </div>
                @endif
                <div class="form-group">
                    <label>First Name</label>
                    <input type="text" name="firstname" class="form-control" placeholder="First Name"/>
                    @if ($errors->has('firstname'))
                        <span class="text-danger">{{ $errors->first('firstname') }}</span>
                    @endif
                </div>
                
                <div class="form-group">
                    <label>Last Name</label>
                    <input type="text" name="lastname" class="form-control" placeholder="Last Name"/>
                    @if ($errors->has('lastname'))
                        <span class="text-danger">{{ $errors->first('lastname') }}</span>
                    @endif
                </div>

                <div class="form-group">
                    <label>Username</label>
                    <input type="text" name="username" class="form-control" placeholder="username.."/>
                    @if ($errors->has('username'))
                        <span class="text-danger">{{ $errors->first('username') }}</span>
                    @endif
                </div>

                <div class="form-group">

                    <label>Password</label>
                    <input type="password" name="password" class="form-control" placeholder="password.."/>
                    @if ($errors->has('password'))
                        <span class="text-danger">{{ $errors->first('password') }}</span>
                    @endif
                </div>

                <div class="form-group">
                    
                    <label for="priv">Privilege</label>
                    <select id="priv" name="privilege" class="form-control">
                        <option value="3">Main</option>
                        <option value="2">Categories</option>
                        <option value="1">Analytics</option>
                    </select>
                    @if ($errors->has('privilege'))
                        <span class="text-danger">{{ $errors->first('privilege') }}</span>
                    @endif

                </div>
                {{ csrf_field() }}
                <input class="btn btn-default" name="add-admin" type="submit" value="Add User" />

            </form>
        </div>
    </div>

    {{-- processed inforation preview --}}
    <div class="col-md-6">
        <div class="panel">
            
            <div class="panel-heading panel-default">
                <i class="fa fa-eye"></i> <b>Preview</b>User
            </div>

            <div class="panel-body">
                
                <figure class="col-sm-4">
                    <img class="thumbnail" 
                        src="{{}}" 
                        alt="profile picture"
                        style="width:100%; object-fit: cover;" />
                </figure>
                <div class="col-sm-12 clearfix">
                    <div class="row">
                        <div id="info" class="list-group">
                            <li class="list-group-item">Username: </li>
                            <li class="list-group-item">Privilege: </li>
                        </div>
                    </div>

                    <div class="row">
                        <button class="btn btn-default">
                            Upload <i class="fa fa-paper-plane"></i>
                        </button>
                    </div>
                </div>

            </div>

        </div>
    </div>

@endsection