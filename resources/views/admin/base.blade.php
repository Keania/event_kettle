<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
	    <meta charset="utf-16">
	    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	    
	    <title>{{ @$title }}</title>
        <link href="{{ asset('css/app.css') }}" rel="stylesheet"/>
        <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">
        <link href="{{ asset('css/cm-admin.css') }}" rel="stylesheet"/>
        <link rel="stylesheet" href="{{ asset('css/animate-css/animate.min.css') }}">
        @yield('styles')

        <script>
            window.Laravel = {
                'csrfToken' : "{!! csrf_token() !!}",
                'state' : "cube",
            } 
        </script>
        <script src="{{ asset("js/jquery.min.js") }}"  type="text/javascript"></script>
        <script src="{{ asset("js/component.js") }}" type="text/javascript"></script>
        <script defer src="{{ asset("js/app.js") }}"  type="text/javascript"></script>
        <script defer src="{{ asset("js/cm-admin.js") }}" type="text/javascript"></script>
        @yield('scripts') 
        @yield('js')
    </head>

    <body>

        {{-- header --}}
        <nav class="navbar navbar-default"style="margin-bottom: 0">
            <div class="container">

                <div class="navbar-header">
                    <span class="navbar-brand" href="#">
                        Admin
                    </span>
                </div>

                <div>
                    <ul class="nav navbar-nav navbar-right">
                        
                        @php

                            $links = [
                                Auth::user()->first_name => [ route('admin.account') , 'glyphicon-user'],
                            ];
                            
                            foreach($links as $navName => $navInfo):
                                echo App\Helpers\Navigation::layout($navName,$navInfo);
                            endforeach;

                        @endphp

                    </ul>
                </div>
            </div>
        </nav>

        {{-- main body --}}
        <main id="main-content" class="container-fluid">

            <div class="row">
                    
                <nav id="main-nav" class="col-md-2">

                    @include('admin.sidebar')
                    
                </nav>

                <!-- Dynamic Content -->
                <div id="pt-container" class="col-md-10">
                    
                    @yield('content')

                </div>

            </div>

        </main>
        
        {{-- footer --}}
        @include('admin.footer')

    </body>
    <script src="{{ asset('js/admin.js') }}"></script>
</html>