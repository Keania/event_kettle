
@extends('_partials.base', ['title' => "404 - Page Not Found."])
@section('styles')
@endsection

@section('container')

<main class="row col-sm-12">
    <div class="container">
        
        <div class="panel">

            <div class="panel-body">
                
            <div class="col-sm-11 col-md-6 col-md-offset-3">

                <h2 class="text-center">404 eRROR - pAGE NOT fOUND</h1>
                
                <hr/>

                <p>
                    Please check your url. This page link is broken or
                    has been moved to another part of this website.
                    Go back to the <a href="{{ route('shop.home') }}">HomePage</a>.
                </p>
            </div>
            </div>

        </div>

    </div>
</main>

@endsection