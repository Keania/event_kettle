<?php
// blog routes
// for blog management
Route::group(
    [
     'namespace' => 'Blog',
     'prefix'    => 'blog',
    ],
    function () {
        Route::get('/', 'IndexController@index')->name('blog');
        Route::get('home', 'IndexController@index')->name('blog.home');
        Route::get('{category}', 'CategoryController@index')->where('category', '[A-Za-z_-]+')->name('blog.category');
        Route::get('{category}/{post_slug}', 'PostController@index')->name('blog.post');
        Route::get('web', 'PostController@index');

        // comments controller
        Route::delete('comment/add', 'CommentController@delete');
        Route::post('comment/add', 'CommentController@add')->name('blog.comment');
    }
);

// shop routes
Route::group(
    [
     'namespace' => 'Shop',
     'prefix'    => 'shop',
    ],
    function () {
        Route::get('/', 'Welcome@index')->name('shop.welcome');
        Route::get('/home', 'Home@index')->name('shop.home');
        Route::get('/terms-and-conditions', 'Home@terms')->name('shop.terms');

        Route::group(
            ['prefix' => 'services'],
            function () {
                Route::get('/', 'Services@index')->name('shop.services');
                Route::get('{category}', 'Services@category')->where(['category' => '[A-Za-z0-9-_0]+' ])->name('service.category');
                Route::get('{category}/{service_id}', 'Services@single')->name('service.single');
            }
        );
        Route::group(
            ['prefix' => 'category'],
            function () {
                Route::get('/', 'Advert@category')->name('shop.categories');
                Route::get('/{category_name}', 'Advert@single')->where(['category_name' => '[A-Za-z0-9-_]+'])->name('shop.category');

                Route::get('/{category_name}/{id}', 'Advert@show')->where('id', '\d+')->name('show.advert.page');
            }
        );

        Route::get('/cm-seller/{username}', 'Profile@index')->name('user.profile');
    }
);

// events route
Route::group(
    ['namespace' => 'Events'],
    function () {
        Route::get('/', 'EventHome@index');
        Route::get('/home', 'EventHome@index')->name('events.home');

        Route::get('/category', 'EventHome@category')->name('event.category');

        Route::get('/category/{category_name}', 'EventHome@single')->where(['category_name' => '[A-Za-z0-9-_]+'])->name('event.category');

        Route::get('/category/{category_name}/{id}', 'EventHome@show')->where('id', '\d+')->name('event.page');
    }
);

// users routes
Route::group(
    [
     'namespace'  => 'Clients',
     'middleware' => 'auth',
     'prefix'     => 'cm-user',
    ],
    function () {
        Route::post('/events/images','EventsController@images')->name('event.images');
        Route::get('/view-events','EventsController@index');
        Route::get('/events', 'EventsController@show')->name('client.events');
        Route::get('/services', 'Services@show')->name('client.services');
        Route::get('/dashboard', 'Dashboard@show')->name('client.dashboard');

        Route::resource('merchandise', 'MerchandiseController');
        Route::post('/events/add', 'EventsController@add')->name('event.add');
        Route::post('/events/delete/{id}', 'EventsController@delete')->name('event.delete');
        Route::post('/events/update', 'EventsController@update')->name('event.update');
        Route::get('/events/{id}', 'EventsController@ajax')->where(['id' => '\d+' ])->name('get-event');

        Route::get('/services/{id}', 'Services@ajaxForm')->where(['id' => '\d+' ]);
        Route::post('/services/add', 'Services@add')->name('service.add');
        Route::put('/services/add', 'Services@edit')->name('service.update');
        Route::post('/services/delete', 'Services@ajaxDelete')->name('service.delete');

        Route::get('/profile', 'Profile@index')->name('client.profile');
        Route::put('/profile/update', 'Profile@update')->name('profile.update');
        Route::post('/profile/delete', 'Profile@deleteAccount');
        Route::get('/profile/delete', 'Profile@showDeletePage')->name('client.truncate');
        Route::put('/profile/password', 'Profile@changePassword')->name('profile.password');
    }
);
// change middleware later
// admin routes
Route::group(
    [
     'namespace' => 'Admin',
     'prefix'    => 'cm-admin',
    ],
    function () {
        Route::get('/', 'LoginController@showLogin');
        Route::get('/login', 'LoginController@showLogin');
        Route::post('/login', 'LoginController@login')->name('admin.login');
        Route::post('/logout', 'LoginController@logout')->name('admin.logout');

        Route::group(
            [ 'middleware' => 'auth:admin' ],
            function () {
                Route::get('/home', 'DashboardController@show')->name('admin.dashboard');
                Route::get('/services', 'ServiceController@show')->name('admin.services');
                Route::post('/services/create', 'ServiceController@create')->name('admin.services.create');
                Route::match(['get', 'post'], '/services/process', 'ServiceController@process');
                Route::get('/category', 'AdvertController@show')->name('admin.category');
                Route::get('/clients', 'ClientController@show')->name('admin.users');

                Route::group(
                    [ 'prefix' => 'admins'],
                    function () {
                        Route::get('/', 'AdminController@show')->name('admin.manager');
                        // staffs account
                        Route::get('/create', 'AdminController@showForm')->name('admin.manager.create');
                        Route::post('/create', 'AdminController@create')->name('admin.manager.create');
                        Route::put('/create', 'AdminController@update')->name('admin.manager.update');
                        Route::get('/manage', 'AdminController@manage')->name('admin.manager.manage');
                        Route::get('/get/{id}', 'AdminController@getAdmin')->name('admin.manager.get-admin');
                    }
                );

                Route::get('/profile', 'AccountController@show')->name('admin.account');
                // personal account
                Route::put('/profile/update', 'AccountController@update')->name('admin.account.update');
                // personal account
                // form routes
                Route::put('/category', 'AdvertController@update');
                Route::post('/category/add', 'AdvertController@add')->name('admin.event-add');
                Route::post('/category/delete', 'AdvertController@delete')->name('admin.event-delete');

                // ajax dedicated route
                Route::get('/fetch', 'ClientController@fetch');
                Route::post('/truncate', 'ClientController@truncate');
                Route::get('/search', 'ClientController@search');
            }
        );
    }
);

Auth::routes();
