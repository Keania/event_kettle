var Laravel;
var jsDebug = false;
var SideBar = {
    init: function () {
        var _parent = this, _self = _parent.self = $('#main-nav');
        _self.children('ul').children('li').each(function (i) {
            $(this).attr('data-lock', i);
            $(this).find('a').click(_parent.active);
        });
        //if(_self !== null){
        //    var contentHolder = this.self.find('div');
        //    contentHolder.map(function(){
        //        var cH = $(this);
        //        cH.prev().find('a').click(_parent.active);
        //    });
        //}
        this.checkActive();
    },
    active: function (e) {
        var _h = $(this).parent().next('div').height(), _index = $(this).parent().attr('data-lock');
        if (_index == SideBar.getActive())
            e.preventDefault();
        //gets the sub nav height
        //_h = (_h > 0 ? 0 : $(this).parent().next('div').find('ul').height());
        //$(this).parent().next('div').height(_h);
        $(this).parent().addClass('active')
            .siblings('li')
            .removeClass('active');
        SideBar.addActive(_index);
    },
    record: null,
    addActive: function (index) {
        this.record = index;
        this.updateActive();
    },
    checkActive: function () {
        if (this.record == null) {
            var _min = this.getActive();
            if (_min !== null) {
                var _curPage = $(this.self.find("li[data-lock]").get(_min));
                _curPage.addClass('active');
                this.record = _min;
                this.updateActive();
            }
        }
    },
    getActive: function () {
        var _min = localStorage.getItem('menuActive');
        return window.location.href.match(/\/cm\-admin\/([A-Za-z0-9-_+]+)/)[1] || _min || false;
    },
    updateActive: function () {
        if (this.record !== null) {
            var _new = this.record;
            localStorage.setItem('menuActive', _new);
        }
    },
    removeActive: function (index) {
        this.record = null;
        this.updateActive();
    }
};
//categories
var Categories = {
    init: function () {
        ManageCat.init();
    }
};
// Manage Category Box
var ManageCat = (function () {
    function ManageCat() {
    }
    ManageCat.init = function () {
        ManageCat.root = $('#manage-categories');
        var list = ManageCat.root.find('li.list-group-item');
        list.map(function (e) {
            ManageCat.List[e] = (new ListBox(this));
        });
    };
    ManageCat.props = {
        baseLink: "/cm-admin/category"
    };
    ManageCat.List = [];
    return ManageCat;
}());
;
// Category List
var ListBox = (function () {
    function ListBox(e) {
        this.self = $(e);
        this.xerox = this.self.attr('data-xerox');
        this.contentHolder = this.self.next();
        this.name = this.contentHolder.find('input[data-name]');
        this.icon = this.contentHolder.find('input[data-icon]');
        this.pos = this.contentHolder.find('input[data-pos]');
        var delete_button = this.self.find("button[data-del]");
        this.self.click(this.dropDown.bind(this));
        delete_button.click(this.delete.bind(this));
    }
    //sends the update
    ListBox.prototype.send = function () {
        //gathers data to send
        var $info = {
            icon: this.icon.val(),
            name: this.name.val(),
            pos: this.pos.val(),
            xerox: this.xerox,
            _method: 'PUT',
            _token: Laravel.csrfToken,
            action: 2
        };
        //ajax updates the category
        $.ajax({
            url: ManageCat.props.baseLink,
            type: 'POST',
            data: $info,
            dataType: 'json',
            success: function (xhr) {
                console.log(xhr);
                if (xhr.success === true) {
                    alert('category updated');
                    this.updateFields(xhr);
                }
                else if (xhr.success == "42S22") {
                    alert('an error occured.');
                }
                else {
                    alert('no changes made');
                }
            }.bind(this)
        });
        this.self.removeClass('active');
        ManageCat.last = null;
    };
    ListBox.prototype.updateFields = function (response) {
        console.log('updating fields');
    };
    //updates and hides the active list
    ListBox.prototype.checkSiblings = function () {
        if (ManageCat.last != null) {
            ManageCat.List.map(function (e, o) {
                if (e == ManageCat.last && e !== this) {
                    e.send();
                }
            }.bind(this));
        }
    };
    //shows the list on click
    ListBox.prototype.dropDown = function () {
        this.checkSiblings();
        ManageCat.last = this;
        var _a = this.self;
        if (_a.hasClass('active')) {
            this.send();
        }
        else {
            _a.addClass('active');
        }
    };
    ListBox.prototype.delete = function (e) {
        e.stopPropagation();
        $.post(ManageCat.props.baseLink, { xerox: this.xerox, _token: Laravel.csrfToken, action: 1 }, function (xhr) {
            if (xhr.code == 200) {
                this.self.removeClass('active').addClass("hide");
                this.contentHolder.addClass("hide");
            }
        }.bind(this));
    };
    return ListBox;
}());
//Users Panel
var UserPanel = {
    init: function () {
        this.construct();
        console.log('running UserPanel');
        var _parent = this, userBox = this.props.usersBox, userPanel = this.props.userPanel, userAdvert = this.props.userAdvert, userServices = this.props.userServicePanel;
        //sets up search
        // this.props.search.on("keyup",function (e) {
        //     var _string = $(this).val();
        //     console.log('searching: ' + _string);
        //     $.get('search',{
        //         person : true,
        //         q : _string,
        //     },_parent.searchCallBack);
        // });
        var subLinks = ['adverts', 'services', 'manage'];
        //side bar click delegation and activity
        SideBar.self.delegate('li[data-lock=3] + div a', 'click', function (i) {
            i.preventDefault();
            var _link = $(this).attr('href').match(/(?!(#|&))\w+$/g)[0];
            _index = $.inArray(_link, subLinks);
            if (_index > -1) {
                //refactors the url
                var _location = window.location.href.split(/#/g)[0];
                window.location = _location + "#" + subLinks[_index];
                //check for function ability
                try {
                    //loads the method in this Object
                    // UserPanel[subLinks[_index]]();
                    //sets the functionality to current
                    UserPanel.activity = subLinks[_index];
                }
                catch (x) {
                    if (x.name == "TypeError") {
                        console.warn('[' + _link + '] has no functionality ');
                    }
                }
            }
        });
        //click and edit user-list
        // this.props.usersBox.delegate(this.props.editButtonSelector,'click', function () {
        //     //removes the active styleClass from all buttons
        //     userBox.find(_parent.props.editButtonSelector).removeClass("set")
        //     console.log('clicking');
        //     //gets the id from the button
        //     var _xerox = UserPanel.current = $(this).addClass('set').attr('data-zero') || 1;
        //     //sets as the last clicked button
        //     UserPanel.props.lastButton = this; //button
        //     //asks for information
        //     $.get(_parent.props.editLink,
        //         {'xerox': _xerox,'activity':
        //         UserPanel.activity},
        //         UserPanel[UserPanel.activity].bind(UserPanel));
        // });
        //user advert delete function
        this.props.userAdvert.delegate(this.props.advertDeleteButton, 'click', function () {
            try {
                var _advertParent = $(this).parents(".list-group-item"), _advertXerox = _advertParent.attr("data-xerox"), _xerox = parseInt(UserPanel.current);
                if (_advertXerox == null) {
                    console.warn('delete exception - xerox not found');
                }
                else {
                    if (!confirm('Are you sure ?'))
                        return null;
                    console.log(_advertXerox);
                    $.post(_parent.props.deleteLink, {
                        activity: "deleteAdvert",
                        _token: Laravel.csrfToken,
                        advertXerox: _advertXerox,
                        xerox: _xerox,
                    }, function (e) {
                        if (e.success === true) {
                            alert("advert deleted for user");
                            $(_advertParent).addClass("collapse");
                        }
                    });
                }
            }
            catch (e) {
                console.log(e.name);
            }
        });
    },
    /**
    * search ajax callback function
    *@param ajaxResponse xhr
    **/
    searchCallBack: function (xhr) {
        UserPanel.populateUsersList(xhr);
    },
    services: function (xml) {
        // console.log(xml);
        this.buildUserServices(xml);
    },
    adverts: function (xml) {
        console.log(xml);
        this.buildUserAdverts(xml);
    },
    manage: function (info) {
        this.props.userAdvert.addClass("collapse");
        this.props.userServicePanel.addClass("collapse");
        UserPanel.props.userPanel.find('#main-info').html(info);
        setTimeout(function () {
            UserPanel.props.userPanel.removeClass('collapse');
        }.bind(this), 500);
    },
    props: {
        search: null,
        userCounter: null,
        usersBox: null,
        userServicePanel: null,
        userPanel: null,
        userAdvert: null,
        sort: null,
        editLink: null,
        deleteLink: null,
        advertDeleteButton: null,
        editButtonSelector: null
    },
    construct: function () {
        this.self = $('#main-bar');
        var userList = $('#users-list');
        this.activity = window.location.hash.slice(1, window.location.hash.length) || 'adverts';
        // COLLECTION OF MAIN ELEMENTS
        this.props = {
            search: this.self.find('[data-search=main]'),
            userCounter: userList.find("span#counter"),
            usersBox: $('#users-list'),
            userServicePanel: $("#user-services"),
            userPanel: $('#user-panel'),
            userAdvert: $("#user-adverts"),
            sort: null,
            editLink: '/cm-admin/fetch',
            deleteLink: "/cm-admin/truncate",
            advertDeleteButton: '.advert-box button',
            editButtonSelector: '.list-group-item button',
        };
    },
    buildUserServices: function (xml) {
        this.props.userPanel.addClass("collapse");
        this.props.userAdvert.addClass("collapse");
        var document = $(xml), serviceBox = this.props.userServicePanel, serviceMap = [], user = document.find("user");
        name = user.find("name").text(),
            image = user.find("image").text(),
            services = user.find("services").find("service");
        services.map(function (e) {
            serviceMap[e] = $("<li class='list-group-item'>").text($(this).text());
        });
        console.log(image, name + "! you have " + serviceMap.length + " services and your image link is '" + image + "'");
        serviceBox.find(".thumbnail > img ").attr("src", image);
        serviceBox.find(".name-title").text(name);
        serviceBox.find(".service-title").html("services");
        if (serviceMap.length > 0)
            serviceBox.find(".service-list").html("").append(serviceMap);
        else {
            serviceBox.find(".service-title").html("<b>No...Service Uploaded for User.</b>");
            serviceBox.find(".service-list").html("");
        }
        console.log("removing class", serviceBox);
        serviceBox.removeClass("collapse");
    },
    buildUserAdverts: function (xml) {
        this.props.userPanel.addClass("collapse");
        this.props.userServicePanel.addClass("collapse");
        var advertsBox = this.props.userAdvert, userInfo = $(xml), adverts = userInfo.find("advert"), username = userInfo.find('username').text().toLowerCase().small(), advertMap = [];
        advertsBox.find(".name-title").html(userInfo.find("name").text() + ("<br> @" + username));
        advertsBox.find(".panel-heading figure > img ").attr("src", userInfo.find("profilePic").text());
        advertsBox.find("#NOA").html(adverts.length);
        var _delButton = "<button><i class='fa fa-trash-o'></i></button>";
        if (adverts.length > 0) {
            adverts.each(function (i) {
                var advert = $(this), label = advert.find("label").text(), descipt = advert.find("description").text(), photo = advert.find("img").attr('href'), price = advert.find("price").text(), xerox = advert.attr("data-xerox");
                advertBox = $("<div class='list-group-item col-sm-11 clearfix'>").attr("data-xerox", xerox);
                advertBox
                    .append($("<figure class='thumbnail col-sm-1'>").append($("<img>").attr("src", photo)))
                    .append($("<div class='advert-box col-sm-10'>").html("<h5 class='advert-label col-sm-8'>" + label + "</h5>")
                    .append($("<span class='pull-right col-sm-4'>").text(price).append(_delButton)))
                    .append($("<div id='advert-description' class='text-muted col-sm-10'>").text(descipt));
                advertMap[i] = advertBox;
            });
            advertsBox.find("#adverts-list").html("").html(advertMap);
        }
        else {
            advertsBox
                .find("#adverts-list")
                .html("<i class='alert-info'> NO ADVERT UPLOADED..");
        }
        this.props.userAdvert.removeClass("collapse");
    },
    /**
    * generates a list for json user in user panel
    *@param json
    */
    populateUsersList: function (json) {
        var _parent = this;
        var length = !json.length ? 0 : json.length;
        if (length < 1) {
            if (jsDebug) {
                alert('Search Found Nothing');
            }
            else {
                return false;
            }
        }
        json.map(function (i, c) {
            var e = c += 1;
            var userCounter = _parent.props.userCounter;
            var message = (length > 1) ? length + " Users Found" : length + ' User Found';
            var listGroup = _parent.props.usersBox.find('.list-group');
            listGroup.html("");
            listGroup.append("<li class='list-group-item'>"
                + e + " | "
                + i.fname + " "
                + i.lname + "<span class=pull-right>"
                + "<button class=submit data-zero=" + i.id + "> view<b>profile </b> </button>"
                + "</span>"
                + "</li>");
            userCounter.text(message);
            console.log('population complete');
        });
    }
};
//services
var Services = {
    props: {
        link: '/cm-admin/services/process'
    },
    constructor: function () {
        return {
            n: $('#service-list'),
            p: 'sleeping'
        };
    },
    init: function () {
        $.extend(this, this.constructor());
        //accordion toggle click
        this.n.delegate('.list-group-item', 'click', this.toggleList);
        //form submit clicks
        this.n.delegate('.list-group-item button[name]', 'click', this.submit);
    },
    submit: function (e) {
        e.preventDefault();
        e.stopPropagation();
        var _parent = $(this).parents(".list-group-item");
        _label = _parent.find("input[name=service_name]").val(),
            _xerox = _parent.find("input[name=serviceXerox]").val(),
            _desc = _parent.find("textarea").val(), mot = {},
            _buttonName = $(this).attr('name');
        mot.xerox = _xerox;
        mot.name = _label;
        mot.desc = _desc;
        mot.action = $(this).attr('name');
        mot._token = Laravel.csrfToken;
        $.post(Services.props.link, mot, function (response) {
            // console.log('am here',_buttonName,response.code);
            if (_buttonName == "update_service") {
                if (response.code === 200) {
                    alert('updated');
                }
                else if (response.code === 300) {
                    alert('No changes made');
                }
                _parent.addClass("hide");
                _parent.prev().find('span').text(mot.name);
            }
            else if (_buttonName == "delete_service" && response.code === 200) {
                alert('deleted');
                _parent.addClass("hide");
                _parent.prev().addClass("hide");
            }
        });
    },
    toggleList: function (evt) {
        evt.preventDefault();
        evt.stopPropagation();
        var _self = $(this), _form = _self.next('form');
        if (_form.hasClass('hide')) {
            _form.removeClass('hide');
        }
        else {
            _form.addClass('hide');
        }
    }
};
var ManageAdmin = {
    current: 0,
    init: function () {
        this.managePanel = $('#manage-panel');
        this.previewPanel = $('#preview-panel');
        this.managePanel.delegate('.list-group-item', 'click', this.getAdminInfo);
    },
    getAdminInfo: function (evt) {
        var _self = $(this);
        var _a = _self.attr('data-zero');
        console.log('i am getting your information' + _a);
        $.get('hdr_p?page=ma', { zerox: _a }, function (html) {
            ManageAdmin.current = _a;
            ManageAdmin.previewPanel.html(html);
        });
    }
};
$(function () {
    SideBar.init();
    var FUNCTIONS = [], PAGE = (SideBar.getActive());
    console.log(PAGE);
    FUNCTIONS[2] = Categories;
    FUNCTIONS['category'] = Categories;
    FUNCTIONS[1] = Services;
    FUNCTIONS['services'] = Services;
    FUNCTIONS[3] = UserPanel;
    FUNCTIONS['clients'] = UserPanel;
    FUNCTIONS[5] = 'ManageAdmin';
    FUNCTIONS['manage_admin'] = ManageAdmin;
    try {
        FUNCTIONS[PAGE].init();
    }
    catch (x) {
        // console.log(x.name,x.message);
        // alert('if Javascript is disabled please enable for a better experience');
        console.error('page has not functionality');
    }
});
