var CACHE_NAME = "cacher_v1";
var urlsToCache = [
    '../',
    '../css/cm_welcome.css',
    '../css/cm_home.css',
    '../css/cm_dashboard.css',
    '../css/fonts.css',
    '../css/bootstrap.min.css'
];
self.addEventListener('fetch', function (event) {
    console.log('a fetch call has made');
    event.respondWith(caches.match(event.request)
        .then(function (response) {
        console.log('now am sending the caches');
        if (response) {
            return response;
        }
        return fetch(event.request);
    }));
});
self.addEventListener('install', function (event) {
    console.log('installing');
    event.waitUntil(caches.open(CACHE_NAME).then(function (cache) {
        console.log(urlsToCache);
        return cache.addAll(urlsToCache);
    }));
});
