/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 1);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/js/components/Modal.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({

  name: 'Modal',
  props: ['show'],
  data: function data() {
    return {};
  },

  watch: {
    show: function show() {
      this.show ? this.showSelf() : this.hideSelf();
    }
  },
  methods: {
    showSelf: function showSelf() {
      $(this.$el).modal('show');
    },
    toggleSelf: function toggleSelf() {
      $(this.$el).modal('toggle');
    },
    hideSelf: function hideSelf() {
      $(this.$el).modal('hide');
      this.$emit('closed');
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/js/components/client/EventsPage.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Modal_vue__ = __webpack_require__("./resources/assets/js/components/Modal.vue");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Modal_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__Modal_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_vue_select__ = __webpack_require__("./node_modules/vue-select/dist/vue-select.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_vue_select___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_vue_select__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__Models_Event_js__ = __webpack_require__("./resources/assets/js/Models/Event.js");





/* harmony default export */ __webpack_exports__["default"] = ({
	components: { Modal: __WEBPACK_IMPORTED_MODULE_0__Modal_vue___default.a, vSelect: __WEBPACK_IMPORTED_MODULE_1_vue_select___default.a },
	name: 'EventsPage',
	data: function data() {
		return {
			images: window.location.protocol + "//" + window.location.host + "/images/avatar.jpg",
			url: 'storage/app/public/event-images/images/',
			showAddForm: false,
			showEdit: false,
			search: '',
			events: [],
			show: false,
			loaded: false,
			pickedEvent: __WEBPACK_IMPORTED_MODULE_2__Models_Event_js__["a" /* default */].params,
			addedEvent: __WEBPACK_IMPORTED_MODULE_2__Models_Event_js__["a" /* default */].params

		};
	},
	mounted: function mounted() {
		$("#datetimepicker_add").datetimepicker({
			format: 'YYYY-MM-DD HH:mm:ss',
			minDate: new Date()
		});
	},

	computed: {
		new_image: function new_image() {},
		load_message: function load_message() {
			return this.loaded == true && this.events.length == 0 ? 'Event repository is empty upload an event' : 'Loading Events in a sec';
		},

		filteredEvent: function filteredEvent() {
			var _this = this;

			return _this.events.filter(function (event) {
				return event.title.toLowerCase().includes(_this.search.toLowerCase());
			});
		}
	},
	created: function created() {
		var _this2 = this;

		__WEBPACK_IMPORTED_MODULE_2__Models_Event_js__["a" /* default */].all().then(function (response) {
			_this2.loaded = true;
			_this2.events = response.data;
		}).catch(function (err) {
			console.log('there was ' + err);
		});
	},

	methods: {
		toggleForm: function toggleForm() {
			this.showAddForm = !this.showAddForm;
		},
		toggleEdit: function toggleEdit() {
			this.showEdit = !this.showEdit;
		},
		editEvent: function editEvent(id) {
			this.showEdit = true;
			this.pickedEvent = {};
			Object.assign(this.pickedEvent, __WEBPACK_IMPORTED_MODULE_2__Models_Event_js__["a" /* default */].find(id));
		},
		saveEvent: function saveEvent() {
			var data = this.addedEvent;
			axios.post('/cm-user/events/add', data).then(function (res) {}).catch(function (err) {
				console.log('error due to ' + err);
			});
		},
		update: function update() {
			var picked = this.pickedEvent;
			axios.post('/cm-user/events/update', picked);
		},
		hideEdit: function hideEdit() {
			this.showEdit = false;
		}
	}
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/js/components/client/MerchandisePage.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Modal_vue__ = __webpack_require__("./resources/assets/js/components/Modal.vue");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Modal_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__Modal_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_vue_select__ = __webpack_require__("./node_modules/vue-select/dist/vue-select.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_vue_select___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_vue_select__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__Models_Merchandise_js__ = __webpack_require__("./resources/assets/js/Models/Merchandise.js");





/* harmony default export */ __webpack_exports__["default"] = ({

  name: 'MerchandisePage',
  components: { Modal: __WEBPACK_IMPORTED_MODULE_0__Modal_vue___default.a, vSelect: __WEBPACK_IMPORTED_MODULE_1_vue_select___default.a },
  created: function created() {
    var _this = this;

    __WEBPACK_IMPORTED_MODULE_2__Models_Merchandise_js__["a" /* default */].all().then(function (data) {
      _this.loaded = true;
      _this.merchs = __WEBPACK_IMPORTED_MODULE_2__Models_Merchandise_js__["a" /* default */].merchs;
    });
  },
  mounted: function mounted() {},
  data: function data() {
    return {
      showForm: true,
      showEdit: false,
      merchs: [],
      loaded: false,
      category: "Shirts",
      pickedMerchandise: __WEBPACK_IMPORTED_MODULE_2__Models_Merchandise_js__["a" /* default */].form
    };
  },

  computed: {
    load_message: function load_message() {
      return this.loaded == false && this.merchs.length <= 0 ? 'Loading Messages' : 'Merchandise repository is empty. Please upload an merchandise.';
    }
  },
  methods: {
    updateMerchandise: __WEBPACK_IMPORTED_MODULE_2__Models_Merchandise_js__["a" /* default */].update,
    updateValue: function updateValue(value) {
      var formattedValue = value.trim().slice(0, value.indexOf('.') === -1 ? value.length : value.indexOf('.') + 3);

      if (formattedValue !== value) {
        this.pickedMerchandise.price = formattedValue;
      }

      this.pickedMerchandise.price = Number(formattedValue);
    },
    toggleForm: function toggleForm() {
      this.showForm = !this.showForm;
      this.editForm = !this.editForm;
    },
    toggleEdit: function toggleEdit() {
      this.showEdit = !this.showEdit;
    },
    hideEdit: function hideEdit() {
      this.showEdit = false;
    },
    editMerchandise: function editMerchandise(id) {
      this.showForm = false;
      this.showEdit = true;
      this.pickedMerchandise = __WEBPACK_IMPORTED_MODULE_2__Models_Merchandise_js__["a" /* default */].find(id);
    },
    shuffle: function shuffle() {
      this.merchs = _.shuffle(this.merchs);
    },
    removeMerchandise: function removeMerchandise(id) {
      __WEBPACK_IMPORTED_MODULE_2__Models_Merchandise_js__["a" /* default */].remove(id);
    }
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js?sourceMap!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-401ee94c\",\"scoped\":false,\"hasInlineConfig\":true}!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/js/components/client/MerchandisePage.vue":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(true);
// imports


// module
exports.push([module.i, "\n.flip-list-move {\n  transition: transform 1s;\n}\n", "", {"version":3,"sources":["C:/Users/keania/Desktop/wavestech/NewKettle/event_kettle/resources/assets/js/components/client/MerchandisePage.vue?04b020f4"],"names":[],"mappings":";AA2EA;EACA,yBAAA;CACA","file":"MerchandisePage.vue","sourcesContent":["<script>\r\nimport Modal from '../Modal.vue';\r\nimport vSelect from 'vue-select';\r\nimport Merchandise from '../../Models/Merchandise.js';\r\n\r\nexport default {\r\n\r\n  name: 'MerchandisePage',\r\n  components: {Modal, vSelect},\r\n  created() {\r\n    Merchandise.all().then((data) => {\r\n        this.loaded = true;\r\n        this.merchs = Merchandise.merchs;\r\n    });\r\n  },\r\n  mounted() {\r\n  },\r\n  data () {\r\n    return {\r\n        showForm: true,\r\n        showEdit: false,\r\n        merchs : [],\r\n        loaded: false,\r\n        category: \"Shirts\",\r\n        pickedMerchandise: Merchandise.form,\r\n    };\r\n  },\r\n  computed: {\r\n    load_message () {\r\n      return this.loaded == false && this.merchs.length <= 0 ? 'Loading Messages' : 'Merchandise repository is empty. Please upload an merchandise.';\r\n    }\r\n  },\r\n  methods: {\r\n    updateMerchandise : Merchandise.update,\r\n    updateValue(value) {\r\n      var formattedValue = value.trim()\r\n      .slice(\r\n          0,\r\n          value.indexOf('.') === -1\r\n            ? value.length\r\n            : value.indexOf('.') + 3\r\n        );\r\n\r\n      if (formattedValue !== value) {\r\n        this.pickedMerchandise.price = formattedValue\r\n      }\r\n\r\n      this.pickedMerchandise.price = Number(formattedValue);\r\n    },\r\n    toggleForm() {\r\n      this.showForm = !this.showForm;\r\n      this.editForm = !this.editForm;\r\n    },\r\n    toggleEdit() {\r\n      this.showEdit = !this.showEdit;\r\n    },\r\n    hideEdit() {\r\n      this.showEdit = false;\r\n    },\r\n    editMerchandise (id) {\r\n      this.showForm = false;\r\n      this.showEdit = true;\r\n      this.pickedMerchandise = Merchandise.find(id);\r\n    },\r\n    shuffle() {\r\n      this.merchs = _.shuffle(this.merchs);\r\n    },\r\n    removeMerchandise (id) {\r\n      Merchandise.remove(id);\r\n    }\r\n  }\r\n};\r\n</script>\r\n\r\n<style>\r\n  .flip-list-move {\r\n    transition: transform 1s;\r\n  }\r\n</style>\r\n"],"sourceRoot":""}]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/lib/css-base.js":
/***/ (function(module, exports) {

/*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author Tobias Koppers @sokra
*/
// css base code, injected by the css-loader
module.exports = function(useSourceMap) {
	var list = [];

	// return the list of modules as css string
	list.toString = function toString() {
		return this.map(function (item) {
			var content = cssWithMappingToString(item, useSourceMap);
			if(item[2]) {
				return "@media " + item[2] + "{" + content + "}";
			} else {
				return content;
			}
		}).join("");
	};

	// import a list of modules into the list
	list.i = function(modules, mediaQuery) {
		if(typeof modules === "string")
			modules = [[null, modules, ""]];
		var alreadyImportedModules = {};
		for(var i = 0; i < this.length; i++) {
			var id = this[i][0];
			if(typeof id === "number")
				alreadyImportedModules[id] = true;
		}
		for(i = 0; i < modules.length; i++) {
			var item = modules[i];
			// skip already imported module
			// this implementation is not 100% perfect for weird media query combinations
			//  when a module is imported multiple times with different media queries.
			//  I hope this will never occur (Hey this way we have smaller bundles)
			if(typeof item[0] !== "number" || !alreadyImportedModules[item[0]]) {
				if(mediaQuery && !item[2]) {
					item[2] = mediaQuery;
				} else if(mediaQuery) {
					item[2] = "(" + item[2] + ") and (" + mediaQuery + ")";
				}
				list.push(item);
			}
		}
	};
	return list;
};

function cssWithMappingToString(item, useSourceMap) {
	var content = item[1] || '';
	var cssMapping = item[3];
	if (!cssMapping) {
		return content;
	}

	if (useSourceMap && typeof btoa === 'function') {
		var sourceMapping = toComment(cssMapping);
		var sourceURLs = cssMapping.sources.map(function (source) {
			return '/*# sourceURL=' + cssMapping.sourceRoot + source + ' */'
		});

		return [content].concat(sourceURLs).concat([sourceMapping]).join('\n');
	}

	return [content].join('\n');
}

// Adapted from convert-source-map (MIT)
function toComment(sourceMap) {
	// eslint-disable-next-line no-undef
	var base64 = btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap))));
	var data = 'sourceMappingURL=data:application/json;charset=utf-8;base64,' + base64;

	return '/*# ' + data + ' */';
}


/***/ }),

/***/ "./node_modules/vue-loader/lib/component-normalizer.js":
/***/ (function(module, exports) {

/* globals __VUE_SSR_CONTEXT__ */

// this module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle

module.exports = function normalizeComponent (
  rawScriptExports,
  compiledTemplate,
  injectStyles,
  scopeId,
  moduleIdentifier /* server only */
) {
  var esModule
  var scriptExports = rawScriptExports = rawScriptExports || {}

  // ES6 modules interop
  var type = typeof rawScriptExports.default
  if (type === 'object' || type === 'function') {
    esModule = rawScriptExports
    scriptExports = rawScriptExports.default
  }

  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (compiledTemplate) {
    options.render = compiledTemplate.render
    options.staticRenderFns = compiledTemplate.staticRenderFns
  }

  // scopedId
  if (scopeId) {
    options._scopeId = scopeId
  }

  var hook
  if (moduleIdentifier) { // server build
    hook = function (context) {
      // 2.3 injection
      context =
        context || // cached call
        (this.$vnode && this.$vnode.ssrContext) || // stateful
        (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext) // functional
      // 2.2 with runInNewContext: true
      if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__
      }
      // inject component styles
      if (injectStyles) {
        injectStyles.call(this, context)
      }
      // register component module identifier for async chunk inferrence
      if (context && context._registeredComponents) {
        context._registeredComponents.add(moduleIdentifier)
      }
    }
    // used by ssr in case component is cached and beforeCreate
    // never gets called
    options._ssrRegister = hook
  } else if (injectStyles) {
    hook = injectStyles
  }

  if (hook) {
    var functional = options.functional
    var existing = functional
      ? options.render
      : options.beforeCreate
    if (!functional) {
      // inject component registration as beforeCreate hook
      options.beforeCreate = existing
        ? [].concat(existing, hook)
        : [hook]
    } else {
      // register for functioal component in vue file
      options.render = function renderWithStyleInjection (h, context) {
        hook.call(context)
        return existing(h, context)
      }
    }
  }

  return {
    esModule: esModule,
    exports: scriptExports,
    options: options
  }
}


/***/ }),

/***/ "./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-078b02f1\",\"hasScoped\":false}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/js/components/Modal.vue":
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "modal fade", attrs: { role: "dialog" } }, [
    _c("div", { staticClass: "modal-dialog" }, [
      _c("div", { staticClass: "modal-content" }, [
        _c("div", { staticClass: "modal-header" }, [
          _c(
            "button",
            {
              staticClass: "close",
              attrs: { type: "button" },
              on: {
                click: function($event) {
                  $event.preventDefault()
                  _vm.hideSelf($event)
                }
              }
            },
            [_vm._v("×")]
          ),
          _vm._v(" "),
          _c("h4", { staticClass: "modal-title" }, [_vm._t("modal-title")], 2)
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "modal-body" }, [_vm._t("modal-body")], 2),
        _vm._v(" "),
        _c("div", { staticClass: "model-body" }, [_vm._t("modal-image")], 2),
        _vm._v(" "),
        _c("div", { staticClass: "modal-footer" }, [
          _c(
            "button",
            {
              staticClass: "btn btn-default",
              attrs: { type: "button" },
              on: {
                click: function($event) {
                  $event.preventDefault()
                  _vm.hideSelf($event)
                }
              }
            },
            [_vm._v("Close")]
          )
        ])
      ])
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-078b02f1", module.exports)
  }
}

/***/ }),

/***/ "./node_modules/vue-select/dist/vue-select.js":
/***/ (function(module, exports, __webpack_require__) {

!function(t,e){ true?module.exports=e():"function"==typeof define&&define.amd?define([],e):"object"==typeof exports?exports.VueSelect=e():t.VueSelect=e()}(this,function(){return function(t){function e(r){if(n[r])return n[r].exports;var o=n[r]={exports:{},id:r,loaded:!1};return t[r].call(o.exports,o,o.exports,e),o.loaded=!0,o.exports}var n={};return e.m=t,e.c=n,e.p="/",e(0)}([function(t,e,n){"use strict";function r(t){return t&&t.__esModule?t:{default:t}}Object.defineProperty(e,"__esModule",{value:!0}),e.mixins=e.VueSelect=void 0;var o=n(83),i=r(o),a=n(42),s=r(a);e.default=i.default,e.VueSelect=i.default,e.mixins=s.default},function(t,e){var n=t.exports="undefined"!=typeof window&&window.Math==Math?window:"undefined"!=typeof self&&self.Math==Math?self:Function("return this")();"number"==typeof __g&&(__g=n)},function(t,e,n){t.exports=!n(9)(function(){return 7!=Object.defineProperty({},"a",{get:function(){return 7}}).a})},function(t,e){var n={}.hasOwnProperty;t.exports=function(t,e){return n.call(t,e)}},function(t,e,n){var r=n(10),o=n(33),i=n(25),a=Object.defineProperty;e.f=n(2)?Object.defineProperty:function(t,e,n){if(r(t),e=i(e,!0),r(n),o)try{return a(t,e,n)}catch(t){}if("get"in n||"set"in n)throw TypeError("Accessors not supported!");return"value"in n&&(t[e]=n.value),t}},function(t,e){var n=t.exports={version:"2.5.1"};"number"==typeof __e&&(__e=n)},function(t,e,n){var r=n(4),o=n(14);t.exports=n(2)?function(t,e,n){return r.f(t,e,o(1,n))}:function(t,e,n){return t[e]=n,t}},function(t,e,n){var r=n(59),o=n(16);t.exports=function(t){return r(o(t))}},function(t,e,n){var r=n(23)("wks"),o=n(15),i=n(1).Symbol,a="function"==typeof i,s=t.exports=function(t){return r[t]||(r[t]=a&&i[t]||(a?i:o)("Symbol."+t))};s.store=r},function(t,e){t.exports=function(t){try{return!!t()}catch(t){return!0}}},function(t,e,n){var r=n(12);t.exports=function(t){if(!r(t))throw TypeError(t+" is not an object!");return t}},function(t,e,n){var r=n(1),o=n(5),i=n(56),a=n(6),s="prototype",u=function(t,e,n){var l,c,f,p=t&u.F,d=t&u.G,h=t&u.S,b=t&u.P,v=t&u.B,g=t&u.W,y=d?o:o[e]||(o[e]={}),m=y[s],x=d?r:h?r[e]:(r[e]||{})[s];d&&(n=e);for(l in n)c=!p&&x&&void 0!==x[l],c&&l in y||(f=c?x[l]:n[l],y[l]=d&&"function"!=typeof x[l]?n[l]:v&&c?i(f,r):g&&x[l]==f?function(t){var e=function(e,n,r){if(this instanceof t){switch(arguments.length){case 0:return new t;case 1:return new t(e);case 2:return new t(e,n)}return new t(e,n,r)}return t.apply(this,arguments)};return e[s]=t[s],e}(f):b&&"function"==typeof f?i(Function.call,f):f,b&&((y.virtual||(y.virtual={}))[l]=f,t&u.R&&m&&!m[l]&&a(m,l,f)))};u.F=1,u.G=2,u.S=4,u.P=8,u.B=16,u.W=32,u.U=64,u.R=128,t.exports=u},function(t,e){t.exports=function(t){return"object"==typeof t?null!==t:"function"==typeof t}},function(t,e,n){var r=n(38),o=n(17);t.exports=Object.keys||function(t){return r(t,o)}},function(t,e){t.exports=function(t,e){return{enumerable:!(1&t),configurable:!(2&t),writable:!(4&t),value:e}}},function(t,e){var n=0,r=Math.random();t.exports=function(t){return"Symbol(".concat(void 0===t?"":t,")_",(++n+r).toString(36))}},function(t,e){t.exports=function(t){if(void 0==t)throw TypeError("Can't call method on  "+t);return t}},function(t,e){t.exports="constructor,hasOwnProperty,isPrototypeOf,propertyIsEnumerable,toLocaleString,toString,valueOf".split(",")},function(t,e){t.exports={}},function(t,e){t.exports=!0},function(t,e){e.f={}.propertyIsEnumerable},function(t,e,n){var r=n(4).f,o=n(3),i=n(8)("toStringTag");t.exports=function(t,e,n){t&&!o(t=n?t:t.prototype,i)&&r(t,i,{configurable:!0,value:e})}},function(t,e,n){var r=n(23)("keys"),o=n(15);t.exports=function(t){return r[t]||(r[t]=o(t))}},function(t,e,n){var r=n(1),o="__core-js_shared__",i=r[o]||(r[o]={});t.exports=function(t){return i[t]||(i[t]={})}},function(t,e){var n=Math.ceil,r=Math.floor;t.exports=function(t){return isNaN(t=+t)?0:(t>0?r:n)(t)}},function(t,e,n){var r=n(12);t.exports=function(t,e){if(!r(t))return t;var n,o;if(e&&"function"==typeof(n=t.toString)&&!r(o=n.call(t)))return o;if("function"==typeof(n=t.valueOf)&&!r(o=n.call(t)))return o;if(!e&&"function"==typeof(n=t.toString)&&!r(o=n.call(t)))return o;throw TypeError("Can't convert object to primitive value")}},function(t,e,n){var r=n(1),o=n(5),i=n(19),a=n(27),s=n(4).f;t.exports=function(t){var e=o.Symbol||(o.Symbol=i?{}:r.Symbol||{});"_"==t.charAt(0)||t in e||s(e,t,{value:a.f(t)})}},function(t,e,n){e.f=n(8)},function(t,e){"use strict";t.exports={props:{loading:{type:Boolean,default:!1},onSearch:{type:Function,default:function(t,e){}}},data:function(){return{mutableLoading:!1}},watch:{search:function(){this.search.length>0&&(this.onSearch(this.search,this.toggleLoading),this.$emit("search",this.search,this.toggleLoading))},loading:function(t){this.mutableLoading=t}},methods:{toggleLoading:function(){var t=arguments.length>0&&void 0!==arguments[0]?arguments[0]:null;return null==t?this.mutableLoading=!this.mutableLoading:this.mutableLoading=t}}}},function(t,e){"use strict";t.exports={watch:{typeAheadPointer:function(){this.maybeAdjustScroll()}},methods:{maybeAdjustScroll:function(){var t=this.pixelsToPointerTop(),e=this.pixelsToPointerBottom();return t<=this.viewport().top?this.scrollTo(t):e>=this.viewport().bottom?this.scrollTo(this.viewport().top+this.pointerHeight()):void 0},pixelsToPointerTop:function t(){var t=0;if(this.$refs.dropdownMenu)for(var e=0;e<this.typeAheadPointer;e++)t+=this.$refs.dropdownMenu.children[e].offsetHeight;return t},pixelsToPointerBottom:function(){return this.pixelsToPointerTop()+this.pointerHeight()},pointerHeight:function(){var t=!!this.$refs.dropdownMenu&&this.$refs.dropdownMenu.children[this.typeAheadPointer];return t?t.offsetHeight:0},viewport:function(){return{top:this.$refs.dropdownMenu?this.$refs.dropdownMenu.scrollTop:0,bottom:this.$refs.dropdownMenu?this.$refs.dropdownMenu.offsetHeight+this.$refs.dropdownMenu.scrollTop:0}},scrollTo:function(t){return this.$refs.dropdownMenu?this.$refs.dropdownMenu.scrollTop=t:null}}}},function(t,e){"use strict";t.exports={data:function(){return{typeAheadPointer:-1}},watch:{filteredOptions:function(){this.typeAheadPointer=0}},methods:{typeAheadUp:function(){this.typeAheadPointer>0&&(this.typeAheadPointer--,this.maybeAdjustScroll&&this.maybeAdjustScroll())},typeAheadDown:function(){this.typeAheadPointer<this.filteredOptions.length-1&&(this.typeAheadPointer++,this.maybeAdjustScroll&&this.maybeAdjustScroll())},typeAheadSelect:function(){this.filteredOptions[this.typeAheadPointer]?this.select(this.filteredOptions[this.typeAheadPointer]):this.taggable&&this.search.length&&this.select(this.search),this.clearSearchOnSelect&&(this.search="")}}}},function(t,e){var n={}.toString;t.exports=function(t){return n.call(t).slice(8,-1)}},function(t,e,n){var r=n(12),o=n(1).document,i=r(o)&&r(o.createElement);t.exports=function(t){return i?o.createElement(t):{}}},function(t,e,n){t.exports=!n(2)&&!n(9)(function(){return 7!=Object.defineProperty(n(32)("div"),"a",{get:function(){return 7}}).a})},function(t,e,n){"use strict";var r=n(19),o=n(11),i=n(39),a=n(6),s=n(3),u=n(18),l=n(61),c=n(21),f=n(67),p=n(8)("iterator"),d=!([].keys&&"next"in[].keys()),h="@@iterator",b="keys",v="values",g=function(){return this};t.exports=function(t,e,n,y,m,x,w){l(n,e,y);var S,O,_,j=function(t){if(!d&&t in M)return M[t];switch(t){case b:return function(){return new n(this,t)};case v:return function(){return new n(this,t)}}return function(){return new n(this,t)}},P=e+" Iterator",k=m==v,A=!1,M=t.prototype,L=M[p]||M[h]||m&&M[m],C=L||j(m),T=m?k?j("entries"):C:void 0,E="Array"==e?M.entries||L:L;if(E&&(_=f(E.call(new t)),_!==Object.prototype&&_.next&&(c(_,P,!0),r||s(_,p)||a(_,p,g))),k&&L&&L.name!==v&&(A=!0,C=function(){return L.call(this)}),r&&!w||!d&&!A&&M[p]||a(M,p,C),u[e]=C,u[P]=g,m)if(S={values:k?C:j(v),keys:x?C:j(b),entries:T},w)for(O in S)O in M||i(M,O,S[O]);else o(o.P+o.F*(d||A),e,S);return S}},function(t,e,n){var r=n(10),o=n(64),i=n(17),a=n(22)("IE_PROTO"),s=function(){},u="prototype",l=function(){var t,e=n(32)("iframe"),r=i.length,o="<",a=">";for(e.style.display="none",n(58).appendChild(e),e.src="javascript:",t=e.contentWindow.document,t.open(),t.write(o+"script"+a+"document.F=Object"+o+"/script"+a),t.close(),l=t.F;r--;)delete l[u][i[r]];return l()};t.exports=Object.create||function(t,e){var n;return null!==t?(s[u]=r(t),n=new s,s[u]=null,n[a]=t):n=l(),void 0===e?n:o(n,e)}},function(t,e,n){var r=n(38),o=n(17).concat("length","prototype");e.f=Object.getOwnPropertyNames||function(t){return r(t,o)}},function(t,e){e.f=Object.getOwnPropertySymbols},function(t,e,n){var r=n(3),o=n(7),i=n(55)(!1),a=n(22)("IE_PROTO");t.exports=function(t,e){var n,s=o(t),u=0,l=[];for(n in s)n!=a&&r(s,n)&&l.push(n);for(;e.length>u;)r(s,n=e[u++])&&(~i(l,n)||l.push(n));return l}},function(t,e,n){t.exports=n(6)},function(t,e,n){var r=n(16);t.exports=function(t){return Object(r(t))}},function(t,e,n){"use strict";function r(t){return t&&t.__esModule?t:{default:t}}Object.defineProperty(e,"__esModule",{value:!0});var o=n(44),i=r(o),a=n(47),s=r(a),u=n(48),l=r(u),c=n(29),f=r(c),p=n(30),d=r(p),h=n(28),b=r(h);e.default={mixins:[f.default,d.default,b.default],props:{value:{default:null},options:{type:Array,default:function(){return[]}},disabled:{type:Boolean,default:!1},maxHeight:{type:String,default:"400px"},searchable:{type:Boolean,default:!0},multiple:{type:Boolean,default:!1},placeholder:{type:String,default:""},transition:{type:String,default:"fade"},clearSearchOnSelect:{type:Boolean,default:!0},closeOnSelect:{type:Boolean,default:!0},label:{type:String,default:"label"},getOptionLabel:{type:Function,default:function(t){return"object"===("undefined"==typeof t?"undefined":(0,l.default)(t))&&this.label&&t[this.label]?t[this.label]:t}},onChange:{type:Function,default:function(t){this.$emit("input",t)}},taggable:{type:Boolean,default:!1},pushTags:{type:Boolean,default:!1},createOption:{type:Function,default:function(t){return"object"===(0,l.default)(this.mutableOptions[0])&&(t=(0,s.default)({},this.label,t)),this.$emit("option:created",t),t}},resetOnOptionsChange:{type:Boolean,default:!1},noDrop:{type:Boolean,default:!1},inputId:{type:String},dir:{type:String,default:"auto"}},data:function(){return{search:"",open:!1,mutableValue:null,mutableOptions:[]}},watch:{value:function(t){this.mutableValue=t},mutableValue:function(t,e){this.multiple?this.onChange?this.onChange(t):null:this.onChange&&t!==e?this.onChange(t):null},options:function(t){this.mutableOptions=t},mutableOptions:function(){!this.taggable&&this.resetOnOptionsChange&&(this.mutableValue=this.multiple?[]:null)},multiple:function(t){this.mutableValue=t?[]:null}},created:function(){this.mutableValue=this.value,this.mutableOptions=this.options.slice(0),this.mutableLoading=this.loading,this.$on("option:created",this.maybePushTag)},methods:{select:function(t){this.isOptionSelected(t)?this.deselect(t):(this.taggable&&!this.optionExists(t)&&(t=this.createOption(t)),this.multiple&&!this.mutableValue?this.mutableValue=[t]:this.multiple?this.mutableValue.push(t):this.mutableValue=t),this.onAfterSelect(t)},deselect:function(t){var e=this;if(this.multiple){var n=-1;this.mutableValue.forEach(function(r){(r===t||"object"===("undefined"==typeof r?"undefined":(0,l.default)(r))&&r[e.label]===t[e.label])&&(n=r)});var r=this.mutableValue.indexOf(n);this.mutableValue.splice(r,1)}else this.mutableValue=null},onAfterSelect:function(t){this.closeOnSelect&&(this.open=!this.open,this.$refs.search.blur()),this.clearSearchOnSelect&&(this.search="")},toggleDropdown:function(t){t.target!==this.$refs.openIndicator&&t.target!==this.$refs.search&&t.target!==this.$refs.toggle&&t.target!==this.$el||(this.open?this.$refs.search.blur():this.disabled||(this.open=!0,this.$refs.search.focus()))},isOptionSelected:function(t){var e=this;if(this.multiple&&this.mutableValue){var n=!1;return this.mutableValue.forEach(function(r){"object"===("undefined"==typeof r?"undefined":(0,l.default)(r))&&r[e.label]===t[e.label]?n=!0:"object"===("undefined"==typeof r?"undefined":(0,l.default)(r))&&r[e.label]===t?n=!0:r===t&&(n=!0)}),n}return this.mutableValue===t},onEscape:function(){this.search.length?this.search="":this.$refs.search.blur()},onSearchBlur:function(){this.clearSearchOnBlur&&(this.search=""),this.open=!1,this.$emit("search:blur")},onSearchFocus:function(){this.open=!0,this.$emit("search:focus")},maybeDeleteValue:function(){if(!this.$refs.search.value.length&&this.mutableValue)return this.multiple?this.mutableValue.pop():this.mutableValue=null},optionExists:function(t){var e=this,n=!1;return this.mutableOptions.forEach(function(r){"object"===("undefined"==typeof r?"undefined":(0,l.default)(r))&&r[e.label]===t?n=!0:r===t&&(n=!0)}),n},maybePushTag:function(t){this.pushTags&&this.mutableOptions.push(t)}},computed:{dropdownClasses:function(){return{open:this.dropdownOpen,single:!this.multiple,searching:this.searching,searchable:this.searchable,unsearchable:!this.searchable,loading:this.mutableLoading,rtl:"rtl"===this.dir}},clearSearchOnBlur:function(){return this.clearSearchOnSelect&&!this.multiple},searching:function(){return!!this.search},dropdownOpen:function(){return!this.noDrop&&(this.open&&!this.mutableLoading)},searchPlaceholder:function(){if(this.isValueEmpty&&this.placeholder)return this.placeholder},filteredOptions:function(){var t=this,e=this.mutableOptions.filter(function(e){return"object"===("undefined"==typeof e?"undefined":(0,l.default)(e))&&e.hasOwnProperty(t.label)?e[t.label].toLowerCase().indexOf(t.search.toLowerCase())>-1:"object"!==("undefined"==typeof e?"undefined":(0,l.default)(e))||e.hasOwnProperty(t.label)?e.toLowerCase().indexOf(t.search.toLowerCase())>-1:console.warn('[vue-select warn]: Label key "option.'+t.label+'" does not exist in options object.\nhttp://sagalbot.github.io/vue-select/#ex-labels')});return this.taggable&&this.search.length&&!this.optionExists(this.search)&&e.unshift(this.search),e},isValueEmpty:function(){return!this.mutableValue||("object"===(0,l.default)(this.mutableValue)?!(0,i.default)(this.mutableValue).length:!this.mutableValue.length)},valueAsArray:function(){return this.multiple?this.mutableValue:this.mutableValue?[this.mutableValue]:[]}}}},function(t,e,n){"use strict";function r(t){return t&&t.__esModule?t:{default:t}}Object.defineProperty(e,"__esModule",{value:!0});var o=n(28),i=r(o),a=n(30),s=r(a),u=n(29),l=r(u);e.default={ajax:i.default,pointer:s.default,pointerScroll:l.default}},function(t,e,n){t.exports={default:n(49),__esModule:!0}},function(t,e,n){t.exports={default:n(50),__esModule:!0}},function(t,e,n){t.exports={default:n(51),__esModule:!0}},function(t,e,n){t.exports={default:n(52),__esModule:!0}},function(t,e,n){"use strict";function r(t){return t&&t.__esModule?t:{default:t}}e.__esModule=!0;var o=n(43),i=r(o);e.default=function(t,e,n){return e in t?(0,i.default)(t,e,{value:n,enumerable:!0,configurable:!0,writable:!0}):t[e]=n,t}},function(t,e,n){"use strict";function r(t){return t&&t.__esModule?t:{default:t}}e.__esModule=!0;var o=n(46),i=r(o),a=n(45),s=r(a),u="function"==typeof s.default&&"symbol"==typeof i.default?function(t){return typeof t}:function(t){return t&&"function"==typeof s.default&&t.constructor===s.default&&t!==s.default.prototype?"symbol":typeof t};e.default="function"==typeof s.default&&"symbol"===u(i.default)?function(t){return"undefined"==typeof t?"undefined":u(t)}:function(t){return t&&"function"==typeof s.default&&t.constructor===s.default&&t!==s.default.prototype?"symbol":"undefined"==typeof t?"undefined":u(t)}},function(t,e,n){n(73);var r=n(5).Object;t.exports=function(t,e,n){return r.defineProperty(t,e,n)}},function(t,e,n){n(74),t.exports=n(5).Object.keys},function(t,e,n){n(77),n(75),n(78),n(79),t.exports=n(5).Symbol},function(t,e,n){n(76),n(80),t.exports=n(27).f("iterator")},function(t,e){t.exports=function(t){if("function"!=typeof t)throw TypeError(t+" is not a function!");return t}},function(t,e){t.exports=function(){}},function(t,e,n){var r=n(7),o=n(71),i=n(70);t.exports=function(t){return function(e,n,a){var s,u=r(e),l=o(u.length),c=i(a,l);if(t&&n!=n){for(;l>c;)if(s=u[c++],s!=s)return!0}else for(;l>c;c++)if((t||c in u)&&u[c]===n)return t||c||0;return!t&&-1}}},function(t,e,n){var r=n(53);t.exports=function(t,e,n){if(r(t),void 0===e)return t;switch(n){case 1:return function(n){return t.call(e,n)};case 2:return function(n,r){return t.call(e,n,r)};case 3:return function(n,r,o){return t.call(e,n,r,o)}}return function(){return t.apply(e,arguments)}}},function(t,e,n){var r=n(13),o=n(37),i=n(20);t.exports=function(t){var e=r(t),n=o.f;if(n)for(var a,s=n(t),u=i.f,l=0;s.length>l;)u.call(t,a=s[l++])&&e.push(a);return e}},function(t,e,n){var r=n(1).document;t.exports=r&&r.documentElement},function(t,e,n){var r=n(31);t.exports=Object("z").propertyIsEnumerable(0)?Object:function(t){return"String"==r(t)?t.split(""):Object(t)}},function(t,e,n){var r=n(31);t.exports=Array.isArray||function(t){return"Array"==r(t)}},function(t,e,n){"use strict";var r=n(35),o=n(14),i=n(21),a={};n(6)(a,n(8)("iterator"),function(){return this}),t.exports=function(t,e,n){t.prototype=r(a,{next:o(1,n)}),i(t,e+" Iterator")}},function(t,e){t.exports=function(t,e){return{value:e,done:!!t}}},function(t,e,n){var r=n(15)("meta"),o=n(12),i=n(3),a=n(4).f,s=0,u=Object.isExtensible||function(){return!0},l=!n(9)(function(){return u(Object.preventExtensions({}))}),c=function(t){a(t,r,{value:{i:"O"+ ++s,w:{}}})},f=function(t,e){if(!o(t))return"symbol"==typeof t?t:("string"==typeof t?"S":"P")+t;if(!i(t,r)){if(!u(t))return"F";if(!e)return"E";c(t)}return t[r].i},p=function(t,e){if(!i(t,r)){if(!u(t))return!0;if(!e)return!1;c(t)}return t[r].w},d=function(t){return l&&h.NEED&&u(t)&&!i(t,r)&&c(t),t},h=t.exports={KEY:r,NEED:!1,fastKey:f,getWeak:p,onFreeze:d}},function(t,e,n){var r=n(4),o=n(10),i=n(13);t.exports=n(2)?Object.defineProperties:function(t,e){o(t);for(var n,a=i(e),s=a.length,u=0;s>u;)r.f(t,n=a[u++],e[n]);return t}},function(t,e,n){var r=n(20),o=n(14),i=n(7),a=n(25),s=n(3),u=n(33),l=Object.getOwnPropertyDescriptor;e.f=n(2)?l:function(t,e){if(t=i(t),e=a(e,!0),u)try{return l(t,e)}catch(t){}if(s(t,e))return o(!r.f.call(t,e),t[e])}},function(t,e,n){var r=n(7),o=n(36).f,i={}.toString,a="object"==typeof window&&window&&Object.getOwnPropertyNames?Object.getOwnPropertyNames(window):[],s=function(t){try{return o(t)}catch(t){return a.slice()}};t.exports.f=function(t){return a&&"[object Window]"==i.call(t)?s(t):o(r(t))}},function(t,e,n){var r=n(3),o=n(40),i=n(22)("IE_PROTO"),a=Object.prototype;t.exports=Object.getPrototypeOf||function(t){return t=o(t),r(t,i)?t[i]:"function"==typeof t.constructor&&t instanceof t.constructor?t.constructor.prototype:t instanceof Object?a:null}},function(t,e,n){var r=n(11),o=n(5),i=n(9);t.exports=function(t,e){var n=(o.Object||{})[t]||Object[t],a={};a[t]=e(n),r(r.S+r.F*i(function(){n(1)}),"Object",a)}},function(t,e,n){var r=n(24),o=n(16);t.exports=function(t){return function(e,n){var i,a,s=String(o(e)),u=r(n),l=s.length;return u<0||u>=l?t?"":void 0:(i=s.charCodeAt(u),i<55296||i>56319||u+1===l||(a=s.charCodeAt(u+1))<56320||a>57343?t?s.charAt(u):i:t?s.slice(u,u+2):(i-55296<<10)+(a-56320)+65536)}}},function(t,e,n){var r=n(24),o=Math.max,i=Math.min;t.exports=function(t,e){return t=r(t),t<0?o(t+e,0):i(t,e)}},function(t,e,n){var r=n(24),o=Math.min;t.exports=function(t){return t>0?o(r(t),9007199254740991):0}},function(t,e,n){"use strict";var r=n(54),o=n(62),i=n(18),a=n(7);t.exports=n(34)(Array,"Array",function(t,e){this._t=a(t),this._i=0,this._k=e},function(){var t=this._t,e=this._k,n=this._i++;return!t||n>=t.length?(this._t=void 0,o(1)):"keys"==e?o(0,n):"values"==e?o(0,t[n]):o(0,[n,t[n]])},"values"),i.Arguments=i.Array,r("keys"),r("values"),r("entries")},function(t,e,n){var r=n(11);r(r.S+r.F*!n(2),"Object",{defineProperty:n(4).f})},function(t,e,n){var r=n(40),o=n(13);n(68)("keys",function(){return function(t){return o(r(t))}})},function(t,e){},function(t,e,n){"use strict";var r=n(69)(!0);n(34)(String,"String",function(t){this._t=String(t),this._i=0},function(){var t,e=this._t,n=this._i;return n>=e.length?{value:void 0,done:!0}:(t=r(e,n),this._i+=t.length,{value:t,done:!1})})},function(t,e,n){"use strict";var r=n(1),o=n(3),i=n(2),a=n(11),s=n(39),u=n(63).KEY,l=n(9),c=n(23),f=n(21),p=n(15),d=n(8),h=n(27),b=n(26),v=n(57),g=n(60),y=n(10),m=n(7),x=n(25),w=n(14),S=n(35),O=n(66),_=n(65),j=n(4),P=n(13),k=_.f,A=j.f,M=O.f,L=r.Symbol,C=r.JSON,T=C&&C.stringify,E="prototype",V=d("_hidden"),F=d("toPrimitive"),$={}.propertyIsEnumerable,B=c("symbol-registry"),N=c("symbols"),D=c("op-symbols"),I=Object[E],R="function"==typeof L,z=r.QObject,H=!z||!z[E]||!z[E].findChild,G=i&&l(function(){return 7!=S(A({},"a",{get:function(){return A(this,"a",{value:7}).a}})).a})?function(t,e,n){var r=k(I,e);r&&delete I[e],A(t,e,n),r&&t!==I&&A(I,e,r)}:A,U=function(t){var e=N[t]=S(L[E]);return e._k=t,e},W=R&&"symbol"==typeof L.iterator?function(t){return"symbol"==typeof t}:function(t){return t instanceof L},J=function(t,e,n){return t===I&&J(D,e,n),y(t),e=x(e,!0),y(n),o(N,e)?(n.enumerable?(o(t,V)&&t[V][e]&&(t[V][e]=!1),n=S(n,{enumerable:w(0,!1)})):(o(t,V)||A(t,V,w(1,{})),t[V][e]=!0),G(t,e,n)):A(t,e,n)},K=function(t,e){y(t);for(var n,r=v(e=m(e)),o=0,i=r.length;i>o;)J(t,n=r[o++],e[n]);return t},Y=function(t,e){return void 0===e?S(t):K(S(t),e)},q=function(t){var e=$.call(this,t=x(t,!0));return!(this===I&&o(N,t)&&!o(D,t))&&(!(e||!o(this,t)||!o(N,t)||o(this,V)&&this[V][t])||e)},Q=function(t,e){if(t=m(t),e=x(e,!0),t!==I||!o(N,e)||o(D,e)){var n=k(t,e);return!n||!o(N,e)||o(t,V)&&t[V][e]||(n.enumerable=!0),n}},Z=function(t){for(var e,n=M(m(t)),r=[],i=0;n.length>i;)o(N,e=n[i++])||e==V||e==u||r.push(e);return r},X=function(t){for(var e,n=t===I,r=M(n?D:m(t)),i=[],a=0;r.length>a;)!o(N,e=r[a++])||n&&!o(I,e)||i.push(N[e]);return i};R||(L=function(){if(this instanceof L)throw TypeError("Symbol is not a constructor!");var t=p(arguments.length>0?arguments[0]:void 0),e=function(n){this===I&&e.call(D,n),o(this,V)&&o(this[V],t)&&(this[V][t]=!1),G(this,t,w(1,n))};return i&&H&&G(I,t,{configurable:!0,set:e}),U(t)},s(L[E],"toString",function(){return this._k}),_.f=Q,j.f=J,n(36).f=O.f=Z,n(20).f=q,n(37).f=X,i&&!n(19)&&s(I,"propertyIsEnumerable",q,!0),h.f=function(t){return U(d(t))}),a(a.G+a.W+a.F*!R,{Symbol:L});for(var tt="hasInstance,isConcatSpreadable,iterator,match,replace,search,species,split,toPrimitive,toStringTag,unscopables".split(","),et=0;tt.length>et;)d(tt[et++]);for(var nt=P(d.store),rt=0;nt.length>rt;)b(nt[rt++]);a(a.S+a.F*!R,"Symbol",{for:function(t){return o(B,t+="")?B[t]:B[t]=L(t)},keyFor:function(t){if(!W(t))throw TypeError(t+" is not a symbol!");for(var e in B)if(B[e]===t)return e},useSetter:function(){H=!0},useSimple:function(){H=!1}}),a(a.S+a.F*!R,"Object",{create:Y,defineProperty:J,defineProperties:K,getOwnPropertyDescriptor:Q,getOwnPropertyNames:Z,getOwnPropertySymbols:X}),C&&a(a.S+a.F*(!R||l(function(){var t=L();return"[null]"!=T([t])||"{}"!=T({a:t})||"{}"!=T(Object(t))})),"JSON",{stringify:function(t){if(void 0!==t&&!W(t)){for(var e,n,r=[t],o=1;arguments.length>o;)r.push(arguments[o++]);return e=r[1],"function"==typeof e&&(n=e),!n&&g(e)||(e=function(t,e){if(n&&(e=n.call(this,t,e)),!W(e))return e}),r[1]=e,T.apply(C,r)}}}),L[E][F]||n(6)(L[E],F,L[E].valueOf),f(L,"Symbol"),f(Math,"Math",!0),f(r.JSON,"JSON",!0)},function(t,e,n){n(26)("asyncIterator")},function(t,e,n){n(26)("observable")},function(t,e,n){n(72);for(var r=n(1),o=n(6),i=n(18),a=n(8)("toStringTag"),s="CSSRuleList,CSSStyleDeclaration,CSSValueList,ClientRectList,DOMRectList,DOMStringList,DOMTokenList,DataTransferItemList,FileList,HTMLAllCollection,HTMLCollection,HTMLFormElement,HTMLSelectElement,MediaList,MimeTypeArray,NamedNodeMap,NodeList,PaintRequestList,Plugin,PluginArray,SVGLengthList,SVGNumberList,SVGPathSegList,SVGPointList,SVGStringList,SVGTransformList,SourceBufferList,StyleSheetList,TextTrackCueList,TextTrackList,TouchList".split(","),u=0;u<s.length;u++){var l=s[u],c=r[l],f=c&&c.prototype;f&&!f[a]&&o(f,a,l),i[l]=i.Array}},function(t,e,n){e=t.exports=n(82)(),e.push([t.id,'.v-select{position:relative;font-family:sans-serif}.v-select .disabled{cursor:not-allowed!important;background-color:#f8f8f8!important}.v-select,.v-select *{box-sizing:border-box}.v-select.rtl .open-indicator{left:10px;right:auto}.v-select.rtl .selected-tag{float:right;margin-right:3px;margin-left:1px}.v-select.rtl .dropdown-menu{text-align:right}.v-select .open-indicator{position:absolute;bottom:6px;right:10px;cursor:pointer;pointer-events:all;opacity:1;height:20px}.v-select .open-indicator,.v-select .open-indicator:before{display:inline-block;transition:all .15s cubic-bezier(1,-.115,.975,.855);transition-timing-function:cubic-bezier(1,-.115,.975,.855);width:10px}.v-select .open-indicator:before{border-color:rgba(60,60,60,.5);border-style:solid;border-width:3px 3px 0 0;content:"";height:10px;vertical-align:top;transform:rotate(133deg);box-sizing:inherit}.v-select.open .open-indicator:before{transform:rotate(315deg)}.v-select.loading .open-indicator{opacity:0}.v-select.open .open-indicator{bottom:1px}.v-select .dropdown-toggle{-webkit-appearance:none;-moz-appearance:none;appearance:none;display:block;padding:0;background:none;border:1px solid rgba(60,60,60,.26);border-radius:4px;white-space:normal}.v-select .dropdown-toggle:after{visibility:hidden;display:block;font-size:0;content:" ";clear:both;height:0}.v-select.searchable .dropdown-toggle{cursor:text}.v-select.unsearchable .dropdown-toggle{cursor:pointer}.v-select.open .dropdown-toggle{border-bottom-color:transparent;border-bottom-left-radius:0;border-bottom-right-radius:0}.v-select .dropdown-menu{display:block;position:absolute;top:100%;left:0;z-index:1000;min-width:160px;padding:5px 0;margin:0;width:100%;overflow-y:scroll;border:1px solid rgba(0,0,0,.26);box-shadow:0 3px 6px 0 rgba(0,0,0,.15);border-top:none;border-radius:0 0 4px 4px;text-align:left;list-style:none;background:#fff}.v-select .no-options{text-align:center}.v-select .selected-tag{color:#333;background-color:#f0f0f0;border:1px solid #ccc;border-radius:4px;height:26px;margin:4px 1px 0 3px;padding:1px .25em;float:left;line-height:24px}.v-select.single .selected-tag{background-color:transparent;border-color:transparent}.v-select.single.open .selected-tag{position:absolute;opacity:.5}.v-select.single.loading .selected-tag,.v-select.single.open.searching .selected-tag{display:none}.v-select .selected-tag .close{float:none;margin-right:0;font-size:20px;appearance:none;padding:0;cursor:pointer;background:0 0;border:0;font-weight:700;line-height:1;color:#000;text-shadow:0 1px 0 #fff;filter:alpha(opacity=20);opacity:.2}.v-select.single.searching:not(.open):not(.loading) input[type=search]{opacity:.2}.v-select input[type=search]::-webkit-search-cancel-button,.v-select input[type=search]::-webkit-search-decoration,.v-select input[type=search]::-webkit-search-results-button,.v-select input[type=search]::-webkit-search-results-decoration{display:none}.v-select input[type=search]::-ms-clear{display:none}.v-select input[type=search],.v-select input[type=search]:focus{appearance:none;-webkit-appearance:none;-moz-appearance:none;line-height:1.42857143;font-size:1em;height:34px;display:inline-block;border:none;outline:none;margin:0;padding:0 .5em;width:10em;max-width:100%;background:none;position:relative;box-shadow:none;float:left;clear:none}.v-select li{line-height:1.42857143}.v-select li>a{display:block;padding:3px 20px;clear:both;color:#333;white-space:nowrap}.v-select li:hover{cursor:pointer}.v-select .dropdown-menu .active>a{color:#333;background:rgba(50,50,50,.1)}.v-select .dropdown-menu>.highlight>a{background:#5897fb;color:#fff}.v-select .highlight:not(:last-child){margin-bottom:0}.v-select .spinner{opacity:0;position:absolute;top:5px;right:10px;font-size:5px;text-indent:-9999em;overflow:hidden;border-top:.9em solid hsla(0,0%,39%,.1);border-right:.9em solid hsla(0,0%,39%,.1);border-bottom:.9em solid hsla(0,0%,39%,.1);border-left:.9em solid rgba(60,60,60,.45);transform:translateZ(0);animation:vSelectSpinner 1.1s infinite linear;transition:opacity .1s}.v-select .spinner,.v-select .spinner:after{border-radius:50%;width:5em;height:5em}.v-select.loading .spinner{opacity:1}@-webkit-keyframes vSelectSpinner{0%{transform:rotate(0deg)}to{transform:rotate(1turn)}}@keyframes vSelectSpinner{0%{transform:rotate(0deg)}to{transform:rotate(1turn)}}.fade-enter-active,.fade-leave-active{transition:opacity .15s cubic-bezier(1,.5,.8,1)}.fade-enter,.fade-leave-to{opacity:0}',""])},function(t,e){t.exports=function(){var t=[];return t.toString=function(){for(var t=[],e=0;e<this.length;e++){var n=this[e];n[2]?t.push("@media "+n[2]+"{"+n[1]+"}"):t.push(n[1])}return t.join("")},t.i=function(e,n){"string"==typeof e&&(e=[[null,e,""]]);for(var r={},o=0;o<this.length;o++){var i=this[o][0];"number"==typeof i&&(r[i]=!0)}for(o=0;o<e.length;o++){var a=e[o];"number"==typeof a[0]&&r[a[0]]||(n&&!a[2]?a[2]=n:n&&(a[2]="("+a[2]+") and ("+n+")"),t.push(a))}},t}},function(t,e,n){n(87);var r=n(84)(n(41),n(85),null,null);t.exports=r.exports},function(t,e){t.exports=function(t,e,n,r){var o,i=t=t||{},a=typeof t.default;"object"!==a&&"function"!==a||(o=t,i=t.default);var s="function"==typeof i?i.options:i;if(e&&(s.render=e.render,s.staticRenderFns=e.staticRenderFns),n&&(s._scopeId=n),r){var u=s.computed||(s.computed={});Object.keys(r).forEach(function(t){var e=r[t];u[t]=function(){return e}})}return{esModule:o,exports:i,options:s}}},function(t,e){t.exports={render:function(){var t=this,e=t.$createElement,n=t._self._c||e;return n("div",{staticClass:"dropdown v-select",class:t.dropdownClasses,attrs:{dir:t.dir}},[n("div",{ref:"toggle",class:["dropdown-toggle","clearfix",{disabled:t.disabled}],on:{mousedown:function(e){e.preventDefault(),t.toggleDropdown(e)}}},[t._l(t.valueAsArray,function(e){return n("span",{key:e.index,staticClass:"selected-tag"},[t._t("selected-option",[t._v("\n        "+t._s(t.getOptionLabel(e))+"\n      ")],null,e),t._v(" "),t.multiple?n("button",{staticClass:"close",attrs:{type:"button","aria-label":"Remove option"},on:{click:function(n){t.deselect(e)}}},[n("span",{attrs:{"aria-hidden":"true"}},[t._v("×")])]):t._e()],2)}),t._v(" "),n("input",{directives:[{name:"model",rawName:"v-model",value:t.search,expression:"search"}],ref:"search",class:[{disabled:t.disabled},"form-control"],style:{width:t.isValueEmpty?"100%":"auto"},attrs:{type:"search",placeholder:t.searchPlaceholder,readonly:!t.searchable,id:t.inputId,"aria-label":"Search for option"},domProps:{value:t.search},on:{keydown:[function(e){return"button"in e||!t._k(e.keyCode,"delete",[8,46])?void t.maybeDeleteValue(e):null},function(e){return"button"in e||!t._k(e.keyCode,"up",38)?(e.preventDefault(),void t.typeAheadUp(e)):null},function(e){return"button"in e||!t._k(e.keyCode,"down",40)?(e.preventDefault(),void t.typeAheadDown(e)):null},function(e){return"button"in e||!t._k(e.keyCode,"enter",13)?(e.preventDefault(),void t.typeAheadSelect(e)):null}],keyup:function(e){return"button"in e||!t._k(e.keyCode,"esc",27)?void t.onEscape(e):null},blur:t.onSearchBlur,focus:t.onSearchFocus,input:function(e){e.target.composing||(t.search=e.target.value)}}}),t._v(" "),t.noDrop?t._e():n("i",{ref:"openIndicator",class:[{disabled:t.disabled},"open-indicator"],attrs:{role:"presentation"}}),t._v(" "),t._t("spinner",[n("div",{directives:[{name:"show",rawName:"v-show",value:t.mutableLoading,expression:"mutableLoading"}],staticClass:"spinner"},[t._v("Loading...")])])],2),t._v(" "),n("transition",{attrs:{name:t.transition}},[t.dropdownOpen?n("ul",{ref:"dropdownMenu",staticClass:"dropdown-menu",style:{"max-height":t.maxHeight}},[t._l(t.filteredOptions,function(e,r){return n("li",{key:r,class:{active:t.isOptionSelected(e),highlight:r===t.typeAheadPointer},on:{mouseover:function(e){t.typeAheadPointer=r}}},[n("a",{on:{mousedown:function(n){n.preventDefault(),t.select(e)}}},[t._t("option",[t._v("\n          "+t._s(t.getOptionLabel(e))+"\n        ")],null,e)],2)])}),t._v(" "),t.filteredOptions.length?t._e():n("li",{
staticClass:"no-options"},[t._t("no-options",[t._v("Sorry, no matching options.")])],2)],2):t._e()])],1)},staticRenderFns:[]}},function(t,e,n){function r(t,e){for(var n=0;n<t.length;n++){var r=t[n],o=f[r.id];if(o){o.refs++;for(var i=0;i<o.parts.length;i++)o.parts[i](r.parts[i]);for(;i<r.parts.length;i++)o.parts.push(u(r.parts[i],e))}else{for(var a=[],i=0;i<r.parts.length;i++)a.push(u(r.parts[i],e));f[r.id]={id:r.id,refs:1,parts:a}}}}function o(t){for(var e=[],n={},r=0;r<t.length;r++){var o=t[r],i=o[0],a=o[1],s=o[2],u=o[3],l={css:a,media:s,sourceMap:u};n[i]?n[i].parts.push(l):e.push(n[i]={id:i,parts:[l]})}return e}function i(t,e){var n=h(),r=g[g.length-1];if("top"===t.insertAt)r?r.nextSibling?n.insertBefore(e,r.nextSibling):n.appendChild(e):n.insertBefore(e,n.firstChild),g.push(e);else{if("bottom"!==t.insertAt)throw new Error("Invalid value for parameter 'insertAt'. Must be 'top' or 'bottom'.");n.appendChild(e)}}function a(t){t.parentNode.removeChild(t);var e=g.indexOf(t);e>=0&&g.splice(e,1)}function s(t){var e=document.createElement("style");return e.type="text/css",i(t,e),e}function u(t,e){var n,r,o;if(e.singleton){var i=v++;n=b||(b=s(e)),r=l.bind(null,n,i,!1),o=l.bind(null,n,i,!0)}else n=s(e),r=c.bind(null,n),o=function(){a(n)};return r(t),function(e){if(e){if(e.css===t.css&&e.media===t.media&&e.sourceMap===t.sourceMap)return;r(t=e)}else o()}}function l(t,e,n,r){var o=n?"":r.css;if(t.styleSheet)t.styleSheet.cssText=y(e,o);else{var i=document.createTextNode(o),a=t.childNodes;a[e]&&t.removeChild(a[e]),a.length?t.insertBefore(i,a[e]):t.appendChild(i)}}function c(t,e){var n=e.css,r=e.media,o=e.sourceMap;if(r&&t.setAttribute("media",r),o&&(n+="\n/*# sourceURL="+o.sources[0]+" */",n+="\n/*# sourceMappingURL=data:application/json;base64,"+btoa(unescape(encodeURIComponent(JSON.stringify(o))))+" */"),t.styleSheet)t.styleSheet.cssText=n;else{for(;t.firstChild;)t.removeChild(t.firstChild);t.appendChild(document.createTextNode(n))}}var f={},p=function(t){var e;return function(){return"undefined"==typeof e&&(e=t.apply(this,arguments)),e}},d=p(function(){return/msie [6-9]\b/.test(window.navigator.userAgent.toLowerCase())}),h=p(function(){return document.head||document.getElementsByTagName("head")[0]}),b=null,v=0,g=[];t.exports=function(t,e){e=e||{},"undefined"==typeof e.singleton&&(e.singleton=d()),"undefined"==typeof e.insertAt&&(e.insertAt="bottom");var n=o(t);return r(n,e),function(t){for(var i=[],a=0;a<n.length;a++){var s=n[a],u=f[s.id];u.refs--,i.push(u)}if(t){var l=o(t);r(l,e)}for(var a=0;a<i.length;a++){var u=i[a];if(0===u.refs){for(var c=0;c<u.parts.length;c++)u.parts[c]();delete f[u.id]}}}};var y=function(){var t=[];return function(e,n){return t[e]=n,t.filter(Boolean).join("\n")}}()},function(t,e,n){var r=n(81);"string"==typeof r&&(r=[[t.id,r,""]]);n(86)(r,{});r.locals&&(t.exports=r.locals)}])});
//# sourceMappingURL=vue-select.js.map

/***/ }),

/***/ "./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js?sourceMap!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-401ee94c\",\"scoped\":false,\"hasInlineConfig\":true}!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/js/components/client/MerchandisePage.vue":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("./node_modules/css-loader/index.js?sourceMap!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-401ee94c\",\"scoped\":false,\"hasInlineConfig\":true}!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/js/components/client/MerchandisePage.vue");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__("./node_modules/vue-style-loader/lib/addStylesClient.js")("73ad1c86", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js?sourceMap!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-401ee94c\",\"scoped\":false,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./MerchandisePage.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js?sourceMap!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-401ee94c\",\"scoped\":false,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./MerchandisePage.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./node_modules/vue-style-loader/lib/addStylesClient.js":
/***/ (function(module, exports, __webpack_require__) {

/*
  MIT License http://www.opensource.org/licenses/mit-license.php
  Author Tobias Koppers @sokra
  Modified by Evan You @yyx990803
*/

var hasDocument = typeof document !== 'undefined'

if (typeof DEBUG !== 'undefined' && DEBUG) {
  if (!hasDocument) {
    throw new Error(
    'vue-style-loader cannot be used in a non-browser environment. ' +
    "Use { target: 'node' } in your Webpack config to indicate a server-rendering environment."
  ) }
}

var listToStyles = __webpack_require__("./node_modules/vue-style-loader/lib/listToStyles.js")

/*
type StyleObject = {
  id: number;
  parts: Array<StyleObjectPart>
}

type StyleObjectPart = {
  css: string;
  media: string;
  sourceMap: ?string
}
*/

var stylesInDom = {/*
  [id: number]: {
    id: number,
    refs: number,
    parts: Array<(obj?: StyleObjectPart) => void>
  }
*/}

var head = hasDocument && (document.head || document.getElementsByTagName('head')[0])
var singletonElement = null
var singletonCounter = 0
var isProduction = false
var noop = function () {}

// Force single-tag solution on IE6-9, which has a hard limit on the # of <style>
// tags it will allow on a page
var isOldIE = typeof navigator !== 'undefined' && /msie [6-9]\b/.test(navigator.userAgent.toLowerCase())

module.exports = function (parentId, list, _isProduction) {
  isProduction = _isProduction

  var styles = listToStyles(parentId, list)
  addStylesToDom(styles)

  return function update (newList) {
    var mayRemove = []
    for (var i = 0; i < styles.length; i++) {
      var item = styles[i]
      var domStyle = stylesInDom[item.id]
      domStyle.refs--
      mayRemove.push(domStyle)
    }
    if (newList) {
      styles = listToStyles(parentId, newList)
      addStylesToDom(styles)
    } else {
      styles = []
    }
    for (var i = 0; i < mayRemove.length; i++) {
      var domStyle = mayRemove[i]
      if (domStyle.refs === 0) {
        for (var j = 0; j < domStyle.parts.length; j++) {
          domStyle.parts[j]()
        }
        delete stylesInDom[domStyle.id]
      }
    }
  }
}

function addStylesToDom (styles /* Array<StyleObject> */) {
  for (var i = 0; i < styles.length; i++) {
    var item = styles[i]
    var domStyle = stylesInDom[item.id]
    if (domStyle) {
      domStyle.refs++
      for (var j = 0; j < domStyle.parts.length; j++) {
        domStyle.parts[j](item.parts[j])
      }
      for (; j < item.parts.length; j++) {
        domStyle.parts.push(addStyle(item.parts[j]))
      }
      if (domStyle.parts.length > item.parts.length) {
        domStyle.parts.length = item.parts.length
      }
    } else {
      var parts = []
      for (var j = 0; j < item.parts.length; j++) {
        parts.push(addStyle(item.parts[j]))
      }
      stylesInDom[item.id] = { id: item.id, refs: 1, parts: parts }
    }
  }
}

function createStyleElement () {
  var styleElement = document.createElement('style')
  styleElement.type = 'text/css'
  head.appendChild(styleElement)
  return styleElement
}

function addStyle (obj /* StyleObjectPart */) {
  var update, remove
  var styleElement = document.querySelector('style[data-vue-ssr-id~="' + obj.id + '"]')

  if (styleElement) {
    if (isProduction) {
      // has SSR styles and in production mode.
      // simply do nothing.
      return noop
    } else {
      // has SSR styles but in dev mode.
      // for some reason Chrome can't handle source map in server-rendered
      // style tags - source maps in <style> only works if the style tag is
      // created and inserted dynamically. So we remove the server rendered
      // styles and inject new ones.
      styleElement.parentNode.removeChild(styleElement)
    }
  }

  if (isOldIE) {
    // use singleton mode for IE9.
    var styleIndex = singletonCounter++
    styleElement = singletonElement || (singletonElement = createStyleElement())
    update = applyToSingletonTag.bind(null, styleElement, styleIndex, false)
    remove = applyToSingletonTag.bind(null, styleElement, styleIndex, true)
  } else {
    // use multi-style-tag mode in all other cases
    styleElement = createStyleElement()
    update = applyToTag.bind(null, styleElement)
    remove = function () {
      styleElement.parentNode.removeChild(styleElement)
    }
  }

  update(obj)

  return function updateStyle (newObj /* StyleObjectPart */) {
    if (newObj) {
      if (newObj.css === obj.css &&
          newObj.media === obj.media &&
          newObj.sourceMap === obj.sourceMap) {
        return
      }
      update(obj = newObj)
    } else {
      remove()
    }
  }
}

var replaceText = (function () {
  var textStore = []

  return function (index, replacement) {
    textStore[index] = replacement
    return textStore.filter(Boolean).join('\n')
  }
})()

function applyToSingletonTag (styleElement, index, remove, obj) {
  var css = remove ? '' : obj.css

  if (styleElement.styleSheet) {
    styleElement.styleSheet.cssText = replaceText(index, css)
  } else {
    var cssNode = document.createTextNode(css)
    var childNodes = styleElement.childNodes
    if (childNodes[index]) styleElement.removeChild(childNodes[index])
    if (childNodes.length) {
      styleElement.insertBefore(cssNode, childNodes[index])
    } else {
      styleElement.appendChild(cssNode)
    }
  }
}

function applyToTag (styleElement, obj) {
  var css = obj.css
  var media = obj.media
  var sourceMap = obj.sourceMap

  if (media) {
    styleElement.setAttribute('media', media)
  }

  if (sourceMap) {
    // https://developer.chrome.com/devtools/docs/javascript-debugging
    // this makes source maps inside style tags work properly in Chrome
    css += '\n/*# sourceURL=' + sourceMap.sources[0] + ' */'
    // http://stackoverflow.com/a/26603875
    css += '\n/*# sourceMappingURL=data:application/json;base64,' + btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))) + ' */'
  }

  if (styleElement.styleSheet) {
    styleElement.styleSheet.cssText = css
  } else {
    while (styleElement.firstChild) {
      styleElement.removeChild(styleElement.firstChild)
    }
    styleElement.appendChild(document.createTextNode(css))
  }
}


/***/ }),

/***/ "./node_modules/vue-style-loader/lib/listToStyles.js":
/***/ (function(module, exports) {

/**
 * Translates the list format produced by css-loader into something
 * easier to manipulate.
 */
module.exports = function listToStyles (parentId, list) {
  var styles = []
  var newStyles = {}
  for (var i = 0; i < list.length; i++) {
    var item = list[i]
    var id = item[0]
    var css = item[1]
    var media = item[2]
    var sourceMap = item[3]
    var part = {
      id: parentId + ':' + i,
      css: css,
      media: media,
      sourceMap: sourceMap
    }
    if (!newStyles[id]) {
      styles.push(newStyles[id] = { id: id, parts: [part] })
    } else {
      newStyles[id].parts.push(part)
    }
  }
  return styles
}


/***/ }),

/***/ "./resources/assets/js/Models/Event.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Form_js__ = __webpack_require__("./resources/assets/js/Models/Form.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__Validator_js__ = __webpack_require__("./resources/assets/js/Models/Validator.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__Validator_js___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__Validator_js__);



/* harmony default export */ __webpack_exports__["a"] = ({

    params: {
        title: "",
        time: "",
        venue: "",
        description: "",
        category: ""

    },
    events: [],
    find: function find(id) {
        var response = {};
        this.events.map(function (x) {
            if (x.id == id) {
                Object.assign(response, x);
            }
        });
        return response;
    },
    all: function all() {
        var _this = this;

        return new Promise(function (res, rej) {
            axios.get('/cm-user/view-events').then(function (response) {
                _this.events = response.data;
                res(response);
            }).catch(function (error) {
                console.log('there was an error getting the file');
                rej(error);
            });
        });
    },
    Deletor: function Deletor(eventID) {
        var _this2 = this;

        axios.post('/cm-user/delete/users', { 'id': eventID }).then(function () {
            _this2.all();
        }).catch(function (error) {
            console.log('there was error deleting the event');
        });
    },
    Updator: function Updator(data) {
        $this.validate().then(function (success) {
            if (success) {
                axios.post('/cm-user/events/update', data).then(function (res) {}).catch(function (err) {});
            }
        });
    },
    Creator: function Creator(data) {}
});

/***/ }),

/***/ "./resources/assets/js/Models/Form.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Form; });
/* unused harmony export FormErrors */
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var image = null;

var Form = function () {
	function Form(data) {
		var _this = this;

		_classCallCheck(this, Form);

		this.originalData = _.extend({}, data);
		_.map(function (value, field) {
			return _this[field] = value;
		});
		this.errors = new FormErrors();
	}

	_createClass(Form, [{
		key: 'data',
		value: function data() {
			var data = _.extend({}, this);
			var formData = new FormData('client-form');
			delete data.originalData;
			delete data.errors;
			_.map(data, function (value, key) {
				return formData.append(key, value);
			});
			return formData;
		}
	}, {
		key: 'persist',
		value: function persist(image) {
			this.image = image;
			var realImage = this.image;
			console.log(this.data());
		}
	}, {
		key: 'reset',
		value: function reset() {

			for (var field in this.originalData) {
				this[field] = '';
			}
			this.errors.clear();
		}
	}, {
		key: 'post',
		value: function post(url) {
			this.submit('post', url);
		}
	}, {
		key: 'patch',
		value: function patch(url) {
			this.submit('patch', url);
		}
	}, {
		key: 'delete',
		value: function _delete(url) {
			this.submit('delete', url);
		}
	}, {
		key: 'submit',
		value: function submit(requestType, url) {
			var _this2 = this;

			return new Promise(function (resolve, reject) {
				axios[requestType](url, _this2.data()).then(function (response) {
					_this2.onSuccess(response.data);
					resolve(response);
				}).catch(function (error) {
					_this2.onFail(error.response.data);
					reject(error);
				});
			});
		}
	}, {
		key: 'onSuccess',
		value: function onSuccess(data) {

			this.reset();
		}
	}, {
		key: 'onFail',
		value: function onFail(errors) {
			this.errors.record(errors);
		}
	}]);

	return Form;
}();

var FormErrors = function () {
	function FormErrors() {
		_classCallCheck(this, FormErrors);

		this.errors = {};
	}

	_createClass(FormErrors, [{
		key: 'get',
		value: function get(field) {
			if (this.errors[field]) {
				return this.errors[field][0];
			}
		}
	}, {
		key: 'has',
		value: function has(field) {
			return this.errors.hasOwnProperty(field);
		}
	}, {
		key: 'any',
		value: function any() {
			return Object.keys(this.errors).length > 0;
		}
	}, {
		key: 'record',
		value: function record(errors) {
			this.errors = errors;
		}
	}, {
		key: 'clear',
		value: function clear(field) {
			if (field) {
				delete this.errors[field];
				return;
			}
			this.errors = {};
		}
	}]);

	return FormErrors;
}();



/***/ }),

/***/ "./resources/assets/js/Models/Merchandise.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Form_js__ = __webpack_require__("./resources/assets/js/Models/Form.js");


var defaults = {
    name: null,
    description: null,
    price: 0,
    category: null,
    image: null,
    quantity: 0
};

/* harmony default export */ __webpack_exports__["a"] = ({
    find: function find(id) {
        return _.extend(this.form, this.merchs[_.findIndex(this.merchs, function (o) {
            return o.id == id;
        })]);
    },

    merchs: [],
    form: new __WEBPACK_IMPORTED_MODULE_0__Form_js__["a" /* Form */](defaults),
    all: function all() {
        var _this = this;

        return new Promise(function (res, rej) {
            _this.form.submit('get', '/api/v1/merchandise').then(function (response) {
                _this.merchs = [].slice.call(response.data);
                res(response);
            }).catch(function (error) {
                console.log('there was an error');
                rej(error.response);
            });
        });
    },
    update: function update(id) {
        console.log(id);
    },
    remove: function remove(merchId) {
        return axios.delete('/api/v1/merchandise/' + merchId);
        return axios.delete('/api/v1/merchandise/' + merchId);
    }
});

/***/ }),

/***/ "./resources/assets/js/Models/Validator.js":
/***/ (function(module, exports) {

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var customValidator = function () {
    function customValidator(labelkey, checkValiidities) {
        _classCallCheck(this, customValidator);

        this.label = document.querySelector("label[for=" + labelkey + "]");
        this.inputNode = this.label.querySelector("input");
        this.elem_list = this.label.querySelector(".input-requirement li:nth-child(1)");
        this.validities = [];
        this.checkValiidities = checkValiidities;
        this.registersListeners();
    }

    _createClass(customValidator, [{
        key: "addValidities",
        value: function addValidities(message) {
            this.validities.push(message);
        }
    }, {
        key: "getValiidities",
        value: function getValiidities() {
            return this.valiidities;
        }
    }, {
        key: "checkValiidities",
        value: function checkValiidities() {
            for (var i = 0; i < this.checkValiidities.length; i++) {
                var isInvalid = this.checkValiidities[i].isEmpty(this.inputNode);
                if (isInvalid) {
                    this.addValidities(this.checkValiidities[i].invalidityMessage);
                    this.elem_list.classList.remove('valid');
                    this.elem_list.classList.add('invalid');
                } else {
                    this.elem_list.classList.remove('invalid');
                    this.elem_list.classList.add('valid');
                }
            }
        }
    }, {
        key: "checkInput",
        value: function checkInput() {
            if (this.inputNode.value != '') {
                this.inputNode.setCustomValidity('');
            } else {
                var msg = this.getValiidities();
                this.inputNode.setCustomValidity(msg);
            }
            this.checkValiidities();
        }
    }, {
        key: "registersListeners",
        value: function registersListeners() {
            var customValidator = this;
            this.inputNode.addEventListener('keyup', function () {
                customValidator.checkInput();
            });
        }
    }]);

    return customValidator;
}();

/***/ }),

/***/ "./resources/assets/js/cm-dashboard.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_client_EventsPage_vue__ = __webpack_require__("./resources/assets/js/components/client/EventsPage.vue");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_client_EventsPage_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__components_client_EventsPage_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__components_client_MerchandisePage_vue__ = __webpack_require__("./resources/assets/js/components/client/MerchandisePage.vue");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__components_client_MerchandisePage_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__components_client_MerchandisePage_vue__);



window.cat = {
    state: 3
};
var App = new Vue({
    el: "#client",
    components: { EventsPage: __WEBPACK_IMPORTED_MODULE_0__components_client_EventsPage_vue___default.a, MerchandisePage: __WEBPACK_IMPORTED_MODULE_1__components_client_MerchandisePage_vue___default.a },
    data: {
        slideState: 0,
        showAddForm: false,
        shopCategories: _.toArray(window.ShopCategories)
    },
    methods: {}
});

var AppConfig = {
    Host: null
};
(function ($) {
    AppConfig.Host = "//" + window.location.host + "/cm-user/";
    load_dependencies();
})(jQuery);

function load_dependencies() {
    console.log('loaded');
    if ($('.chosen-select')[0]) {
        $('.chosen-select').chosen({});
    }
}

//navbar-top menu button event
var menuButton = {
    init: function init() {
        var _menuBar = this.self = $('#menu-button').first(),
            _sideNav = NavBar.self = $('#client-sidebar'),
            _parentCont = $('#main-col'),
            _parent = this;
        var state = _parent.state;
        state.active = parseInt(Cookie.getCookie('menuState'));
        _menuBar.on('click', function () {
            state.active = state.active === 1 ? 0 : 1;
            if (state.active === 1) {
                console.trace(_sideNav);
                _menuBar.addClass('active');
                _sideNav.addClass('col-slide-hide');
                _parentCont.removeClass('col-lg-offset-2');
            } else {
                _menuBar.removeClass('active');
                _sideNav.removeClass('col-slide-hide');
                _parentCont.addClass('col-lg-offset-2');
            }
            console.log(Cookie.getCookie('menuState'), state.active);
            $(this).toggleClass('active');
            document.cookie = "menuState=" + state.active + "";
        });
    },
    state: { active: false }
};
//
var NavBar = {
    self: null,
    init: function init() {
        var Navigation = this.self;
        Navigation.children('ul').delegate('li', 'click', function () {
            var _subNav = $(this).next();
            if (_subNav[0].tagName === "UL") {
                _subNav.toggleClass('collapse');
            }
        });
    }
};

var Cookie = {
    loadCookie: function loadCookie() {
        var DocCookie = document.cookie,
            CookieArray = [];
        this.cookies = DocCookie.split(/\s/g);
        for (var _i = 0, _a = this.cookies; _i < _a.length; _i++) {
            var cookie = _a[_i];
            var _b = cookie.split(/\=/),
                name = _b[0],
                value = _b[1];
            CookieArray.push({ "name": name, "value": value.split(/\;$/)[0] });
        }
        return CookieArray;
    },
    setCookie: function setCookie() {
        console.log('setting cookie');
    },
    getCookie: function getCookie(cookieString) {
        var cookies = this.loadCookie();
        for (var _i = 0, cookies_1 = cookies; _i < cookies_1.length; _i++) {
            var mi = cookies_1[_i];
            if (mi.name == cookieString) {
                return mi.value;
            }
        }
        return false;
    }
};
var Services = {
    init: function init() {
        this.construct();
        var _a = this.props,
            list = _a.list,
            EditBox = _a.EditBox,
            ServiceForm = _a.ServiceForm,
            delLink = _a.delLink;
        var _hide = "collapse";
        var list_item = list.find(".list-group-item");
        function getParent(x) {
            return $(x).parents('.list-group-item');
        }
        list_item.map(function () {
            $(this).removeClass(_hide);
        });
        list.delegate("button[data-edit]", "click", function (evt) {
            var _parent = getParent(this),
                _xerox = _parent.attr("data-xerox");
            showLoader(evt);
            EditBox.load(AppConfig.Host + "services/" + _xerox, function (sonct, xhr) {
                removeLoader();
                var image = new ImageEdit('input[name=imageColl]', '.drag-drop');
                ServiceForm.addClass(_hide);
                EditBox.scrollTo(100);
            });
        });
        list.delegate("button[data-del]", "click", function () {
            var parentList = getParent(this),
                _serviceXerox = parentList.attr("data-xerox"),
                _confirm = confirm("Delete ?");
            if (_confirm) $.post(delLink, { _token: Laravel.csrfToken, activity: "services", serviceXerox: _serviceXerox }, function (xhr) {
                if (xhr.code === true) {
                    alert("Deleted");
                    parentList.addClass(_hide);
                }
            }, "json");
        });
    },
    construct: function construct() {
        this.props = {
            list: $("#service-list"),
            EditBox: $('#edit-services'),
            ServiceForm: $("#register__service"),
            delLink: AppConfig.Host + "services/delete"
        };
    }
};
var AdvertsPage = {
    init: function init() {
        this.constructor();
        var _hide = "collapse";
        var _a = this.props,
            Form = _a.Form,
            EditPanel = _a.EditPanel,
            EditPage = _a.EditPage,
            AdvertBox = _a.AdvertBox,
            panelBody = _a.panelBody,
            AdvertBoxes = _a.AdvertBoxes;
        AdvertBox.find("button[data-collapse]").click(function () {
            var i = $(this).find("i"),
                rm = i.attr("data-toggle");
            if (Form.hasClass(_hide)) {
                i.removeClass(rm);
                Form.removeClass(_hide);
                if (window.innerWidth > 1200) {
                    AdvertBox.removeClass("expand");
                }
            } else {
                i.addClass(rm);
                Form.addClass(_hide);
                if (window.innerWidth > 1200) {
                    AdvertBox.addClass("expand");
                }
            }
        });
        AdvertBoxes.delegate('[data-edit]', 'click', function (evt) {
            if (evt.ctrlKey == true) {
                evt.preventDefault();
                evt.stopPropagation();
                var xerox = $(this).attr("data-edit");
                var link = $(this).attr("href");
                EditPanel.removeClass(_hide);
                EditPanel.load(link, showLoader);
                EditPanel.scrollTo(89);
            }
        });
        AdvertBox.delegate('[data-del]', 'click', function (evt) {
            var _self = $(this),
                _xerox = _self.attr("data-del"),
                _advertPreview = _self.parents(".adverts-preview");
            if (confirm("Delete?")) {
                showLoader(evt);
                $.post(EditPage + "/delete", { advertXerox: _xerox, deleteSubmit: 10, _token: window.Laravel.csrfToken }, function (xhr) {
                    if (xhr.code === 200) {
                        _advertPreview.addClass("collapse");
                        removeLoader();
                        EditPanel.addClass('_hide');
                    }
                });
            }
        });
        EditPanel.delegate("span[data-collapse]", "click", function () {
            EditPanel.addClass(_hide);
        });
    },
    constructor: function constructor() {
        var Form = $("#add_advert"),
            AdvertBox = $("#advert-box"),
            editPanel = $("#edit-advert"),
            _a = window.location,
            pathname = _a.pathname,
            hash = _a.hash;
        this.props = {
            Form: Form,
            EditPage: pathname,
            AdvertBox: AdvertBox,
            EditPanel: editPanel,
            AdvertBoxes: AdvertBox.find(".panel")
        };
    }
};
var Account = {
    init: function init() {
        this.construct();
        var _a = this.props,
            BasicPanel = _a.BasicPanel,
            PPanel = _a.PPanel,
            UpdatePanel = _a.UpdatePanel,
            ModifyBtn = _a.ModifyBtn,
            ChangePass = _a.ChangePass;
        if (BasicPanel.length !== 0) {
            ModifyBtn.click(function () {
                UpdatePanel.toggleClass("collapse").scrollTo(100);
            });
            ChangePass.click(function () {
                PPanel.toggleClass("collapse").scrollTo(100);
            });
        }
        console.log('working on the Account panel');
    },
    construct: function construct() {
        this.props = {
            BasicPanel: $('#account__profile'),
            PPanel: $("#change__password"),
            UpdatePanel: $("#account__update"),
            ModifyBtn: $("#modify"),
            ChangePass: $("#ch-pwd")
        };
    }
};

function showLoader(e, b) {
    if (b === void 0) {
        b = "failed";
    }
    if (arguments.length < 2) {
        console.log('just a simple loader');
    } else {
        console.log('hide loader');
        if (b == "success") {
            console.log('hiding with success');
        } else {
            console.warn('hiding with error');
        }
    }
}

function removeLoader() {
    console.log("removing loader");
}

$(function () {
    menuButton.init();
    Services.init();
    AdvertsPage.init();
    NavBar.init();
    Account.init();
});

/***/ }),

/***/ "./resources/assets/js/components/Modal.vue":
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__("./node_modules/vue-loader/lib/component-normalizer.js")
/* script */
var __vue_script__ = __webpack_require__("./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/js/components/Modal.vue")
/* template */
var __vue_template__ = __webpack_require__("./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-078b02f1\",\"hasScoped\":false}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/js/components/Modal.vue")
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources\\assets\\js\\components\\Modal.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key.substr(0, 2) !== "__"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] Modal.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-078b02f1", Component.options)
  } else {
    hotAPI.reload("data-v-078b02f1", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ "./resources/assets/js/components/client/EventsPage.vue":
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__("./node_modules/vue-loader/lib/component-normalizer.js")
/* script */
var __vue_script__ = __webpack_require__("./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/js/components/client/EventsPage.vue")
/* template */
var __vue_template__ = null
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources\\assets\\js\\components\\client\\EventsPage.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key.substr(0, 2) !== "__"})) {console.error("named exports are not supported in *.vue files.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-7a297f10", Component.options)
  } else {
    hotAPI.reload("data-v-7a297f10", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ "./resources/assets/js/components/client/MerchandisePage.vue":
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__("./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js?sourceMap!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-401ee94c\",\"scoped\":false,\"hasInlineConfig\":true}!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/js/components/client/MerchandisePage.vue")
}
var normalizeComponent = __webpack_require__("./node_modules/vue-loader/lib/component-normalizer.js")
/* script */
var __vue_script__ = __webpack_require__("./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/js/components/client/MerchandisePage.vue")
/* template */
var __vue_template__ = null
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources\\assets\\js\\components\\client\\MerchandisePage.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key.substr(0, 2) !== "__"})) {console.error("named exports are not supported in *.vue files.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-401ee94c", Component.options)
  } else {
    hotAPI.reload("data-v-401ee94c", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 1:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("./resources/assets/js/cm-dashboard.js");


/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAgMmEyNjkxZmFlZjYyMGY0ZDE2MDAiLCJ3ZWJwYWNrOi8vL01vZGFsLnZ1ZSIsIndlYnBhY2s6Ly8vRXZlbnRzUGFnZS52dWUiLCJ3ZWJwYWNrOi8vL01lcmNoYW5kaXNlUGFnZS52dWUiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jb21wb25lbnRzL2NsaWVudC9NZXJjaGFuZGlzZVBhZ2UudnVlP2M3NDEiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvbGliL2Nzcy1iYXNlLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9jb21wb25lbnQtbm9ybWFsaXplci5qcyIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NvbXBvbmVudHMvTW9kYWwudnVlP2M3ZDciLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3Z1ZS1zZWxlY3QvZGlzdC92dWUtc2VsZWN0LmpzIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvY29tcG9uZW50cy9jbGllbnQvTWVyY2hhbmRpc2VQYWdlLnZ1ZT80OTlmIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy92dWUtc3R5bGUtbG9hZGVyL2xpYi9hZGRTdHlsZXNDbGllbnQuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3Z1ZS1zdHlsZS1sb2FkZXIvbGliL2xpc3RUb1N0eWxlcy5qcyIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL01vZGVscy9FdmVudC5qcyIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL01vZGVscy9Gb3JtLmpzIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvTW9kZWxzL01lcmNoYW5kaXNlLmpzIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvTW9kZWxzL1ZhbGlkYXRvci5qcyIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NtLWRhc2hib2FyZC5qcyIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NvbXBvbmVudHMvTW9kYWwudnVlIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvY29tcG9uZW50cy9jbGllbnQvRXZlbnRzUGFnZS52dWUiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jb21wb25lbnRzL2NsaWVudC9NZXJjaGFuZGlzZVBhZ2UudnVlIl0sIm5hbWVzIjpbInBhcmFtcyIsInRpdGxlIiwidGltZSIsInZlbnVlIiwiZGVzY3JpcHRpb24iLCJjYXRlZ29yeSIsImV2ZW50cyIsImZpbmQiLCJpZCIsInJlc3BvbnNlIiwibWFwIiwieCIsIk9iamVjdCIsImFzc2lnbiIsImFsbCIsIlByb21pc2UiLCJyZXMiLCJyZWoiLCJheGlvcyIsImdldCIsInRoZW4iLCJkYXRhIiwiY2F0Y2giLCJlcnJvciIsImNvbnNvbGUiLCJsb2ciLCJEZWxldG9yIiwiZXZlbnRJRCIsInBvc3QiLCJVcGRhdG9yIiwiJHRoaXMiLCJ2YWxpZGF0ZSIsInN1Y2Nlc3MiLCJlcnIiLCJDcmVhdG9yIiwiaW1hZ2UiLCJGb3JtIiwib3JpZ2luYWxEYXRhIiwiXyIsImV4dGVuZCIsInZhbHVlIiwiZmllbGQiLCJlcnJvcnMiLCJGb3JtRXJyb3JzIiwiZm9ybURhdGEiLCJGb3JtRGF0YSIsImtleSIsImFwcGVuZCIsInJlYWxJbWFnZSIsImNsZWFyIiwidXJsIiwic3VibWl0IiwicmVxdWVzdFR5cGUiLCJyZXNvbHZlIiwicmVqZWN0Iiwib25TdWNjZXNzIiwib25GYWlsIiwicmVzZXQiLCJyZWNvcmQiLCJoYXNPd25Qcm9wZXJ0eSIsImtleXMiLCJsZW5ndGgiLCJkZWZhdWx0cyIsIm5hbWUiLCJwcmljZSIsInF1YW50aXR5IiwiZm9ybSIsIm1lcmNocyIsImZpbmRJbmRleCIsIm8iLCJzbGljZSIsImNhbGwiLCJ1cGRhdGUiLCJyZW1vdmUiLCJtZXJjaElkIiwiZGVsZXRlIiwiY3VzdG9tVmFsaWRhdG9yIiwibGFiZWxrZXkiLCJjaGVja1ZhbGlpZGl0aWVzIiwibGFiZWwiLCJkb2N1bWVudCIsInF1ZXJ5U2VsZWN0b3IiLCJpbnB1dE5vZGUiLCJlbGVtX2xpc3QiLCJ2YWxpZGl0aWVzIiwicmVnaXN0ZXJzTGlzdGVuZXJzIiwibWVzc2FnZSIsInB1c2giLCJ2YWxpaWRpdGllcyIsImkiLCJpc0ludmFsaWQiLCJpc0VtcHR5IiwiYWRkVmFsaWRpdGllcyIsImludmFsaWRpdHlNZXNzYWdlIiwiY2xhc3NMaXN0IiwiYWRkIiwic2V0Q3VzdG9tVmFsaWRpdHkiLCJtc2ciLCJnZXRWYWxpaWRpdGllcyIsImFkZEV2ZW50TGlzdGVuZXIiLCJjaGVja0lucHV0Iiwid2luZG93IiwiY2F0Iiwic3RhdGUiLCJBcHAiLCJWdWUiLCJlbCIsImNvbXBvbmVudHMiLCJFdmVudHNQYWdlIiwiTWVyY2hhbmRpc2VQYWdlIiwic2xpZGVTdGF0ZSIsInNob3dBZGRGb3JtIiwic2hvcENhdGVnb3JpZXMiLCJ0b0FycmF5IiwiU2hvcENhdGVnb3JpZXMiLCJtZXRob2RzIiwiQXBwQ29uZmlnIiwiSG9zdCIsIiQiLCJsb2NhdGlvbiIsImhvc3QiLCJsb2FkX2RlcGVuZGVuY2llcyIsImpRdWVyeSIsImNob3NlbiIsIm1lbnVCdXR0b24iLCJpbml0IiwiX21lbnVCYXIiLCJzZWxmIiwiZmlyc3QiLCJfc2lkZU5hdiIsIk5hdkJhciIsIl9wYXJlbnRDb250IiwiX3BhcmVudCIsImFjdGl2ZSIsInBhcnNlSW50IiwiQ29va2llIiwiZ2V0Q29va2llIiwib24iLCJ0cmFjZSIsImFkZENsYXNzIiwicmVtb3ZlQ2xhc3MiLCJ0b2dnbGVDbGFzcyIsImNvb2tpZSIsIk5hdmlnYXRpb24iLCJjaGlsZHJlbiIsImRlbGVnYXRlIiwiX3N1Yk5hdiIsIm5leHQiLCJ0YWdOYW1lIiwibG9hZENvb2tpZSIsIkRvY0Nvb2tpZSIsIkNvb2tpZUFycmF5IiwiY29va2llcyIsInNwbGl0IiwiX2kiLCJfYSIsIl9iIiwic2V0Q29va2llIiwiY29va2llU3RyaW5nIiwiY29va2llc18xIiwibWkiLCJTZXJ2aWNlcyIsImNvbnN0cnVjdCIsInByb3BzIiwibGlzdCIsIkVkaXRCb3giLCJTZXJ2aWNlRm9ybSIsImRlbExpbmsiLCJfaGlkZSIsImxpc3RfaXRlbSIsImdldFBhcmVudCIsInBhcmVudHMiLCJldnQiLCJfeGVyb3giLCJhdHRyIiwic2hvd0xvYWRlciIsImxvYWQiLCJzb25jdCIsInhociIsInJlbW92ZUxvYWRlciIsIkltYWdlRWRpdCIsInNjcm9sbFRvIiwicGFyZW50TGlzdCIsIl9zZXJ2aWNlWGVyb3giLCJfY29uZmlybSIsImNvbmZpcm0iLCJfdG9rZW4iLCJMYXJhdmVsIiwiY3NyZlRva2VuIiwiYWN0aXZpdHkiLCJzZXJ2aWNlWGVyb3giLCJjb2RlIiwiYWxlcnQiLCJBZHZlcnRzUGFnZSIsImNvbnN0cnVjdG9yIiwiRWRpdFBhbmVsIiwiRWRpdFBhZ2UiLCJBZHZlcnRCb3giLCJwYW5lbEJvZHkiLCJBZHZlcnRCb3hlcyIsImNsaWNrIiwicm0iLCJoYXNDbGFzcyIsImlubmVyV2lkdGgiLCJjdHJsS2V5IiwicHJldmVudERlZmF1bHQiLCJzdG9wUHJvcGFnYXRpb24iLCJ4ZXJveCIsImxpbmsiLCJfc2VsZiIsIl9hZHZlcnRQcmV2aWV3IiwiYWR2ZXJ0WGVyb3giLCJkZWxldGVTdWJtaXQiLCJlZGl0UGFuZWwiLCJwYXRobmFtZSIsImhhc2giLCJBY2NvdW50IiwiQmFzaWNQYW5lbCIsIlBQYW5lbCIsIlVwZGF0ZVBhbmVsIiwiTW9kaWZ5QnRuIiwiQ2hhbmdlUGFzcyIsImUiLCJiIiwiYXJndW1lbnRzIiwid2FybiJdLCJtYXBwaW5ncyI6IjtBQUFBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOzs7QUFHQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFLO0FBQ0w7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxtQ0FBMkIsMEJBQTBCLEVBQUU7QUFDdkQseUNBQWlDLGVBQWU7QUFDaEQ7QUFDQTtBQUNBOztBQUVBO0FBQ0EsOERBQXNELCtEQUErRDs7QUFFckg7QUFDQTs7QUFFQTtBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ3BDQTs7UUFHQTtVQUNBO3dCQUNBO1dBR0E7QUFDQTs7OzBCQUVBO3lDQUNBO0FBRUE7QUFKQTs7a0NBTUE7d0JBQ0E7QUFDQTtzQ0FDQTt3QkFDQTtBQUNBO2tDQUNBO3dCQUNBO2lCQUNBO0FBRUE7QUFYQTtBQWJBLEc7Ozs7Ozs7Ozs7Ozs7OztBQzFCQTtBQUNBO0FBR0E7OzswRUFFQTtPQUNBO3VCQUNBOztvRUFFQTtRQUNBO2dCQUNBO2FBQ0E7V0FDQTtXQUNBO1NBQ0E7V0FDQTtrRkFDQTtpRkFJQTs7QUFiQTtBQWNBOzZCQUNBOztXQUVBO2dCQUVBO0FBSEE7QUFJQTs7O2tDQUdBLENBQ0E7d0NBQ0E7eUdBQ0E7QUFDQTs7MENBQ0E7ZUFFQTs7K0NBQ0E7MkRBQ0E7QUFDQTtBQUVBO0FBZEE7O0FBZUE7O21HQUNBO21CQUNBOzRCQUNBOzBCQUNBOzhCQUNBO0FBR0E7QUFDQTs7O29DQUVBOzRCQUVBO0FBQ0E7b0NBQ0E7eUJBQ0E7QUFDQTtvQ0FDQTttQkFDQTtzQkFDQTswR0FFQTtBQUNBO2tDQUNBO21CQUNBOytEQUVBLHlCQUNBO2tDQUNBO0FBQ0E7QUFDQTs0QkFDQTtxQkFDQTt3Q0FDQTtBQUNBO2dDQUNBO21CQUNBO0FBR0E7QUE5QkE7QUFsREEsRzs7Ozs7Ozs7Ozs7Ozs7O0FDTEE7QUFDQTtBQUNBOztBQUVBOztRQUdBOzJFQUNBOztBQUNBOzt1R0FDQTtxQkFDQTs2RkFDQTtBQUNBO0FBQ0E7OEJBQ0EsQ0FDQTt3QkFDQTs7Z0JBRUE7Z0JBQ0E7Y0FDQTtjQUNBO2dCQUNBO2lHQUVBO0FBUEE7QUFRQTs7OzBDQUVBO29GQUNBO0FBRUE7QUFKQTs7K0ZBTUE7NkNBQ0E7aUNBQ0EsT0FDQSxNQUNBLDJCQUNBLFVBQ0EsOEJBR0E7O29DQUNBO3VDQUNBO0FBRUE7OzRDQUNBO0FBQ0E7c0NBQ0E7NEJBQ0E7NEJBQ0E7QUFDQTtzQ0FDQTs0QkFDQTtBQUNBO2tDQUNBO3NCQUNBO0FBQ0E7a0RBQ0E7c0JBQ0E7c0JBQ0E7NEdBQ0E7QUFDQTtnQ0FDQTttQ0FDQTtBQUNBO3NEQUNBO3FGQUNBO0FBRUE7QUF0Q0E7QUExQkEsRzs7Ozs7OztBQ1BBO0FBQ0E7OztBQUdBO0FBQ0EsNENBQTZDLDZCQUE2QixHQUFHLFVBQVUsOEtBQThLLE1BQU0sV0FBVyxtR0FBbUcscUNBQXFDLDBEQUEwRCx3QkFBd0IscURBQXFELGVBQWUsa0JBQWtCLDBDQUEwQywrQkFBK0IsNkNBQTZDLFNBQVMsRUFBRSxPQUFPLGtCQUFrQixPQUFPLGdCQUFnQixnQkFBZ0Isb01BQW9NLE9BQU8sa0JBQWtCLHlCQUF5Qix5SkFBeUosU0FBUyxPQUFPLGlCQUFpQiwyRUFBMkUsdU1BQXVNLDZDQUE2QyxvRUFBb0Usb0VBQW9FLFNBQVMsdUJBQXVCLHlDQUF5Qyx5Q0FBeUMsU0FBUyx1QkFBdUIseUNBQXlDLFNBQVMscUJBQXFCLGdDQUFnQyxTQUFTLCtCQUErQixnQ0FBZ0MsK0JBQStCLHdEQUF3RCxTQUFTLG9CQUFvQiwrQ0FBK0MsU0FBUyxpQ0FBaUMsaUNBQWlDLFNBQVMsT0FBTyxNQUFNLG1EQUFtRCxpQ0FBaUMsT0FBTyxtQ0FBbUM7O0FBRWowRTs7Ozs7Ozs7QUNQQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsbUNBQW1DLGdCQUFnQjtBQUNuRCxJQUFJO0FBQ0o7QUFDQTtBQUNBLEdBQUc7QUFDSDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZ0JBQWdCLGlCQUFpQjtBQUNqQztBQUNBO0FBQ0E7QUFDQTtBQUNBLFlBQVksb0JBQW9CO0FBQ2hDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHOztBQUVIO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLG9EQUFvRCxjQUFjOztBQUVsRTtBQUNBOzs7Ozs7OztBQzNFQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLHlCQUF5QjtBQUN6QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7QUMxRkE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxvQkFBb0Isb0NBQW9DLGlCQUFpQixFQUFFO0FBQzNFLGVBQWUsOEJBQThCO0FBQzdDLGlCQUFpQiwrQkFBK0I7QUFDaEQsbUJBQW1CLDhCQUE4QjtBQUNqRDtBQUNBO0FBQ0E7QUFDQTtBQUNBLHNCQUFzQixpQkFBaUI7QUFDdkM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYTtBQUNiO0FBQ0E7QUFDQTtBQUNBLG9CQUFvQiw2QkFBNkI7QUFDakQ7QUFDQTtBQUNBLG1CQUFtQiw0QkFBNEI7QUFDL0M7QUFDQSxtQkFBbUIsNEJBQTRCO0FBQy9DO0FBQ0EsbUJBQW1CLDhCQUE4QjtBQUNqRDtBQUNBO0FBQ0E7QUFDQTtBQUNBLHNCQUFzQixpQkFBaUI7QUFDdkM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYTtBQUNiO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGtCQUFrQjtBQUNsQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQzs7Ozs7OztBQzFEQSxlQUFlLDJJQUF1TCxpQkFBaUIsbUJBQW1CLGNBQWMsNEJBQTRCLFlBQVksVUFBVSxpQkFBaUIsZ0VBQWdFLFNBQVMsZ0NBQWdDLGtCQUFrQixhQUFhLGNBQWMsMEJBQTBCLFdBQVcsc0NBQXNDLFNBQVMsOEJBQThCLGtDQUFrQyw2REFBNkQsZUFBZSw4SUFBOEksOEJBQThCLGlCQUFpQiwyQkFBMkIsa0NBQWtDLE1BQU0sZUFBZSxVQUFVLElBQUksRUFBRSxlQUFlLFFBQVEsZ0JBQWdCLHdCQUF3QixvQkFBb0IsaUJBQWlCLG9EQUFvRCwrQ0FBK0MsNkJBQTZCLGdCQUFnQixVQUFVLG9FQUFvRSxxQ0FBcUMsZUFBZSxpQkFBaUIsaUJBQWlCLDhCQUE4QixpQkFBaUIsbUJBQW1CLCtCQUErQix1QkFBdUIsaUJBQWlCLGlCQUFpQixpQkFBaUIsb0JBQW9CLHNCQUFzQixnQkFBZ0IsaUJBQWlCLHdGQUF3RixtREFBbUQsVUFBVSxlQUFlLHNCQUFzQixJQUFJLFlBQVksU0FBUyxXQUFXLGlCQUFpQixZQUFZLHNCQUFzQixpREFBaUQsVUFBVSxpQkFBaUIsaUVBQWlFLDhFQUE4RSwrQkFBK0IsS0FBSyxTQUFTLG9JQUFvSSxzQkFBc0Isc0JBQXNCLHlCQUF5QixvQkFBb0IsdUJBQXVCLHlCQUF5QixvQkFBb0IsZ0NBQWdDLG1CQUFtQiw4RUFBOEUscUNBQXFDLGlFQUFpRSxlQUFlLHNCQUFzQix3REFBd0QsaUJBQWlCLG9CQUFvQixtQ0FBbUMsZUFBZSxlQUFlLHdCQUF3QixPQUFPLGdFQUFnRSxlQUFlLHdCQUF3QixzQkFBc0IsbUVBQW1FLGVBQWUsc0JBQXNCLHlEQUF5RCxVQUFVLGVBQWUscUhBQXFILGVBQWUsYUFBYSxlQUFlLGFBQWEsZUFBZSxNQUFNLHNCQUFzQixpQkFBaUIsMENBQTBDLDBCQUEwQixtQ0FBbUMsd0JBQXdCLEdBQUcsaUJBQWlCLDRCQUE0QixzQkFBc0IsMEJBQTBCLGlCQUFpQixrREFBa0QsRUFBRSxzQkFBc0IscUJBQXFCLEdBQUcsZUFBZSw2QkFBNkIsc0JBQXNCLG1DQUFtQyxpQkFBaUIsWUFBWSx3QkFBd0Isa0JBQWtCLFFBQVEsaUVBQWlFLDZEQUE2RCxrRUFBa0UsNERBQTRELGlCQUFpQiwyQ0FBMkMsc0JBQXNCLDhCQUE4QixhQUFhLEVBQUUsaUNBQWlDLGFBQWEsR0FBRyxpQkFBaUIsU0FBUyxlQUFlLGFBQWEsV0FBVyxPQUFPLFNBQVMsd0JBQXdCLFdBQVcsdUNBQXVDLGlCQUFpQixPQUFPLG1CQUFtQixRQUFRLGtCQUFrQiwwSEFBMEgscUJBQXFCLHVCQUF1QixVQUFVLHlCQUF5QixrRUFBa0UsaUZBQWlGLGVBQWUsYUFBYSxXQUFXLE9BQU8sNEJBQTRCLDBCQUEwQixVQUFVLDZCQUE2QiwrREFBK0Qsd0lBQXdJLGlDQUFpQyxRQUFRLHVDQUF1Qyx3QkFBd0Isd0RBQXdELFNBQVMsa0NBQWtDLHNEQUFzRCwwQkFBMEIseUZBQXlGLDBCQUEwQixxQkFBcUIsT0FBTyx5S0FBeUssc0JBQXNCLDJFQUEyRSxlQUFlLGFBQWEsV0FBVyxnQkFBZ0IsT0FBTyxxQkFBcUIsUUFBUSwyQkFBMkIseUJBQXlCLFVBQVUsdUJBQXVCLG9HQUFvRywwQkFBMEIsZ0lBQWdJLDRCQUE0QiwrTUFBK00sZUFBZSxRQUFRLFVBQVUsc0JBQXNCLDhCQUE4QixpQkFBaUIsdURBQXVELHNCQUFzQixnQ0FBZ0MsaUJBQWlCLGtDQUFrQyxrREFBa0QsZUFBZSxVQUFVLElBQUksRUFBRSxpQkFBaUIsYUFBYSw2S0FBNkssYUFBYSxrQ0FBa0MsU0FBUyx3QkFBd0IsMEJBQTBCLFVBQVUseUJBQXlCLHNCQUFzQix5QkFBeUIsc0JBQXNCLGtCQUFrQixzQkFBc0IsbUlBQW1JLDhIQUE4SCxvQkFBb0Isc0RBQXNELHdDQUF3QyxrQ0FBa0MsMkJBQTJCLFVBQVUsaUJBQWlCLDhEQUE4RCw0QkFBNEIsK0NBQStDLGdMQUFnTCxJQUFJLG1CQUFtQixZQUFZLHVDQUF1QyxNQUFNLGdGQUFnRixpQkFBaUIsaURBQWlELDRDQUE0QyxlQUFlLGVBQWUsaUNBQWlDLGlCQUFpQixrREFBa0Qsd0JBQXdCLHNCQUFzQixtQ0FBbUMsS0FBSyxXQUFXLHFDQUFxQyxVQUFVLGlCQUFpQixlQUFlLGlCQUFpQixZQUFZLHNCQUFzQixxQkFBcUIsaUJBQWlCLGFBQWEsY0FBYywwQkFBMEIsV0FBVyxzQ0FBc0MsU0FBUyxFQUFFLDhGQUE4RixXQUFXLDhDQUE4QyxPQUFPLGFBQWEsVUFBVSw4QkFBOEIsVUFBVSxXQUFXLHdCQUF3QixZQUFZLDRCQUE0QixhQUFhLHdCQUF3QixXQUFXLHdCQUF3QixjQUFjLHVCQUF1QixhQUFhLDJCQUEyQixzQkFBc0Isd0JBQXdCLGdCQUFnQix3QkFBd0IsUUFBUSw0QkFBNEIsaUJBQWlCLGtDQUFrQyxrSEFBa0gsV0FBVyxrQ0FBa0MsdUJBQXVCLFdBQVcsd0JBQXdCLFdBQVcsd0JBQXdCLGVBQWUsa0NBQWtDLDJFQUEyRSxrREFBa0QsdUJBQXVCLHdCQUF3QixTQUFTLHdCQUF3QixVQUFVLFlBQVksTUFBTSw0QkFBNEIsaUJBQWlCLE9BQU8sdURBQXVELFFBQVEsa0JBQWtCLG9CQUFvQiw0QkFBNEIsNkZBQTZGLHFCQUFxQixzQkFBc0IsMkJBQTJCLHFGQUFxRixzQkFBc0IsNkJBQTZCLG9CQUFvQixxSkFBcUosVUFBVSxtQkFBbUIscVBBQXFQLHNCQUFzQixXQUFXLGtCQUFrQixTQUFTLHNDQUFzQyx5R0FBeUcsRUFBRSxtQ0FBbUMsOEJBQThCLDRCQUE0QiwyQkFBMkIsK0dBQStHLDRCQUE0QixtTkFBbU4sOEJBQThCLFdBQVcscUNBQXFDLFNBQVMsNkNBQTZDLGlNQUFpTSxJQUFJLDZCQUE2QixxQkFBcUIsMkRBQTJELHlCQUF5QixnRkFBZ0YsMEJBQTBCLHdDQUF3Qyw2QkFBNkIsMEhBQTBILDBCQUEwQixnQkFBZ0IsK0NBQStDLG1HQUFtRyxJQUFJLDBCQUEwQiw0Q0FBNEMsV0FBVywyQkFBMkIsT0FBTyxpTEFBaUwsOEJBQThCLGdEQUFnRCxzQkFBc0Isb0JBQW9CLHlCQUF5QixzREFBc0QsOEJBQThCLCtEQUErRCw0QkFBNEIsb0RBQW9ELGdjQUFnYyxFQUFFLG9HQUFvRyx5QkFBeUIsMklBQTJJLHlCQUF5QixtRkFBbUYsaUJBQWlCLGFBQWEsY0FBYywwQkFBMEIsV0FBVyxzQ0FBc0MsU0FBUyxFQUFFLGlEQUFpRCxXQUFXLDBEQUEwRCxpQkFBaUIsV0FBVyw2QkFBNkIsaUJBQWlCLFdBQVcsNkJBQTZCLGlCQUFpQixXQUFXLDZCQUE2QixpQkFBaUIsV0FBVyw2QkFBNkIsaUJBQWlCLGFBQWEsY0FBYywwQkFBMEIsV0FBVyxnQkFBZ0IsbUJBQW1CLDBCQUEwQixpQ0FBaUMsa0RBQWtELFlBQVksaUJBQWlCLGFBQWEsY0FBYywwQkFBMEIsV0FBVyxnQkFBZ0IseUdBQXlHLGdCQUFnQixhQUFhLDhHQUE4Ryw0RUFBNEUsNkNBQTZDLGFBQWEsNElBQTRJLGlCQUFpQixNQUFNLGtCQUFrQiwwQkFBMEIsZ0NBQWdDLGlCQUFpQixpQ0FBaUMsaUJBQWlCLDhDQUE4QyxpQkFBaUIsMENBQTBDLGVBQWUsc0JBQXNCLGlFQUFpRSxVQUFVLGVBQWUsdUJBQXVCLGlCQUFpQiwyQkFBMkIsc0JBQXNCLHVCQUF1QixvQ0FBb0MsWUFBWSxLQUFLLElBQUksMkJBQTJCLFVBQVUsSUFBSSw0Q0FBNEMsZUFBZSxpQkFBaUIsWUFBWSwwQkFBMEIsNEJBQTRCLFVBQVUsMEJBQTBCLG9CQUFvQiw0QkFBNEIsc0JBQXNCLDhCQUE4Qix3QkFBd0Isa0JBQWtCLDhCQUE4QixpQkFBaUIsNEJBQTRCLHNCQUFzQixpQkFBaUIsZ0NBQWdDLFdBQVcsK0JBQStCLFVBQVUsaUJBQWlCLG9CQUFvQiwrQkFBK0IsaUJBQWlCLFlBQVksaUVBQWlFLDRDQUE0QyxpQkFBaUIsWUFBWSxxQ0FBcUMscUJBQXFCLGlCQUFpQixhQUFhLGlDQUFpQyxtQ0FBbUMsWUFBWSw0QkFBNEIsaUJBQWlCLFlBQVksc0JBQXNCLGVBQWUsd0JBQXdCLE9BQU8sbUJBQW1CLGlCQUFpQixrRkFBa0YsU0FBUyxvQkFBb0Isb0NBQW9DLEdBQUcsZ0JBQWdCLE9BQU8sT0FBTyxpQkFBaUIsRUFBRSxpQkFBaUIsbUVBQW1FLFlBQVksbUJBQW1CLGdCQUFnQixLQUFLLGNBQWMsaUJBQWlCLFlBQVksa0JBQWtCLGVBQWUsS0FBSyxjQUFjLGVBQWUsd0NBQXdDLGNBQWMsOENBQThDLGlCQUFpQiwyQkFBMkIscURBQXFELEtBQUssZ0NBQWdDLElBQUksc0JBQXNCLFVBQVUsaUJBQWlCLG9GQUFvRix5QkFBeUIsMEJBQTBCLGNBQWMsVUFBVSx5Q0FBeUMsaUJBQWlCLHlCQUF5Qiw0SEFBNEgsSUFBSSxZQUFZLFNBQVMsbUJBQW1CLHdCQUF3QixxREFBcUQsaUJBQWlCLDBEQUEwRCw2Q0FBNkMsMklBQTJJLGlCQUFpQiwwQkFBMEIsd0JBQXdCLG1CQUFtQixxQkFBcUIsaUNBQWlDLEtBQUssZUFBZSxpQkFBaUIsb0JBQW9CLHNCQUFzQixxQkFBcUIseUNBQXlDLGtMQUFrTCxpQkFBaUIsa0NBQWtDLHdCQUF3QixtQ0FBbUMsaUJBQWlCLHVCQUF1QixzQkFBc0IsdUNBQXVDLGlCQUFpQixhQUFhLG1DQUFtQyw0Q0FBNEMsaUNBQWlDLFlBQVksb0NBQW9DLGlHQUFpRyxrRUFBa0UsaUJBQWlCLFlBQVksMEJBQTBCLHNCQUFzQixFQUFFLGlCQUFpQixvQkFBb0Isd0JBQXdCLG1CQUFtQixnQkFBZ0IsRUFBRSxnQkFBZ0IsaUJBQWlCLGFBQWEsZ0JBQWdCLGtDQUFrQyw0QkFBNEIsWUFBWSwwQkFBMEIsb0JBQW9CLHFCQUFxQiw4QkFBOEIsZ0JBQWdCLEVBQUUsRUFBRSxpQkFBaUIsYUFBYSw0U0FBNFMsNEtBQTRLLGdCQUFnQixNQUFNLGVBQWUsbUJBQW1CLFFBQVEsS0FBSyxLQUFLLGtCQUFrQixhQUFhLDJDQUEyQyxpQkFBaUIsbUJBQW1CLGdCQUFnQiw4Q0FBOEMseUJBQXlCLGFBQWEsc0JBQXNCLG1CQUFtQixzR0FBc0csbUJBQW1CLHdCQUF3QixrQ0FBa0MsaUJBQWlCLEtBQUsscUNBQXFDLElBQUksb0JBQW9CLFNBQVMsaUJBQWlCLGlDQUFpQyxlQUFlLDZCQUE2QiwwRkFBMEYsaUJBQWlCLDRDQUE0QyxhQUFhLHlEQUF5RCxlQUFlLDZCQUE2QixXQUFXLHNDQUFzQyxTQUFTLGVBQWUseUNBQXlDLFdBQVcsMENBQTBDLFVBQVUsaUJBQWlCLHFFQUFxRSw4REFBOEQsaUZBQWlGLG9CQUFvQixzQkFBc0IsT0FBTyw4QkFBOEIsZUFBZSw0R0FBNEcsZUFBZSxvQkFBb0IsU0FBUyxFQUFFLDRJQUE0SSxhQUFhLGFBQWEsMkJBQTJCLGFBQWEsYUFBYSx1QkFBdUIsZ0JBQWdCLGlDQUFpQyxvQkFBb0IsZ0RBQWdELG9DQUFvQyxzQkFBc0IsS0FBSyxzQkFBc0IsTUFBTSx5QkFBeUIsc0hBQXNILGlDQUFpQyxVQUFVLDJCQUEyQixNQUFNLElBQUksTUFBTSxnQkFBZ0IsV0FBVyxzQkFBc0Isc0JBQXNCLHNCQUFzQixtQkFBbUIsd0JBQXdCLHFFQUFxRSwwQ0FBMEMsd0JBQXdCLHlGQUF5RixpQkFBaUIsdUJBQXVCLGlCQUFpQixvQkFBb0IsaUJBQWlCLE1BQU0sNmZBQTZmLFdBQVcsS0FBSyxtQ0FBbUMsaUNBQWlDLGlCQUFpQiw0Q0FBNEMsa0JBQWtCLHVCQUF1QixvQkFBb0IsNkJBQTZCLG1DQUFtQyxzQkFBc0Isc0JBQXNCLDhCQUE4QixVQUFVLFdBQVcsNEJBQTRCLFlBQVksaUJBQWlCLGdCQUFnQiw2QkFBNkIsaUJBQWlCLDBCQUEwQixrQkFBa0IsV0FBVyxXQUFXLGVBQWUsbUJBQW1CLFVBQVUsWUFBWSwyREFBMkQscUJBQXFCLG9EQUFvRCwyREFBMkQsV0FBVyxpQ0FBaUMsK0JBQStCLG1CQUFtQix5QkFBeUIsV0FBVyxZQUFZLG1CQUFtQix5QkFBeUIsbUJBQW1CLHNDQUFzQyx5QkFBeUIsa0NBQWtDLFVBQVUsK0JBQStCLFdBQVcsMkJBQTJCLHdCQUF3QixxQkFBcUIsZ0JBQWdCLGNBQWMsVUFBVSxnQkFBZ0Isb0NBQW9DLGtCQUFrQixtQkFBbUIsaUNBQWlDLGtCQUFrQixjQUFjLFlBQVksWUFBWSxXQUFXLFNBQVMsc0NBQXNDLFlBQVksd0NBQXdDLGVBQWUsZ0NBQWdDLGdDQUFnQyw0QkFBNEIsNkJBQTZCLHlCQUF5QixjQUFjLGtCQUFrQixTQUFTLE9BQU8sYUFBYSxnQkFBZ0IsY0FBYyxTQUFTLFdBQVcsa0JBQWtCLGlDQUFpQyx1Q0FBdUMsZ0JBQWdCLDBCQUEwQixnQkFBZ0IsZ0JBQWdCLGdCQUFnQixzQkFBc0Isa0JBQWtCLHdCQUF3QixXQUFXLHlCQUF5QixzQkFBc0Isa0JBQWtCLFlBQVkscUJBQXFCLGtCQUFrQixXQUFXLGlCQUFpQiwrQkFBK0IsNkJBQTZCLHlCQUF5QixvQ0FBb0Msa0JBQWtCLFdBQVcscUZBQXFGLGFBQWEsK0JBQStCLFdBQVcsZUFBZSxlQUFlLGdCQUFnQixVQUFVLGVBQWUsZUFBZSxTQUFTLGdCQUFnQixjQUFjLFdBQVcseUJBQXlCLHlCQUF5QixXQUFXLHVFQUF1RSxXQUFXLCtPQUErTyxhQUFhLHdDQUF3QyxhQUFhLGdFQUFnRSxnQkFBZ0Isd0JBQXdCLHFCQUFxQix1QkFBdUIsY0FBYyxZQUFZLHFCQUFxQixZQUFZLGFBQWEsU0FBUyxlQUFlLFdBQVcsZUFBZSxnQkFBZ0Isa0JBQWtCLGdCQUFnQixXQUFXLFdBQVcsYUFBYSx1QkFBdUIsZUFBZSxjQUFjLGlCQUFpQixXQUFXLFdBQVcsbUJBQW1CLG1CQUFtQixlQUFlLG1DQUFtQyxXQUFXLDZCQUE2QixzQ0FBc0MsbUJBQW1CLFdBQVcsc0NBQXNDLGdCQUFnQixtQkFBbUIsVUFBVSxrQkFBa0IsUUFBUSxXQUFXLGNBQWMsb0JBQW9CLGdCQUFnQix3Q0FBd0MsMENBQTBDLDJDQUEyQywwQ0FBMEMsd0JBQXdCLDhDQUE4Qyx1QkFBdUIsNENBQTRDLGtCQUFrQixVQUFVLFdBQVcsMkJBQTJCLFVBQVUsa0NBQWtDLEdBQUcsdUJBQXVCLEdBQUcseUJBQXlCLDBCQUEwQixHQUFHLHVCQUF1QixHQUFHLHlCQUF5QixzQ0FBc0MsZ0RBQWdELDJCQUEyQixVQUFVLE9BQU8sZUFBZSxxQkFBcUIsU0FBUyw2QkFBNkIsaUJBQWlCLGNBQWMsS0FBSyxjQUFjLDZCQUE2QixTQUFTLGdCQUFnQixrQkFBa0IsbUJBQW1CLHNDQUFzQyxZQUFZLEtBQUssY0FBYyxLQUFLLGlCQUFpQiw4QkFBOEIsUUFBUSxXQUFXLEtBQUssV0FBVyxnR0FBZ0csSUFBSSxpQkFBaUIsTUFBTSxtQ0FBbUMsb0JBQW9CLGVBQWUsNEJBQTRCLGVBQWUsb0JBQW9CLGdEQUFnRCx1Q0FBdUMsbUZBQW1GLGdDQUFnQyxFQUFFLG1DQUFtQyxXQUFXLGdCQUFnQixVQUFVLEVBQUUsT0FBTyxpQ0FBaUMsZUFBZSxXQUFXLGtCQUFrQiw4Q0FBOEMsZ0JBQWdCLCtEQUErRCxXQUFXLFdBQVcsa0RBQWtELG9CQUFvQixNQUFNLHNCQUFzQix5Q0FBeUMsa0NBQWtDLGlCQUFpQix1Q0FBdUMsNEhBQTRILDJCQUEyQiwyQ0FBMkMsS0FBSyxrQkFBa0IsZ0JBQWdCLFlBQVksT0FBTyxzQkFBc0IsMkJBQTJCLHVCQUF1QixhQUFhLGtFQUFrRSx1QkFBdUIsb0JBQW9CLHdCQUF3QixtQ0FBbUMsUUFBUSxtSEFBbUgsV0FBVyxlQUFlLEtBQUsscUJBQXFCLHFGQUFxRixhQUFhLDZGQUE2RixhQUFhLGlHQUFpRyxhQUFhLG9HQUFvRyxvQkFBb0Isc0VBQXNFLDZEQUE2RCxnREFBZ0QsbUNBQW1DLDRCQUE0QixvQkFBb0IsMEJBQTBCLHFCQUFxQixxQ0FBcUMsYUFBYSxnRkFBZ0Ysd0JBQXdCLHVEQUF1RCxPQUFPLG1CQUFtQix5QkFBeUIsc0RBQXNELDBCQUEwQix1Q0FBdUMsZUFBZSxhQUFhLDhEQUE4RCxLQUFLLHNCQUFzQix1QkFBdUIsU0FBUyxJQUFJLHNCQUFzQixpQ0FBaUMsNEZBQTRGO0FBQzd0K0IseUJBQXlCLGdGQUFnRixxQkFBcUIsaUJBQWlCLGdCQUFnQixZQUFZLFdBQVcsS0FBSyxxQkFBcUIsTUFBTSxTQUFTLFlBQVksaUJBQWlCLDJCQUEyQixLQUFLLGlCQUFpQixrQ0FBa0MsS0FBSyxpQkFBaUIsaUJBQWlCLDRCQUE0QixTQUFTLDBCQUEwQixjQUFjLGlCQUFpQixLQUFLLFdBQVcsS0FBSywwQ0FBMEMsMkJBQTJCLHFDQUFxQyxlQUFlLEVBQUUsU0FBUyxnQkFBZ0IsMEJBQTBCLGdJQUFnSSxLQUFLLCtHQUErRyxrQkFBa0IsY0FBYyw0QkFBNEIsbUJBQW1CLG9CQUFvQixjQUFjLHNDQUFzQyxrQ0FBa0MsZ0JBQWdCLFVBQVUsZ0JBQWdCLFVBQVUsMERBQTBELDBDQUEwQyxNQUFNLHdCQUF3QixNQUFNLHNFQUFzRSxPQUFPLFVBQVUsb0JBQW9CLGlCQUFpQiw0Q0FBNEMsS0FBSyxnREFBZ0QsNEVBQTRFLGdCQUFnQixvQ0FBb0MsOEhBQThILDBHQUEwRyxLQUFLLEtBQUssYUFBYSw2QkFBNkIsMkNBQTJDLFFBQVEsZUFBZSxNQUFNLGtCQUFrQiw0REFBNEQsZ0JBQWdCLG9FQUFvRSxpQkFBaUIsK0RBQStELGtCQUFrQix3QkFBd0IsT0FBTywwR0FBMEcsV0FBVywwQkFBMEIsaUJBQWlCLFdBQVcsS0FBSyxxQkFBcUIsbUJBQW1CLE1BQU0sV0FBVyxPQUFPLFlBQVksV0FBVyxLQUFLLFdBQVcsZUFBZSxZQUFZLGlCQUFpQixpQkFBaUIsbUJBQW1CLGlCQUFpQixTQUFTLHFCQUFxQiw0Q0FBNEMsR0FBRyxpQkFBaUIsWUFBWSxzQ0FBc0MsVUFBVSxFQUFFLCtCQUErQixHQUFHO0FBQzF4RixzQzs7Ozs7OztBQ0ZBOztBQUVBO0FBQ0Esb1ZBQXlPO0FBQ3pPO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxzSkFBc0osa0ZBQWtGO0FBQ3hPLCtKQUErSixrRkFBa0Y7QUFDalA7QUFDQTtBQUNBLElBQUk7QUFDSjtBQUNBO0FBQ0EsZ0NBQWdDLFVBQVUsRUFBRTtBQUM1QyxDOzs7Ozs7O0FDcEJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxVQUFVLGlCQUFpQjtBQUMzQjtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLG1CQUFtQjtBQUNuQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxtQkFBbUIsbUJBQW1CO0FBQ3RDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLG1CQUFtQixzQkFBc0I7QUFDekM7QUFDQTtBQUNBLHVCQUF1QiwyQkFBMkI7QUFDbEQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSxpQkFBaUIsbUJBQW1CO0FBQ3BDO0FBQ0E7QUFDQTtBQUNBO0FBQ0EscUJBQXFCLDJCQUEyQjtBQUNoRDtBQUNBO0FBQ0EsWUFBWSx1QkFBdUI7QUFDbkM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBLHFCQUFxQix1QkFBdUI7QUFDNUM7QUFDQTtBQUNBLDhCQUE4QjtBQUM5QjtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLENBQUM7O0FBRUQ7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSx5REFBeUQ7QUFDekQ7O0FBRUE7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7O0FDdE5BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCLGlCQUFpQjtBQUNsQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxtQ0FBbUMsd0JBQXdCO0FBQzNELEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7QUMxQkE7QUFDQTs7QUFFQSx5REFBZTs7QUFHWEEsWUFBUTtBQUNKQyxlQUFPLEVBREg7QUFFSkMsY0FBTSxFQUZGO0FBR0pDLGVBQU8sRUFISDtBQUlKQyxxQkFBYSxFQUpUO0FBS0pDLGtCQUFVOztBQUxOLEtBSEc7QUFXWEMsWUFBUSxFQVhHO0FBWVhDLFFBWlcsZ0JBWU5DLEVBWk0sRUFZRjtBQUNMLFlBQUlDLFdBQVcsRUFBZjtBQUNBLGFBQUtILE1BQUwsQ0FBWUksR0FBWixDQUFnQixVQUFTQyxDQUFULEVBQVk7QUFDeEIsZ0JBQUlBLEVBQUVILEVBQUYsSUFBUUEsRUFBWixFQUFnQjtBQUNaSSx1QkFBT0MsTUFBUCxDQUFjSixRQUFkLEVBQXdCRSxDQUF4QjtBQUNIO0FBQ0osU0FKRDtBQUtBLGVBQU9GLFFBQVA7QUFFSCxLQXJCVTtBQXNCWEssT0F0QlcsaUJBc0JMO0FBQUE7O0FBQ0YsZUFBTyxJQUFJQyxPQUFKLENBQVksVUFBQ0MsR0FBRCxFQUFNQyxHQUFOLEVBQWM7QUFDN0JDLGtCQUFNQyxHQUFOLENBQVUsc0JBQVYsRUFBa0NDLElBQWxDLENBQXVDLFVBQUNYLFFBQUQsRUFBYztBQUNqRCxzQkFBS0gsTUFBTCxHQUFjRyxTQUFTWSxJQUF2QjtBQUNBTCxvQkFBSVAsUUFBSjtBQUNILGFBSEQsRUFHR2EsS0FISCxDQUdTLFVBQUNDLEtBQUQsRUFBVztBQUNoQkMsd0JBQVFDLEdBQVIsQ0FBWSxxQ0FBWjtBQUNBUixvQkFBSU0sS0FBSjtBQUNILGFBTkQ7QUFPSCxTQVJNLENBQVA7QUFTSCxLQWhDVTtBQWlDWEcsV0FqQ1csbUJBaUNIQyxPQWpDRyxFQWlDTTtBQUFBOztBQUNiVCxjQUFNVSxJQUFOLENBQVcsdUJBQVgsRUFBb0MsRUFBRSxNQUFNRCxPQUFSLEVBQXBDLEVBQXVEUCxJQUF2RCxDQUE0RCxZQUFNO0FBQzlELG1CQUFLTixHQUFMO0FBQ0gsU0FGRCxFQUVHUSxLQUZILENBRVMsVUFBQ0MsS0FBRCxFQUFXO0FBQ2hCQyxvQkFBUUMsR0FBUixDQUFZLG9DQUFaO0FBQ0gsU0FKRDtBQUtILEtBdkNVO0FBd0NYSSxXQXhDVyxtQkF3Q0hSLElBeENHLEVBd0NHO0FBQ1ZTLGNBQU1DLFFBQU4sR0FBaUJYLElBQWpCLENBQXNCLFVBQVNZLE9BQVQsRUFBa0I7QUFDcEMsZ0JBQUlBLE9BQUosRUFBYTtBQUNUZCxzQkFBTVUsSUFBTixDQUFXLHdCQUFYLEVBQXFDUCxJQUFyQyxFQUEyQ0QsSUFBM0MsQ0FBZ0QsVUFBQ0osR0FBRCxFQUFTLENBRXhELENBRkQsRUFFR00sS0FGSCxDQUVTLFVBQUNXLEdBQUQsRUFBUyxDQUVqQixDQUpEO0FBS0g7QUFDSixTQVJEO0FBU0gsS0FsRFU7QUFtRFhDLFdBbkRXLG1CQW1ESGIsSUFuREcsRUFtREcsQ0FHYjtBQXREVSxDQUFmLEU7Ozs7Ozs7Ozs7Ozs7O0FDSEEsSUFBSWMsUUFBUSxJQUFaOztJQUVNQyxJO0FBRUwsZUFBWWYsSUFBWixFQUFpQjtBQUFBOztBQUFBOztBQUNoQixPQUFLZ0IsWUFBTCxHQUFvQkMsRUFBRUMsTUFBRixDQUFTLEVBQVQsRUFBYWxCLElBQWIsQ0FBcEI7QUFDQWlCLElBQUU1QixHQUFGLENBQU0sVUFBQzhCLEtBQUQsRUFBUUMsS0FBUjtBQUFBLFVBQWtCLE1BQUtBLEtBQUwsSUFBY0QsS0FBaEM7QUFBQSxHQUFOO0FBQ0EsT0FBS0UsTUFBTCxHQUFjLElBQUlDLFVBQUosRUFBZDtBQUNBOzs7O3lCQUVNO0FBQ04sT0FBSXRCLE9BQU9pQixFQUFFQyxNQUFGLENBQVMsRUFBVCxFQUFhLElBQWIsQ0FBWDtBQUNBLE9BQUlLLFdBQVcsSUFBSUMsUUFBSixDQUFhLGFBQWIsQ0FBZjtBQUNDLFVBQU94QixLQUFLZ0IsWUFBWjtBQUNBLFVBQU9oQixLQUFLcUIsTUFBWjtBQUNESixLQUFFNUIsR0FBRixDQUFNVyxJQUFOLEVBQVksVUFBQ21CLEtBQUQsRUFBUU0sR0FBUjtBQUFBLFdBQWdCRixTQUFTRyxNQUFULENBQWdCRCxHQUFoQixFQUFxQk4sS0FBckIsQ0FBaEI7QUFBQSxJQUFaO0FBQ0EsVUFBT0ksUUFBUDtBQUNBOzs7MEJBRU9ULEssRUFBTztBQUNkLFFBQUtBLEtBQUwsR0FBYUEsS0FBYjtBQUNBLE9BQUlhLFlBQVksS0FBS2IsS0FBckI7QUFDQVgsV0FBUUMsR0FBUixDQUFZLEtBQUtKLElBQUwsRUFBWjtBQUNBOzs7MEJBRU87O0FBRVAsUUFBSSxJQUFJb0IsS0FBUixJQUFpQixLQUFLSixZQUF0QixFQUFtQztBQUNsQyxTQUFLSSxLQUFMLElBQWMsRUFBZDtBQUNBO0FBQ0QsUUFBS0MsTUFBTCxDQUFZTyxLQUFaO0FBQ0E7Ozt1QkFFSUMsRyxFQUFLO0FBQUUsUUFBS0MsTUFBTCxDQUFZLE1BQVosRUFBb0JELEdBQXBCO0FBQTBCOzs7d0JBRWhDQSxHLEVBQUs7QUFBRSxRQUFLQyxNQUFMLENBQVksT0FBWixFQUFxQkQsR0FBckI7QUFBMkI7OzswQkFFakNBLEcsRUFBSztBQUFFLFFBQUtDLE1BQUwsQ0FBWSxRQUFaLEVBQXNCRCxHQUF0QjtBQUE0Qjs7O3lCQUVuQ0UsVyxFQUFhRixHLEVBQUs7QUFBQTs7QUFDeEIsVUFBTyxJQUFJbkMsT0FBSixDQUFZLFVBQUNzQyxPQUFELEVBQVNDLE1BQVQsRUFBb0I7QUFDdENwQyxVQUFNa0MsV0FBTixFQUFtQkYsR0FBbkIsRUFBd0IsT0FBSzdCLElBQUwsRUFBeEIsRUFDRUQsSUFERixDQUNPLG9CQUFZO0FBQ2pCLFlBQUttQyxTQUFMLENBQWU5QyxTQUFTWSxJQUF4QjtBQUNBZ0MsYUFBUTVDLFFBQVI7QUFDQSxLQUpGLEVBS0VhLEtBTEYsQ0FLUSxpQkFBUztBQUNmLFlBQUtrQyxNQUFMLENBQVlqQyxNQUFNZCxRQUFOLENBQWVZLElBQTNCO0FBQ0FpQyxZQUFPL0IsS0FBUDtBQUNBLEtBUkY7QUFTQSxJQVZNLENBQVA7QUFXQTs7OzRCQUVTRixJLEVBQU07O0FBRWYsUUFBS29DLEtBQUw7QUFFQTs7O3lCQUVNZixNLEVBQVE7QUFDZCxRQUFLQSxNQUFMLENBQVlnQixNQUFaLENBQW1CaEIsTUFBbkI7QUFFQTs7Ozs7O0lBR0lDLFU7QUFFRix1QkFBYztBQUFBOztBQUNoQixPQUFLRCxNQUFMLEdBQWMsRUFBZDtBQUNHOzs7O3NCQUVHRCxLLEVBQU87QUFDUCxPQUFHLEtBQUtDLE1BQUwsQ0FBWUQsS0FBWixDQUFILEVBQXNCO0FBQ2xCLFdBQU8sS0FBS0MsTUFBTCxDQUFZRCxLQUFaLEVBQW1CLENBQW5CLENBQVA7QUFDSDtBQUNKOzs7c0JBRUdBLEssRUFBTztBQUFFLFVBQU8sS0FBS0MsTUFBTCxDQUFZaUIsY0FBWixDQUEyQmxCLEtBQTNCLENBQVA7QUFBMEM7Ozt3QkFFakQ7QUFBRSxVQUFPN0IsT0FBT2dELElBQVAsQ0FBWSxLQUFLbEIsTUFBakIsRUFBeUJtQixNQUF6QixHQUFrQyxDQUF6QztBQUE2Qzs7O3lCQUU5Q25CLE0sRUFBUTtBQUFFLFFBQUtBLE1BQUwsR0FBY0EsTUFBZDtBQUFzQjs7O3dCQUVqQ0QsSyxFQUFPO0FBQ1YsT0FBR0EsS0FBSCxFQUFTO0FBQ1IsV0FBTyxLQUFLQyxNQUFMLENBQVlELEtBQVosQ0FBUDtBQUNBO0FBQ0E7QUFDRCxRQUFLQyxNQUFMLEdBQWMsRUFBZDtBQUNGOzs7Ozs7Ozs7Ozs7Ozs7QUN6Rkw7O0FBRUEsSUFBTW9CLFdBQVc7QUFDYkMsVUFBTSxJQURPO0FBRWIzRCxpQkFBYSxJQUZBO0FBR2I0RCxXQUFPLENBSE07QUFJYjNELGNBQVUsSUFKRztBQUtiOEIsV0FBTyxJQUxNO0FBTWI4QixjQUFVO0FBTkcsQ0FBakI7O0FBU0EseURBQWU7QUFDWDFELFFBRFcsZ0JBQ05DLEVBRE0sRUFDRjtBQUNMLGVBQU84QixFQUFFQyxNQUFGLENBQVMsS0FBSzJCLElBQWQsRUFBb0IsS0FBS0MsTUFBTCxDQUFZN0IsRUFBRThCLFNBQUYsQ0FBWSxLQUFLRCxNQUFqQixFQUF5QjtBQUFBLG1CQUFLRSxFQUFFN0QsRUFBRixJQUFRQSxFQUFiO0FBQUEsU0FBekIsQ0FBWixDQUFwQixDQUFQO0FBQ0gsS0FIVTs7QUFJWDJELFlBQVEsRUFKRztBQUtYRCxVQUFNLElBQUksc0RBQUosQ0FBU0osUUFBVCxDQUxLO0FBTVhoRCxPQU5XLGlCQU1MO0FBQUE7O0FBQ0YsZUFBTyxJQUFJQyxPQUFKLENBQVksVUFBQ0MsR0FBRCxFQUFNQyxHQUFOLEVBQWM7QUFDN0Isa0JBQUtpRCxJQUFMLENBQVVmLE1BQVYsQ0FBaUIsS0FBakIsRUFBd0IscUJBQXhCLEVBQ0svQixJQURMLENBQ1Usb0JBQVk7QUFDZCxzQkFBSytDLE1BQUwsR0FBYyxHQUFHRyxLQUFILENBQVNDLElBQVQsQ0FBYzlELFNBQVNZLElBQXZCLENBQWQ7QUFDQUwsb0JBQUlQLFFBQUo7QUFDSCxhQUpMLEVBS0thLEtBTEwsQ0FLVyxpQkFBUztBQUNaRSx3QkFBUUMsR0FBUixDQUFZLG9CQUFaO0FBQ0FSLG9CQUFJTSxNQUFNZCxRQUFWO0FBQ0gsYUFSTDtBQVNILFNBVk0sQ0FBUDtBQVdILEtBbEJVO0FBbUJYK0QsVUFuQlcsa0JBbUJKaEUsRUFuQkksRUFtQkE7QUFDUGdCLGdCQUFRQyxHQUFSLENBQVlqQixFQUFaO0FBQ0gsS0FyQlU7QUFzQlhpRSxVQXRCVyxrQkFzQkpDLE9BdEJJLEVBc0JLO0FBQ1osZUFBT3hELE1BQU15RCxNQUFOLDBCQUFvQ0QsT0FBcEMsQ0FBUDtBQUNBLGVBQU94RCxNQUFNeUQsTUFBTiwwQkFBb0NELE9BQXBDLENBQVA7QUFDSDtBQXpCVSxDQUFmLEU7Ozs7Ozs7Ozs7O0lDWE1FLGU7QUFFRiw2QkFBWUMsUUFBWixFQUFzQkMsZ0JBQXRCLEVBQXdDO0FBQUE7O0FBQ3BDLGFBQUtDLEtBQUwsR0FBYUMsU0FBU0MsYUFBVCxDQUF1QixlQUFlSixRQUFmLEdBQTBCLEdBQWpELENBQWI7QUFDQSxhQUFLSyxTQUFMLEdBQWlCLEtBQUtILEtBQUwsQ0FBV0UsYUFBWCxDQUF5QixPQUF6QixDQUFqQjtBQUNBLGFBQUtFLFNBQUwsR0FBaUIsS0FBS0osS0FBTCxDQUFXRSxhQUFYLENBQXlCLG9DQUF6QixDQUFqQjtBQUNBLGFBQUtHLFVBQUwsR0FBa0IsRUFBbEI7QUFDQSxhQUFLTixnQkFBTCxHQUF3QkEsZ0JBQXhCO0FBQ0EsYUFBS08sa0JBQUw7QUFDSDs7OztzQ0FDYUMsTyxFQUFTO0FBQ25CLGlCQUFLRixVQUFMLENBQWdCRyxJQUFoQixDQUFxQkQsT0FBckI7QUFDSDs7O3lDQUNnQjtBQUNiLG1CQUFPLEtBQUtFLFdBQVo7QUFDSDs7OzJDQUNrQjtBQUNmLGlCQUFLLElBQUlDLElBQUksQ0FBYixFQUFnQkEsSUFBSSxLQUFLWCxnQkFBTCxDQUFzQmpCLE1BQTFDLEVBQWtENEIsR0FBbEQsRUFBdUQ7QUFDbkQsb0JBQUlDLFlBQVksS0FBS1osZ0JBQUwsQ0FBc0JXLENBQXRCLEVBQXlCRSxPQUF6QixDQUFpQyxLQUFLVCxTQUF0QyxDQUFoQjtBQUNBLG9CQUFJUSxTQUFKLEVBQWU7QUFDWCx5QkFBS0UsYUFBTCxDQUFtQixLQUFLZCxnQkFBTCxDQUFzQlcsQ0FBdEIsRUFBeUJJLGlCQUE1QztBQUNBLHlCQUFLVixTQUFMLENBQWVXLFNBQWYsQ0FBeUJyQixNQUF6QixDQUFnQyxPQUFoQztBQUNBLHlCQUFLVSxTQUFMLENBQWVXLFNBQWYsQ0FBeUJDLEdBQXpCLENBQTZCLFNBQTdCO0FBQ0gsaUJBSkQsTUFJTztBQUNILHlCQUFLWixTQUFMLENBQWVXLFNBQWYsQ0FBeUJyQixNQUF6QixDQUFnQyxTQUFoQztBQUNBLHlCQUFLVSxTQUFMLENBQWVXLFNBQWYsQ0FBeUJDLEdBQXpCLENBQTZCLE9BQTdCO0FBQ0g7QUFDSjtBQUNKOzs7cUNBQ1k7QUFDVCxnQkFBSSxLQUFLYixTQUFMLENBQWUxQyxLQUFmLElBQXdCLEVBQTVCLEVBQWdDO0FBQzVCLHFCQUFLMEMsU0FBTCxDQUFlYyxpQkFBZixDQUFpQyxFQUFqQztBQUNILGFBRkQsTUFFTztBQUNILG9CQUFJQyxNQUFNLEtBQUtDLGNBQUwsRUFBVjtBQUNBLHFCQUFLaEIsU0FBTCxDQUFlYyxpQkFBZixDQUFpQ0MsR0FBakM7QUFDSDtBQUNELGlCQUFLbkIsZ0JBQUw7QUFDSDs7OzZDQUNvQjtBQUNqQixnQkFBSUYsa0JBQWtCLElBQXRCO0FBQ0EsaUJBQUtNLFNBQUwsQ0FBZWlCLGdCQUFmLENBQWdDLE9BQWhDLEVBQXlDLFlBQVc7QUFDaER2QixnQ0FBZ0J3QixVQUFoQjtBQUNILGFBRkQ7QUFHSDs7Ozs7Ozs7Ozs7Ozs7Ozs7QUMzQ0w7QUFDQTs7QUFFQUMsT0FBT0MsR0FBUCxHQUFhO0FBQ1RDLFdBQU87QUFERSxDQUFiO0FBR0EsSUFBTUMsTUFBTSxJQUFJQyxHQUFKLENBQVE7QUFDaEJDLFFBQUksU0FEWTtBQUVoQkMsZ0JBQVksRUFBQ0MsWUFBQSx5RUFBRCxFQUFhQyxpQkFBQSw4RUFBYixFQUZJO0FBR2hCeEYsVUFBTztBQUNIeUYsb0JBQVksQ0FEVDtBQUVIQyxxQkFBYSxLQUZWO0FBR0hDLHdCQUFnQjFFLEVBQUUyRSxPQUFGLENBQVVaLE9BQU9hLGNBQWpCO0FBSGIsS0FIUztBQVFoQkMsYUFBUztBQVJPLENBQVIsQ0FBWjs7QUFZQSxJQUFNQyxZQUFZO0FBQ2RDLFVBQU07QUFEUSxDQUFsQjtBQUdBLENBQUMsVUFBVUMsQ0FBVixFQUFhO0FBQ1ZGLGNBQVVDLElBQVYsR0FBaUIsT0FBT2hCLE9BQU9rQixRQUFQLENBQWdCQyxJQUF2QixHQUE4QixXQUEvQztBQUNBQztBQUNILENBSEQsRUFHR0MsTUFISDs7QUFLQSxTQUFTRCxpQkFBVCxHQUE2QjtBQUN6QmpHLFlBQVFDLEdBQVIsQ0FBWSxRQUFaO0FBQ0EsUUFBRzZGLEVBQUUsZ0JBQUYsRUFBb0IsQ0FBcEIsQ0FBSCxFQUEyQjtBQUN2QkEsVUFBRSxnQkFBRixFQUFvQkssTUFBcEIsQ0FBMkIsRUFBM0I7QUFDSDtBQUNKOztBQUVEO0FBQ0EsSUFBSUMsYUFBYTtBQUNiQyxVQUFNLGdCQUFZO0FBQ2QsWUFBSUMsV0FBVyxLQUFLQyxJQUFMLEdBQVlULEVBQUUsY0FBRixFQUFrQlUsS0FBbEIsRUFBM0I7QUFBQSxZQUNJQyxXQUFXQyxPQUFPSCxJQUFQLEdBQWNULEVBQUUsaUJBQUYsQ0FEN0I7QUFBQSxZQUNtRGEsY0FBY2IsRUFBRSxXQUFGLENBRGpFO0FBQUEsWUFDaUZjLFVBQVUsSUFEM0Y7QUFFQSxZQUFJN0IsUUFBUTZCLFFBQVE3QixLQUFwQjtBQUNBQSxjQUFNOEIsTUFBTixHQUFlQyxTQUFTQyxPQUFPQyxTQUFQLENBQWlCLFdBQWpCLENBQVQsQ0FBZjtBQUNBVixpQkFBU1csRUFBVCxDQUFZLE9BQVosRUFBcUIsWUFBWTtBQUM3QmxDLGtCQUFNOEIsTUFBTixHQUFnQjlCLE1BQU04QixNQUFOLEtBQWlCLENBQWpCLEdBQXFCLENBQXJCLEdBQXlCLENBQXpDO0FBQ0EsZ0JBQUk5QixNQUFNOEIsTUFBTixLQUFpQixDQUFyQixFQUF3QjtBQUN2QjdHLHdCQUFRa0gsS0FBUixDQUFjVCxRQUFkO0FBQ0dILHlCQUFTYSxRQUFULENBQWtCLFFBQWxCO0FBQ0FWLHlCQUFTVSxRQUFULENBQWtCLGdCQUFsQjtBQUNBUiw0QkFBWVMsV0FBWixDQUF3QixpQkFBeEI7QUFDSCxhQUxELE1BTUs7QUFDRGQseUJBQVNjLFdBQVQsQ0FBcUIsUUFBckI7QUFDQVgseUJBQVNXLFdBQVQsQ0FBcUIsZ0JBQXJCO0FBQ0FULDRCQUFZUSxRQUFaLENBQXFCLGlCQUFyQjtBQUNIO0FBQ0RuSCxvQkFBUUMsR0FBUixDQUFZOEcsT0FBT0MsU0FBUCxDQUFpQixXQUFqQixDQUFaLEVBQTJDakMsTUFBTThCLE1BQWpEO0FBQ0FmLGNBQUUsSUFBRixFQUFRdUIsV0FBUixDQUFvQixRQUFwQjtBQUNBN0QscUJBQVM4RCxNQUFULEdBQWtCLGVBQWV2QyxNQUFNOEIsTUFBckIsR0FBOEIsRUFBaEQ7QUFDSCxTQWhCRDtBQWlCSCxLQXZCWTtBQXdCYjlCLFdBQU8sRUFBRThCLFFBQVEsS0FBVjtBQXhCTSxDQUFqQjtBQTBCQTtBQUNBLElBQUlILFNBQVM7QUFDVEgsVUFBTSxJQURHO0FBRVRGLFVBQU0sZ0JBQVk7QUFDZCxZQUFJa0IsYUFBYSxLQUFLaEIsSUFBdEI7QUFDQWdCLG1CQUFXQyxRQUFYLENBQW9CLElBQXBCLEVBQTBCQyxRQUExQixDQUFtQyxJQUFuQyxFQUF5QyxPQUF6QyxFQUFrRCxZQUFZO0FBQzFELGdCQUFJQyxVQUFVNUIsRUFBRSxJQUFGLEVBQVE2QixJQUFSLEVBQWQ7QUFDQSxnQkFBSUQsUUFBUSxDQUFSLEVBQVdFLE9BQVgsS0FBdUIsSUFBM0IsRUFBaUM7QUFDN0JGLHdCQUFRTCxXQUFSLENBQW9CLFVBQXBCO0FBQ0g7QUFDSixTQUxEO0FBTUg7QUFWUSxDQUFiOztBQWFBLElBQUlOLFNBQVM7QUFDVGMsZ0JBQVksc0JBQVk7QUFDcEIsWUFBSUMsWUFBWXRFLFNBQVM4RCxNQUF6QjtBQUFBLFlBQWlDUyxjQUFjLEVBQS9DO0FBQ0EsYUFBS0MsT0FBTCxHQUFlRixVQUFVRyxLQUFWLENBQWdCLEtBQWhCLENBQWY7QUFDQSxhQUFLLElBQUlDLEtBQUssQ0FBVCxFQUFZQyxLQUFLLEtBQUtILE9BQTNCLEVBQW9DRSxLQUFLQyxHQUFHOUYsTUFBNUMsRUFBb0Q2RixJQUFwRCxFQUEwRDtBQUN0RCxnQkFBSVosU0FBU2EsR0FBR0QsRUFBSCxDQUFiO0FBQ0EsZ0JBQUlFLEtBQUtkLE9BQU9XLEtBQVAsQ0FBYSxJQUFiLENBQVQ7QUFBQSxnQkFBNkIxRixPQUFPNkYsR0FBRyxDQUFILENBQXBDO0FBQUEsZ0JBQTJDcEgsUUFBUW9ILEdBQUcsQ0FBSCxDQUFuRDtBQUNBTCx3QkFBWWhFLElBQVosQ0FBaUIsRUFBRSxRQUFReEIsSUFBVixFQUFnQixTQUFTdkIsTUFBTWlILEtBQU4sQ0FBWSxLQUFaLEVBQW1CLENBQW5CLENBQXpCLEVBQWpCO0FBQ0g7QUFDRCxlQUFPRixXQUFQO0FBQ0gsS0FWUTtBQVdUTSxlQUFXLHFCQUFZO0FBQ25CckksZ0JBQVFDLEdBQVIsQ0FBWSxnQkFBWjtBQUNILEtBYlE7QUFjVCtHLGVBQVcsbUJBQVVzQixZQUFWLEVBQXdCO0FBQy9CLFlBQUlOLFVBQVUsS0FBS0gsVUFBTCxFQUFkO0FBQ0EsYUFBSyxJQUFJSyxLQUFLLENBQVQsRUFBWUssWUFBWVAsT0FBN0IsRUFBc0NFLEtBQUtLLFVBQVVsRyxNQUFyRCxFQUE2RDZGLElBQTdELEVBQW1FO0FBQy9ELGdCQUFJTSxLQUFLRCxVQUFVTCxFQUFWLENBQVQ7QUFDQSxnQkFBSU0sR0FBR2pHLElBQUgsSUFBVytGLFlBQWYsRUFBNkI7QUFDekIsdUJBQU9FLEdBQUd4SCxLQUFWO0FBQ0g7QUFDSjtBQUNELGVBQU8sS0FBUDtBQUNIO0FBdkJRLENBQWI7QUF5QkEsSUFBSXlILFdBQVc7QUFDWHBDLFVBQU0sZ0JBQVk7QUFDZCxhQUFLcUMsU0FBTDtBQUNBLFlBQUlQLEtBQUssS0FBS1EsS0FBZDtBQUFBLFlBQXFCQyxPQUFPVCxHQUFHUyxJQUEvQjtBQUFBLFlBQXFDQyxVQUFVVixHQUFHVSxPQUFsRDtBQUFBLFlBQTJEQyxjQUFjWCxHQUFHVyxXQUE1RTtBQUFBLFlBQXlGQyxVQUFVWixHQUFHWSxPQUF0RztBQUNBLFlBQUlDLFFBQVEsVUFBWjtBQUNBLFlBQUlDLFlBQVlMLEtBQUs3SixJQUFMLENBQVUsa0JBQVYsQ0FBaEI7QUFDQSxpQkFBU21LLFNBQVQsQ0FBbUIvSixDQUFuQixFQUFzQjtBQUNsQixtQkFBTzJHLEVBQUUzRyxDQUFGLEVBQUtnSyxPQUFMLENBQWEsa0JBQWIsQ0FBUDtBQUNIO0FBQ0RGLGtCQUFVL0osR0FBVixDQUFjLFlBQVk7QUFDdEI0RyxjQUFFLElBQUYsRUFBUXNCLFdBQVIsQ0FBb0I0QixLQUFwQjtBQUNILFNBRkQ7QUFHQUosYUFBS25CLFFBQUwsQ0FBYyxtQkFBZCxFQUFtQyxPQUFuQyxFQUE0QyxVQUFVMkIsR0FBVixFQUFlO0FBQ3ZELGdCQUFJeEMsVUFBVXNDLFVBQVUsSUFBVixDQUFkO0FBQUEsZ0JBQStCRyxTQUFTekMsUUFBUTBDLElBQVIsQ0FBYSxZQUFiLENBQXhDO0FBQ0FDLHVCQUFXSCxHQUFYO0FBQ0FQLG9CQUFRVyxJQUFSLENBQWE1RCxVQUFVQyxJQUFWLEdBQWlCLFdBQWpCLEdBQStCd0QsTUFBNUMsRUFBb0QsVUFBVUksS0FBVixFQUFpQkMsR0FBakIsRUFBc0I7QUFDdEVDO0FBQ0Esb0JBQUloSixRQUFRLElBQUlpSixTQUFKLENBQWMsdUJBQWQsRUFBdUMsWUFBdkMsQ0FBWjtBQUNBZCw0QkFBWTNCLFFBQVosQ0FBcUI2QixLQUFyQjtBQUNBSCx3QkFBUWdCLFFBQVIsQ0FBaUIsR0FBakI7QUFDSCxhQUxEO0FBTUgsU0FURDtBQVVBakIsYUFBS25CLFFBQUwsQ0FBYyxrQkFBZCxFQUFrQyxPQUFsQyxFQUEyQyxZQUFZO0FBQ25ELGdCQUFJcUMsYUFBYVosVUFBVSxJQUFWLENBQWpCO0FBQUEsZ0JBQWtDYSxnQkFBZ0JELFdBQVdSLElBQVgsQ0FBZ0IsWUFBaEIsQ0FBbEQ7QUFBQSxnQkFBaUZVLFdBQVdDLFFBQVEsVUFBUixDQUE1RjtBQUNBLGdCQUFJRCxRQUFKLEVBQ0lsRSxFQUFFMUYsSUFBRixDQUFPMkksT0FBUCxFQUFnQixFQUFFbUIsUUFBUUMsUUFBUUMsU0FBbEIsRUFBNkJDLFVBQVUsVUFBdkMsRUFBbURDLGNBQWNQLGFBQWpFLEVBQWhCLEVBQWtHLFVBQVVMLEdBQVYsRUFBZTtBQUM3RyxvQkFBSUEsSUFBSWEsSUFBSixLQUFhLElBQWpCLEVBQXVCO0FBQ25CQywwQkFBTSxTQUFOO0FBQ0FWLCtCQUFXM0MsUUFBWCxDQUFvQjZCLEtBQXBCO0FBQ0g7QUFDSixhQUxELEVBS0csTUFMSDtBQU1QLFNBVEQ7QUFVSCxLQWhDVTtBQWlDWE4sZUFBVyxxQkFBWTtBQUNuQixhQUFLQyxLQUFMLEdBQWE7QUFDVEMsa0JBQU05QyxFQUFFLGVBQUYsQ0FERztBQUVUK0MscUJBQVMvQyxFQUFFLGdCQUFGLENBRkE7QUFHVGdELHlCQUFhaEQsRUFBRSxvQkFBRixDQUhKO0FBSVRpRCxxQkFBU25ELFVBQVVDLElBQVYsR0FBaUI7QUFKakIsU0FBYjtBQU1IO0FBeENVLENBQWY7QUEwQ0EsSUFBSTRFLGNBQWM7QUFDZHBFLFVBQU0sZ0JBQVk7QUFDZCxhQUFLcUUsV0FBTDtBQUNBLFlBQUkxQixRQUFRLFVBQVo7QUFDQSxZQUFJYixLQUFLLEtBQUtRLEtBQWQ7QUFBQSxZQUFxQi9ILE9BQU91SCxHQUFHdkgsSUFBL0I7QUFBQSxZQUFxQytKLFlBQVl4QyxHQUFHd0MsU0FBcEQ7QUFBQSxZQUErREMsV0FBV3pDLEdBQUd5QyxRQUE3RTtBQUFBLFlBQXVGQyxZQUFZMUMsR0FBRzBDLFNBQXRHO0FBQUEsWUFBaUhDLFlBQVkzQyxHQUFHMkMsU0FBaEk7QUFBQSxZQUEySUMsY0FBYzVDLEdBQUc0QyxXQUE1SjtBQUNBRixrQkFBVTlMLElBQVYsQ0FBZSx1QkFBZixFQUF3Q2lNLEtBQXhDLENBQThDLFlBQVk7QUFDdEQsZ0JBQUkvRyxJQUFJNkIsRUFBRSxJQUFGLEVBQVEvRyxJQUFSLENBQWEsR0FBYixDQUFSO0FBQUEsZ0JBQTJCa00sS0FBS2hILEVBQUVxRixJQUFGLENBQU8sYUFBUCxDQUFoQztBQUNBLGdCQUFJMUksS0FBS3NLLFFBQUwsQ0FBY2xDLEtBQWQsQ0FBSixFQUEwQjtBQUN0Qi9FLGtCQUFFbUQsV0FBRixDQUFjNkQsRUFBZDtBQUNBcksscUJBQUt3RyxXQUFMLENBQWlCNEIsS0FBakI7QUFDQSxvQkFBSW5FLE9BQU9zRyxVQUFQLEdBQW9CLElBQXhCLEVBQThCO0FBQzFCTiw4QkFBVXpELFdBQVYsQ0FBc0IsUUFBdEI7QUFDSDtBQUNKLGFBTkQsTUFPSztBQUNEbkQsa0JBQUVrRCxRQUFGLENBQVc4RCxFQUFYO0FBQ0FySyxxQkFBS3VHLFFBQUwsQ0FBYzZCLEtBQWQ7QUFDQSxvQkFBSW5FLE9BQU9zRyxVQUFQLEdBQW9CLElBQXhCLEVBQThCO0FBQzFCTiw4QkFBVTFELFFBQVYsQ0FBbUIsUUFBbkI7QUFDSDtBQUNKO0FBQ0osU0FoQkQ7QUFpQkE0RCxvQkFBWXRELFFBQVosQ0FBcUIsYUFBckIsRUFBb0MsT0FBcEMsRUFBNkMsVUFBVTJCLEdBQVYsRUFBZTtBQUN4RCxnQkFBSUEsSUFBSWdDLE9BQUosSUFBZSxJQUFuQixFQUF5QjtBQUNyQmhDLG9CQUFJaUMsY0FBSjtBQUNBakMsb0JBQUlrQyxlQUFKO0FBQ0Esb0JBQUlDLFFBQVF6RixFQUFFLElBQUYsRUFBUXdELElBQVIsQ0FBYSxXQUFiLENBQVo7QUFDQSxvQkFBSWtDLE9BQU8xRixFQUFFLElBQUYsRUFBUXdELElBQVIsQ0FBYSxNQUFiLENBQVg7QUFDQXFCLDBCQUFVdkQsV0FBVixDQUFzQjRCLEtBQXRCO0FBQ0EyQiwwQkFBVW5CLElBQVYsQ0FBZWdDLElBQWYsRUFBcUJqQyxVQUFyQjtBQUNBb0IsMEJBQVVkLFFBQVYsQ0FBbUIsRUFBbkI7QUFDSDtBQUNKLFNBVkQ7QUFXQWdCLGtCQUFVcEQsUUFBVixDQUFtQixZQUFuQixFQUFpQyxPQUFqQyxFQUEwQyxVQUFVMkIsR0FBVixFQUFlO0FBQ3JELGdCQUFJcUMsUUFBUTNGLEVBQUUsSUFBRixDQUFaO0FBQUEsZ0JBQXFCdUQsU0FBU29DLE1BQU1uQyxJQUFOLENBQVcsVUFBWCxDQUE5QjtBQUFBLGdCQUFzRG9DLGlCQUFpQkQsTUFBTXRDLE9BQU4sQ0FBYyxrQkFBZCxDQUF2RTtBQUNBLGdCQUFJYyxRQUFRLFNBQVIsQ0FBSixFQUF3QjtBQUNwQlYsMkJBQVdILEdBQVg7QUFDQXRELGtCQUFFMUYsSUFBRixDQUFPd0ssV0FBVyxTQUFsQixFQUE2QixFQUFFZSxhQUFhdEMsTUFBZixFQUF1QnVDLGNBQWMsRUFBckMsRUFBeUMxQixRQUFRckYsT0FBT3NGLE9BQVAsQ0FBZUMsU0FBaEUsRUFBN0IsRUFBMEcsVUFBVVYsR0FBVixFQUFlO0FBQ3JILHdCQUFJQSxJQUFJYSxJQUFKLEtBQWEsR0FBakIsRUFBc0I7QUFDbEJtQix1Q0FBZXZFLFFBQWYsQ0FBd0IsVUFBeEI7QUFDQXdDO0FBQ0FnQixrQ0FBVXhELFFBQVYsQ0FBbUIsT0FBbkI7QUFDSDtBQUNKLGlCQU5EO0FBT0g7QUFDSixTQVpEO0FBYUF3RCxrQkFBVWxELFFBQVYsQ0FBbUIscUJBQW5CLEVBQTBDLE9BQTFDLEVBQW1ELFlBQVk7QUFDM0RrRCxzQkFBVXhELFFBQVYsQ0FBbUI2QixLQUFuQjtBQUNILFNBRkQ7QUFHSCxLQWpEYTtBQWtEZDBCLGlCQUFhLHVCQUFZO0FBQ3JCLFlBQUk5SixPQUFPa0YsRUFBRSxhQUFGLENBQVg7QUFBQSxZQUE2QitFLFlBQVkvRSxFQUFFLGFBQUYsQ0FBekM7QUFBQSxZQUEyRCtGLFlBQVkvRixFQUFFLGNBQUYsQ0FBdkU7QUFBQSxZQUEwRnFDLEtBQUt0RCxPQUFPa0IsUUFBdEc7QUFBQSxZQUFnSCtGLFdBQVczRCxHQUFHMkQsUUFBOUg7QUFBQSxZQUF3SUMsT0FBTzVELEdBQUc0RCxJQUFsSjtBQUNBLGFBQUtwRCxLQUFMLEdBQWE7QUFDVC9ILGtCQUFNQSxJQURHO0FBRVRnSyxzQkFBVWtCLFFBRkQ7QUFHVGpCLHVCQUFXQSxTQUhGO0FBSVRGLHVCQUFXa0IsU0FKRjtBQUtUZCx5QkFBYUYsVUFBVTlMLElBQVYsQ0FBZSxRQUFmO0FBTEosU0FBYjtBQU9IO0FBM0RhLENBQWxCO0FBNkRBLElBQUlpTixVQUFVO0FBQ1YzRixVQUFNLGdCQUFZO0FBQ2QsYUFBS3FDLFNBQUw7QUFDQSxZQUFJUCxLQUFLLEtBQUtRLEtBQWQ7QUFBQSxZQUFxQnNELGFBQWE5RCxHQUFHOEQsVUFBckM7QUFBQSxZQUFpREMsU0FBUy9ELEdBQUcrRCxNQUE3RDtBQUFBLFlBQXFFQyxjQUFjaEUsR0FBR2dFLFdBQXRGO0FBQUEsWUFBbUdDLFlBQVlqRSxHQUFHaUUsU0FBbEg7QUFBQSxZQUE2SEMsYUFBYWxFLEdBQUdrRSxVQUE3STtBQUNBLFlBQUlKLFdBQVc1SixNQUFYLEtBQXNCLENBQTFCLEVBQTZCO0FBQ3pCK0osc0JBQVVwQixLQUFWLENBQWdCLFlBQVk7QUFDeEJtQiw0QkFBWTlFLFdBQVosQ0FBd0IsVUFBeEIsRUFBb0N3QyxRQUFwQyxDQUE2QyxHQUE3QztBQUNILGFBRkQ7QUFHQXdDLHVCQUFXckIsS0FBWCxDQUFpQixZQUFZO0FBQ3pCa0IsdUJBQU83RSxXQUFQLENBQW1CLFVBQW5CLEVBQStCd0MsUUFBL0IsQ0FBd0MsR0FBeEM7QUFDSCxhQUZEO0FBR0g7QUFDRDdKLGdCQUFRQyxHQUFSLENBQVksOEJBQVo7QUFDSCxLQWJTO0FBY1Z5SSxlQUFXLHFCQUFZO0FBQ25CLGFBQUtDLEtBQUwsR0FBYTtBQUNUc0Qsd0JBQVluRyxFQUFFLG1CQUFGLENBREg7QUFFVG9HLG9CQUFRcEcsRUFBRSxtQkFBRixDQUZDO0FBR1RxRyx5QkFBYXJHLEVBQUUsa0JBQUYsQ0FISjtBQUlUc0csdUJBQVd0RyxFQUFFLFNBQUYsQ0FKRjtBQUtUdUcsd0JBQVl2RyxFQUFFLFNBQUY7QUFMSCxTQUFiO0FBT0g7QUF0QlMsQ0FBZDs7QUF5QkEsU0FBU3lELFVBQVQsQ0FBb0IrQyxDQUFwQixFQUF1QkMsQ0FBdkIsRUFBMEI7QUFDdEIsUUFBSUEsTUFBTSxLQUFLLENBQWYsRUFBa0I7QUFBRUEsWUFBSSxRQUFKO0FBQWU7QUFDbkMsUUFBSUMsVUFBVW5LLE1BQVYsR0FBbUIsQ0FBdkIsRUFBMEI7QUFDdEJyQyxnQkFBUUMsR0FBUixDQUFZLHNCQUFaO0FBQ0gsS0FGRCxNQUdLO0FBQ0RELGdCQUFRQyxHQUFSLENBQVksYUFBWjtBQUNBLFlBQUlzTSxLQUFLLFNBQVQsRUFBb0I7QUFDaEJ2TSxvQkFBUUMsR0FBUixDQUFZLHFCQUFaO0FBQ0gsU0FGRCxNQUdLO0FBQ0RELG9CQUFReU0sSUFBUixDQUFhLG1CQUFiO0FBQ0g7QUFDSjtBQUNKOztBQUVELFNBQVM5QyxZQUFULEdBQXdCO0FBQ3BCM0osWUFBUUMsR0FBUixDQUFZLGlCQUFaO0FBQ0g7O0FBRUQ2RixFQUFFLFlBQVk7QUFDVk0sZUFBV0MsSUFBWDtBQUNBb0MsYUFBU3BDLElBQVQ7QUFDQW9FLGdCQUFZcEUsSUFBWjtBQUNBSyxXQUFPTCxJQUFQO0FBQ0EyRixZQUFRM0YsSUFBUjtBQUNILENBTkQsRTs7Ozs7OztBQ3ZQQTtBQUNBO0FBQ0E7QUFDQSx3VEFBd0s7QUFDeEs7QUFDQSxpUUFBa0o7QUFDbEo7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLCtFQUErRSxzREFBc0QsSUFBSTtBQUN6SSxtQ0FBbUM7O0FBRW5DO0FBQ0EsWUFBaUI7QUFDakI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNILENBQUM7O0FBRUQ7Ozs7Ozs7O0FDdkNBO0FBQ0E7QUFDQTtBQUNBLG9VQUF3SztBQUN4SztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLCtFQUErRSxzREFBc0QsSUFBSTs7QUFFekk7QUFDQSxZQUFpQjtBQUNqQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0gsQ0FBQzs7QUFFRDs7Ozs7Ozs7QUN0Q0E7QUFDQTtBQUNBO0FBQ0EsaVhBQXNNO0FBQ3RNO0FBQ0E7QUFDQTtBQUNBLHlVQUF3SztBQUN4SztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLCtFQUErRSxzREFBc0QsSUFBSTs7QUFFekk7QUFDQSxZQUFpQjtBQUNqQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0gsQ0FBQzs7QUFFRCIsImZpbGUiOiJcXGpzXFxjbS1kYXNoYm9hcmQuanMiLCJzb3VyY2VzQ29udGVudCI6WyIgXHQvLyBUaGUgbW9kdWxlIGNhY2hlXG4gXHR2YXIgaW5zdGFsbGVkTW9kdWxlcyA9IHt9O1xuXG4gXHQvLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuIFx0ZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXG4gXHRcdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuIFx0XHRpZihpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSkge1xuIFx0XHRcdHJldHVybiBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXS5leHBvcnRzO1xuIFx0XHR9XG4gXHRcdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG4gXHRcdHZhciBtb2R1bGUgPSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSA9IHtcbiBcdFx0XHRpOiBtb2R1bGVJZCxcbiBcdFx0XHRsOiBmYWxzZSxcbiBcdFx0XHRleHBvcnRzOiB7fVxuIFx0XHR9O1xuXG4gXHRcdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuIFx0XHRtb2R1bGVzW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuIFx0XHQvLyBGbGFnIHRoZSBtb2R1bGUgYXMgbG9hZGVkXG4gXHRcdG1vZHVsZS5sID0gdHJ1ZTtcblxuIFx0XHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuIFx0XHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG4gXHR9XG5cblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGVzIG9iamVjdCAoX193ZWJwYWNrX21vZHVsZXNfXylcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubSA9IG1vZHVsZXM7XG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlIGNhY2hlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmMgPSBpbnN0YWxsZWRNb2R1bGVzO1xuXG4gXHQvLyBkZWZpbmUgZ2V0dGVyIGZ1bmN0aW9uIGZvciBoYXJtb255IGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uZCA9IGZ1bmN0aW9uKGV4cG9ydHMsIG5hbWUsIGdldHRlcikge1xuIFx0XHRpZighX193ZWJwYWNrX3JlcXVpcmVfXy5vKGV4cG9ydHMsIG5hbWUpKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIG5hbWUsIHtcbiBcdFx0XHRcdGNvbmZpZ3VyYWJsZTogZmFsc2UsXG4gXHRcdFx0XHRlbnVtZXJhYmxlOiB0cnVlLFxuIFx0XHRcdFx0Z2V0OiBnZXR0ZXJcbiBcdFx0XHR9KTtcbiBcdFx0fVxuIFx0fTtcblxuIFx0Ly8gZ2V0RGVmYXVsdEV4cG9ydCBmdW5jdGlvbiBmb3IgY29tcGF0aWJpbGl0eSB3aXRoIG5vbi1oYXJtb255IG1vZHVsZXNcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubiA9IGZ1bmN0aW9uKG1vZHVsZSkge1xuIFx0XHR2YXIgZ2V0dGVyID0gbW9kdWxlICYmIG1vZHVsZS5fX2VzTW9kdWxlID9cbiBcdFx0XHRmdW5jdGlvbiBnZXREZWZhdWx0KCkgeyByZXR1cm4gbW9kdWxlWydkZWZhdWx0J107IH0gOlxuIFx0XHRcdGZ1bmN0aW9uIGdldE1vZHVsZUV4cG9ydHMoKSB7IHJldHVybiBtb2R1bGU7IH07XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18uZChnZXR0ZXIsICdhJywgZ2V0dGVyKTtcbiBcdFx0cmV0dXJuIGdldHRlcjtcbiBcdH07XG5cbiBcdC8vIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbFxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5vID0gZnVuY3Rpb24ob2JqZWN0LCBwcm9wZXJ0eSkgeyByZXR1cm4gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKG9iamVjdCwgcHJvcGVydHkpOyB9O1xuXG4gXHQvLyBfX3dlYnBhY2tfcHVibGljX3BhdGhfX1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5wID0gXCJcIjtcblxuIFx0Ly8gTG9hZCBlbnRyeSBtb2R1bGUgYW5kIHJldHVybiBleHBvcnRzXG4gXHRyZXR1cm4gX193ZWJwYWNrX3JlcXVpcmVfXyhfX3dlYnBhY2tfcmVxdWlyZV9fLnMgPSAxKTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyB3ZWJwYWNrL2Jvb3RzdHJhcCAyYTI2OTFmYWVmNjIwZjRkMTYwMCIsIjx0ZW1wbGF0ZT5cclxuICAgIDxkaXYgY2xhc3M9XCJtb2RhbCBmYWRlXCIgcm9sZT1cImRpYWxvZ1wiPlxyXG4gICAgICA8ZGl2IGNsYXNzPVwibW9kYWwtZGlhbG9nXCI+XHJcbiAgICAgICAgPGRpdiBjbGFzcz1cIm1vZGFsLWNvbnRlbnRcIj5cclxuICAgICAgICAgIDxkaXYgY2xhc3M9XCJtb2RhbC1oZWFkZXJcIj5cclxuICAgICAgICAgICAgPGJ1dHRvbiB0eXBlPVwiYnV0dG9uXCIgY2xhc3M9XCJjbG9zZVwiIEBjbGljay5wcmV2ZW50PVwiaGlkZVNlbGZcIj4mdGltZXM7PC9idXR0b24+XHJcbiAgICAgICAgICAgIDxoNCBjbGFzcz1cIm1vZGFsLXRpdGxlXCI+XHJcbiAgICAgICAgICAgICAgPHNsb3QgbmFtZT1cIm1vZGFsLXRpdGxlXCI+PC9zbG90PlxyXG4gICAgICAgICAgICA8L2g0PlxyXG4gICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICA8ZGl2IGNsYXNzPVwibW9kYWwtYm9keVwiPlxyXG4gICAgICAgICAgICA8c2xvdCBuYW1lPVwibW9kYWwtYm9keVwiPjwvc2xvdD5cclxuICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgPGRpdiBjbGFzcz1cIm1vZGVsLWJvZHlcIj5cclxuICAgICAgICAgICA8c2xvdCBuYW1lPVwibW9kYWwtaW1hZ2VcIj48L3Nsb3Q+XHJcbiAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgIDxkaXYgY2xhc3M9XCJtb2RhbC1mb290ZXJcIj5cclxuICAgICAgICAgICAgPGJ1dHRvbiB0eXBlPVwiYnV0dG9uXCIgY2xhc3M9XCJidG4gYnRuLWRlZmF1bHRcIiAgQGNsaWNrLnByZXZlbnQ9XCJoaWRlU2VsZlwiID5DbG9zZTwvYnV0dG9uPlxyXG4gICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgPC9kaXY+XHJcbiAgICAgIDwvZGl2PlxyXG4gICAgPC9kaXY+XHJcbjwvdGVtcGxhdGU+XHJcblxyXG48c2NyaXB0PlxyXG4gICAgZXhwb3J0IGRlZmF1bHQge1xyXG5cclxuICAgICAgbmFtZTogJ01vZGFsJyxcclxuICAgICAgcHJvcHM6IFsnc2hvdyddLFxyXG4gICAgICBkYXRhICgpIHtcclxuICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgXHJcbiAgICAgICAgfTtcclxuICAgICAgfSxcclxuICAgICAgd2F0Y2ggOiB7XHJcbiAgICAgICAgIHNob3coKSB7XHJcbiAgICAgICAgICAgIHRoaXMuc2hvdyA/IHRoaXMuc2hvd1NlbGYoKSA6IHRoaXMuaGlkZVNlbGYoKTtcclxuICAgICAgICAgfVxyXG4gICAgICB9LFxyXG4gICAgICBtZXRob2RzOiB7XHJcbiAgICAgICAgc2hvd1NlbGYgKCkge1xyXG4gICAgICAgICAgICAkKHRoaXMuJGVsKS5tb2RhbCgnc2hvdycpO1xyXG4gICAgICAgIH0sXHJcbiAgICAgICAgdG9nZ2xlU2VsZigpIHtcclxuICAgICAgICAgICAgJCh0aGlzLiRlbCkubW9kYWwoJ3RvZ2dsZScpO1xyXG4gICAgICAgIH0sXHJcbiAgICAgICAgaGlkZVNlbGYoKSB7XHJcbiAgICAgICAgICAgICQodGhpcy4kZWwpLm1vZGFsKCdoaWRlJyk7XHJcbiAgICAgICAgICAgIHRoaXMuJGVtaXQoJ2Nsb3NlZCcpO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfTtcclxuPC9zY3JpcHQ+XG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIE1vZGFsLnZ1ZT85NDYwY2ViYyIsIjxzY3JpcHQ+XHJcbmltcG9ydCBNb2RhbCBmcm9tICcuLi9Nb2RhbC52dWUnO1xyXG5pbXBvcnQgdlNlbGVjdCBmcm9tICd2dWUtc2VsZWN0JztcclxuaW1wb3J0IEV2ZW50IGZyb20gXCIuLi8uLi9Nb2RlbHMvRXZlbnQuanNcIjtcclxuXHJcbiBleHBvcnQgZGVmYXVsdCB7XHJcblx0IGNvbXBvbmVudHM6e01vZGFsLHZTZWxlY3R9LFxyXG5cdCBuYW1lIDogJ0V2ZW50c1BhZ2UnLFxyXG4gXHRkYXRhKCkge1xyXG4gXHRcdHJldHVybiB7XHJcblx0XHRcdCBpbWFnZXM6IHdpbmRvdy5sb2NhdGlvbi5wcm90b2NvbCArXCIvL1wiKyB3aW5kb3cubG9jYXRpb24uaG9zdCtcIi9pbWFnZXMvYXZhdGFyLmpwZ1wiLFxyXG5cdFx0XHQgdXJsIDogJ3N0b3JhZ2UvYXBwL3B1YmxpYy9ldmVudC1pbWFnZXMvaW1hZ2VzLycsXHJcblx0XHRcdCBzaG93QWRkRm9ybTogZmFsc2UsXHJcblx0XHRcdCBzaG93RWRpdDpmYWxzZSxcclxuXHRcdFx0IHNlYXJjaCA6ICcnLFxyXG5cdFx0XHQgZXZlbnRzIDogW10sXHJcblx0XHRcdCBzaG93IDogZmFsc2UsXHJcblx0XHRcdCBsb2FkZWQgOiBmYWxzZSxcclxuXHRcdFx0IHBpY2tlZEV2ZW50OkV2ZW50LnBhcmFtcyxcclxuXHRcdFx0IGFkZGVkRXZlbnQgOkV2ZW50LnBhcmFtc1xyXG5cdFx0XHRcclxuXHJcbiBcdFx0fTtcclxuXHQgfSxcclxuXHQgbW91bnRlZCgpe1xyXG5cdFx0ICQoXCIjZGF0ZXRpbWVwaWNrZXJfYWRkXCIpLmRhdGV0aW1lcGlja2VyKHtcclxuXHRcdFx0ICBmb3JtYXQ6J1lZWVktTU0tREQgSEg6bW06c3MnLFxyXG4gICAgICAgICAgICAgIG1pbkRhdGU6IG5ldyBEYXRlKClcclxuXHRcdCB9KTtcclxuXHQgfSxcclxuXHQgY29tcHV0ZWQ6IHtcclxuXHQgICBuZXdfaW1hZ2UoKXtcclxuICAgICAgICAgICAgIFxyXG5cdCAgIH0sXHJcbiAgICAgICBsb2FkX21lc3NhZ2UoKXtcclxuXHRcdCAgIHJldHVybiAodGhpcy5sb2FkZWQgPT0gdHJ1ZSAmJiB0aGlzLmV2ZW50cy5sZW5ndGggPT0gMCkgPyAnRXZlbnQgcmVwb3NpdG9yeSBpcyBlbXB0eSB1cGxvYWQgYW4gZXZlbnQnOidMb2FkaW5nIEV2ZW50cyBpbiBhIHNlYyc7XHJcblx0ICAgfSwgXHJcblx0ICAgZmlsdGVyZWRFdmVudCA6IGZ1bmN0aW9uIGZpbHRlcmVkRXZlbnQoKXtcclxuXHQgICAgIHZhciBfdGhpcyA9IHRoaXM7XHJcblxyXG5cdCAgICAgcmV0dXJuIF90aGlzLmV2ZW50cy5maWx0ZXIoZnVuY3Rpb24oZXZlbnQpe1xyXG4gICAgICAgICAgIHJldHVybiBldmVudC50aXRsZS50b0xvd2VyQ2FzZSgpLmluY2x1ZGVzKF90aGlzLnNlYXJjaC50b0xvd2VyQ2FzZSgpKTtcclxuXHQgICAgIH0pO1xyXG5cdCAgIH1cclxuXHQgfSxcdFxyXG5cdCBjcmVhdGVkKCl7XHJcbiAgICAgICAgIEV2ZW50LmFsbCgpLnRoZW4oKHJlc3BvbnNlKT0+e1xyXG5cdFx0XHQgdGhpcy5sb2FkZWQgPSB0cnVlO1xyXG5cdFx0XHQgdGhpcy5ldmVudHMgPSByZXNwb25zZS5kYXRhO1xyXG5cdFx0IH0pLmNhdGNoKChlcnIpPT57XHJcblx0XHRcdCBjb25zb2xlLmxvZygndGhlcmUgd2FzICcrIGVycik7XHJcblx0XHQgfSk7XHJcblx0XHRcclxuXHRcdFxyXG5cdCB9LFxyXG4gXHRtZXRob2RzOiB7XHJcbiBcdFx0IHRvZ2dsZUZvcm0oKSB7XHJcblx0XHRcdCB0aGlzLnNob3dBZGRGb3JtID0gIXRoaXMuc2hvd0FkZEZvcm07XHJcblx0XHRcdCBcclxuXHRcdCB9LFxyXG5cdFx0IHRvZ2dsZUVkaXQoKSB7XHJcblx0XHRcdCB0aGlzLnNob3dFZGl0ID0gIXRoaXMuc2hvd0VkaXQ7XHJcblx0XHQgfSxcclxuXHRcdCBlZGl0RXZlbnQoaWQpe1xyXG5cdFx0XHQgdGhpcy5zaG93RWRpdCA9IHRydWU7XHJcblx0XHRcdHRoaXMucGlja2VkRXZlbnQgPSB7fTtcclxuXHRcdFx0T2JqZWN0LmFzc2lnbih0aGlzLnBpY2tlZEV2ZW50LEV2ZW50LmZpbmQoaWQpKTtcclxuXHRcdFx0XHJcblx0XHQgfSxcclxuXHRcdCBzYXZlRXZlbnQoKXtcclxuXHRcdFx0IHZhciBkYXRhID0gIHRoaXMuYWRkZWRFdmVudDtcclxuICAgICAgICAgICAgYXhpb3MucG9zdCgnL2NtLXVzZXIvZXZlbnRzL2FkZCcsZGF0YSkudGhlbigocmVzKT0+e1xyXG5cdFx0XHRcdFxyXG5cdFx0XHR9KS5jYXRjaCgoZXJyKT0+e1xyXG5cdFx0XHRcdGNvbnNvbGUubG9nKCdlcnJvciBkdWUgdG8gJyArIGVycik7XHJcblx0XHRcdH0pO1xyXG5cdFx0IH0sXHJcblx0XHQgdXBkYXRlKCl7XHJcblx0XHRcdCB2YXIgcGlja2VkID0gdGhpcy5waWNrZWRFdmVudDtcclxuICAgICAgICAgICBheGlvcy5wb3N0KCcvY20tdXNlci9ldmVudHMvdXBkYXRlJyxwaWNrZWQpXHJcblx0XHQgfSxcclxuXHRcdCBoaWRlRWRpdCgpe1xyXG5cdFx0XHQgdGhpcy5zaG93RWRpdCA9IGZhbHNlO1xyXG5cdFx0IH0sXHJcblx0XHRcclxuIFx0fVxyXG4gfVxyXG48L3NjcmlwdD5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gRXZlbnRzUGFnZS52dWU/MWU0MDkyMTQiLCI8c2NyaXB0PlxyXG5pbXBvcnQgTW9kYWwgZnJvbSAnLi4vTW9kYWwudnVlJztcclxuaW1wb3J0IHZTZWxlY3QgZnJvbSAndnVlLXNlbGVjdCc7XHJcbmltcG9ydCBNZXJjaGFuZGlzZSBmcm9tICcuLi8uLi9Nb2RlbHMvTWVyY2hhbmRpc2UuanMnO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQge1xyXG5cclxuICBuYW1lOiAnTWVyY2hhbmRpc2VQYWdlJyxcclxuICBjb21wb25lbnRzOiB7TW9kYWwsIHZTZWxlY3R9LFxyXG4gIGNyZWF0ZWQoKSB7XHJcbiAgICBNZXJjaGFuZGlzZS5hbGwoKS50aGVuKChkYXRhKSA9PiB7XHJcbiAgICAgICAgdGhpcy5sb2FkZWQgPSB0cnVlO1xyXG4gICAgICAgIHRoaXMubWVyY2hzID0gTWVyY2hhbmRpc2UubWVyY2hzO1xyXG4gICAgfSk7XHJcbiAgfSxcclxuICBtb3VudGVkKCkge1xyXG4gIH0sXHJcbiAgZGF0YSAoKSB7XHJcbiAgICByZXR1cm4ge1xyXG4gICAgICAgIHNob3dGb3JtOiB0cnVlLFxyXG4gICAgICAgIHNob3dFZGl0OiBmYWxzZSxcclxuICAgICAgICBtZXJjaHMgOiBbXSxcclxuICAgICAgICBsb2FkZWQ6IGZhbHNlLFxyXG4gICAgICAgIGNhdGVnb3J5OiBcIlNoaXJ0c1wiLFxyXG4gICAgICAgIHBpY2tlZE1lcmNoYW5kaXNlOiBNZXJjaGFuZGlzZS5mb3JtLFxyXG4gICAgfTtcclxuICB9LFxyXG4gIGNvbXB1dGVkOiB7XHJcbiAgICBsb2FkX21lc3NhZ2UgKCkge1xyXG4gICAgICByZXR1cm4gdGhpcy5sb2FkZWQgPT0gZmFsc2UgJiYgdGhpcy5tZXJjaHMubGVuZ3RoIDw9IDAgPyAnTG9hZGluZyBNZXNzYWdlcycgOiAnTWVyY2hhbmRpc2UgcmVwb3NpdG9yeSBpcyBlbXB0eS4gUGxlYXNlIHVwbG9hZCBhbiBtZXJjaGFuZGlzZS4nO1xyXG4gICAgfVxyXG4gIH0sXHJcbiAgbWV0aG9kczoge1xyXG4gICAgdXBkYXRlTWVyY2hhbmRpc2UgOiBNZXJjaGFuZGlzZS51cGRhdGUsXHJcbiAgICB1cGRhdGVWYWx1ZSh2YWx1ZSkge1xyXG4gICAgICB2YXIgZm9ybWF0dGVkVmFsdWUgPSB2YWx1ZS50cmltKClcclxuICAgICAgLnNsaWNlKFxyXG4gICAgICAgICAgMCxcclxuICAgICAgICAgIHZhbHVlLmluZGV4T2YoJy4nKSA9PT0gLTFcclxuICAgICAgICAgICAgPyB2YWx1ZS5sZW5ndGhcclxuICAgICAgICAgICAgOiB2YWx1ZS5pbmRleE9mKCcuJykgKyAzXHJcbiAgICAgICAgKTtcclxuXHJcbiAgICAgIGlmIChmb3JtYXR0ZWRWYWx1ZSAhPT0gdmFsdWUpIHtcclxuICAgICAgICB0aGlzLnBpY2tlZE1lcmNoYW5kaXNlLnByaWNlID0gZm9ybWF0dGVkVmFsdWVcclxuICAgICAgfVxyXG5cclxuICAgICAgdGhpcy5waWNrZWRNZXJjaGFuZGlzZS5wcmljZSA9IE51bWJlcihmb3JtYXR0ZWRWYWx1ZSk7XHJcbiAgICB9LFxyXG4gICAgdG9nZ2xlRm9ybSgpIHtcclxuICAgICAgdGhpcy5zaG93Rm9ybSA9ICF0aGlzLnNob3dGb3JtO1xyXG4gICAgICB0aGlzLmVkaXRGb3JtID0gIXRoaXMuZWRpdEZvcm07XHJcbiAgICB9LFxyXG4gICAgdG9nZ2xlRWRpdCgpIHtcclxuICAgICAgdGhpcy5zaG93RWRpdCA9ICF0aGlzLnNob3dFZGl0O1xyXG4gICAgfSxcclxuICAgIGhpZGVFZGl0KCkge1xyXG4gICAgICB0aGlzLnNob3dFZGl0ID0gZmFsc2U7XHJcbiAgICB9LFxyXG4gICAgZWRpdE1lcmNoYW5kaXNlIChpZCkge1xyXG4gICAgICB0aGlzLnNob3dGb3JtID0gZmFsc2U7XHJcbiAgICAgIHRoaXMuc2hvd0VkaXQgPSB0cnVlO1xyXG4gICAgICB0aGlzLnBpY2tlZE1lcmNoYW5kaXNlID0gTWVyY2hhbmRpc2UuZmluZChpZCk7XHJcbiAgICB9LFxyXG4gICAgc2h1ZmZsZSgpIHtcclxuICAgICAgdGhpcy5tZXJjaHMgPSBfLnNodWZmbGUodGhpcy5tZXJjaHMpO1xyXG4gICAgfSxcclxuICAgIHJlbW92ZU1lcmNoYW5kaXNlIChpZCkge1xyXG4gICAgICBNZXJjaGFuZGlzZS5yZW1vdmUoaWQpO1xyXG4gICAgfVxyXG4gIH1cclxufTtcclxuPC9zY3JpcHQ+XHJcblxyXG48c3R5bGU+XHJcbiAgLmZsaXAtbGlzdC1tb3ZlIHtcclxuICAgIHRyYW5zaXRpb246IHRyYW5zZm9ybSAxcztcclxuICB9XHJcbjwvc3R5bGU+XHJcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyBNZXJjaGFuZGlzZVBhZ2UudnVlPzA0YjAyMGY0IiwiZXhwb3J0cyA9IG1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcIi4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2xpYi9jc3MtYmFzZS5qc1wiKSh0cnVlKTtcbi8vIGltcG9ydHNcblxuXG4vLyBtb2R1bGVcbmV4cG9ydHMucHVzaChbbW9kdWxlLmlkLCBcIlxcbi5mbGlwLWxpc3QtbW92ZSB7XFxuICB0cmFuc2l0aW9uOiB0cmFuc2Zvcm0gMXM7XFxufVxcblwiLCBcIlwiLCB7XCJ2ZXJzaW9uXCI6MyxcInNvdXJjZXNcIjpbXCJDOi9Vc2Vycy9rZWFuaWEvRGVza3RvcC93YXZlc3RlY2gvTmV3S2V0dGxlL2V2ZW50X2tldHRsZS9yZXNvdXJjZXMvYXNzZXRzL2pzL2NvbXBvbmVudHMvY2xpZW50L01lcmNoYW5kaXNlUGFnZS52dWU/MDRiMDIwZjRcIl0sXCJuYW1lc1wiOltdLFwibWFwcGluZ3NcIjpcIjtBQTJFQTtFQUNBLHlCQUFBO0NBQ0FcIixcImZpbGVcIjpcIk1lcmNoYW5kaXNlUGFnZS52dWVcIixcInNvdXJjZXNDb250ZW50XCI6W1wiPHNjcmlwdD5cXHJcXG5pbXBvcnQgTW9kYWwgZnJvbSAnLi4vTW9kYWwudnVlJztcXHJcXG5pbXBvcnQgdlNlbGVjdCBmcm9tICd2dWUtc2VsZWN0JztcXHJcXG5pbXBvcnQgTWVyY2hhbmRpc2UgZnJvbSAnLi4vLi4vTW9kZWxzL01lcmNoYW5kaXNlLmpzJztcXHJcXG5cXHJcXG5leHBvcnQgZGVmYXVsdCB7XFxyXFxuXFxyXFxuICBuYW1lOiAnTWVyY2hhbmRpc2VQYWdlJyxcXHJcXG4gIGNvbXBvbmVudHM6IHtNb2RhbCwgdlNlbGVjdH0sXFxyXFxuICBjcmVhdGVkKCkge1xcclxcbiAgICBNZXJjaGFuZGlzZS5hbGwoKS50aGVuKChkYXRhKSA9PiB7XFxyXFxuICAgICAgICB0aGlzLmxvYWRlZCA9IHRydWU7XFxyXFxuICAgICAgICB0aGlzLm1lcmNocyA9IE1lcmNoYW5kaXNlLm1lcmNocztcXHJcXG4gICAgfSk7XFxyXFxuICB9LFxcclxcbiAgbW91bnRlZCgpIHtcXHJcXG4gIH0sXFxyXFxuICBkYXRhICgpIHtcXHJcXG4gICAgcmV0dXJuIHtcXHJcXG4gICAgICAgIHNob3dGb3JtOiB0cnVlLFxcclxcbiAgICAgICAgc2hvd0VkaXQ6IGZhbHNlLFxcclxcbiAgICAgICAgbWVyY2hzIDogW10sXFxyXFxuICAgICAgICBsb2FkZWQ6IGZhbHNlLFxcclxcbiAgICAgICAgY2F0ZWdvcnk6IFxcXCJTaGlydHNcXFwiLFxcclxcbiAgICAgICAgcGlja2VkTWVyY2hhbmRpc2U6IE1lcmNoYW5kaXNlLmZvcm0sXFxyXFxuICAgIH07XFxyXFxuICB9LFxcclxcbiAgY29tcHV0ZWQ6IHtcXHJcXG4gICAgbG9hZF9tZXNzYWdlICgpIHtcXHJcXG4gICAgICByZXR1cm4gdGhpcy5sb2FkZWQgPT0gZmFsc2UgJiYgdGhpcy5tZXJjaHMubGVuZ3RoIDw9IDAgPyAnTG9hZGluZyBNZXNzYWdlcycgOiAnTWVyY2hhbmRpc2UgcmVwb3NpdG9yeSBpcyBlbXB0eS4gUGxlYXNlIHVwbG9hZCBhbiBtZXJjaGFuZGlzZS4nO1xcclxcbiAgICB9XFxyXFxuICB9LFxcclxcbiAgbWV0aG9kczoge1xcclxcbiAgICB1cGRhdGVNZXJjaGFuZGlzZSA6IE1lcmNoYW5kaXNlLnVwZGF0ZSxcXHJcXG4gICAgdXBkYXRlVmFsdWUodmFsdWUpIHtcXHJcXG4gICAgICB2YXIgZm9ybWF0dGVkVmFsdWUgPSB2YWx1ZS50cmltKClcXHJcXG4gICAgICAuc2xpY2UoXFxyXFxuICAgICAgICAgIDAsXFxyXFxuICAgICAgICAgIHZhbHVlLmluZGV4T2YoJy4nKSA9PT0gLTFcXHJcXG4gICAgICAgICAgICA/IHZhbHVlLmxlbmd0aFxcclxcbiAgICAgICAgICAgIDogdmFsdWUuaW5kZXhPZignLicpICsgM1xcclxcbiAgICAgICAgKTtcXHJcXG5cXHJcXG4gICAgICBpZiAoZm9ybWF0dGVkVmFsdWUgIT09IHZhbHVlKSB7XFxyXFxuICAgICAgICB0aGlzLnBpY2tlZE1lcmNoYW5kaXNlLnByaWNlID0gZm9ybWF0dGVkVmFsdWVcXHJcXG4gICAgICB9XFxyXFxuXFxyXFxuICAgICAgdGhpcy5waWNrZWRNZXJjaGFuZGlzZS5wcmljZSA9IE51bWJlcihmb3JtYXR0ZWRWYWx1ZSk7XFxyXFxuICAgIH0sXFxyXFxuICAgIHRvZ2dsZUZvcm0oKSB7XFxyXFxuICAgICAgdGhpcy5zaG93Rm9ybSA9ICF0aGlzLnNob3dGb3JtO1xcclxcbiAgICAgIHRoaXMuZWRpdEZvcm0gPSAhdGhpcy5lZGl0Rm9ybTtcXHJcXG4gICAgfSxcXHJcXG4gICAgdG9nZ2xlRWRpdCgpIHtcXHJcXG4gICAgICB0aGlzLnNob3dFZGl0ID0gIXRoaXMuc2hvd0VkaXQ7XFxyXFxuICAgIH0sXFxyXFxuICAgIGhpZGVFZGl0KCkge1xcclxcbiAgICAgIHRoaXMuc2hvd0VkaXQgPSBmYWxzZTtcXHJcXG4gICAgfSxcXHJcXG4gICAgZWRpdE1lcmNoYW5kaXNlIChpZCkge1xcclxcbiAgICAgIHRoaXMuc2hvd0Zvcm0gPSBmYWxzZTtcXHJcXG4gICAgICB0aGlzLnNob3dFZGl0ID0gdHJ1ZTtcXHJcXG4gICAgICB0aGlzLnBpY2tlZE1lcmNoYW5kaXNlID0gTWVyY2hhbmRpc2UuZmluZChpZCk7XFxyXFxuICAgIH0sXFxyXFxuICAgIHNodWZmbGUoKSB7XFxyXFxuICAgICAgdGhpcy5tZXJjaHMgPSBfLnNodWZmbGUodGhpcy5tZXJjaHMpO1xcclxcbiAgICB9LFxcclxcbiAgICByZW1vdmVNZXJjaGFuZGlzZSAoaWQpIHtcXHJcXG4gICAgICBNZXJjaGFuZGlzZS5yZW1vdmUoaWQpO1xcclxcbiAgICB9XFxyXFxuICB9XFxyXFxufTtcXHJcXG48L3NjcmlwdD5cXHJcXG5cXHJcXG48c3R5bGU+XFxyXFxuICAuZmxpcC1saXN0LW1vdmUge1xcclxcbiAgICB0cmFuc2l0aW9uOiB0cmFuc2Zvcm0gMXM7XFxyXFxuICB9XFxyXFxuPC9zdHlsZT5cXHJcXG5cIl0sXCJzb3VyY2VSb290XCI6XCJcIn1dKTtcblxuLy8gZXhwb3J0c1xuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlcj9zb3VyY2VNYXAhLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc3R5bGUtY29tcGlsZXI/e1widnVlXCI6dHJ1ZSxcImlkXCI6XCJkYXRhLXYtNDAxZWU5NGNcIixcInNjb3BlZFwiOmZhbHNlLFwiaGFzSW5saW5lQ29uZmlnXCI6dHJ1ZX0hLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT1zdHlsZXMmaW5kZXg9MCEuL3Jlc291cmNlcy9hc3NldHMvanMvY29tcG9uZW50cy9jbGllbnQvTWVyY2hhbmRpc2VQYWdlLnZ1ZVxuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz9zb3VyY2VNYXAhLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc3R5bGUtY29tcGlsZXIvaW5kZXguanM/e1widnVlXCI6dHJ1ZSxcImlkXCI6XCJkYXRhLXYtNDAxZWU5NGNcIixcInNjb3BlZFwiOmZhbHNlLFwiaGFzSW5saW5lQ29uZmlnXCI6dHJ1ZX0hLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT1zdHlsZXMmaW5kZXg9MCEuL3Jlc291cmNlcy9hc3NldHMvanMvY29tcG9uZW50cy9jbGllbnQvTWVyY2hhbmRpc2VQYWdlLnZ1ZVxuLy8gbW9kdWxlIGNodW5rcyA9IDEiLCIvKlxuXHRNSVQgTGljZW5zZSBodHRwOi8vd3d3Lm9wZW5zb3VyY2Uub3JnL2xpY2Vuc2VzL21pdC1saWNlbnNlLnBocFxuXHRBdXRob3IgVG9iaWFzIEtvcHBlcnMgQHNva3JhXG4qL1xuLy8gY3NzIGJhc2UgY29kZSwgaW5qZWN0ZWQgYnkgdGhlIGNzcy1sb2FkZXJcbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24odXNlU291cmNlTWFwKSB7XG5cdHZhciBsaXN0ID0gW107XG5cblx0Ly8gcmV0dXJuIHRoZSBsaXN0IG9mIG1vZHVsZXMgYXMgY3NzIHN0cmluZ1xuXHRsaXN0LnRvU3RyaW5nID0gZnVuY3Rpb24gdG9TdHJpbmcoKSB7XG5cdFx0cmV0dXJuIHRoaXMubWFwKGZ1bmN0aW9uIChpdGVtKSB7XG5cdFx0XHR2YXIgY29udGVudCA9IGNzc1dpdGhNYXBwaW5nVG9TdHJpbmcoaXRlbSwgdXNlU291cmNlTWFwKTtcblx0XHRcdGlmKGl0ZW1bMl0pIHtcblx0XHRcdFx0cmV0dXJuIFwiQG1lZGlhIFwiICsgaXRlbVsyXSArIFwie1wiICsgY29udGVudCArIFwifVwiO1xuXHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0cmV0dXJuIGNvbnRlbnQ7XG5cdFx0XHR9XG5cdFx0fSkuam9pbihcIlwiKTtcblx0fTtcblxuXHQvLyBpbXBvcnQgYSBsaXN0IG9mIG1vZHVsZXMgaW50byB0aGUgbGlzdFxuXHRsaXN0LmkgPSBmdW5jdGlvbihtb2R1bGVzLCBtZWRpYVF1ZXJ5KSB7XG5cdFx0aWYodHlwZW9mIG1vZHVsZXMgPT09IFwic3RyaW5nXCIpXG5cdFx0XHRtb2R1bGVzID0gW1tudWxsLCBtb2R1bGVzLCBcIlwiXV07XG5cdFx0dmFyIGFscmVhZHlJbXBvcnRlZE1vZHVsZXMgPSB7fTtcblx0XHRmb3IodmFyIGkgPSAwOyBpIDwgdGhpcy5sZW5ndGg7IGkrKykge1xuXHRcdFx0dmFyIGlkID0gdGhpc1tpXVswXTtcblx0XHRcdGlmKHR5cGVvZiBpZCA9PT0gXCJudW1iZXJcIilcblx0XHRcdFx0YWxyZWFkeUltcG9ydGVkTW9kdWxlc1tpZF0gPSB0cnVlO1xuXHRcdH1cblx0XHRmb3IoaSA9IDA7IGkgPCBtb2R1bGVzLmxlbmd0aDsgaSsrKSB7XG5cdFx0XHR2YXIgaXRlbSA9IG1vZHVsZXNbaV07XG5cdFx0XHQvLyBza2lwIGFscmVhZHkgaW1wb3J0ZWQgbW9kdWxlXG5cdFx0XHQvLyB0aGlzIGltcGxlbWVudGF0aW9uIGlzIG5vdCAxMDAlIHBlcmZlY3QgZm9yIHdlaXJkIG1lZGlhIHF1ZXJ5IGNvbWJpbmF0aW9uc1xuXHRcdFx0Ly8gIHdoZW4gYSBtb2R1bGUgaXMgaW1wb3J0ZWQgbXVsdGlwbGUgdGltZXMgd2l0aCBkaWZmZXJlbnQgbWVkaWEgcXVlcmllcy5cblx0XHRcdC8vICBJIGhvcGUgdGhpcyB3aWxsIG5ldmVyIG9jY3VyIChIZXkgdGhpcyB3YXkgd2UgaGF2ZSBzbWFsbGVyIGJ1bmRsZXMpXG5cdFx0XHRpZih0eXBlb2YgaXRlbVswXSAhPT0gXCJudW1iZXJcIiB8fCAhYWxyZWFkeUltcG9ydGVkTW9kdWxlc1tpdGVtWzBdXSkge1xuXHRcdFx0XHRpZihtZWRpYVF1ZXJ5ICYmICFpdGVtWzJdKSB7XG5cdFx0XHRcdFx0aXRlbVsyXSA9IG1lZGlhUXVlcnk7XG5cdFx0XHRcdH0gZWxzZSBpZihtZWRpYVF1ZXJ5KSB7XG5cdFx0XHRcdFx0aXRlbVsyXSA9IFwiKFwiICsgaXRlbVsyXSArIFwiKSBhbmQgKFwiICsgbWVkaWFRdWVyeSArIFwiKVwiO1xuXHRcdFx0XHR9XG5cdFx0XHRcdGxpc3QucHVzaChpdGVtKTtcblx0XHRcdH1cblx0XHR9XG5cdH07XG5cdHJldHVybiBsaXN0O1xufTtcblxuZnVuY3Rpb24gY3NzV2l0aE1hcHBpbmdUb1N0cmluZyhpdGVtLCB1c2VTb3VyY2VNYXApIHtcblx0dmFyIGNvbnRlbnQgPSBpdGVtWzFdIHx8ICcnO1xuXHR2YXIgY3NzTWFwcGluZyA9IGl0ZW1bM107XG5cdGlmICghY3NzTWFwcGluZykge1xuXHRcdHJldHVybiBjb250ZW50O1xuXHR9XG5cblx0aWYgKHVzZVNvdXJjZU1hcCAmJiB0eXBlb2YgYnRvYSA9PT0gJ2Z1bmN0aW9uJykge1xuXHRcdHZhciBzb3VyY2VNYXBwaW5nID0gdG9Db21tZW50KGNzc01hcHBpbmcpO1xuXHRcdHZhciBzb3VyY2VVUkxzID0gY3NzTWFwcGluZy5zb3VyY2VzLm1hcChmdW5jdGlvbiAoc291cmNlKSB7XG5cdFx0XHRyZXR1cm4gJy8qIyBzb3VyY2VVUkw9JyArIGNzc01hcHBpbmcuc291cmNlUm9vdCArIHNvdXJjZSArICcgKi8nXG5cdFx0fSk7XG5cblx0XHRyZXR1cm4gW2NvbnRlbnRdLmNvbmNhdChzb3VyY2VVUkxzKS5jb25jYXQoW3NvdXJjZU1hcHBpbmddKS5qb2luKCdcXG4nKTtcblx0fVxuXG5cdHJldHVybiBbY29udGVudF0uam9pbignXFxuJyk7XG59XG5cbi8vIEFkYXB0ZWQgZnJvbSBjb252ZXJ0LXNvdXJjZS1tYXAgKE1JVClcbmZ1bmN0aW9uIHRvQ29tbWVudChzb3VyY2VNYXApIHtcblx0Ly8gZXNsaW50LWRpc2FibGUtbmV4dC1saW5lIG5vLXVuZGVmXG5cdHZhciBiYXNlNjQgPSBidG9hKHVuZXNjYXBlKGVuY29kZVVSSUNvbXBvbmVudChKU09OLnN0cmluZ2lmeShzb3VyY2VNYXApKSkpO1xuXHR2YXIgZGF0YSA9ICdzb3VyY2VNYXBwaW5nVVJMPWRhdGE6YXBwbGljYXRpb24vanNvbjtjaGFyc2V0PXV0Zi04O2Jhc2U2NCwnICsgYmFzZTY0O1xuXG5cdHJldHVybiAnLyojICcgKyBkYXRhICsgJyAqLyc7XG59XG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2xpYi9jc3MtYmFzZS5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9saWIvY3NzLWJhc2UuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEiLCIvKiBnbG9iYWxzIF9fVlVFX1NTUl9DT05URVhUX18gKi9cblxuLy8gdGhpcyBtb2R1bGUgaXMgYSBydW50aW1lIHV0aWxpdHkgZm9yIGNsZWFuZXIgY29tcG9uZW50IG1vZHVsZSBvdXRwdXQgYW5kIHdpbGxcbi8vIGJlIGluY2x1ZGVkIGluIHRoZSBmaW5hbCB3ZWJwYWNrIHVzZXIgYnVuZGxlXG5cbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gbm9ybWFsaXplQ29tcG9uZW50IChcbiAgcmF3U2NyaXB0RXhwb3J0cyxcbiAgY29tcGlsZWRUZW1wbGF0ZSxcbiAgaW5qZWN0U3R5bGVzLFxuICBzY29wZUlkLFxuICBtb2R1bGVJZGVudGlmaWVyIC8qIHNlcnZlciBvbmx5ICovXG4pIHtcbiAgdmFyIGVzTW9kdWxlXG4gIHZhciBzY3JpcHRFeHBvcnRzID0gcmF3U2NyaXB0RXhwb3J0cyA9IHJhd1NjcmlwdEV4cG9ydHMgfHwge31cblxuICAvLyBFUzYgbW9kdWxlcyBpbnRlcm9wXG4gIHZhciB0eXBlID0gdHlwZW9mIHJhd1NjcmlwdEV4cG9ydHMuZGVmYXVsdFxuICBpZiAodHlwZSA9PT0gJ29iamVjdCcgfHwgdHlwZSA9PT0gJ2Z1bmN0aW9uJykge1xuICAgIGVzTW9kdWxlID0gcmF3U2NyaXB0RXhwb3J0c1xuICAgIHNjcmlwdEV4cG9ydHMgPSByYXdTY3JpcHRFeHBvcnRzLmRlZmF1bHRcbiAgfVxuXG4gIC8vIFZ1ZS5leHRlbmQgY29uc3RydWN0b3IgZXhwb3J0IGludGVyb3BcbiAgdmFyIG9wdGlvbnMgPSB0eXBlb2Ygc2NyaXB0RXhwb3J0cyA9PT0gJ2Z1bmN0aW9uJ1xuICAgID8gc2NyaXB0RXhwb3J0cy5vcHRpb25zXG4gICAgOiBzY3JpcHRFeHBvcnRzXG5cbiAgLy8gcmVuZGVyIGZ1bmN0aW9uc1xuICBpZiAoY29tcGlsZWRUZW1wbGF0ZSkge1xuICAgIG9wdGlvbnMucmVuZGVyID0gY29tcGlsZWRUZW1wbGF0ZS5yZW5kZXJcbiAgICBvcHRpb25zLnN0YXRpY1JlbmRlckZucyA9IGNvbXBpbGVkVGVtcGxhdGUuc3RhdGljUmVuZGVyRm5zXG4gIH1cblxuICAvLyBzY29wZWRJZFxuICBpZiAoc2NvcGVJZCkge1xuICAgIG9wdGlvbnMuX3Njb3BlSWQgPSBzY29wZUlkXG4gIH1cblxuICB2YXIgaG9va1xuICBpZiAobW9kdWxlSWRlbnRpZmllcikgeyAvLyBzZXJ2ZXIgYnVpbGRcbiAgICBob29rID0gZnVuY3Rpb24gKGNvbnRleHQpIHtcbiAgICAgIC8vIDIuMyBpbmplY3Rpb25cbiAgICAgIGNvbnRleHQgPVxuICAgICAgICBjb250ZXh0IHx8IC8vIGNhY2hlZCBjYWxsXG4gICAgICAgICh0aGlzLiR2bm9kZSAmJiB0aGlzLiR2bm9kZS5zc3JDb250ZXh0KSB8fCAvLyBzdGF0ZWZ1bFxuICAgICAgICAodGhpcy5wYXJlbnQgJiYgdGhpcy5wYXJlbnQuJHZub2RlICYmIHRoaXMucGFyZW50LiR2bm9kZS5zc3JDb250ZXh0KSAvLyBmdW5jdGlvbmFsXG4gICAgICAvLyAyLjIgd2l0aCBydW5Jbk5ld0NvbnRleHQ6IHRydWVcbiAgICAgIGlmICghY29udGV4dCAmJiB0eXBlb2YgX19WVUVfU1NSX0NPTlRFWFRfXyAhPT0gJ3VuZGVmaW5lZCcpIHtcbiAgICAgICAgY29udGV4dCA9IF9fVlVFX1NTUl9DT05URVhUX19cbiAgICAgIH1cbiAgICAgIC8vIGluamVjdCBjb21wb25lbnQgc3R5bGVzXG4gICAgICBpZiAoaW5qZWN0U3R5bGVzKSB7XG4gICAgICAgIGluamVjdFN0eWxlcy5jYWxsKHRoaXMsIGNvbnRleHQpXG4gICAgICB9XG4gICAgICAvLyByZWdpc3RlciBjb21wb25lbnQgbW9kdWxlIGlkZW50aWZpZXIgZm9yIGFzeW5jIGNodW5rIGluZmVycmVuY2VcbiAgICAgIGlmIChjb250ZXh0ICYmIGNvbnRleHQuX3JlZ2lzdGVyZWRDb21wb25lbnRzKSB7XG4gICAgICAgIGNvbnRleHQuX3JlZ2lzdGVyZWRDb21wb25lbnRzLmFkZChtb2R1bGVJZGVudGlmaWVyKVxuICAgICAgfVxuICAgIH1cbiAgICAvLyB1c2VkIGJ5IHNzciBpbiBjYXNlIGNvbXBvbmVudCBpcyBjYWNoZWQgYW5kIGJlZm9yZUNyZWF0ZVxuICAgIC8vIG5ldmVyIGdldHMgY2FsbGVkXG4gICAgb3B0aW9ucy5fc3NyUmVnaXN0ZXIgPSBob29rXG4gIH0gZWxzZSBpZiAoaW5qZWN0U3R5bGVzKSB7XG4gICAgaG9vayA9IGluamVjdFN0eWxlc1xuICB9XG5cbiAgaWYgKGhvb2spIHtcbiAgICB2YXIgZnVuY3Rpb25hbCA9IG9wdGlvbnMuZnVuY3Rpb25hbFxuICAgIHZhciBleGlzdGluZyA9IGZ1bmN0aW9uYWxcbiAgICAgID8gb3B0aW9ucy5yZW5kZXJcbiAgICAgIDogb3B0aW9ucy5iZWZvcmVDcmVhdGVcbiAgICBpZiAoIWZ1bmN0aW9uYWwpIHtcbiAgICAgIC8vIGluamVjdCBjb21wb25lbnQgcmVnaXN0cmF0aW9uIGFzIGJlZm9yZUNyZWF0ZSBob29rXG4gICAgICBvcHRpb25zLmJlZm9yZUNyZWF0ZSA9IGV4aXN0aW5nXG4gICAgICAgID8gW10uY29uY2F0KGV4aXN0aW5nLCBob29rKVxuICAgICAgICA6IFtob29rXVxuICAgIH0gZWxzZSB7XG4gICAgICAvLyByZWdpc3RlciBmb3IgZnVuY3Rpb2FsIGNvbXBvbmVudCBpbiB2dWUgZmlsZVxuICAgICAgb3B0aW9ucy5yZW5kZXIgPSBmdW5jdGlvbiByZW5kZXJXaXRoU3R5bGVJbmplY3Rpb24gKGgsIGNvbnRleHQpIHtcbiAgICAgICAgaG9vay5jYWxsKGNvbnRleHQpXG4gICAgICAgIHJldHVybiBleGlzdGluZyhoLCBjb250ZXh0KVxuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIHJldHVybiB7XG4gICAgZXNNb2R1bGU6IGVzTW9kdWxlLFxuICAgIGV4cG9ydHM6IHNjcmlwdEV4cG9ydHMsXG4gICAgb3B0aW9uczogb3B0aW9uc1xuICB9XG59XG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9jb21wb25lbnQtbm9ybWFsaXplci5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvY29tcG9uZW50LW5vcm1hbGl6ZXIuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEiLCJ2YXIgcmVuZGVyID0gZnVuY3Rpb24oKSB7XG4gIHZhciBfdm0gPSB0aGlzXG4gIHZhciBfaCA9IF92bS4kY3JlYXRlRWxlbWVudFxuICB2YXIgX2MgPSBfdm0uX3NlbGYuX2MgfHwgX2hcbiAgcmV0dXJuIF9jKFwiZGl2XCIsIHsgc3RhdGljQ2xhc3M6IFwibW9kYWwgZmFkZVwiLCBhdHRyczogeyByb2xlOiBcImRpYWxvZ1wiIH0gfSwgW1xuICAgIF9jKFwiZGl2XCIsIHsgc3RhdGljQ2xhc3M6IFwibW9kYWwtZGlhbG9nXCIgfSwgW1xuICAgICAgX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJtb2RhbC1jb250ZW50XCIgfSwgW1xuICAgICAgICBfYyhcImRpdlwiLCB7IHN0YXRpY0NsYXNzOiBcIm1vZGFsLWhlYWRlclwiIH0sIFtcbiAgICAgICAgICBfYyhcbiAgICAgICAgICAgIFwiYnV0dG9uXCIsXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIHN0YXRpY0NsYXNzOiBcImNsb3NlXCIsXG4gICAgICAgICAgICAgIGF0dHJzOiB7IHR5cGU6IFwiYnV0dG9uXCIgfSxcbiAgICAgICAgICAgICAgb246IHtcbiAgICAgICAgICAgICAgICBjbGljazogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgICAgICAgICAgICAkZXZlbnQucHJldmVudERlZmF1bHQoKVxuICAgICAgICAgICAgICAgICAgX3ZtLmhpZGVTZWxmKCRldmVudClcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBbX3ZtLl92KFwiw5dcIildXG4gICAgICAgICAgKSxcbiAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgIF9jKFwiaDRcIiwgeyBzdGF0aWNDbGFzczogXCJtb2RhbC10aXRsZVwiIH0sIFtfdm0uX3QoXCJtb2RhbC10aXRsZVwiKV0sIDIpXG4gICAgICAgIF0pLFxuICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICBfYyhcImRpdlwiLCB7IHN0YXRpY0NsYXNzOiBcIm1vZGFsLWJvZHlcIiB9LCBbX3ZtLl90KFwibW9kYWwtYm9keVwiKV0sIDIpLFxuICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICBfYyhcImRpdlwiLCB7IHN0YXRpY0NsYXNzOiBcIm1vZGVsLWJvZHlcIiB9LCBbX3ZtLl90KFwibW9kYWwtaW1hZ2VcIildLCAyKSxcbiAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJtb2RhbC1mb290ZXJcIiB9LCBbXG4gICAgICAgICAgX2MoXG4gICAgICAgICAgICBcImJ1dHRvblwiLFxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICBzdGF0aWNDbGFzczogXCJidG4gYnRuLWRlZmF1bHRcIixcbiAgICAgICAgICAgICAgYXR0cnM6IHsgdHlwZTogXCJidXR0b25cIiB9LFxuICAgICAgICAgICAgICBvbjoge1xuICAgICAgICAgICAgICAgIGNsaWNrOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgICAgICAgICAgICRldmVudC5wcmV2ZW50RGVmYXVsdCgpXG4gICAgICAgICAgICAgICAgICBfdm0uaGlkZVNlbGYoJGV2ZW50KVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIFtfdm0uX3YoXCJDbG9zZVwiKV1cbiAgICAgICAgICApXG4gICAgICAgIF0pXG4gICAgICBdKVxuICAgIF0pXG4gIF0pXG59XG52YXIgc3RhdGljUmVuZGVyRm5zID0gW11cbnJlbmRlci5fd2l0aFN0cmlwcGVkID0gdHJ1ZVxubW9kdWxlLmV4cG9ydHMgPSB7IHJlbmRlcjogcmVuZGVyLCBzdGF0aWNSZW5kZXJGbnM6IHN0YXRpY1JlbmRlckZucyB9XG5pZiAobW9kdWxlLmhvdCkge1xuICBtb2R1bGUuaG90LmFjY2VwdCgpXG4gIGlmIChtb2R1bGUuaG90LmRhdGEpIHtcbiAgICAgcmVxdWlyZShcInZ1ZS1ob3QtcmVsb2FkLWFwaVwiKS5yZXJlbmRlcihcImRhdGEtdi0wNzhiMDJmMVwiLCBtb2R1bGUuZXhwb3J0cylcbiAgfVxufVxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3RlbXBsYXRlLWNvbXBpbGVyP3tcImlkXCI6XCJkYXRhLXYtMDc4YjAyZjFcIixcImhhc1Njb3BlZFwiOmZhbHNlfSEuL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvci5qcz90eXBlPXRlbXBsYXRlJmluZGV4PTAhLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NvbXBvbmVudHMvTW9kYWwudnVlXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi90ZW1wbGF0ZS1jb21waWxlci9pbmRleC5qcz97XCJpZFwiOlwiZGF0YS12LTA3OGIwMmYxXCIsXCJoYXNTY29wZWRcIjpmYWxzZX0hLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT10ZW1wbGF0ZSZpbmRleD0wIS4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jb21wb25lbnRzL01vZGFsLnZ1ZVxuLy8gbW9kdWxlIGNodW5rcyA9IDEiLCIhZnVuY3Rpb24odCxlKXtcIm9iamVjdFwiPT10eXBlb2YgZXhwb3J0cyYmXCJvYmplY3RcIj09dHlwZW9mIG1vZHVsZT9tb2R1bGUuZXhwb3J0cz1lKCk6XCJmdW5jdGlvblwiPT10eXBlb2YgZGVmaW5lJiZkZWZpbmUuYW1kP2RlZmluZShbXSxlKTpcIm9iamVjdFwiPT10eXBlb2YgZXhwb3J0cz9leHBvcnRzLlZ1ZVNlbGVjdD1lKCk6dC5WdWVTZWxlY3Q9ZSgpfSh0aGlzLGZ1bmN0aW9uKCl7cmV0dXJuIGZ1bmN0aW9uKHQpe2Z1bmN0aW9uIGUocil7aWYobltyXSlyZXR1cm4gbltyXS5leHBvcnRzO3ZhciBvPW5bcl09e2V4cG9ydHM6e30saWQ6cixsb2FkZWQ6ITF9O3JldHVybiB0W3JdLmNhbGwoby5leHBvcnRzLG8sby5leHBvcnRzLGUpLG8ubG9hZGVkPSEwLG8uZXhwb3J0c312YXIgbj17fTtyZXR1cm4gZS5tPXQsZS5jPW4sZS5wPVwiL1wiLGUoMCl9KFtmdW5jdGlvbih0LGUsbil7XCJ1c2Ugc3RyaWN0XCI7ZnVuY3Rpb24gcih0KXtyZXR1cm4gdCYmdC5fX2VzTW9kdWxlP3Q6e2RlZmF1bHQ6dH19T2JqZWN0LmRlZmluZVByb3BlcnR5KGUsXCJfX2VzTW9kdWxlXCIse3ZhbHVlOiEwfSksZS5taXhpbnM9ZS5WdWVTZWxlY3Q9dm9pZCAwO3ZhciBvPW4oODMpLGk9cihvKSxhPW4oNDIpLHM9cihhKTtlLmRlZmF1bHQ9aS5kZWZhdWx0LGUuVnVlU2VsZWN0PWkuZGVmYXVsdCxlLm1peGlucz1zLmRlZmF1bHR9LGZ1bmN0aW9uKHQsZSl7dmFyIG49dC5leHBvcnRzPVwidW5kZWZpbmVkXCIhPXR5cGVvZiB3aW5kb3cmJndpbmRvdy5NYXRoPT1NYXRoP3dpbmRvdzpcInVuZGVmaW5lZFwiIT10eXBlb2Ygc2VsZiYmc2VsZi5NYXRoPT1NYXRoP3NlbGY6RnVuY3Rpb24oXCJyZXR1cm4gdGhpc1wiKSgpO1wibnVtYmVyXCI9PXR5cGVvZiBfX2cmJihfX2c9bil9LGZ1bmN0aW9uKHQsZSxuKXt0LmV4cG9ydHM9IW4oOSkoZnVuY3Rpb24oKXtyZXR1cm4gNyE9T2JqZWN0LmRlZmluZVByb3BlcnR5KHt9LFwiYVwiLHtnZXQ6ZnVuY3Rpb24oKXtyZXR1cm4gN319KS5hfSl9LGZ1bmN0aW9uKHQsZSl7dmFyIG49e30uaGFzT3duUHJvcGVydHk7dC5leHBvcnRzPWZ1bmN0aW9uKHQsZSl7cmV0dXJuIG4uY2FsbCh0LGUpfX0sZnVuY3Rpb24odCxlLG4pe3ZhciByPW4oMTApLG89bigzMyksaT1uKDI1KSxhPU9iamVjdC5kZWZpbmVQcm9wZXJ0eTtlLmY9bigyKT9PYmplY3QuZGVmaW5lUHJvcGVydHk6ZnVuY3Rpb24odCxlLG4pe2lmKHIodCksZT1pKGUsITApLHIobiksbyl0cnl7cmV0dXJuIGEodCxlLG4pfWNhdGNoKHQpe31pZihcImdldFwiaW4gbnx8XCJzZXRcImluIG4pdGhyb3cgVHlwZUVycm9yKFwiQWNjZXNzb3JzIG5vdCBzdXBwb3J0ZWQhXCIpO3JldHVyblwidmFsdWVcImluIG4mJih0W2VdPW4udmFsdWUpLHR9fSxmdW5jdGlvbih0LGUpe3ZhciBuPXQuZXhwb3J0cz17dmVyc2lvbjpcIjIuNS4xXCJ9O1wibnVtYmVyXCI9PXR5cGVvZiBfX2UmJihfX2U9bil9LGZ1bmN0aW9uKHQsZSxuKXt2YXIgcj1uKDQpLG89bigxNCk7dC5leHBvcnRzPW4oMik/ZnVuY3Rpb24odCxlLG4pe3JldHVybiByLmYodCxlLG8oMSxuKSl9OmZ1bmN0aW9uKHQsZSxuKXtyZXR1cm4gdFtlXT1uLHR9fSxmdW5jdGlvbih0LGUsbil7dmFyIHI9big1OSksbz1uKDE2KTt0LmV4cG9ydHM9ZnVuY3Rpb24odCl7cmV0dXJuIHIobyh0KSl9fSxmdW5jdGlvbih0LGUsbil7dmFyIHI9bigyMykoXCJ3a3NcIiksbz1uKDE1KSxpPW4oMSkuU3ltYm9sLGE9XCJmdW5jdGlvblwiPT10eXBlb2YgaSxzPXQuZXhwb3J0cz1mdW5jdGlvbih0KXtyZXR1cm4gclt0XXx8KHJbdF09YSYmaVt0XXx8KGE/aTpvKShcIlN5bWJvbC5cIit0KSl9O3Muc3RvcmU9cn0sZnVuY3Rpb24odCxlKXt0LmV4cG9ydHM9ZnVuY3Rpb24odCl7dHJ5e3JldHVybiEhdCgpfWNhdGNoKHQpe3JldHVybiEwfX19LGZ1bmN0aW9uKHQsZSxuKXt2YXIgcj1uKDEyKTt0LmV4cG9ydHM9ZnVuY3Rpb24odCl7aWYoIXIodCkpdGhyb3cgVHlwZUVycm9yKHQrXCIgaXMgbm90IGFuIG9iamVjdCFcIik7cmV0dXJuIHR9fSxmdW5jdGlvbih0LGUsbil7dmFyIHI9bigxKSxvPW4oNSksaT1uKDU2KSxhPW4oNikscz1cInByb3RvdHlwZVwiLHU9ZnVuY3Rpb24odCxlLG4pe3ZhciBsLGMsZixwPXQmdS5GLGQ9dCZ1LkcsaD10JnUuUyxiPXQmdS5QLHY9dCZ1LkIsZz10JnUuVyx5PWQ/bzpvW2VdfHwob1tlXT17fSksbT15W3NdLHg9ZD9yOmg/cltlXToocltlXXx8e30pW3NdO2QmJihuPWUpO2ZvcihsIGluIG4pYz0hcCYmeCYmdm9pZCAwIT09eFtsXSxjJiZsIGluIHl8fChmPWM/eFtsXTpuW2xdLHlbbF09ZCYmXCJmdW5jdGlvblwiIT10eXBlb2YgeFtsXT9uW2xdOnYmJmM/aShmLHIpOmcmJnhbbF09PWY/ZnVuY3Rpb24odCl7dmFyIGU9ZnVuY3Rpb24oZSxuLHIpe2lmKHRoaXMgaW5zdGFuY2VvZiB0KXtzd2l0Y2goYXJndW1lbnRzLmxlbmd0aCl7Y2FzZSAwOnJldHVybiBuZXcgdDtjYXNlIDE6cmV0dXJuIG5ldyB0KGUpO2Nhc2UgMjpyZXR1cm4gbmV3IHQoZSxuKX1yZXR1cm4gbmV3IHQoZSxuLHIpfXJldHVybiB0LmFwcGx5KHRoaXMsYXJndW1lbnRzKX07cmV0dXJuIGVbc109dFtzXSxlfShmKTpiJiZcImZ1bmN0aW9uXCI9PXR5cGVvZiBmP2koRnVuY3Rpb24uY2FsbCxmKTpmLGImJigoeS52aXJ0dWFsfHwoeS52aXJ0dWFsPXt9KSlbbF09Zix0JnUuUiYmbSYmIW1bbF0mJmEobSxsLGYpKSl9O3UuRj0xLHUuRz0yLHUuUz00LHUuUD04LHUuQj0xNix1Llc9MzIsdS5VPTY0LHUuUj0xMjgsdC5leHBvcnRzPXV9LGZ1bmN0aW9uKHQsZSl7dC5leHBvcnRzPWZ1bmN0aW9uKHQpe3JldHVyblwib2JqZWN0XCI9PXR5cGVvZiB0P251bGwhPT10OlwiZnVuY3Rpb25cIj09dHlwZW9mIHR9fSxmdW5jdGlvbih0LGUsbil7dmFyIHI9bigzOCksbz1uKDE3KTt0LmV4cG9ydHM9T2JqZWN0LmtleXN8fGZ1bmN0aW9uKHQpe3JldHVybiByKHQsbyl9fSxmdW5jdGlvbih0LGUpe3QuZXhwb3J0cz1mdW5jdGlvbih0LGUpe3JldHVybntlbnVtZXJhYmxlOiEoMSZ0KSxjb25maWd1cmFibGU6ISgyJnQpLHdyaXRhYmxlOiEoNCZ0KSx2YWx1ZTplfX19LGZ1bmN0aW9uKHQsZSl7dmFyIG49MCxyPU1hdGgucmFuZG9tKCk7dC5leHBvcnRzPWZ1bmN0aW9uKHQpe3JldHVyblwiU3ltYm9sKFwiLmNvbmNhdCh2b2lkIDA9PT10P1wiXCI6dCxcIilfXCIsKCsrbityKS50b1N0cmluZygzNikpfX0sZnVuY3Rpb24odCxlKXt0LmV4cG9ydHM9ZnVuY3Rpb24odCl7aWYodm9pZCAwPT10KXRocm93IFR5cGVFcnJvcihcIkNhbid0IGNhbGwgbWV0aG9kIG9uICBcIit0KTtyZXR1cm4gdH19LGZ1bmN0aW9uKHQsZSl7dC5leHBvcnRzPVwiY29uc3RydWN0b3IsaGFzT3duUHJvcGVydHksaXNQcm90b3R5cGVPZixwcm9wZXJ0eUlzRW51bWVyYWJsZSx0b0xvY2FsZVN0cmluZyx0b1N0cmluZyx2YWx1ZU9mXCIuc3BsaXQoXCIsXCIpfSxmdW5jdGlvbih0LGUpe3QuZXhwb3J0cz17fX0sZnVuY3Rpb24odCxlKXt0LmV4cG9ydHM9ITB9LGZ1bmN0aW9uKHQsZSl7ZS5mPXt9LnByb3BlcnR5SXNFbnVtZXJhYmxlfSxmdW5jdGlvbih0LGUsbil7dmFyIHI9big0KS5mLG89bigzKSxpPW4oOCkoXCJ0b1N0cmluZ1RhZ1wiKTt0LmV4cG9ydHM9ZnVuY3Rpb24odCxlLG4pe3QmJiFvKHQ9bj90OnQucHJvdG90eXBlLGkpJiZyKHQsaSx7Y29uZmlndXJhYmxlOiEwLHZhbHVlOmV9KX19LGZ1bmN0aW9uKHQsZSxuKXt2YXIgcj1uKDIzKShcImtleXNcIiksbz1uKDE1KTt0LmV4cG9ydHM9ZnVuY3Rpb24odCl7cmV0dXJuIHJbdF18fChyW3RdPW8odCkpfX0sZnVuY3Rpb24odCxlLG4pe3ZhciByPW4oMSksbz1cIl9fY29yZS1qc19zaGFyZWRfX1wiLGk9cltvXXx8KHJbb109e30pO3QuZXhwb3J0cz1mdW5jdGlvbih0KXtyZXR1cm4gaVt0XXx8KGlbdF09e30pfX0sZnVuY3Rpb24odCxlKXt2YXIgbj1NYXRoLmNlaWwscj1NYXRoLmZsb29yO3QuZXhwb3J0cz1mdW5jdGlvbih0KXtyZXR1cm4gaXNOYU4odD0rdCk/MDoodD4wP3I6bikodCl9fSxmdW5jdGlvbih0LGUsbil7dmFyIHI9bigxMik7dC5leHBvcnRzPWZ1bmN0aW9uKHQsZSl7aWYoIXIodCkpcmV0dXJuIHQ7dmFyIG4sbztpZihlJiZcImZ1bmN0aW9uXCI9PXR5cGVvZihuPXQudG9TdHJpbmcpJiYhcihvPW4uY2FsbCh0KSkpcmV0dXJuIG87aWYoXCJmdW5jdGlvblwiPT10eXBlb2Yobj10LnZhbHVlT2YpJiYhcihvPW4uY2FsbCh0KSkpcmV0dXJuIG87aWYoIWUmJlwiZnVuY3Rpb25cIj09dHlwZW9mKG49dC50b1N0cmluZykmJiFyKG89bi5jYWxsKHQpKSlyZXR1cm4gbzt0aHJvdyBUeXBlRXJyb3IoXCJDYW4ndCBjb252ZXJ0IG9iamVjdCB0byBwcmltaXRpdmUgdmFsdWVcIil9fSxmdW5jdGlvbih0LGUsbil7dmFyIHI9bigxKSxvPW4oNSksaT1uKDE5KSxhPW4oMjcpLHM9big0KS5mO3QuZXhwb3J0cz1mdW5jdGlvbih0KXt2YXIgZT1vLlN5bWJvbHx8KG8uU3ltYm9sPWk/e306ci5TeW1ib2x8fHt9KTtcIl9cIj09dC5jaGFyQXQoMCl8fHQgaW4gZXx8cyhlLHQse3ZhbHVlOmEuZih0KX0pfX0sZnVuY3Rpb24odCxlLG4pe2UuZj1uKDgpfSxmdW5jdGlvbih0LGUpe1widXNlIHN0cmljdFwiO3QuZXhwb3J0cz17cHJvcHM6e2xvYWRpbmc6e3R5cGU6Qm9vbGVhbixkZWZhdWx0OiExfSxvblNlYXJjaDp7dHlwZTpGdW5jdGlvbixkZWZhdWx0OmZ1bmN0aW9uKHQsZSl7fX19LGRhdGE6ZnVuY3Rpb24oKXtyZXR1cm57bXV0YWJsZUxvYWRpbmc6ITF9fSx3YXRjaDp7c2VhcmNoOmZ1bmN0aW9uKCl7dGhpcy5zZWFyY2gubGVuZ3RoPjAmJih0aGlzLm9uU2VhcmNoKHRoaXMuc2VhcmNoLHRoaXMudG9nZ2xlTG9hZGluZyksdGhpcy4kZW1pdChcInNlYXJjaFwiLHRoaXMuc2VhcmNoLHRoaXMudG9nZ2xlTG9hZGluZykpfSxsb2FkaW5nOmZ1bmN0aW9uKHQpe3RoaXMubXV0YWJsZUxvYWRpbmc9dH19LG1ldGhvZHM6e3RvZ2dsZUxvYWRpbmc6ZnVuY3Rpb24oKXt2YXIgdD1hcmd1bWVudHMubGVuZ3RoPjAmJnZvaWQgMCE9PWFyZ3VtZW50c1swXT9hcmd1bWVudHNbMF06bnVsbDtyZXR1cm4gbnVsbD09dD90aGlzLm11dGFibGVMb2FkaW5nPSF0aGlzLm11dGFibGVMb2FkaW5nOnRoaXMubXV0YWJsZUxvYWRpbmc9dH19fX0sZnVuY3Rpb24odCxlKXtcInVzZSBzdHJpY3RcIjt0LmV4cG9ydHM9e3dhdGNoOnt0eXBlQWhlYWRQb2ludGVyOmZ1bmN0aW9uKCl7dGhpcy5tYXliZUFkanVzdFNjcm9sbCgpfX0sbWV0aG9kczp7bWF5YmVBZGp1c3RTY3JvbGw6ZnVuY3Rpb24oKXt2YXIgdD10aGlzLnBpeGVsc1RvUG9pbnRlclRvcCgpLGU9dGhpcy5waXhlbHNUb1BvaW50ZXJCb3R0b20oKTtyZXR1cm4gdDw9dGhpcy52aWV3cG9ydCgpLnRvcD90aGlzLnNjcm9sbFRvKHQpOmU+PXRoaXMudmlld3BvcnQoKS5ib3R0b20/dGhpcy5zY3JvbGxUbyh0aGlzLnZpZXdwb3J0KCkudG9wK3RoaXMucG9pbnRlckhlaWdodCgpKTp2b2lkIDB9LHBpeGVsc1RvUG9pbnRlclRvcDpmdW5jdGlvbiB0KCl7dmFyIHQ9MDtpZih0aGlzLiRyZWZzLmRyb3Bkb3duTWVudSlmb3IodmFyIGU9MDtlPHRoaXMudHlwZUFoZWFkUG9pbnRlcjtlKyspdCs9dGhpcy4kcmVmcy5kcm9wZG93bk1lbnUuY2hpbGRyZW5bZV0ub2Zmc2V0SGVpZ2h0O3JldHVybiB0fSxwaXhlbHNUb1BvaW50ZXJCb3R0b206ZnVuY3Rpb24oKXtyZXR1cm4gdGhpcy5waXhlbHNUb1BvaW50ZXJUb3AoKSt0aGlzLnBvaW50ZXJIZWlnaHQoKX0scG9pbnRlckhlaWdodDpmdW5jdGlvbigpe3ZhciB0PSEhdGhpcy4kcmVmcy5kcm9wZG93bk1lbnUmJnRoaXMuJHJlZnMuZHJvcGRvd25NZW51LmNoaWxkcmVuW3RoaXMudHlwZUFoZWFkUG9pbnRlcl07cmV0dXJuIHQ/dC5vZmZzZXRIZWlnaHQ6MH0sdmlld3BvcnQ6ZnVuY3Rpb24oKXtyZXR1cm57dG9wOnRoaXMuJHJlZnMuZHJvcGRvd25NZW51P3RoaXMuJHJlZnMuZHJvcGRvd25NZW51LnNjcm9sbFRvcDowLGJvdHRvbTp0aGlzLiRyZWZzLmRyb3Bkb3duTWVudT90aGlzLiRyZWZzLmRyb3Bkb3duTWVudS5vZmZzZXRIZWlnaHQrdGhpcy4kcmVmcy5kcm9wZG93bk1lbnUuc2Nyb2xsVG9wOjB9fSxzY3JvbGxUbzpmdW5jdGlvbih0KXtyZXR1cm4gdGhpcy4kcmVmcy5kcm9wZG93bk1lbnU/dGhpcy4kcmVmcy5kcm9wZG93bk1lbnUuc2Nyb2xsVG9wPXQ6bnVsbH19fX0sZnVuY3Rpb24odCxlKXtcInVzZSBzdHJpY3RcIjt0LmV4cG9ydHM9e2RhdGE6ZnVuY3Rpb24oKXtyZXR1cm57dHlwZUFoZWFkUG9pbnRlcjotMX19LHdhdGNoOntmaWx0ZXJlZE9wdGlvbnM6ZnVuY3Rpb24oKXt0aGlzLnR5cGVBaGVhZFBvaW50ZXI9MH19LG1ldGhvZHM6e3R5cGVBaGVhZFVwOmZ1bmN0aW9uKCl7dGhpcy50eXBlQWhlYWRQb2ludGVyPjAmJih0aGlzLnR5cGVBaGVhZFBvaW50ZXItLSx0aGlzLm1heWJlQWRqdXN0U2Nyb2xsJiZ0aGlzLm1heWJlQWRqdXN0U2Nyb2xsKCkpfSx0eXBlQWhlYWREb3duOmZ1bmN0aW9uKCl7dGhpcy50eXBlQWhlYWRQb2ludGVyPHRoaXMuZmlsdGVyZWRPcHRpb25zLmxlbmd0aC0xJiYodGhpcy50eXBlQWhlYWRQb2ludGVyKyssdGhpcy5tYXliZUFkanVzdFNjcm9sbCYmdGhpcy5tYXliZUFkanVzdFNjcm9sbCgpKX0sdHlwZUFoZWFkU2VsZWN0OmZ1bmN0aW9uKCl7dGhpcy5maWx0ZXJlZE9wdGlvbnNbdGhpcy50eXBlQWhlYWRQb2ludGVyXT90aGlzLnNlbGVjdCh0aGlzLmZpbHRlcmVkT3B0aW9uc1t0aGlzLnR5cGVBaGVhZFBvaW50ZXJdKTp0aGlzLnRhZ2dhYmxlJiZ0aGlzLnNlYXJjaC5sZW5ndGgmJnRoaXMuc2VsZWN0KHRoaXMuc2VhcmNoKSx0aGlzLmNsZWFyU2VhcmNoT25TZWxlY3QmJih0aGlzLnNlYXJjaD1cIlwiKX19fX0sZnVuY3Rpb24odCxlKXt2YXIgbj17fS50b1N0cmluZzt0LmV4cG9ydHM9ZnVuY3Rpb24odCl7cmV0dXJuIG4uY2FsbCh0KS5zbGljZSg4LC0xKX19LGZ1bmN0aW9uKHQsZSxuKXt2YXIgcj1uKDEyKSxvPW4oMSkuZG9jdW1lbnQsaT1yKG8pJiZyKG8uY3JlYXRlRWxlbWVudCk7dC5leHBvcnRzPWZ1bmN0aW9uKHQpe3JldHVybiBpP28uY3JlYXRlRWxlbWVudCh0KTp7fX19LGZ1bmN0aW9uKHQsZSxuKXt0LmV4cG9ydHM9IW4oMikmJiFuKDkpKGZ1bmN0aW9uKCl7cmV0dXJuIDchPU9iamVjdC5kZWZpbmVQcm9wZXJ0eShuKDMyKShcImRpdlwiKSxcImFcIix7Z2V0OmZ1bmN0aW9uKCl7cmV0dXJuIDd9fSkuYX0pfSxmdW5jdGlvbih0LGUsbil7XCJ1c2Ugc3RyaWN0XCI7dmFyIHI9bigxOSksbz1uKDExKSxpPW4oMzkpLGE9big2KSxzPW4oMyksdT1uKDE4KSxsPW4oNjEpLGM9bigyMSksZj1uKDY3KSxwPW4oOCkoXCJpdGVyYXRvclwiKSxkPSEoW10ua2V5cyYmXCJuZXh0XCJpbltdLmtleXMoKSksaD1cIkBAaXRlcmF0b3JcIixiPVwia2V5c1wiLHY9XCJ2YWx1ZXNcIixnPWZ1bmN0aW9uKCl7cmV0dXJuIHRoaXN9O3QuZXhwb3J0cz1mdW5jdGlvbih0LGUsbix5LG0seCx3KXtsKG4sZSx5KTt2YXIgUyxPLF8saj1mdW5jdGlvbih0KXtpZighZCYmdCBpbiBNKXJldHVybiBNW3RdO3N3aXRjaCh0KXtjYXNlIGI6cmV0dXJuIGZ1bmN0aW9uKCl7cmV0dXJuIG5ldyBuKHRoaXMsdCl9O2Nhc2UgdjpyZXR1cm4gZnVuY3Rpb24oKXtyZXR1cm4gbmV3IG4odGhpcyx0KX19cmV0dXJuIGZ1bmN0aW9uKCl7cmV0dXJuIG5ldyBuKHRoaXMsdCl9fSxQPWUrXCIgSXRlcmF0b3JcIixrPW09PXYsQT0hMSxNPXQucHJvdG90eXBlLEw9TVtwXXx8TVtoXXx8bSYmTVttXSxDPUx8fGoobSksVD1tP2s/aihcImVudHJpZXNcIik6Qzp2b2lkIDAsRT1cIkFycmF5XCI9PWU/TS5lbnRyaWVzfHxMOkw7aWYoRSYmKF89ZihFLmNhbGwobmV3IHQpKSxfIT09T2JqZWN0LnByb3RvdHlwZSYmXy5uZXh0JiYoYyhfLFAsITApLHJ8fHMoXyxwKXx8YShfLHAsZykpKSxrJiZMJiZMLm5hbWUhPT12JiYoQT0hMCxDPWZ1bmN0aW9uKCl7cmV0dXJuIEwuY2FsbCh0aGlzKX0pLHImJiF3fHwhZCYmIUEmJk1bcF18fGEoTSxwLEMpLHVbZV09Qyx1W1BdPWcsbSlpZihTPXt2YWx1ZXM6az9DOmoodiksa2V5czp4P0M6aihiKSxlbnRyaWVzOlR9LHcpZm9yKE8gaW4gUylPIGluIE18fGkoTSxPLFNbT10pO2Vsc2UgbyhvLlArby5GKihkfHxBKSxlLFMpO3JldHVybiBTfX0sZnVuY3Rpb24odCxlLG4pe3ZhciByPW4oMTApLG89big2NCksaT1uKDE3KSxhPW4oMjIpKFwiSUVfUFJPVE9cIikscz1mdW5jdGlvbigpe30sdT1cInByb3RvdHlwZVwiLGw9ZnVuY3Rpb24oKXt2YXIgdCxlPW4oMzIpKFwiaWZyYW1lXCIpLHI9aS5sZW5ndGgsbz1cIjxcIixhPVwiPlwiO2ZvcihlLnN0eWxlLmRpc3BsYXk9XCJub25lXCIsbig1OCkuYXBwZW5kQ2hpbGQoZSksZS5zcmM9XCJqYXZhc2NyaXB0OlwiLHQ9ZS5jb250ZW50V2luZG93LmRvY3VtZW50LHQub3BlbigpLHQud3JpdGUobytcInNjcmlwdFwiK2ErXCJkb2N1bWVudC5GPU9iamVjdFwiK28rXCIvc2NyaXB0XCIrYSksdC5jbG9zZSgpLGw9dC5GO3ItLTspZGVsZXRlIGxbdV1baVtyXV07cmV0dXJuIGwoKX07dC5leHBvcnRzPU9iamVjdC5jcmVhdGV8fGZ1bmN0aW9uKHQsZSl7dmFyIG47cmV0dXJuIG51bGwhPT10PyhzW3VdPXIodCksbj1uZXcgcyxzW3VdPW51bGwsblthXT10KTpuPWwoKSx2b2lkIDA9PT1lP246byhuLGUpfX0sZnVuY3Rpb24odCxlLG4pe3ZhciByPW4oMzgpLG89bigxNykuY29uY2F0KFwibGVuZ3RoXCIsXCJwcm90b3R5cGVcIik7ZS5mPU9iamVjdC5nZXRPd25Qcm9wZXJ0eU5hbWVzfHxmdW5jdGlvbih0KXtyZXR1cm4gcih0LG8pfX0sZnVuY3Rpb24odCxlKXtlLmY9T2JqZWN0LmdldE93blByb3BlcnR5U3ltYm9sc30sZnVuY3Rpb24odCxlLG4pe3ZhciByPW4oMyksbz1uKDcpLGk9big1NSkoITEpLGE9bigyMikoXCJJRV9QUk9UT1wiKTt0LmV4cG9ydHM9ZnVuY3Rpb24odCxlKXt2YXIgbixzPW8odCksdT0wLGw9W107Zm9yKG4gaW4gcyluIT1hJiZyKHMsbikmJmwucHVzaChuKTtmb3IoO2UubGVuZ3RoPnU7KXIocyxuPWVbdSsrXSkmJih+aShsLG4pfHxsLnB1c2gobikpO3JldHVybiBsfX0sZnVuY3Rpb24odCxlLG4pe3QuZXhwb3J0cz1uKDYpfSxmdW5jdGlvbih0LGUsbil7dmFyIHI9bigxNik7dC5leHBvcnRzPWZ1bmN0aW9uKHQpe3JldHVybiBPYmplY3Qocih0KSl9fSxmdW5jdGlvbih0LGUsbil7XCJ1c2Ugc3RyaWN0XCI7ZnVuY3Rpb24gcih0KXtyZXR1cm4gdCYmdC5fX2VzTW9kdWxlP3Q6e2RlZmF1bHQ6dH19T2JqZWN0LmRlZmluZVByb3BlcnR5KGUsXCJfX2VzTW9kdWxlXCIse3ZhbHVlOiEwfSk7dmFyIG89big0NCksaT1yKG8pLGE9big0Nykscz1yKGEpLHU9big0OCksbD1yKHUpLGM9bigyOSksZj1yKGMpLHA9bigzMCksZD1yKHApLGg9bigyOCksYj1yKGgpO2UuZGVmYXVsdD17bWl4aW5zOltmLmRlZmF1bHQsZC5kZWZhdWx0LGIuZGVmYXVsdF0scHJvcHM6e3ZhbHVlOntkZWZhdWx0Om51bGx9LG9wdGlvbnM6e3R5cGU6QXJyYXksZGVmYXVsdDpmdW5jdGlvbigpe3JldHVybltdfX0sZGlzYWJsZWQ6e3R5cGU6Qm9vbGVhbixkZWZhdWx0OiExfSxtYXhIZWlnaHQ6e3R5cGU6U3RyaW5nLGRlZmF1bHQ6XCI0MDBweFwifSxzZWFyY2hhYmxlOnt0eXBlOkJvb2xlYW4sZGVmYXVsdDohMH0sbXVsdGlwbGU6e3R5cGU6Qm9vbGVhbixkZWZhdWx0OiExfSxwbGFjZWhvbGRlcjp7dHlwZTpTdHJpbmcsZGVmYXVsdDpcIlwifSx0cmFuc2l0aW9uOnt0eXBlOlN0cmluZyxkZWZhdWx0OlwiZmFkZVwifSxjbGVhclNlYXJjaE9uU2VsZWN0Ont0eXBlOkJvb2xlYW4sZGVmYXVsdDohMH0sY2xvc2VPblNlbGVjdDp7dHlwZTpCb29sZWFuLGRlZmF1bHQ6ITB9LGxhYmVsOnt0eXBlOlN0cmluZyxkZWZhdWx0OlwibGFiZWxcIn0sZ2V0T3B0aW9uTGFiZWw6e3R5cGU6RnVuY3Rpb24sZGVmYXVsdDpmdW5jdGlvbih0KXtyZXR1cm5cIm9iamVjdFwiPT09KFwidW5kZWZpbmVkXCI9PXR5cGVvZiB0P1widW5kZWZpbmVkXCI6KDAsbC5kZWZhdWx0KSh0KSkmJnRoaXMubGFiZWwmJnRbdGhpcy5sYWJlbF0/dFt0aGlzLmxhYmVsXTp0fX0sb25DaGFuZ2U6e3R5cGU6RnVuY3Rpb24sZGVmYXVsdDpmdW5jdGlvbih0KXt0aGlzLiRlbWl0KFwiaW5wdXRcIix0KX19LHRhZ2dhYmxlOnt0eXBlOkJvb2xlYW4sZGVmYXVsdDohMX0scHVzaFRhZ3M6e3R5cGU6Qm9vbGVhbixkZWZhdWx0OiExfSxjcmVhdGVPcHRpb246e3R5cGU6RnVuY3Rpb24sZGVmYXVsdDpmdW5jdGlvbih0KXtyZXR1cm5cIm9iamVjdFwiPT09KDAsbC5kZWZhdWx0KSh0aGlzLm11dGFibGVPcHRpb25zWzBdKSYmKHQ9KDAscy5kZWZhdWx0KSh7fSx0aGlzLmxhYmVsLHQpKSx0aGlzLiRlbWl0KFwib3B0aW9uOmNyZWF0ZWRcIix0KSx0fX0scmVzZXRPbk9wdGlvbnNDaGFuZ2U6e3R5cGU6Qm9vbGVhbixkZWZhdWx0OiExfSxub0Ryb3A6e3R5cGU6Qm9vbGVhbixkZWZhdWx0OiExfSxpbnB1dElkOnt0eXBlOlN0cmluZ30sZGlyOnt0eXBlOlN0cmluZyxkZWZhdWx0OlwiYXV0b1wifX0sZGF0YTpmdW5jdGlvbigpe3JldHVybntzZWFyY2g6XCJcIixvcGVuOiExLG11dGFibGVWYWx1ZTpudWxsLG11dGFibGVPcHRpb25zOltdfX0sd2F0Y2g6e3ZhbHVlOmZ1bmN0aW9uKHQpe3RoaXMubXV0YWJsZVZhbHVlPXR9LG11dGFibGVWYWx1ZTpmdW5jdGlvbih0LGUpe3RoaXMubXVsdGlwbGU/dGhpcy5vbkNoYW5nZT90aGlzLm9uQ2hhbmdlKHQpOm51bGw6dGhpcy5vbkNoYW5nZSYmdCE9PWU/dGhpcy5vbkNoYW5nZSh0KTpudWxsfSxvcHRpb25zOmZ1bmN0aW9uKHQpe3RoaXMubXV0YWJsZU9wdGlvbnM9dH0sbXV0YWJsZU9wdGlvbnM6ZnVuY3Rpb24oKXshdGhpcy50YWdnYWJsZSYmdGhpcy5yZXNldE9uT3B0aW9uc0NoYW5nZSYmKHRoaXMubXV0YWJsZVZhbHVlPXRoaXMubXVsdGlwbGU/W106bnVsbCl9LG11bHRpcGxlOmZ1bmN0aW9uKHQpe3RoaXMubXV0YWJsZVZhbHVlPXQ/W106bnVsbH19LGNyZWF0ZWQ6ZnVuY3Rpb24oKXt0aGlzLm11dGFibGVWYWx1ZT10aGlzLnZhbHVlLHRoaXMubXV0YWJsZU9wdGlvbnM9dGhpcy5vcHRpb25zLnNsaWNlKDApLHRoaXMubXV0YWJsZUxvYWRpbmc9dGhpcy5sb2FkaW5nLHRoaXMuJG9uKFwib3B0aW9uOmNyZWF0ZWRcIix0aGlzLm1heWJlUHVzaFRhZyl9LG1ldGhvZHM6e3NlbGVjdDpmdW5jdGlvbih0KXt0aGlzLmlzT3B0aW9uU2VsZWN0ZWQodCk/dGhpcy5kZXNlbGVjdCh0KToodGhpcy50YWdnYWJsZSYmIXRoaXMub3B0aW9uRXhpc3RzKHQpJiYodD10aGlzLmNyZWF0ZU9wdGlvbih0KSksdGhpcy5tdWx0aXBsZSYmIXRoaXMubXV0YWJsZVZhbHVlP3RoaXMubXV0YWJsZVZhbHVlPVt0XTp0aGlzLm11bHRpcGxlP3RoaXMubXV0YWJsZVZhbHVlLnB1c2godCk6dGhpcy5tdXRhYmxlVmFsdWU9dCksdGhpcy5vbkFmdGVyU2VsZWN0KHQpfSxkZXNlbGVjdDpmdW5jdGlvbih0KXt2YXIgZT10aGlzO2lmKHRoaXMubXVsdGlwbGUpe3ZhciBuPS0xO3RoaXMubXV0YWJsZVZhbHVlLmZvckVhY2goZnVuY3Rpb24ocil7KHI9PT10fHxcIm9iamVjdFwiPT09KFwidW5kZWZpbmVkXCI9PXR5cGVvZiByP1widW5kZWZpbmVkXCI6KDAsbC5kZWZhdWx0KShyKSkmJnJbZS5sYWJlbF09PT10W2UubGFiZWxdKSYmKG49cil9KTt2YXIgcj10aGlzLm11dGFibGVWYWx1ZS5pbmRleE9mKG4pO3RoaXMubXV0YWJsZVZhbHVlLnNwbGljZShyLDEpfWVsc2UgdGhpcy5tdXRhYmxlVmFsdWU9bnVsbH0sb25BZnRlclNlbGVjdDpmdW5jdGlvbih0KXt0aGlzLmNsb3NlT25TZWxlY3QmJih0aGlzLm9wZW49IXRoaXMub3Blbix0aGlzLiRyZWZzLnNlYXJjaC5ibHVyKCkpLHRoaXMuY2xlYXJTZWFyY2hPblNlbGVjdCYmKHRoaXMuc2VhcmNoPVwiXCIpfSx0b2dnbGVEcm9wZG93bjpmdW5jdGlvbih0KXt0LnRhcmdldCE9PXRoaXMuJHJlZnMub3BlbkluZGljYXRvciYmdC50YXJnZXQhPT10aGlzLiRyZWZzLnNlYXJjaCYmdC50YXJnZXQhPT10aGlzLiRyZWZzLnRvZ2dsZSYmdC50YXJnZXQhPT10aGlzLiRlbHx8KHRoaXMub3Blbj90aGlzLiRyZWZzLnNlYXJjaC5ibHVyKCk6dGhpcy5kaXNhYmxlZHx8KHRoaXMub3Blbj0hMCx0aGlzLiRyZWZzLnNlYXJjaC5mb2N1cygpKSl9LGlzT3B0aW9uU2VsZWN0ZWQ6ZnVuY3Rpb24odCl7dmFyIGU9dGhpcztpZih0aGlzLm11bHRpcGxlJiZ0aGlzLm11dGFibGVWYWx1ZSl7dmFyIG49ITE7cmV0dXJuIHRoaXMubXV0YWJsZVZhbHVlLmZvckVhY2goZnVuY3Rpb24ocil7XCJvYmplY3RcIj09PShcInVuZGVmaW5lZFwiPT10eXBlb2Ygcj9cInVuZGVmaW5lZFwiOigwLGwuZGVmYXVsdCkocikpJiZyW2UubGFiZWxdPT09dFtlLmxhYmVsXT9uPSEwOlwib2JqZWN0XCI9PT0oXCJ1bmRlZmluZWRcIj09dHlwZW9mIHI/XCJ1bmRlZmluZWRcIjooMCxsLmRlZmF1bHQpKHIpKSYmcltlLmxhYmVsXT09PXQ/bj0hMDpyPT09dCYmKG49ITApfSksbn1yZXR1cm4gdGhpcy5tdXRhYmxlVmFsdWU9PT10fSxvbkVzY2FwZTpmdW5jdGlvbigpe3RoaXMuc2VhcmNoLmxlbmd0aD90aGlzLnNlYXJjaD1cIlwiOnRoaXMuJHJlZnMuc2VhcmNoLmJsdXIoKX0sb25TZWFyY2hCbHVyOmZ1bmN0aW9uKCl7dGhpcy5jbGVhclNlYXJjaE9uQmx1ciYmKHRoaXMuc2VhcmNoPVwiXCIpLHRoaXMub3Blbj0hMSx0aGlzLiRlbWl0KFwic2VhcmNoOmJsdXJcIil9LG9uU2VhcmNoRm9jdXM6ZnVuY3Rpb24oKXt0aGlzLm9wZW49ITAsdGhpcy4kZW1pdChcInNlYXJjaDpmb2N1c1wiKX0sbWF5YmVEZWxldGVWYWx1ZTpmdW5jdGlvbigpe2lmKCF0aGlzLiRyZWZzLnNlYXJjaC52YWx1ZS5sZW5ndGgmJnRoaXMubXV0YWJsZVZhbHVlKXJldHVybiB0aGlzLm11bHRpcGxlP3RoaXMubXV0YWJsZVZhbHVlLnBvcCgpOnRoaXMubXV0YWJsZVZhbHVlPW51bGx9LG9wdGlvbkV4aXN0czpmdW5jdGlvbih0KXt2YXIgZT10aGlzLG49ITE7cmV0dXJuIHRoaXMubXV0YWJsZU9wdGlvbnMuZm9yRWFjaChmdW5jdGlvbihyKXtcIm9iamVjdFwiPT09KFwidW5kZWZpbmVkXCI9PXR5cGVvZiByP1widW5kZWZpbmVkXCI6KDAsbC5kZWZhdWx0KShyKSkmJnJbZS5sYWJlbF09PT10P249ITA6cj09PXQmJihuPSEwKX0pLG59LG1heWJlUHVzaFRhZzpmdW5jdGlvbih0KXt0aGlzLnB1c2hUYWdzJiZ0aGlzLm11dGFibGVPcHRpb25zLnB1c2godCl9fSxjb21wdXRlZDp7ZHJvcGRvd25DbGFzc2VzOmZ1bmN0aW9uKCl7cmV0dXJue29wZW46dGhpcy5kcm9wZG93bk9wZW4sc2luZ2xlOiF0aGlzLm11bHRpcGxlLHNlYXJjaGluZzp0aGlzLnNlYXJjaGluZyxzZWFyY2hhYmxlOnRoaXMuc2VhcmNoYWJsZSx1bnNlYXJjaGFibGU6IXRoaXMuc2VhcmNoYWJsZSxsb2FkaW5nOnRoaXMubXV0YWJsZUxvYWRpbmcscnRsOlwicnRsXCI9PT10aGlzLmRpcn19LGNsZWFyU2VhcmNoT25CbHVyOmZ1bmN0aW9uKCl7cmV0dXJuIHRoaXMuY2xlYXJTZWFyY2hPblNlbGVjdCYmIXRoaXMubXVsdGlwbGV9LHNlYXJjaGluZzpmdW5jdGlvbigpe3JldHVybiEhdGhpcy5zZWFyY2h9LGRyb3Bkb3duT3BlbjpmdW5jdGlvbigpe3JldHVybiF0aGlzLm5vRHJvcCYmKHRoaXMub3BlbiYmIXRoaXMubXV0YWJsZUxvYWRpbmcpfSxzZWFyY2hQbGFjZWhvbGRlcjpmdW5jdGlvbigpe2lmKHRoaXMuaXNWYWx1ZUVtcHR5JiZ0aGlzLnBsYWNlaG9sZGVyKXJldHVybiB0aGlzLnBsYWNlaG9sZGVyfSxmaWx0ZXJlZE9wdGlvbnM6ZnVuY3Rpb24oKXt2YXIgdD10aGlzLGU9dGhpcy5tdXRhYmxlT3B0aW9ucy5maWx0ZXIoZnVuY3Rpb24oZSl7cmV0dXJuXCJvYmplY3RcIj09PShcInVuZGVmaW5lZFwiPT10eXBlb2YgZT9cInVuZGVmaW5lZFwiOigwLGwuZGVmYXVsdCkoZSkpJiZlLmhhc093blByb3BlcnR5KHQubGFiZWwpP2VbdC5sYWJlbF0udG9Mb3dlckNhc2UoKS5pbmRleE9mKHQuc2VhcmNoLnRvTG93ZXJDYXNlKCkpPi0xOlwib2JqZWN0XCIhPT0oXCJ1bmRlZmluZWRcIj09dHlwZW9mIGU/XCJ1bmRlZmluZWRcIjooMCxsLmRlZmF1bHQpKGUpKXx8ZS5oYXNPd25Qcm9wZXJ0eSh0LmxhYmVsKT9lLnRvTG93ZXJDYXNlKCkuaW5kZXhPZih0LnNlYXJjaC50b0xvd2VyQ2FzZSgpKT4tMTpjb25zb2xlLndhcm4oJ1t2dWUtc2VsZWN0IHdhcm5dOiBMYWJlbCBrZXkgXCJvcHRpb24uJyt0LmxhYmVsKydcIiBkb2VzIG5vdCBleGlzdCBpbiBvcHRpb25zIG9iamVjdC5cXG5odHRwOi8vc2FnYWxib3QuZ2l0aHViLmlvL3Z1ZS1zZWxlY3QvI2V4LWxhYmVscycpfSk7cmV0dXJuIHRoaXMudGFnZ2FibGUmJnRoaXMuc2VhcmNoLmxlbmd0aCYmIXRoaXMub3B0aW9uRXhpc3RzKHRoaXMuc2VhcmNoKSYmZS51bnNoaWZ0KHRoaXMuc2VhcmNoKSxlfSxpc1ZhbHVlRW1wdHk6ZnVuY3Rpb24oKXtyZXR1cm4hdGhpcy5tdXRhYmxlVmFsdWV8fChcIm9iamVjdFwiPT09KDAsbC5kZWZhdWx0KSh0aGlzLm11dGFibGVWYWx1ZSk/ISgwLGkuZGVmYXVsdCkodGhpcy5tdXRhYmxlVmFsdWUpLmxlbmd0aDohdGhpcy5tdXRhYmxlVmFsdWUubGVuZ3RoKX0sdmFsdWVBc0FycmF5OmZ1bmN0aW9uKCl7cmV0dXJuIHRoaXMubXVsdGlwbGU/dGhpcy5tdXRhYmxlVmFsdWU6dGhpcy5tdXRhYmxlVmFsdWU/W3RoaXMubXV0YWJsZVZhbHVlXTpbXX19fX0sZnVuY3Rpb24odCxlLG4pe1widXNlIHN0cmljdFwiO2Z1bmN0aW9uIHIodCl7cmV0dXJuIHQmJnQuX19lc01vZHVsZT90OntkZWZhdWx0OnR9fU9iamVjdC5kZWZpbmVQcm9wZXJ0eShlLFwiX19lc01vZHVsZVwiLHt2YWx1ZTohMH0pO3ZhciBvPW4oMjgpLGk9cihvKSxhPW4oMzApLHM9cihhKSx1PW4oMjkpLGw9cih1KTtlLmRlZmF1bHQ9e2FqYXg6aS5kZWZhdWx0LHBvaW50ZXI6cy5kZWZhdWx0LHBvaW50ZXJTY3JvbGw6bC5kZWZhdWx0fX0sZnVuY3Rpb24odCxlLG4pe3QuZXhwb3J0cz17ZGVmYXVsdDpuKDQ5KSxfX2VzTW9kdWxlOiEwfX0sZnVuY3Rpb24odCxlLG4pe3QuZXhwb3J0cz17ZGVmYXVsdDpuKDUwKSxfX2VzTW9kdWxlOiEwfX0sZnVuY3Rpb24odCxlLG4pe3QuZXhwb3J0cz17ZGVmYXVsdDpuKDUxKSxfX2VzTW9kdWxlOiEwfX0sZnVuY3Rpb24odCxlLG4pe3QuZXhwb3J0cz17ZGVmYXVsdDpuKDUyKSxfX2VzTW9kdWxlOiEwfX0sZnVuY3Rpb24odCxlLG4pe1widXNlIHN0cmljdFwiO2Z1bmN0aW9uIHIodCl7cmV0dXJuIHQmJnQuX19lc01vZHVsZT90OntkZWZhdWx0OnR9fWUuX19lc01vZHVsZT0hMDt2YXIgbz1uKDQzKSxpPXIobyk7ZS5kZWZhdWx0PWZ1bmN0aW9uKHQsZSxuKXtyZXR1cm4gZSBpbiB0PygwLGkuZGVmYXVsdCkodCxlLHt2YWx1ZTpuLGVudW1lcmFibGU6ITAsY29uZmlndXJhYmxlOiEwLHdyaXRhYmxlOiEwfSk6dFtlXT1uLHR9fSxmdW5jdGlvbih0LGUsbil7XCJ1c2Ugc3RyaWN0XCI7ZnVuY3Rpb24gcih0KXtyZXR1cm4gdCYmdC5fX2VzTW9kdWxlP3Q6e2RlZmF1bHQ6dH19ZS5fX2VzTW9kdWxlPSEwO3ZhciBvPW4oNDYpLGk9cihvKSxhPW4oNDUpLHM9cihhKSx1PVwiZnVuY3Rpb25cIj09dHlwZW9mIHMuZGVmYXVsdCYmXCJzeW1ib2xcIj09dHlwZW9mIGkuZGVmYXVsdD9mdW5jdGlvbih0KXtyZXR1cm4gdHlwZW9mIHR9OmZ1bmN0aW9uKHQpe3JldHVybiB0JiZcImZ1bmN0aW9uXCI9PXR5cGVvZiBzLmRlZmF1bHQmJnQuY29uc3RydWN0b3I9PT1zLmRlZmF1bHQmJnQhPT1zLmRlZmF1bHQucHJvdG90eXBlP1wic3ltYm9sXCI6dHlwZW9mIHR9O2UuZGVmYXVsdD1cImZ1bmN0aW9uXCI9PXR5cGVvZiBzLmRlZmF1bHQmJlwic3ltYm9sXCI9PT11KGkuZGVmYXVsdCk/ZnVuY3Rpb24odCl7cmV0dXJuXCJ1bmRlZmluZWRcIj09dHlwZW9mIHQ/XCJ1bmRlZmluZWRcIjp1KHQpfTpmdW5jdGlvbih0KXtyZXR1cm4gdCYmXCJmdW5jdGlvblwiPT10eXBlb2Ygcy5kZWZhdWx0JiZ0LmNvbnN0cnVjdG9yPT09cy5kZWZhdWx0JiZ0IT09cy5kZWZhdWx0LnByb3RvdHlwZT9cInN5bWJvbFwiOlwidW5kZWZpbmVkXCI9PXR5cGVvZiB0P1widW5kZWZpbmVkXCI6dSh0KX19LGZ1bmN0aW9uKHQsZSxuKXtuKDczKTt2YXIgcj1uKDUpLk9iamVjdDt0LmV4cG9ydHM9ZnVuY3Rpb24odCxlLG4pe3JldHVybiByLmRlZmluZVByb3BlcnR5KHQsZSxuKX19LGZ1bmN0aW9uKHQsZSxuKXtuKDc0KSx0LmV4cG9ydHM9big1KS5PYmplY3Qua2V5c30sZnVuY3Rpb24odCxlLG4pe24oNzcpLG4oNzUpLG4oNzgpLG4oNzkpLHQuZXhwb3J0cz1uKDUpLlN5bWJvbH0sZnVuY3Rpb24odCxlLG4pe24oNzYpLG4oODApLHQuZXhwb3J0cz1uKDI3KS5mKFwiaXRlcmF0b3JcIil9LGZ1bmN0aW9uKHQsZSl7dC5leHBvcnRzPWZ1bmN0aW9uKHQpe2lmKFwiZnVuY3Rpb25cIiE9dHlwZW9mIHQpdGhyb3cgVHlwZUVycm9yKHQrXCIgaXMgbm90IGEgZnVuY3Rpb24hXCIpO3JldHVybiB0fX0sZnVuY3Rpb24odCxlKXt0LmV4cG9ydHM9ZnVuY3Rpb24oKXt9fSxmdW5jdGlvbih0LGUsbil7dmFyIHI9big3KSxvPW4oNzEpLGk9big3MCk7dC5leHBvcnRzPWZ1bmN0aW9uKHQpe3JldHVybiBmdW5jdGlvbihlLG4sYSl7dmFyIHMsdT1yKGUpLGw9byh1Lmxlbmd0aCksYz1pKGEsbCk7aWYodCYmbiE9bil7Zm9yKDtsPmM7KWlmKHM9dVtjKytdLHMhPXMpcmV0dXJuITB9ZWxzZSBmb3IoO2w+YztjKyspaWYoKHR8fGMgaW4gdSkmJnVbY109PT1uKXJldHVybiB0fHxjfHwwO3JldHVybiF0JiYtMX19fSxmdW5jdGlvbih0LGUsbil7dmFyIHI9big1Myk7dC5leHBvcnRzPWZ1bmN0aW9uKHQsZSxuKXtpZihyKHQpLHZvaWQgMD09PWUpcmV0dXJuIHQ7c3dpdGNoKG4pe2Nhc2UgMTpyZXR1cm4gZnVuY3Rpb24obil7cmV0dXJuIHQuY2FsbChlLG4pfTtjYXNlIDI6cmV0dXJuIGZ1bmN0aW9uKG4scil7cmV0dXJuIHQuY2FsbChlLG4scil9O2Nhc2UgMzpyZXR1cm4gZnVuY3Rpb24obixyLG8pe3JldHVybiB0LmNhbGwoZSxuLHIsbyl9fXJldHVybiBmdW5jdGlvbigpe3JldHVybiB0LmFwcGx5KGUsYXJndW1lbnRzKX19fSxmdW5jdGlvbih0LGUsbil7dmFyIHI9bigxMyksbz1uKDM3KSxpPW4oMjApO3QuZXhwb3J0cz1mdW5jdGlvbih0KXt2YXIgZT1yKHQpLG49by5mO2lmKG4pZm9yKHZhciBhLHM9bih0KSx1PWkuZixsPTA7cy5sZW5ndGg+bDspdS5jYWxsKHQsYT1zW2wrK10pJiZlLnB1c2goYSk7cmV0dXJuIGV9fSxmdW5jdGlvbih0LGUsbil7dmFyIHI9bigxKS5kb2N1bWVudDt0LmV4cG9ydHM9ciYmci5kb2N1bWVudEVsZW1lbnR9LGZ1bmN0aW9uKHQsZSxuKXt2YXIgcj1uKDMxKTt0LmV4cG9ydHM9T2JqZWN0KFwielwiKS5wcm9wZXJ0eUlzRW51bWVyYWJsZSgwKT9PYmplY3Q6ZnVuY3Rpb24odCl7cmV0dXJuXCJTdHJpbmdcIj09cih0KT90LnNwbGl0KFwiXCIpOk9iamVjdCh0KX19LGZ1bmN0aW9uKHQsZSxuKXt2YXIgcj1uKDMxKTt0LmV4cG9ydHM9QXJyYXkuaXNBcnJheXx8ZnVuY3Rpb24odCl7cmV0dXJuXCJBcnJheVwiPT1yKHQpfX0sZnVuY3Rpb24odCxlLG4pe1widXNlIHN0cmljdFwiO3ZhciByPW4oMzUpLG89bigxNCksaT1uKDIxKSxhPXt9O24oNikoYSxuKDgpKFwiaXRlcmF0b3JcIiksZnVuY3Rpb24oKXtyZXR1cm4gdGhpc30pLHQuZXhwb3J0cz1mdW5jdGlvbih0LGUsbil7dC5wcm90b3R5cGU9cihhLHtuZXh0Om8oMSxuKX0pLGkodCxlK1wiIEl0ZXJhdG9yXCIpfX0sZnVuY3Rpb24odCxlKXt0LmV4cG9ydHM9ZnVuY3Rpb24odCxlKXtyZXR1cm57dmFsdWU6ZSxkb25lOiEhdH19fSxmdW5jdGlvbih0LGUsbil7dmFyIHI9bigxNSkoXCJtZXRhXCIpLG89bigxMiksaT1uKDMpLGE9big0KS5mLHM9MCx1PU9iamVjdC5pc0V4dGVuc2libGV8fGZ1bmN0aW9uKCl7cmV0dXJuITB9LGw9IW4oOSkoZnVuY3Rpb24oKXtyZXR1cm4gdShPYmplY3QucHJldmVudEV4dGVuc2lvbnMoe30pKX0pLGM9ZnVuY3Rpb24odCl7YSh0LHIse3ZhbHVlOntpOlwiT1wiKyArK3Msdzp7fX19KX0sZj1mdW5jdGlvbih0LGUpe2lmKCFvKHQpKXJldHVyblwic3ltYm9sXCI9PXR5cGVvZiB0P3Q6KFwic3RyaW5nXCI9PXR5cGVvZiB0P1wiU1wiOlwiUFwiKSt0O2lmKCFpKHQscikpe2lmKCF1KHQpKXJldHVyblwiRlwiO2lmKCFlKXJldHVyblwiRVwiO2ModCl9cmV0dXJuIHRbcl0uaX0scD1mdW5jdGlvbih0LGUpe2lmKCFpKHQscikpe2lmKCF1KHQpKXJldHVybiEwO2lmKCFlKXJldHVybiExO2ModCl9cmV0dXJuIHRbcl0ud30sZD1mdW5jdGlvbih0KXtyZXR1cm4gbCYmaC5ORUVEJiZ1KHQpJiYhaSh0LHIpJiZjKHQpLHR9LGg9dC5leHBvcnRzPXtLRVk6cixORUVEOiExLGZhc3RLZXk6ZixnZXRXZWFrOnAsb25GcmVlemU6ZH19LGZ1bmN0aW9uKHQsZSxuKXt2YXIgcj1uKDQpLG89bigxMCksaT1uKDEzKTt0LmV4cG9ydHM9bigyKT9PYmplY3QuZGVmaW5lUHJvcGVydGllczpmdW5jdGlvbih0LGUpe28odCk7Zm9yKHZhciBuLGE9aShlKSxzPWEubGVuZ3RoLHU9MDtzPnU7KXIuZih0LG49YVt1KytdLGVbbl0pO3JldHVybiB0fX0sZnVuY3Rpb24odCxlLG4pe3ZhciByPW4oMjApLG89bigxNCksaT1uKDcpLGE9bigyNSkscz1uKDMpLHU9bigzMyksbD1PYmplY3QuZ2V0T3duUHJvcGVydHlEZXNjcmlwdG9yO2UuZj1uKDIpP2w6ZnVuY3Rpb24odCxlKXtpZih0PWkodCksZT1hKGUsITApLHUpdHJ5e3JldHVybiBsKHQsZSl9Y2F0Y2godCl7fWlmKHModCxlKSlyZXR1cm4gbyghci5mLmNhbGwodCxlKSx0W2VdKX19LGZ1bmN0aW9uKHQsZSxuKXt2YXIgcj1uKDcpLG89bigzNikuZixpPXt9LnRvU3RyaW5nLGE9XCJvYmplY3RcIj09dHlwZW9mIHdpbmRvdyYmd2luZG93JiZPYmplY3QuZ2V0T3duUHJvcGVydHlOYW1lcz9PYmplY3QuZ2V0T3duUHJvcGVydHlOYW1lcyh3aW5kb3cpOltdLHM9ZnVuY3Rpb24odCl7dHJ5e3JldHVybiBvKHQpfWNhdGNoKHQpe3JldHVybiBhLnNsaWNlKCl9fTt0LmV4cG9ydHMuZj1mdW5jdGlvbih0KXtyZXR1cm4gYSYmXCJbb2JqZWN0IFdpbmRvd11cIj09aS5jYWxsKHQpP3ModCk6byhyKHQpKX19LGZ1bmN0aW9uKHQsZSxuKXt2YXIgcj1uKDMpLG89big0MCksaT1uKDIyKShcIklFX1BST1RPXCIpLGE9T2JqZWN0LnByb3RvdHlwZTt0LmV4cG9ydHM9T2JqZWN0LmdldFByb3RvdHlwZU9mfHxmdW5jdGlvbih0KXtyZXR1cm4gdD1vKHQpLHIodCxpKT90W2ldOlwiZnVuY3Rpb25cIj09dHlwZW9mIHQuY29uc3RydWN0b3ImJnQgaW5zdGFuY2VvZiB0LmNvbnN0cnVjdG9yP3QuY29uc3RydWN0b3IucHJvdG90eXBlOnQgaW5zdGFuY2VvZiBPYmplY3Q/YTpudWxsfX0sZnVuY3Rpb24odCxlLG4pe3ZhciByPW4oMTEpLG89big1KSxpPW4oOSk7dC5leHBvcnRzPWZ1bmN0aW9uKHQsZSl7dmFyIG49KG8uT2JqZWN0fHx7fSlbdF18fE9iamVjdFt0XSxhPXt9O2FbdF09ZShuKSxyKHIuUytyLkYqaShmdW5jdGlvbigpe24oMSl9KSxcIk9iamVjdFwiLGEpfX0sZnVuY3Rpb24odCxlLG4pe3ZhciByPW4oMjQpLG89bigxNik7dC5leHBvcnRzPWZ1bmN0aW9uKHQpe3JldHVybiBmdW5jdGlvbihlLG4pe3ZhciBpLGEscz1TdHJpbmcobyhlKSksdT1yKG4pLGw9cy5sZW5ndGg7cmV0dXJuIHU8MHx8dT49bD90P1wiXCI6dm9pZCAwOihpPXMuY2hhckNvZGVBdCh1KSxpPDU1Mjk2fHxpPjU2MzE5fHx1KzE9PT1sfHwoYT1zLmNoYXJDb2RlQXQodSsxKSk8NTYzMjB8fGE+NTczNDM/dD9zLmNoYXJBdCh1KTppOnQ/cy5zbGljZSh1LHUrMik6KGktNTUyOTY8PDEwKSsoYS01NjMyMCkrNjU1MzYpfX19LGZ1bmN0aW9uKHQsZSxuKXt2YXIgcj1uKDI0KSxvPU1hdGgubWF4LGk9TWF0aC5taW47dC5leHBvcnRzPWZ1bmN0aW9uKHQsZSl7cmV0dXJuIHQ9cih0KSx0PDA/byh0K2UsMCk6aSh0LGUpfX0sZnVuY3Rpb24odCxlLG4pe3ZhciByPW4oMjQpLG89TWF0aC5taW47dC5leHBvcnRzPWZ1bmN0aW9uKHQpe3JldHVybiB0PjA/byhyKHQpLDkwMDcxOTkyNTQ3NDA5OTEpOjB9fSxmdW5jdGlvbih0LGUsbil7XCJ1c2Ugc3RyaWN0XCI7dmFyIHI9big1NCksbz1uKDYyKSxpPW4oMTgpLGE9big3KTt0LmV4cG9ydHM9bigzNCkoQXJyYXksXCJBcnJheVwiLGZ1bmN0aW9uKHQsZSl7dGhpcy5fdD1hKHQpLHRoaXMuX2k9MCx0aGlzLl9rPWV9LGZ1bmN0aW9uKCl7dmFyIHQ9dGhpcy5fdCxlPXRoaXMuX2ssbj10aGlzLl9pKys7cmV0dXJuIXR8fG4+PXQubGVuZ3RoPyh0aGlzLl90PXZvaWQgMCxvKDEpKTpcImtleXNcIj09ZT9vKDAsbik6XCJ2YWx1ZXNcIj09ZT9vKDAsdFtuXSk6bygwLFtuLHRbbl1dKX0sXCJ2YWx1ZXNcIiksaS5Bcmd1bWVudHM9aS5BcnJheSxyKFwia2V5c1wiKSxyKFwidmFsdWVzXCIpLHIoXCJlbnRyaWVzXCIpfSxmdW5jdGlvbih0LGUsbil7dmFyIHI9bigxMSk7cihyLlMrci5GKiFuKDIpLFwiT2JqZWN0XCIse2RlZmluZVByb3BlcnR5Om4oNCkuZn0pfSxmdW5jdGlvbih0LGUsbil7dmFyIHI9big0MCksbz1uKDEzKTtuKDY4KShcImtleXNcIixmdW5jdGlvbigpe3JldHVybiBmdW5jdGlvbih0KXtyZXR1cm4gbyhyKHQpKX19KX0sZnVuY3Rpb24odCxlKXt9LGZ1bmN0aW9uKHQsZSxuKXtcInVzZSBzdHJpY3RcIjt2YXIgcj1uKDY5KSghMCk7bigzNCkoU3RyaW5nLFwiU3RyaW5nXCIsZnVuY3Rpb24odCl7dGhpcy5fdD1TdHJpbmcodCksdGhpcy5faT0wfSxmdW5jdGlvbigpe3ZhciB0LGU9dGhpcy5fdCxuPXRoaXMuX2k7cmV0dXJuIG4+PWUubGVuZ3RoP3t2YWx1ZTp2b2lkIDAsZG9uZTohMH06KHQ9cihlLG4pLHRoaXMuX2krPXQubGVuZ3RoLHt2YWx1ZTp0LGRvbmU6ITF9KX0pfSxmdW5jdGlvbih0LGUsbil7XCJ1c2Ugc3RyaWN0XCI7dmFyIHI9bigxKSxvPW4oMyksaT1uKDIpLGE9bigxMSkscz1uKDM5KSx1PW4oNjMpLktFWSxsPW4oOSksYz1uKDIzKSxmPW4oMjEpLHA9bigxNSksZD1uKDgpLGg9bigyNyksYj1uKDI2KSx2PW4oNTcpLGc9big2MCkseT1uKDEwKSxtPW4oNykseD1uKDI1KSx3PW4oMTQpLFM9bigzNSksTz1uKDY2KSxfPW4oNjUpLGo9big0KSxQPW4oMTMpLGs9Xy5mLEE9ai5mLE09Ty5mLEw9ci5TeW1ib2wsQz1yLkpTT04sVD1DJiZDLnN0cmluZ2lmeSxFPVwicHJvdG90eXBlXCIsVj1kKFwiX2hpZGRlblwiKSxGPWQoXCJ0b1ByaW1pdGl2ZVwiKSwkPXt9LnByb3BlcnR5SXNFbnVtZXJhYmxlLEI9YyhcInN5bWJvbC1yZWdpc3RyeVwiKSxOPWMoXCJzeW1ib2xzXCIpLEQ9YyhcIm9wLXN5bWJvbHNcIiksST1PYmplY3RbRV0sUj1cImZ1bmN0aW9uXCI9PXR5cGVvZiBMLHo9ci5RT2JqZWN0LEg9IXp8fCF6W0VdfHwheltFXS5maW5kQ2hpbGQsRz1pJiZsKGZ1bmN0aW9uKCl7cmV0dXJuIDchPVMoQSh7fSxcImFcIix7Z2V0OmZ1bmN0aW9uKCl7cmV0dXJuIEEodGhpcyxcImFcIix7dmFsdWU6N30pLmF9fSkpLmF9KT9mdW5jdGlvbih0LGUsbil7dmFyIHI9ayhJLGUpO3ImJmRlbGV0ZSBJW2VdLEEodCxlLG4pLHImJnQhPT1JJiZBKEksZSxyKX06QSxVPWZ1bmN0aW9uKHQpe3ZhciBlPU5bdF09UyhMW0VdKTtyZXR1cm4gZS5faz10LGV9LFc9UiYmXCJzeW1ib2xcIj09dHlwZW9mIEwuaXRlcmF0b3I/ZnVuY3Rpb24odCl7cmV0dXJuXCJzeW1ib2xcIj09dHlwZW9mIHR9OmZ1bmN0aW9uKHQpe3JldHVybiB0IGluc3RhbmNlb2YgTH0sSj1mdW5jdGlvbih0LGUsbil7cmV0dXJuIHQ9PT1JJiZKKEQsZSxuKSx5KHQpLGU9eChlLCEwKSx5KG4pLG8oTixlKT8obi5lbnVtZXJhYmxlPyhvKHQsVikmJnRbVl1bZV0mJih0W1ZdW2VdPSExKSxuPVMobix7ZW51bWVyYWJsZTp3KDAsITEpfSkpOihvKHQsVil8fEEodCxWLHcoMSx7fSkpLHRbVl1bZV09ITApLEcodCxlLG4pKTpBKHQsZSxuKX0sSz1mdW5jdGlvbih0LGUpe3kodCk7Zm9yKHZhciBuLHI9dihlPW0oZSkpLG89MCxpPXIubGVuZ3RoO2k+bzspSih0LG49cltvKytdLGVbbl0pO3JldHVybiB0fSxZPWZ1bmN0aW9uKHQsZSl7cmV0dXJuIHZvaWQgMD09PWU/Uyh0KTpLKFModCksZSl9LHE9ZnVuY3Rpb24odCl7dmFyIGU9JC5jYWxsKHRoaXMsdD14KHQsITApKTtyZXR1cm4hKHRoaXM9PT1JJiZvKE4sdCkmJiFvKEQsdCkpJiYoIShlfHwhbyh0aGlzLHQpfHwhbyhOLHQpfHxvKHRoaXMsVikmJnRoaXNbVl1bdF0pfHxlKX0sUT1mdW5jdGlvbih0LGUpe2lmKHQ9bSh0KSxlPXgoZSwhMCksdCE9PUl8fCFvKE4sZSl8fG8oRCxlKSl7dmFyIG49ayh0LGUpO3JldHVybiFufHwhbyhOLGUpfHxvKHQsVikmJnRbVl1bZV18fChuLmVudW1lcmFibGU9ITApLG59fSxaPWZ1bmN0aW9uKHQpe2Zvcih2YXIgZSxuPU0obSh0KSkscj1bXSxpPTA7bi5sZW5ndGg+aTspbyhOLGU9bltpKytdKXx8ZT09Vnx8ZT09dXx8ci5wdXNoKGUpO3JldHVybiByfSxYPWZ1bmN0aW9uKHQpe2Zvcih2YXIgZSxuPXQ9PT1JLHI9TShuP0Q6bSh0KSksaT1bXSxhPTA7ci5sZW5ndGg+YTspIW8oTixlPXJbYSsrXSl8fG4mJiFvKEksZSl8fGkucHVzaChOW2VdKTtyZXR1cm4gaX07Unx8KEw9ZnVuY3Rpb24oKXtpZih0aGlzIGluc3RhbmNlb2YgTCl0aHJvdyBUeXBlRXJyb3IoXCJTeW1ib2wgaXMgbm90IGEgY29uc3RydWN0b3IhXCIpO3ZhciB0PXAoYXJndW1lbnRzLmxlbmd0aD4wP2FyZ3VtZW50c1swXTp2b2lkIDApLGU9ZnVuY3Rpb24obil7dGhpcz09PUkmJmUuY2FsbChELG4pLG8odGhpcyxWKSYmbyh0aGlzW1ZdLHQpJiYodGhpc1tWXVt0XT0hMSksRyh0aGlzLHQsdygxLG4pKX07cmV0dXJuIGkmJkgmJkcoSSx0LHtjb25maWd1cmFibGU6ITAsc2V0OmV9KSxVKHQpfSxzKExbRV0sXCJ0b1N0cmluZ1wiLGZ1bmN0aW9uKCl7cmV0dXJuIHRoaXMuX2t9KSxfLmY9USxqLmY9SixuKDM2KS5mPU8uZj1aLG4oMjApLmY9cSxuKDM3KS5mPVgsaSYmIW4oMTkpJiZzKEksXCJwcm9wZXJ0eUlzRW51bWVyYWJsZVwiLHEsITApLGguZj1mdW5jdGlvbih0KXtyZXR1cm4gVShkKHQpKX0pLGEoYS5HK2EuVythLkYqIVIse1N5bWJvbDpMfSk7Zm9yKHZhciB0dD1cImhhc0luc3RhbmNlLGlzQ29uY2F0U3ByZWFkYWJsZSxpdGVyYXRvcixtYXRjaCxyZXBsYWNlLHNlYXJjaCxzcGVjaWVzLHNwbGl0LHRvUHJpbWl0aXZlLHRvU3RyaW5nVGFnLHVuc2NvcGFibGVzXCIuc3BsaXQoXCIsXCIpLGV0PTA7dHQubGVuZ3RoPmV0OylkKHR0W2V0KytdKTtmb3IodmFyIG50PVAoZC5zdG9yZSkscnQ9MDtudC5sZW5ndGg+cnQ7KWIobnRbcnQrK10pO2EoYS5TK2EuRiohUixcIlN5bWJvbFwiLHtmb3I6ZnVuY3Rpb24odCl7cmV0dXJuIG8oQix0Kz1cIlwiKT9CW3RdOkJbdF09TCh0KX0sa2V5Rm9yOmZ1bmN0aW9uKHQpe2lmKCFXKHQpKXRocm93IFR5cGVFcnJvcih0K1wiIGlzIG5vdCBhIHN5bWJvbCFcIik7Zm9yKHZhciBlIGluIEIpaWYoQltlXT09PXQpcmV0dXJuIGV9LHVzZVNldHRlcjpmdW5jdGlvbigpe0g9ITB9LHVzZVNpbXBsZTpmdW5jdGlvbigpe0g9ITF9fSksYShhLlMrYS5GKiFSLFwiT2JqZWN0XCIse2NyZWF0ZTpZLGRlZmluZVByb3BlcnR5OkosZGVmaW5lUHJvcGVydGllczpLLGdldE93blByb3BlcnR5RGVzY3JpcHRvcjpRLGdldE93blByb3BlcnR5TmFtZXM6WixnZXRPd25Qcm9wZXJ0eVN5bWJvbHM6WH0pLEMmJmEoYS5TK2EuRiooIVJ8fGwoZnVuY3Rpb24oKXt2YXIgdD1MKCk7cmV0dXJuXCJbbnVsbF1cIiE9VChbdF0pfHxcInt9XCIhPVQoe2E6dH0pfHxcInt9XCIhPVQoT2JqZWN0KHQpKX0pKSxcIkpTT05cIix7c3RyaW5naWZ5OmZ1bmN0aW9uKHQpe2lmKHZvaWQgMCE9PXQmJiFXKHQpKXtmb3IodmFyIGUsbixyPVt0XSxvPTE7YXJndW1lbnRzLmxlbmd0aD5vOylyLnB1c2goYXJndW1lbnRzW28rK10pO3JldHVybiBlPXJbMV0sXCJmdW5jdGlvblwiPT10eXBlb2YgZSYmKG49ZSksIW4mJmcoZSl8fChlPWZ1bmN0aW9uKHQsZSl7aWYobiYmKGU9bi5jYWxsKHRoaXMsdCxlKSksIVcoZSkpcmV0dXJuIGV9KSxyWzFdPWUsVC5hcHBseShDLHIpfX19KSxMW0VdW0ZdfHxuKDYpKExbRV0sRixMW0VdLnZhbHVlT2YpLGYoTCxcIlN5bWJvbFwiKSxmKE1hdGgsXCJNYXRoXCIsITApLGYoci5KU09OLFwiSlNPTlwiLCEwKX0sZnVuY3Rpb24odCxlLG4pe24oMjYpKFwiYXN5bmNJdGVyYXRvclwiKX0sZnVuY3Rpb24odCxlLG4pe24oMjYpKFwib2JzZXJ2YWJsZVwiKX0sZnVuY3Rpb24odCxlLG4pe24oNzIpO2Zvcih2YXIgcj1uKDEpLG89big2KSxpPW4oMTgpLGE9big4KShcInRvU3RyaW5nVGFnXCIpLHM9XCJDU1NSdWxlTGlzdCxDU1NTdHlsZURlY2xhcmF0aW9uLENTU1ZhbHVlTGlzdCxDbGllbnRSZWN0TGlzdCxET01SZWN0TGlzdCxET01TdHJpbmdMaXN0LERPTVRva2VuTGlzdCxEYXRhVHJhbnNmZXJJdGVtTGlzdCxGaWxlTGlzdCxIVE1MQWxsQ29sbGVjdGlvbixIVE1MQ29sbGVjdGlvbixIVE1MRm9ybUVsZW1lbnQsSFRNTFNlbGVjdEVsZW1lbnQsTWVkaWFMaXN0LE1pbWVUeXBlQXJyYXksTmFtZWROb2RlTWFwLE5vZGVMaXN0LFBhaW50UmVxdWVzdExpc3QsUGx1Z2luLFBsdWdpbkFycmF5LFNWR0xlbmd0aExpc3QsU1ZHTnVtYmVyTGlzdCxTVkdQYXRoU2VnTGlzdCxTVkdQb2ludExpc3QsU1ZHU3RyaW5nTGlzdCxTVkdUcmFuc2Zvcm1MaXN0LFNvdXJjZUJ1ZmZlckxpc3QsU3R5bGVTaGVldExpc3QsVGV4dFRyYWNrQ3VlTGlzdCxUZXh0VHJhY2tMaXN0LFRvdWNoTGlzdFwiLnNwbGl0KFwiLFwiKSx1PTA7dTxzLmxlbmd0aDt1Kyspe3ZhciBsPXNbdV0sYz1yW2xdLGY9YyYmYy5wcm90b3R5cGU7ZiYmIWZbYV0mJm8oZixhLGwpLGlbbF09aS5BcnJheX19LGZ1bmN0aW9uKHQsZSxuKXtlPXQuZXhwb3J0cz1uKDgyKSgpLGUucHVzaChbdC5pZCwnLnYtc2VsZWN0e3Bvc2l0aW9uOnJlbGF0aXZlO2ZvbnQtZmFtaWx5OnNhbnMtc2VyaWZ9LnYtc2VsZWN0IC5kaXNhYmxlZHtjdXJzb3I6bm90LWFsbG93ZWQhaW1wb3J0YW50O2JhY2tncm91bmQtY29sb3I6I2Y4ZjhmOCFpbXBvcnRhbnR9LnYtc2VsZWN0LC52LXNlbGVjdCAqe2JveC1zaXppbmc6Ym9yZGVyLWJveH0udi1zZWxlY3QucnRsIC5vcGVuLWluZGljYXRvcntsZWZ0OjEwcHg7cmlnaHQ6YXV0b30udi1zZWxlY3QucnRsIC5zZWxlY3RlZC10YWd7ZmxvYXQ6cmlnaHQ7bWFyZ2luLXJpZ2h0OjNweDttYXJnaW4tbGVmdDoxcHh9LnYtc2VsZWN0LnJ0bCAuZHJvcGRvd24tbWVudXt0ZXh0LWFsaWduOnJpZ2h0fS52LXNlbGVjdCAub3Blbi1pbmRpY2F0b3J7cG9zaXRpb246YWJzb2x1dGU7Ym90dG9tOjZweDtyaWdodDoxMHB4O2N1cnNvcjpwb2ludGVyO3BvaW50ZXItZXZlbnRzOmFsbDtvcGFjaXR5OjE7aGVpZ2h0OjIwcHh9LnYtc2VsZWN0IC5vcGVuLWluZGljYXRvciwudi1zZWxlY3QgLm9wZW4taW5kaWNhdG9yOmJlZm9yZXtkaXNwbGF5OmlubGluZS1ibG9jazt0cmFuc2l0aW9uOmFsbCAuMTVzIGN1YmljLWJlemllcigxLC0uMTE1LC45NzUsLjg1NSk7dHJhbnNpdGlvbi10aW1pbmctZnVuY3Rpb246Y3ViaWMtYmV6aWVyKDEsLS4xMTUsLjk3NSwuODU1KTt3aWR0aDoxMHB4fS52LXNlbGVjdCAub3Blbi1pbmRpY2F0b3I6YmVmb3Jle2JvcmRlci1jb2xvcjpyZ2JhKDYwLDYwLDYwLC41KTtib3JkZXItc3R5bGU6c29saWQ7Ym9yZGVyLXdpZHRoOjNweCAzcHggMCAwO2NvbnRlbnQ6XCJcIjtoZWlnaHQ6MTBweDt2ZXJ0aWNhbC1hbGlnbjp0b3A7dHJhbnNmb3JtOnJvdGF0ZSgxMzNkZWcpO2JveC1zaXppbmc6aW5oZXJpdH0udi1zZWxlY3Qub3BlbiAub3Blbi1pbmRpY2F0b3I6YmVmb3Jle3RyYW5zZm9ybTpyb3RhdGUoMzE1ZGVnKX0udi1zZWxlY3QubG9hZGluZyAub3Blbi1pbmRpY2F0b3J7b3BhY2l0eTowfS52LXNlbGVjdC5vcGVuIC5vcGVuLWluZGljYXRvcntib3R0b206MXB4fS52LXNlbGVjdCAuZHJvcGRvd24tdG9nZ2xley13ZWJraXQtYXBwZWFyYW5jZTpub25lOy1tb3otYXBwZWFyYW5jZTpub25lO2FwcGVhcmFuY2U6bm9uZTtkaXNwbGF5OmJsb2NrO3BhZGRpbmc6MDtiYWNrZ3JvdW5kOm5vbmU7Ym9yZGVyOjFweCBzb2xpZCByZ2JhKDYwLDYwLDYwLC4yNik7Ym9yZGVyLXJhZGl1czo0cHg7d2hpdGUtc3BhY2U6bm9ybWFsfS52LXNlbGVjdCAuZHJvcGRvd24tdG9nZ2xlOmFmdGVye3Zpc2liaWxpdHk6aGlkZGVuO2Rpc3BsYXk6YmxvY2s7Zm9udC1zaXplOjA7Y29udGVudDpcIiBcIjtjbGVhcjpib3RoO2hlaWdodDowfS52LXNlbGVjdC5zZWFyY2hhYmxlIC5kcm9wZG93bi10b2dnbGV7Y3Vyc29yOnRleHR9LnYtc2VsZWN0LnVuc2VhcmNoYWJsZSAuZHJvcGRvd24tdG9nZ2xle2N1cnNvcjpwb2ludGVyfS52LXNlbGVjdC5vcGVuIC5kcm9wZG93bi10b2dnbGV7Ym9yZGVyLWJvdHRvbS1jb2xvcjp0cmFuc3BhcmVudDtib3JkZXItYm90dG9tLWxlZnQtcmFkaXVzOjA7Ym9yZGVyLWJvdHRvbS1yaWdodC1yYWRpdXM6MH0udi1zZWxlY3QgLmRyb3Bkb3duLW1lbnV7ZGlzcGxheTpibG9jaztwb3NpdGlvbjphYnNvbHV0ZTt0b3A6MTAwJTtsZWZ0OjA7ei1pbmRleDoxMDAwO21pbi13aWR0aDoxNjBweDtwYWRkaW5nOjVweCAwO21hcmdpbjowO3dpZHRoOjEwMCU7b3ZlcmZsb3cteTpzY3JvbGw7Ym9yZGVyOjFweCBzb2xpZCByZ2JhKDAsMCwwLC4yNik7Ym94LXNoYWRvdzowIDNweCA2cHggMCByZ2JhKDAsMCwwLC4xNSk7Ym9yZGVyLXRvcDpub25lO2JvcmRlci1yYWRpdXM6MCAwIDRweCA0cHg7dGV4dC1hbGlnbjpsZWZ0O2xpc3Qtc3R5bGU6bm9uZTtiYWNrZ3JvdW5kOiNmZmZ9LnYtc2VsZWN0IC5uby1vcHRpb25ze3RleHQtYWxpZ246Y2VudGVyfS52LXNlbGVjdCAuc2VsZWN0ZWQtdGFne2NvbG9yOiMzMzM7YmFja2dyb3VuZC1jb2xvcjojZjBmMGYwO2JvcmRlcjoxcHggc29saWQgI2NjYztib3JkZXItcmFkaXVzOjRweDtoZWlnaHQ6MjZweDttYXJnaW46NHB4IDFweCAwIDNweDtwYWRkaW5nOjFweCAuMjVlbTtmbG9hdDpsZWZ0O2xpbmUtaGVpZ2h0OjI0cHh9LnYtc2VsZWN0LnNpbmdsZSAuc2VsZWN0ZWQtdGFne2JhY2tncm91bmQtY29sb3I6dHJhbnNwYXJlbnQ7Ym9yZGVyLWNvbG9yOnRyYW5zcGFyZW50fS52LXNlbGVjdC5zaW5nbGUub3BlbiAuc2VsZWN0ZWQtdGFne3Bvc2l0aW9uOmFic29sdXRlO29wYWNpdHk6LjV9LnYtc2VsZWN0LnNpbmdsZS5sb2FkaW5nIC5zZWxlY3RlZC10YWcsLnYtc2VsZWN0LnNpbmdsZS5vcGVuLnNlYXJjaGluZyAuc2VsZWN0ZWQtdGFne2Rpc3BsYXk6bm9uZX0udi1zZWxlY3QgLnNlbGVjdGVkLXRhZyAuY2xvc2V7ZmxvYXQ6bm9uZTttYXJnaW4tcmlnaHQ6MDtmb250LXNpemU6MjBweDthcHBlYXJhbmNlOm5vbmU7cGFkZGluZzowO2N1cnNvcjpwb2ludGVyO2JhY2tncm91bmQ6MCAwO2JvcmRlcjowO2ZvbnQtd2VpZ2h0OjcwMDtsaW5lLWhlaWdodDoxO2NvbG9yOiMwMDA7dGV4dC1zaGFkb3c6MCAxcHggMCAjZmZmO2ZpbHRlcjphbHBoYShvcGFjaXR5PTIwKTtvcGFjaXR5Oi4yfS52LXNlbGVjdC5zaW5nbGUuc2VhcmNoaW5nOm5vdCgub3Blbik6bm90KC5sb2FkaW5nKSBpbnB1dFt0eXBlPXNlYXJjaF17b3BhY2l0eTouMn0udi1zZWxlY3QgaW5wdXRbdHlwZT1zZWFyY2hdOjotd2Via2l0LXNlYXJjaC1jYW5jZWwtYnV0dG9uLC52LXNlbGVjdCBpbnB1dFt0eXBlPXNlYXJjaF06Oi13ZWJraXQtc2VhcmNoLWRlY29yYXRpb24sLnYtc2VsZWN0IGlucHV0W3R5cGU9c2VhcmNoXTo6LXdlYmtpdC1zZWFyY2gtcmVzdWx0cy1idXR0b24sLnYtc2VsZWN0IGlucHV0W3R5cGU9c2VhcmNoXTo6LXdlYmtpdC1zZWFyY2gtcmVzdWx0cy1kZWNvcmF0aW9ue2Rpc3BsYXk6bm9uZX0udi1zZWxlY3QgaW5wdXRbdHlwZT1zZWFyY2hdOjotbXMtY2xlYXJ7ZGlzcGxheTpub25lfS52LXNlbGVjdCBpbnB1dFt0eXBlPXNlYXJjaF0sLnYtc2VsZWN0IGlucHV0W3R5cGU9c2VhcmNoXTpmb2N1c3thcHBlYXJhbmNlOm5vbmU7LXdlYmtpdC1hcHBlYXJhbmNlOm5vbmU7LW1vei1hcHBlYXJhbmNlOm5vbmU7bGluZS1oZWlnaHQ6MS40Mjg1NzE0Mztmb250LXNpemU6MWVtO2hlaWdodDozNHB4O2Rpc3BsYXk6aW5saW5lLWJsb2NrO2JvcmRlcjpub25lO291dGxpbmU6bm9uZTttYXJnaW46MDtwYWRkaW5nOjAgLjVlbTt3aWR0aDoxMGVtO21heC13aWR0aDoxMDAlO2JhY2tncm91bmQ6bm9uZTtwb3NpdGlvbjpyZWxhdGl2ZTtib3gtc2hhZG93Om5vbmU7ZmxvYXQ6bGVmdDtjbGVhcjpub25lfS52LXNlbGVjdCBsaXtsaW5lLWhlaWdodDoxLjQyODU3MTQzfS52LXNlbGVjdCBsaT5he2Rpc3BsYXk6YmxvY2s7cGFkZGluZzozcHggMjBweDtjbGVhcjpib3RoO2NvbG9yOiMzMzM7d2hpdGUtc3BhY2U6bm93cmFwfS52LXNlbGVjdCBsaTpob3ZlcntjdXJzb3I6cG9pbnRlcn0udi1zZWxlY3QgLmRyb3Bkb3duLW1lbnUgLmFjdGl2ZT5he2NvbG9yOiMzMzM7YmFja2dyb3VuZDpyZ2JhKDUwLDUwLDUwLC4xKX0udi1zZWxlY3QgLmRyb3Bkb3duLW1lbnU+LmhpZ2hsaWdodD5he2JhY2tncm91bmQ6IzU4OTdmYjtjb2xvcjojZmZmfS52LXNlbGVjdCAuaGlnaGxpZ2h0Om5vdCg6bGFzdC1jaGlsZCl7bWFyZ2luLWJvdHRvbTowfS52LXNlbGVjdCAuc3Bpbm5lcntvcGFjaXR5OjA7cG9zaXRpb246YWJzb2x1dGU7dG9wOjVweDtyaWdodDoxMHB4O2ZvbnQtc2l6ZTo1cHg7dGV4dC1pbmRlbnQ6LTk5OTllbTtvdmVyZmxvdzpoaWRkZW47Ym9yZGVyLXRvcDouOWVtIHNvbGlkIGhzbGEoMCwwJSwzOSUsLjEpO2JvcmRlci1yaWdodDouOWVtIHNvbGlkIGhzbGEoMCwwJSwzOSUsLjEpO2JvcmRlci1ib3R0b206LjllbSBzb2xpZCBoc2xhKDAsMCUsMzklLC4xKTtib3JkZXItbGVmdDouOWVtIHNvbGlkIHJnYmEoNjAsNjAsNjAsLjQ1KTt0cmFuc2Zvcm06dHJhbnNsYXRlWigwKTthbmltYXRpb246dlNlbGVjdFNwaW5uZXIgMS4xcyBpbmZpbml0ZSBsaW5lYXI7dHJhbnNpdGlvbjpvcGFjaXR5IC4xc30udi1zZWxlY3QgLnNwaW5uZXIsLnYtc2VsZWN0IC5zcGlubmVyOmFmdGVye2JvcmRlci1yYWRpdXM6NTAlO3dpZHRoOjVlbTtoZWlnaHQ6NWVtfS52LXNlbGVjdC5sb2FkaW5nIC5zcGlubmVye29wYWNpdHk6MX1ALXdlYmtpdC1rZXlmcmFtZXMgdlNlbGVjdFNwaW5uZXJ7MCV7dHJhbnNmb3JtOnJvdGF0ZSgwZGVnKX10b3t0cmFuc2Zvcm06cm90YXRlKDF0dXJuKX19QGtleWZyYW1lcyB2U2VsZWN0U3Bpbm5lcnswJXt0cmFuc2Zvcm06cm90YXRlKDBkZWcpfXRve3RyYW5zZm9ybTpyb3RhdGUoMXR1cm4pfX0uZmFkZS1lbnRlci1hY3RpdmUsLmZhZGUtbGVhdmUtYWN0aXZle3RyYW5zaXRpb246b3BhY2l0eSAuMTVzIGN1YmljLWJlemllcigxLC41LC44LDEpfS5mYWRlLWVudGVyLC5mYWRlLWxlYXZlLXRve29wYWNpdHk6MH0nLFwiXCJdKX0sZnVuY3Rpb24odCxlKXt0LmV4cG9ydHM9ZnVuY3Rpb24oKXt2YXIgdD1bXTtyZXR1cm4gdC50b1N0cmluZz1mdW5jdGlvbigpe2Zvcih2YXIgdD1bXSxlPTA7ZTx0aGlzLmxlbmd0aDtlKyspe3ZhciBuPXRoaXNbZV07blsyXT90LnB1c2goXCJAbWVkaWEgXCIrblsyXStcIntcIituWzFdK1wifVwiKTp0LnB1c2goblsxXSl9cmV0dXJuIHQuam9pbihcIlwiKX0sdC5pPWZ1bmN0aW9uKGUsbil7XCJzdHJpbmdcIj09dHlwZW9mIGUmJihlPVtbbnVsbCxlLFwiXCJdXSk7Zm9yKHZhciByPXt9LG89MDtvPHRoaXMubGVuZ3RoO28rKyl7dmFyIGk9dGhpc1tvXVswXTtcIm51bWJlclwiPT10eXBlb2YgaSYmKHJbaV09ITApfWZvcihvPTA7bzxlLmxlbmd0aDtvKyspe3ZhciBhPWVbb107XCJudW1iZXJcIj09dHlwZW9mIGFbMF0mJnJbYVswXV18fChuJiYhYVsyXT9hWzJdPW46biYmKGFbMl09XCIoXCIrYVsyXStcIikgYW5kIChcIituK1wiKVwiKSx0LnB1c2goYSkpfX0sdH19LGZ1bmN0aW9uKHQsZSxuKXtuKDg3KTt2YXIgcj1uKDg0KShuKDQxKSxuKDg1KSxudWxsLG51bGwpO3QuZXhwb3J0cz1yLmV4cG9ydHN9LGZ1bmN0aW9uKHQsZSl7dC5leHBvcnRzPWZ1bmN0aW9uKHQsZSxuLHIpe3ZhciBvLGk9dD10fHx7fSxhPXR5cGVvZiB0LmRlZmF1bHQ7XCJvYmplY3RcIiE9PWEmJlwiZnVuY3Rpb25cIiE9PWF8fChvPXQsaT10LmRlZmF1bHQpO3ZhciBzPVwiZnVuY3Rpb25cIj09dHlwZW9mIGk/aS5vcHRpb25zOmk7aWYoZSYmKHMucmVuZGVyPWUucmVuZGVyLHMuc3RhdGljUmVuZGVyRm5zPWUuc3RhdGljUmVuZGVyRm5zKSxuJiYocy5fc2NvcGVJZD1uKSxyKXt2YXIgdT1zLmNvbXB1dGVkfHwocy5jb21wdXRlZD17fSk7T2JqZWN0LmtleXMocikuZm9yRWFjaChmdW5jdGlvbih0KXt2YXIgZT1yW3RdO3VbdF09ZnVuY3Rpb24oKXtyZXR1cm4gZX19KX1yZXR1cm57ZXNNb2R1bGU6byxleHBvcnRzOmksb3B0aW9uczpzfX19LGZ1bmN0aW9uKHQsZSl7dC5leHBvcnRzPXtyZW5kZXI6ZnVuY3Rpb24oKXt2YXIgdD10aGlzLGU9dC4kY3JlYXRlRWxlbWVudCxuPXQuX3NlbGYuX2N8fGU7cmV0dXJuIG4oXCJkaXZcIix7c3RhdGljQ2xhc3M6XCJkcm9wZG93biB2LXNlbGVjdFwiLGNsYXNzOnQuZHJvcGRvd25DbGFzc2VzLGF0dHJzOntkaXI6dC5kaXJ9fSxbbihcImRpdlwiLHtyZWY6XCJ0b2dnbGVcIixjbGFzczpbXCJkcm9wZG93bi10b2dnbGVcIixcImNsZWFyZml4XCIse2Rpc2FibGVkOnQuZGlzYWJsZWR9XSxvbjp7bW91c2Vkb3duOmZ1bmN0aW9uKGUpe2UucHJldmVudERlZmF1bHQoKSx0LnRvZ2dsZURyb3Bkb3duKGUpfX19LFt0Ll9sKHQudmFsdWVBc0FycmF5LGZ1bmN0aW9uKGUpe3JldHVybiBuKFwic3BhblwiLHtrZXk6ZS5pbmRleCxzdGF0aWNDbGFzczpcInNlbGVjdGVkLXRhZ1wifSxbdC5fdChcInNlbGVjdGVkLW9wdGlvblwiLFt0Ll92KFwiXFxuICAgICAgICBcIit0Ll9zKHQuZ2V0T3B0aW9uTGFiZWwoZSkpK1wiXFxuICAgICAgXCIpXSxudWxsLGUpLHQuX3YoXCIgXCIpLHQubXVsdGlwbGU/bihcImJ1dHRvblwiLHtzdGF0aWNDbGFzczpcImNsb3NlXCIsYXR0cnM6e3R5cGU6XCJidXR0b25cIixcImFyaWEtbGFiZWxcIjpcIlJlbW92ZSBvcHRpb25cIn0sb246e2NsaWNrOmZ1bmN0aW9uKG4pe3QuZGVzZWxlY3QoZSl9fX0sW24oXCJzcGFuXCIse2F0dHJzOntcImFyaWEtaGlkZGVuXCI6XCJ0cnVlXCJ9fSxbdC5fdihcIsOXXCIpXSldKTp0Ll9lKCldLDIpfSksdC5fdihcIiBcIiksbihcImlucHV0XCIse2RpcmVjdGl2ZXM6W3tuYW1lOlwibW9kZWxcIixyYXdOYW1lOlwidi1tb2RlbFwiLHZhbHVlOnQuc2VhcmNoLGV4cHJlc3Npb246XCJzZWFyY2hcIn1dLHJlZjpcInNlYXJjaFwiLGNsYXNzOlt7ZGlzYWJsZWQ6dC5kaXNhYmxlZH0sXCJmb3JtLWNvbnRyb2xcIl0sc3R5bGU6e3dpZHRoOnQuaXNWYWx1ZUVtcHR5P1wiMTAwJVwiOlwiYXV0b1wifSxhdHRyczp7dHlwZTpcInNlYXJjaFwiLHBsYWNlaG9sZGVyOnQuc2VhcmNoUGxhY2Vob2xkZXIscmVhZG9ubHk6IXQuc2VhcmNoYWJsZSxpZDp0LmlucHV0SWQsXCJhcmlhLWxhYmVsXCI6XCJTZWFyY2ggZm9yIG9wdGlvblwifSxkb21Qcm9wczp7dmFsdWU6dC5zZWFyY2h9LG9uOntrZXlkb3duOltmdW5jdGlvbihlKXtyZXR1cm5cImJ1dHRvblwiaW4gZXx8IXQuX2soZS5rZXlDb2RlLFwiZGVsZXRlXCIsWzgsNDZdKT92b2lkIHQubWF5YmVEZWxldGVWYWx1ZShlKTpudWxsfSxmdW5jdGlvbihlKXtyZXR1cm5cImJ1dHRvblwiaW4gZXx8IXQuX2soZS5rZXlDb2RlLFwidXBcIiwzOCk/KGUucHJldmVudERlZmF1bHQoKSx2b2lkIHQudHlwZUFoZWFkVXAoZSkpOm51bGx9LGZ1bmN0aW9uKGUpe3JldHVyblwiYnV0dG9uXCJpbiBlfHwhdC5fayhlLmtleUNvZGUsXCJkb3duXCIsNDApPyhlLnByZXZlbnREZWZhdWx0KCksdm9pZCB0LnR5cGVBaGVhZERvd24oZSkpOm51bGx9LGZ1bmN0aW9uKGUpe3JldHVyblwiYnV0dG9uXCJpbiBlfHwhdC5fayhlLmtleUNvZGUsXCJlbnRlclwiLDEzKT8oZS5wcmV2ZW50RGVmYXVsdCgpLHZvaWQgdC50eXBlQWhlYWRTZWxlY3QoZSkpOm51bGx9XSxrZXl1cDpmdW5jdGlvbihlKXtyZXR1cm5cImJ1dHRvblwiaW4gZXx8IXQuX2soZS5rZXlDb2RlLFwiZXNjXCIsMjcpP3ZvaWQgdC5vbkVzY2FwZShlKTpudWxsfSxibHVyOnQub25TZWFyY2hCbHVyLGZvY3VzOnQub25TZWFyY2hGb2N1cyxpbnB1dDpmdW5jdGlvbihlKXtlLnRhcmdldC5jb21wb3Npbmd8fCh0LnNlYXJjaD1lLnRhcmdldC52YWx1ZSl9fX0pLHQuX3YoXCIgXCIpLHQubm9Ecm9wP3QuX2UoKTpuKFwiaVwiLHtyZWY6XCJvcGVuSW5kaWNhdG9yXCIsY2xhc3M6W3tkaXNhYmxlZDp0LmRpc2FibGVkfSxcIm9wZW4taW5kaWNhdG9yXCJdLGF0dHJzOntyb2xlOlwicHJlc2VudGF0aW9uXCJ9fSksdC5fdihcIiBcIiksdC5fdChcInNwaW5uZXJcIixbbihcImRpdlwiLHtkaXJlY3RpdmVzOlt7bmFtZTpcInNob3dcIixyYXdOYW1lOlwidi1zaG93XCIsdmFsdWU6dC5tdXRhYmxlTG9hZGluZyxleHByZXNzaW9uOlwibXV0YWJsZUxvYWRpbmdcIn1dLHN0YXRpY0NsYXNzOlwic3Bpbm5lclwifSxbdC5fdihcIkxvYWRpbmcuLi5cIildKV0pXSwyKSx0Ll92KFwiIFwiKSxuKFwidHJhbnNpdGlvblwiLHthdHRyczp7bmFtZTp0LnRyYW5zaXRpb259fSxbdC5kcm9wZG93bk9wZW4/bihcInVsXCIse3JlZjpcImRyb3Bkb3duTWVudVwiLHN0YXRpY0NsYXNzOlwiZHJvcGRvd24tbWVudVwiLHN0eWxlOntcIm1heC1oZWlnaHRcIjp0Lm1heEhlaWdodH19LFt0Ll9sKHQuZmlsdGVyZWRPcHRpb25zLGZ1bmN0aW9uKGUscil7cmV0dXJuIG4oXCJsaVwiLHtrZXk6cixjbGFzczp7YWN0aXZlOnQuaXNPcHRpb25TZWxlY3RlZChlKSxoaWdobGlnaHQ6cj09PXQudHlwZUFoZWFkUG9pbnRlcn0sb246e21vdXNlb3ZlcjpmdW5jdGlvbihlKXt0LnR5cGVBaGVhZFBvaW50ZXI9cn19fSxbbihcImFcIix7b246e21vdXNlZG93bjpmdW5jdGlvbihuKXtuLnByZXZlbnREZWZhdWx0KCksdC5zZWxlY3QoZSl9fX0sW3QuX3QoXCJvcHRpb25cIixbdC5fdihcIlxcbiAgICAgICAgICBcIit0Ll9zKHQuZ2V0T3B0aW9uTGFiZWwoZSkpK1wiXFxuICAgICAgICBcIildLG51bGwsZSldLDIpXSl9KSx0Ll92KFwiIFwiKSx0LmZpbHRlcmVkT3B0aW9ucy5sZW5ndGg/dC5fZSgpOm4oXCJsaVwiLHtcclxuc3RhdGljQ2xhc3M6XCJuby1vcHRpb25zXCJ9LFt0Ll90KFwibm8tb3B0aW9uc1wiLFt0Ll92KFwiU29ycnksIG5vIG1hdGNoaW5nIG9wdGlvbnMuXCIpXSldLDIpXSwyKTp0Ll9lKCldKV0sMSl9LHN0YXRpY1JlbmRlckZuczpbXX19LGZ1bmN0aW9uKHQsZSxuKXtmdW5jdGlvbiByKHQsZSl7Zm9yKHZhciBuPTA7bjx0Lmxlbmd0aDtuKyspe3ZhciByPXRbbl0sbz1mW3IuaWRdO2lmKG8pe28ucmVmcysrO2Zvcih2YXIgaT0wO2k8by5wYXJ0cy5sZW5ndGg7aSsrKW8ucGFydHNbaV0oci5wYXJ0c1tpXSk7Zm9yKDtpPHIucGFydHMubGVuZ3RoO2krKylvLnBhcnRzLnB1c2godShyLnBhcnRzW2ldLGUpKX1lbHNle2Zvcih2YXIgYT1bXSxpPTA7aTxyLnBhcnRzLmxlbmd0aDtpKyspYS5wdXNoKHUoci5wYXJ0c1tpXSxlKSk7ZltyLmlkXT17aWQ6ci5pZCxyZWZzOjEscGFydHM6YX19fX1mdW5jdGlvbiBvKHQpe2Zvcih2YXIgZT1bXSxuPXt9LHI9MDtyPHQubGVuZ3RoO3IrKyl7dmFyIG89dFtyXSxpPW9bMF0sYT1vWzFdLHM9b1syXSx1PW9bM10sbD17Y3NzOmEsbWVkaWE6cyxzb3VyY2VNYXA6dX07bltpXT9uW2ldLnBhcnRzLnB1c2gobCk6ZS5wdXNoKG5baV09e2lkOmkscGFydHM6W2xdfSl9cmV0dXJuIGV9ZnVuY3Rpb24gaSh0LGUpe3ZhciBuPWgoKSxyPWdbZy5sZW5ndGgtMV07aWYoXCJ0b3BcIj09PXQuaW5zZXJ0QXQpcj9yLm5leHRTaWJsaW5nP24uaW5zZXJ0QmVmb3JlKGUsci5uZXh0U2libGluZyk6bi5hcHBlbmRDaGlsZChlKTpuLmluc2VydEJlZm9yZShlLG4uZmlyc3RDaGlsZCksZy5wdXNoKGUpO2Vsc2V7aWYoXCJib3R0b21cIiE9PXQuaW5zZXJ0QXQpdGhyb3cgbmV3IEVycm9yKFwiSW52YWxpZCB2YWx1ZSBmb3IgcGFyYW1ldGVyICdpbnNlcnRBdCcuIE11c3QgYmUgJ3RvcCcgb3IgJ2JvdHRvbScuXCIpO24uYXBwZW5kQ2hpbGQoZSl9fWZ1bmN0aW9uIGEodCl7dC5wYXJlbnROb2RlLnJlbW92ZUNoaWxkKHQpO3ZhciBlPWcuaW5kZXhPZih0KTtlPj0wJiZnLnNwbGljZShlLDEpfWZ1bmN0aW9uIHModCl7dmFyIGU9ZG9jdW1lbnQuY3JlYXRlRWxlbWVudChcInN0eWxlXCIpO3JldHVybiBlLnR5cGU9XCJ0ZXh0L2Nzc1wiLGkodCxlKSxlfWZ1bmN0aW9uIHUodCxlKXt2YXIgbixyLG87aWYoZS5zaW5nbGV0b24pe3ZhciBpPXYrKztuPWJ8fChiPXMoZSkpLHI9bC5iaW5kKG51bGwsbixpLCExKSxvPWwuYmluZChudWxsLG4saSwhMCl9ZWxzZSBuPXMoZSkscj1jLmJpbmQobnVsbCxuKSxvPWZ1bmN0aW9uKCl7YShuKX07cmV0dXJuIHIodCksZnVuY3Rpb24oZSl7aWYoZSl7aWYoZS5jc3M9PT10LmNzcyYmZS5tZWRpYT09PXQubWVkaWEmJmUuc291cmNlTWFwPT09dC5zb3VyY2VNYXApcmV0dXJuO3IodD1lKX1lbHNlIG8oKX19ZnVuY3Rpb24gbCh0LGUsbixyKXt2YXIgbz1uP1wiXCI6ci5jc3M7aWYodC5zdHlsZVNoZWV0KXQuc3R5bGVTaGVldC5jc3NUZXh0PXkoZSxvKTtlbHNle3ZhciBpPWRvY3VtZW50LmNyZWF0ZVRleHROb2RlKG8pLGE9dC5jaGlsZE5vZGVzO2FbZV0mJnQucmVtb3ZlQ2hpbGQoYVtlXSksYS5sZW5ndGg/dC5pbnNlcnRCZWZvcmUoaSxhW2VdKTp0LmFwcGVuZENoaWxkKGkpfX1mdW5jdGlvbiBjKHQsZSl7dmFyIG49ZS5jc3Mscj1lLm1lZGlhLG89ZS5zb3VyY2VNYXA7aWYociYmdC5zZXRBdHRyaWJ1dGUoXCJtZWRpYVwiLHIpLG8mJihuKz1cIlxcbi8qIyBzb3VyY2VVUkw9XCIrby5zb3VyY2VzWzBdK1wiICovXCIsbis9XCJcXG4vKiMgc291cmNlTWFwcGluZ1VSTD1kYXRhOmFwcGxpY2F0aW9uL2pzb247YmFzZTY0LFwiK2J0b2EodW5lc2NhcGUoZW5jb2RlVVJJQ29tcG9uZW50KEpTT04uc3RyaW5naWZ5KG8pKSkpK1wiICovXCIpLHQuc3R5bGVTaGVldCl0LnN0eWxlU2hlZXQuY3NzVGV4dD1uO2Vsc2V7Zm9yKDt0LmZpcnN0Q2hpbGQ7KXQucmVtb3ZlQ2hpbGQodC5maXJzdENoaWxkKTt0LmFwcGVuZENoaWxkKGRvY3VtZW50LmNyZWF0ZVRleHROb2RlKG4pKX19dmFyIGY9e30scD1mdW5jdGlvbih0KXt2YXIgZTtyZXR1cm4gZnVuY3Rpb24oKXtyZXR1cm5cInVuZGVmaW5lZFwiPT10eXBlb2YgZSYmKGU9dC5hcHBseSh0aGlzLGFyZ3VtZW50cykpLGV9fSxkPXAoZnVuY3Rpb24oKXtyZXR1cm4vbXNpZSBbNi05XVxcYi8udGVzdCh3aW5kb3cubmF2aWdhdG9yLnVzZXJBZ2VudC50b0xvd2VyQ2FzZSgpKX0pLGg9cChmdW5jdGlvbigpe3JldHVybiBkb2N1bWVudC5oZWFkfHxkb2N1bWVudC5nZXRFbGVtZW50c0J5VGFnTmFtZShcImhlYWRcIilbMF19KSxiPW51bGwsdj0wLGc9W107dC5leHBvcnRzPWZ1bmN0aW9uKHQsZSl7ZT1lfHx7fSxcInVuZGVmaW5lZFwiPT10eXBlb2YgZS5zaW5nbGV0b24mJihlLnNpbmdsZXRvbj1kKCkpLFwidW5kZWZpbmVkXCI9PXR5cGVvZiBlLmluc2VydEF0JiYoZS5pbnNlcnRBdD1cImJvdHRvbVwiKTt2YXIgbj1vKHQpO3JldHVybiByKG4sZSksZnVuY3Rpb24odCl7Zm9yKHZhciBpPVtdLGE9MDthPG4ubGVuZ3RoO2ErKyl7dmFyIHM9blthXSx1PWZbcy5pZF07dS5yZWZzLS0saS5wdXNoKHUpfWlmKHQpe3ZhciBsPW8odCk7cihsLGUpfWZvcih2YXIgYT0wO2E8aS5sZW5ndGg7YSsrKXt2YXIgdT1pW2FdO2lmKDA9PT11LnJlZnMpe2Zvcih2YXIgYz0wO2M8dS5wYXJ0cy5sZW5ndGg7YysrKXUucGFydHNbY10oKTtkZWxldGUgZlt1LmlkXX19fX07dmFyIHk9ZnVuY3Rpb24oKXt2YXIgdD1bXTtyZXR1cm4gZnVuY3Rpb24oZSxuKXtyZXR1cm4gdFtlXT1uLHQuZmlsdGVyKEJvb2xlYW4pLmpvaW4oXCJcXG5cIil9fSgpfSxmdW5jdGlvbih0LGUsbil7dmFyIHI9big4MSk7XCJzdHJpbmdcIj09dHlwZW9mIHImJihyPVtbdC5pZCxyLFwiXCJdXSk7big4Nikocix7fSk7ci5sb2NhbHMmJih0LmV4cG9ydHM9ci5sb2NhbHMpfV0pfSk7XHJcbi8vIyBzb3VyY2VNYXBwaW5nVVJMPXZ1ZS1zZWxlY3QuanMubWFwXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvdnVlLXNlbGVjdC9kaXN0L3Z1ZS1zZWxlY3QuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL3Z1ZS1zZWxlY3QvZGlzdC92dWUtc2VsZWN0LmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMSIsIi8vIHN0eWxlLWxvYWRlcjogQWRkcyBzb21lIGNzcyB0byB0aGUgRE9NIGJ5IGFkZGluZyBhIDxzdHlsZT4gdGFnXG5cbi8vIGxvYWQgdGhlIHN0eWxlc1xudmFyIGNvbnRlbnQgPSByZXF1aXJlKFwiISEuLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz9zb3VyY2VNYXAhLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3N0eWxlLWNvbXBpbGVyL2luZGV4LmpzP3tcXFwidnVlXFxcIjp0cnVlLFxcXCJpZFxcXCI6XFxcImRhdGEtdi00MDFlZTk0Y1xcXCIsXFxcInNjb3BlZFxcXCI6ZmFsc2UsXFxcImhhc0lubGluZUNvbmZpZ1xcXCI6dHJ1ZX0hLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yLmpzP3R5cGU9c3R5bGVzJmluZGV4PTAhLi9NZXJjaGFuZGlzZVBhZ2UudnVlXCIpO1xuaWYodHlwZW9mIGNvbnRlbnQgPT09ICdzdHJpbmcnKSBjb250ZW50ID0gW1ttb2R1bGUuaWQsIGNvbnRlbnQsICcnXV07XG5pZihjb250ZW50LmxvY2FscykgbW9kdWxlLmV4cG9ydHMgPSBjb250ZW50LmxvY2Fscztcbi8vIGFkZCB0aGUgc3R5bGVzIHRvIHRoZSBET01cbnZhciB1cGRhdGUgPSByZXF1aXJlKFwiIS4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtc3R5bGUtbG9hZGVyL2xpYi9hZGRTdHlsZXNDbGllbnQuanNcIikoXCI3M2FkMWM4NlwiLCBjb250ZW50LCBmYWxzZSk7XG4vLyBIb3QgTW9kdWxlIFJlcGxhY2VtZW50XG5pZihtb2R1bGUuaG90KSB7XG4gLy8gV2hlbiB0aGUgc3R5bGVzIGNoYW5nZSwgdXBkYXRlIHRoZSA8c3R5bGU+IHRhZ3NcbiBpZighY29udGVudC5sb2NhbHMpIHtcbiAgIG1vZHVsZS5ob3QuYWNjZXB0KFwiISEuLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz9zb3VyY2VNYXAhLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3N0eWxlLWNvbXBpbGVyL2luZGV4LmpzP3tcXFwidnVlXFxcIjp0cnVlLFxcXCJpZFxcXCI6XFxcImRhdGEtdi00MDFlZTk0Y1xcXCIsXFxcInNjb3BlZFxcXCI6ZmFsc2UsXFxcImhhc0lubGluZUNvbmZpZ1xcXCI6dHJ1ZX0hLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yLmpzP3R5cGU9c3R5bGVzJmluZGV4PTAhLi9NZXJjaGFuZGlzZVBhZ2UudnVlXCIsIGZ1bmN0aW9uKCkge1xuICAgICB2YXIgbmV3Q29udGVudCA9IHJlcXVpcmUoXCIhIS4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzP3NvdXJjZU1hcCEuLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc3R5bGUtY29tcGlsZXIvaW5kZXguanM/e1xcXCJ2dWVcXFwiOnRydWUsXFxcImlkXFxcIjpcXFwiZGF0YS12LTQwMWVlOTRjXFxcIixcXFwic2NvcGVkXFxcIjpmYWxzZSxcXFwiaGFzSW5saW5lQ29uZmlnXFxcIjp0cnVlfSEuLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT1zdHlsZXMmaW5kZXg9MCEuL01lcmNoYW5kaXNlUGFnZS52dWVcIik7XG4gICAgIGlmKHR5cGVvZiBuZXdDb250ZW50ID09PSAnc3RyaW5nJykgbmV3Q29udGVudCA9IFtbbW9kdWxlLmlkLCBuZXdDb250ZW50LCAnJ11dO1xuICAgICB1cGRhdGUobmV3Q29udGVudCk7XG4gICB9KTtcbiB9XG4gLy8gV2hlbiB0aGUgbW9kdWxlIGlzIGRpc3Bvc2VkLCByZW1vdmUgdGhlIDxzdHlsZT4gdGFnc1xuIG1vZHVsZS5ob3QuZGlzcG9zZShmdW5jdGlvbigpIHsgdXBkYXRlKCk7IH0pO1xufVxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL3Z1ZS1zdHlsZS1sb2FkZXIhLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlcj9zb3VyY2VNYXAhLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc3R5bGUtY29tcGlsZXI/e1widnVlXCI6dHJ1ZSxcImlkXCI6XCJkYXRhLXYtNDAxZWU5NGNcIixcInNjb3BlZFwiOmZhbHNlLFwiaGFzSW5saW5lQ29uZmlnXCI6dHJ1ZX0hLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT1zdHlsZXMmaW5kZXg9MCEuL3Jlc291cmNlcy9hc3NldHMvanMvY29tcG9uZW50cy9jbGllbnQvTWVyY2hhbmRpc2VQYWdlLnZ1ZVxuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvdnVlLXN0eWxlLWxvYWRlci9pbmRleC5qcyEuL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzP3NvdXJjZU1hcCEuL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zdHlsZS1jb21waWxlci9pbmRleC5qcz97XCJ2dWVcIjp0cnVlLFwiaWRcIjpcImRhdGEtdi00MDFlZTk0Y1wiLFwic2NvcGVkXCI6ZmFsc2UsXCJoYXNJbmxpbmVDb25maWdcIjp0cnVlfSEuL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvci5qcz90eXBlPXN0eWxlcyZpbmRleD0wIS4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jb21wb25lbnRzL2NsaWVudC9NZXJjaGFuZGlzZVBhZ2UudnVlXG4vLyBtb2R1bGUgY2h1bmtzID0gMSIsIi8qXG4gIE1JVCBMaWNlbnNlIGh0dHA6Ly93d3cub3BlbnNvdXJjZS5vcmcvbGljZW5zZXMvbWl0LWxpY2Vuc2UucGhwXG4gIEF1dGhvciBUb2JpYXMgS29wcGVycyBAc29rcmFcbiAgTW9kaWZpZWQgYnkgRXZhbiBZb3UgQHl5eDk5MDgwM1xuKi9cblxudmFyIGhhc0RvY3VtZW50ID0gdHlwZW9mIGRvY3VtZW50ICE9PSAndW5kZWZpbmVkJ1xuXG5pZiAodHlwZW9mIERFQlVHICE9PSAndW5kZWZpbmVkJyAmJiBERUJVRykge1xuICBpZiAoIWhhc0RvY3VtZW50KSB7XG4gICAgdGhyb3cgbmV3IEVycm9yKFxuICAgICd2dWUtc3R5bGUtbG9hZGVyIGNhbm5vdCBiZSB1c2VkIGluIGEgbm9uLWJyb3dzZXIgZW52aXJvbm1lbnQuICcgK1xuICAgIFwiVXNlIHsgdGFyZ2V0OiAnbm9kZScgfSBpbiB5b3VyIFdlYnBhY2sgY29uZmlnIHRvIGluZGljYXRlIGEgc2VydmVyLXJlbmRlcmluZyBlbnZpcm9ubWVudC5cIlxuICApIH1cbn1cblxudmFyIGxpc3RUb1N0eWxlcyA9IHJlcXVpcmUoJy4vbGlzdFRvU3R5bGVzJylcblxuLypcbnR5cGUgU3R5bGVPYmplY3QgPSB7XG4gIGlkOiBudW1iZXI7XG4gIHBhcnRzOiBBcnJheTxTdHlsZU9iamVjdFBhcnQ+XG59XG5cbnR5cGUgU3R5bGVPYmplY3RQYXJ0ID0ge1xuICBjc3M6IHN0cmluZztcbiAgbWVkaWE6IHN0cmluZztcbiAgc291cmNlTWFwOiA/c3RyaW5nXG59XG4qL1xuXG52YXIgc3R5bGVzSW5Eb20gPSB7LypcbiAgW2lkOiBudW1iZXJdOiB7XG4gICAgaWQ6IG51bWJlcixcbiAgICByZWZzOiBudW1iZXIsXG4gICAgcGFydHM6IEFycmF5PChvYmo/OiBTdHlsZU9iamVjdFBhcnQpID0+IHZvaWQ+XG4gIH1cbiovfVxuXG52YXIgaGVhZCA9IGhhc0RvY3VtZW50ICYmIChkb2N1bWVudC5oZWFkIHx8IGRvY3VtZW50LmdldEVsZW1lbnRzQnlUYWdOYW1lKCdoZWFkJylbMF0pXG52YXIgc2luZ2xldG9uRWxlbWVudCA9IG51bGxcbnZhciBzaW5nbGV0b25Db3VudGVyID0gMFxudmFyIGlzUHJvZHVjdGlvbiA9IGZhbHNlXG52YXIgbm9vcCA9IGZ1bmN0aW9uICgpIHt9XG5cbi8vIEZvcmNlIHNpbmdsZS10YWcgc29sdXRpb24gb24gSUU2LTksIHdoaWNoIGhhcyBhIGhhcmQgbGltaXQgb24gdGhlICMgb2YgPHN0eWxlPlxuLy8gdGFncyBpdCB3aWxsIGFsbG93IG9uIGEgcGFnZVxudmFyIGlzT2xkSUUgPSB0eXBlb2YgbmF2aWdhdG9yICE9PSAndW5kZWZpbmVkJyAmJiAvbXNpZSBbNi05XVxcYi8udGVzdChuYXZpZ2F0b3IudXNlckFnZW50LnRvTG93ZXJDYXNlKCkpXG5cbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gKHBhcmVudElkLCBsaXN0LCBfaXNQcm9kdWN0aW9uKSB7XG4gIGlzUHJvZHVjdGlvbiA9IF9pc1Byb2R1Y3Rpb25cblxuICB2YXIgc3R5bGVzID0gbGlzdFRvU3R5bGVzKHBhcmVudElkLCBsaXN0KVxuICBhZGRTdHlsZXNUb0RvbShzdHlsZXMpXG5cbiAgcmV0dXJuIGZ1bmN0aW9uIHVwZGF0ZSAobmV3TGlzdCkge1xuICAgIHZhciBtYXlSZW1vdmUgPSBbXVxuICAgIGZvciAodmFyIGkgPSAwOyBpIDwgc3R5bGVzLmxlbmd0aDsgaSsrKSB7XG4gICAgICB2YXIgaXRlbSA9IHN0eWxlc1tpXVxuICAgICAgdmFyIGRvbVN0eWxlID0gc3R5bGVzSW5Eb21baXRlbS5pZF1cbiAgICAgIGRvbVN0eWxlLnJlZnMtLVxuICAgICAgbWF5UmVtb3ZlLnB1c2goZG9tU3R5bGUpXG4gICAgfVxuICAgIGlmIChuZXdMaXN0KSB7XG4gICAgICBzdHlsZXMgPSBsaXN0VG9TdHlsZXMocGFyZW50SWQsIG5ld0xpc3QpXG4gICAgICBhZGRTdHlsZXNUb0RvbShzdHlsZXMpXG4gICAgfSBlbHNlIHtcbiAgICAgIHN0eWxlcyA9IFtdXG4gICAgfVxuICAgIGZvciAodmFyIGkgPSAwOyBpIDwgbWF5UmVtb3ZlLmxlbmd0aDsgaSsrKSB7XG4gICAgICB2YXIgZG9tU3R5bGUgPSBtYXlSZW1vdmVbaV1cbiAgICAgIGlmIChkb21TdHlsZS5yZWZzID09PSAwKSB7XG4gICAgICAgIGZvciAodmFyIGogPSAwOyBqIDwgZG9tU3R5bGUucGFydHMubGVuZ3RoOyBqKyspIHtcbiAgICAgICAgICBkb21TdHlsZS5wYXJ0c1tqXSgpXG4gICAgICAgIH1cbiAgICAgICAgZGVsZXRlIHN0eWxlc0luRG9tW2RvbVN0eWxlLmlkXVxuICAgICAgfVxuICAgIH1cbiAgfVxufVxuXG5mdW5jdGlvbiBhZGRTdHlsZXNUb0RvbSAoc3R5bGVzIC8qIEFycmF5PFN0eWxlT2JqZWN0PiAqLykge1xuICBmb3IgKHZhciBpID0gMDsgaSA8IHN0eWxlcy5sZW5ndGg7IGkrKykge1xuICAgIHZhciBpdGVtID0gc3R5bGVzW2ldXG4gICAgdmFyIGRvbVN0eWxlID0gc3R5bGVzSW5Eb21baXRlbS5pZF1cbiAgICBpZiAoZG9tU3R5bGUpIHtcbiAgICAgIGRvbVN0eWxlLnJlZnMrK1xuICAgICAgZm9yICh2YXIgaiA9IDA7IGogPCBkb21TdHlsZS5wYXJ0cy5sZW5ndGg7IGorKykge1xuICAgICAgICBkb21TdHlsZS5wYXJ0c1tqXShpdGVtLnBhcnRzW2pdKVxuICAgICAgfVxuICAgICAgZm9yICg7IGogPCBpdGVtLnBhcnRzLmxlbmd0aDsgaisrKSB7XG4gICAgICAgIGRvbVN0eWxlLnBhcnRzLnB1c2goYWRkU3R5bGUoaXRlbS5wYXJ0c1tqXSkpXG4gICAgICB9XG4gICAgICBpZiAoZG9tU3R5bGUucGFydHMubGVuZ3RoID4gaXRlbS5wYXJ0cy5sZW5ndGgpIHtcbiAgICAgICAgZG9tU3R5bGUucGFydHMubGVuZ3RoID0gaXRlbS5wYXJ0cy5sZW5ndGhcbiAgICAgIH1cbiAgICB9IGVsc2Uge1xuICAgICAgdmFyIHBhcnRzID0gW11cbiAgICAgIGZvciAodmFyIGogPSAwOyBqIDwgaXRlbS5wYXJ0cy5sZW5ndGg7IGorKykge1xuICAgICAgICBwYXJ0cy5wdXNoKGFkZFN0eWxlKGl0ZW0ucGFydHNbal0pKVxuICAgICAgfVxuICAgICAgc3R5bGVzSW5Eb21baXRlbS5pZF0gPSB7IGlkOiBpdGVtLmlkLCByZWZzOiAxLCBwYXJ0czogcGFydHMgfVxuICAgIH1cbiAgfVxufVxuXG5mdW5jdGlvbiBjcmVhdGVTdHlsZUVsZW1lbnQgKCkge1xuICB2YXIgc3R5bGVFbGVtZW50ID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnc3R5bGUnKVxuICBzdHlsZUVsZW1lbnQudHlwZSA9ICd0ZXh0L2NzcydcbiAgaGVhZC5hcHBlbmRDaGlsZChzdHlsZUVsZW1lbnQpXG4gIHJldHVybiBzdHlsZUVsZW1lbnRcbn1cblxuZnVuY3Rpb24gYWRkU3R5bGUgKG9iaiAvKiBTdHlsZU9iamVjdFBhcnQgKi8pIHtcbiAgdmFyIHVwZGF0ZSwgcmVtb3ZlXG4gIHZhciBzdHlsZUVsZW1lbnQgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCdzdHlsZVtkYXRhLXZ1ZS1zc3ItaWR+PVwiJyArIG9iai5pZCArICdcIl0nKVxuXG4gIGlmIChzdHlsZUVsZW1lbnQpIHtcbiAgICBpZiAoaXNQcm9kdWN0aW9uKSB7XG4gICAgICAvLyBoYXMgU1NSIHN0eWxlcyBhbmQgaW4gcHJvZHVjdGlvbiBtb2RlLlxuICAgICAgLy8gc2ltcGx5IGRvIG5vdGhpbmcuXG4gICAgICByZXR1cm4gbm9vcFxuICAgIH0gZWxzZSB7XG4gICAgICAvLyBoYXMgU1NSIHN0eWxlcyBidXQgaW4gZGV2IG1vZGUuXG4gICAgICAvLyBmb3Igc29tZSByZWFzb24gQ2hyb21lIGNhbid0IGhhbmRsZSBzb3VyY2UgbWFwIGluIHNlcnZlci1yZW5kZXJlZFxuICAgICAgLy8gc3R5bGUgdGFncyAtIHNvdXJjZSBtYXBzIGluIDxzdHlsZT4gb25seSB3b3JrcyBpZiB0aGUgc3R5bGUgdGFnIGlzXG4gICAgICAvLyBjcmVhdGVkIGFuZCBpbnNlcnRlZCBkeW5hbWljYWxseS4gU28gd2UgcmVtb3ZlIHRoZSBzZXJ2ZXIgcmVuZGVyZWRcbiAgICAgIC8vIHN0eWxlcyBhbmQgaW5qZWN0IG5ldyBvbmVzLlxuICAgICAgc3R5bGVFbGVtZW50LnBhcmVudE5vZGUucmVtb3ZlQ2hpbGQoc3R5bGVFbGVtZW50KVxuICAgIH1cbiAgfVxuXG4gIGlmIChpc09sZElFKSB7XG4gICAgLy8gdXNlIHNpbmdsZXRvbiBtb2RlIGZvciBJRTkuXG4gICAgdmFyIHN0eWxlSW5kZXggPSBzaW5nbGV0b25Db3VudGVyKytcbiAgICBzdHlsZUVsZW1lbnQgPSBzaW5nbGV0b25FbGVtZW50IHx8IChzaW5nbGV0b25FbGVtZW50ID0gY3JlYXRlU3R5bGVFbGVtZW50KCkpXG4gICAgdXBkYXRlID0gYXBwbHlUb1NpbmdsZXRvblRhZy5iaW5kKG51bGwsIHN0eWxlRWxlbWVudCwgc3R5bGVJbmRleCwgZmFsc2UpXG4gICAgcmVtb3ZlID0gYXBwbHlUb1NpbmdsZXRvblRhZy5iaW5kKG51bGwsIHN0eWxlRWxlbWVudCwgc3R5bGVJbmRleCwgdHJ1ZSlcbiAgfSBlbHNlIHtcbiAgICAvLyB1c2UgbXVsdGktc3R5bGUtdGFnIG1vZGUgaW4gYWxsIG90aGVyIGNhc2VzXG4gICAgc3R5bGVFbGVtZW50ID0gY3JlYXRlU3R5bGVFbGVtZW50KClcbiAgICB1cGRhdGUgPSBhcHBseVRvVGFnLmJpbmQobnVsbCwgc3R5bGVFbGVtZW50KVxuICAgIHJlbW92ZSA9IGZ1bmN0aW9uICgpIHtcbiAgICAgIHN0eWxlRWxlbWVudC5wYXJlbnROb2RlLnJlbW92ZUNoaWxkKHN0eWxlRWxlbWVudClcbiAgICB9XG4gIH1cblxuICB1cGRhdGUob2JqKVxuXG4gIHJldHVybiBmdW5jdGlvbiB1cGRhdGVTdHlsZSAobmV3T2JqIC8qIFN0eWxlT2JqZWN0UGFydCAqLykge1xuICAgIGlmIChuZXdPYmopIHtcbiAgICAgIGlmIChuZXdPYmouY3NzID09PSBvYmouY3NzICYmXG4gICAgICAgICAgbmV3T2JqLm1lZGlhID09PSBvYmoubWVkaWEgJiZcbiAgICAgICAgICBuZXdPYmouc291cmNlTWFwID09PSBvYmouc291cmNlTWFwKSB7XG4gICAgICAgIHJldHVyblxuICAgICAgfVxuICAgICAgdXBkYXRlKG9iaiA9IG5ld09iailcbiAgICB9IGVsc2Uge1xuICAgICAgcmVtb3ZlKClcbiAgICB9XG4gIH1cbn1cblxudmFyIHJlcGxhY2VUZXh0ID0gKGZ1bmN0aW9uICgpIHtcbiAgdmFyIHRleHRTdG9yZSA9IFtdXG5cbiAgcmV0dXJuIGZ1bmN0aW9uIChpbmRleCwgcmVwbGFjZW1lbnQpIHtcbiAgICB0ZXh0U3RvcmVbaW5kZXhdID0gcmVwbGFjZW1lbnRcbiAgICByZXR1cm4gdGV4dFN0b3JlLmZpbHRlcihCb29sZWFuKS5qb2luKCdcXG4nKVxuICB9XG59KSgpXG5cbmZ1bmN0aW9uIGFwcGx5VG9TaW5nbGV0b25UYWcgKHN0eWxlRWxlbWVudCwgaW5kZXgsIHJlbW92ZSwgb2JqKSB7XG4gIHZhciBjc3MgPSByZW1vdmUgPyAnJyA6IG9iai5jc3NcblxuICBpZiAoc3R5bGVFbGVtZW50LnN0eWxlU2hlZXQpIHtcbiAgICBzdHlsZUVsZW1lbnQuc3R5bGVTaGVldC5jc3NUZXh0ID0gcmVwbGFjZVRleHQoaW5kZXgsIGNzcylcbiAgfSBlbHNlIHtcbiAgICB2YXIgY3NzTm9kZSA9IGRvY3VtZW50LmNyZWF0ZVRleHROb2RlKGNzcylcbiAgICB2YXIgY2hpbGROb2RlcyA9IHN0eWxlRWxlbWVudC5jaGlsZE5vZGVzXG4gICAgaWYgKGNoaWxkTm9kZXNbaW5kZXhdKSBzdHlsZUVsZW1lbnQucmVtb3ZlQ2hpbGQoY2hpbGROb2Rlc1tpbmRleF0pXG4gICAgaWYgKGNoaWxkTm9kZXMubGVuZ3RoKSB7XG4gICAgICBzdHlsZUVsZW1lbnQuaW5zZXJ0QmVmb3JlKGNzc05vZGUsIGNoaWxkTm9kZXNbaW5kZXhdKVxuICAgIH0gZWxzZSB7XG4gICAgICBzdHlsZUVsZW1lbnQuYXBwZW5kQ2hpbGQoY3NzTm9kZSlcbiAgICB9XG4gIH1cbn1cblxuZnVuY3Rpb24gYXBwbHlUb1RhZyAoc3R5bGVFbGVtZW50LCBvYmopIHtcbiAgdmFyIGNzcyA9IG9iai5jc3NcbiAgdmFyIG1lZGlhID0gb2JqLm1lZGlhXG4gIHZhciBzb3VyY2VNYXAgPSBvYmouc291cmNlTWFwXG5cbiAgaWYgKG1lZGlhKSB7XG4gICAgc3R5bGVFbGVtZW50LnNldEF0dHJpYnV0ZSgnbWVkaWEnLCBtZWRpYSlcbiAgfVxuXG4gIGlmIChzb3VyY2VNYXApIHtcbiAgICAvLyBodHRwczovL2RldmVsb3Blci5jaHJvbWUuY29tL2RldnRvb2xzL2RvY3MvamF2YXNjcmlwdC1kZWJ1Z2dpbmdcbiAgICAvLyB0aGlzIG1ha2VzIHNvdXJjZSBtYXBzIGluc2lkZSBzdHlsZSB0YWdzIHdvcmsgcHJvcGVybHkgaW4gQ2hyb21lXG4gICAgY3NzICs9ICdcXG4vKiMgc291cmNlVVJMPScgKyBzb3VyY2VNYXAuc291cmNlc1swXSArICcgKi8nXG4gICAgLy8gaHR0cDovL3N0YWNrb3ZlcmZsb3cuY29tL2EvMjY2MDM4NzVcbiAgICBjc3MgKz0gJ1xcbi8qIyBzb3VyY2VNYXBwaW5nVVJMPWRhdGE6YXBwbGljYXRpb24vanNvbjtiYXNlNjQsJyArIGJ0b2EodW5lc2NhcGUoZW5jb2RlVVJJQ29tcG9uZW50KEpTT04uc3RyaW5naWZ5KHNvdXJjZU1hcCkpKSkgKyAnICovJ1xuICB9XG5cbiAgaWYgKHN0eWxlRWxlbWVudC5zdHlsZVNoZWV0KSB7XG4gICAgc3R5bGVFbGVtZW50LnN0eWxlU2hlZXQuY3NzVGV4dCA9IGNzc1xuICB9IGVsc2Uge1xuICAgIHdoaWxlIChzdHlsZUVsZW1lbnQuZmlyc3RDaGlsZCkge1xuICAgICAgc3R5bGVFbGVtZW50LnJlbW92ZUNoaWxkKHN0eWxlRWxlbWVudC5maXJzdENoaWxkKVxuICAgIH1cbiAgICBzdHlsZUVsZW1lbnQuYXBwZW5kQ2hpbGQoZG9jdW1lbnQuY3JlYXRlVGV4dE5vZGUoY3NzKSlcbiAgfVxufVxuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvdnVlLXN0eWxlLWxvYWRlci9saWIvYWRkU3R5bGVzQ2xpZW50LmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy92dWUtc3R5bGUtbG9hZGVyL2xpYi9hZGRTdHlsZXNDbGllbnQuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEiLCIvKipcbiAqIFRyYW5zbGF0ZXMgdGhlIGxpc3QgZm9ybWF0IHByb2R1Y2VkIGJ5IGNzcy1sb2FkZXIgaW50byBzb21ldGhpbmdcbiAqIGVhc2llciB0byBtYW5pcHVsYXRlLlxuICovXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIGxpc3RUb1N0eWxlcyAocGFyZW50SWQsIGxpc3QpIHtcbiAgdmFyIHN0eWxlcyA9IFtdXG4gIHZhciBuZXdTdHlsZXMgPSB7fVxuICBmb3IgKHZhciBpID0gMDsgaSA8IGxpc3QubGVuZ3RoOyBpKyspIHtcbiAgICB2YXIgaXRlbSA9IGxpc3RbaV1cbiAgICB2YXIgaWQgPSBpdGVtWzBdXG4gICAgdmFyIGNzcyA9IGl0ZW1bMV1cbiAgICB2YXIgbWVkaWEgPSBpdGVtWzJdXG4gICAgdmFyIHNvdXJjZU1hcCA9IGl0ZW1bM11cbiAgICB2YXIgcGFydCA9IHtcbiAgICAgIGlkOiBwYXJlbnRJZCArICc6JyArIGksXG4gICAgICBjc3M6IGNzcyxcbiAgICAgIG1lZGlhOiBtZWRpYSxcbiAgICAgIHNvdXJjZU1hcDogc291cmNlTWFwXG4gICAgfVxuICAgIGlmICghbmV3U3R5bGVzW2lkXSkge1xuICAgICAgc3R5bGVzLnB1c2gobmV3U3R5bGVzW2lkXSA9IHsgaWQ6IGlkLCBwYXJ0czogW3BhcnRdIH0pXG4gICAgfSBlbHNlIHtcbiAgICAgIG5ld1N0eWxlc1tpZF0ucGFydHMucHVzaChwYXJ0KVxuICAgIH1cbiAgfVxuICByZXR1cm4gc3R5bGVzXG59XG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy92dWUtc3R5bGUtbG9hZGVyL2xpYi9saXN0VG9TdHlsZXMuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL3Z1ZS1zdHlsZS1sb2FkZXIvbGliL2xpc3RUb1N0eWxlcy5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSIsImltcG9ydCB7IEZvcm0sIEVycm9ycyB9IGZyb20gJy4vRm9ybS5qcyc7XHJcbmltcG9ydCB7IFZhbGlkYXRvciB9IGZyb20gJy4vVmFsaWRhdG9yLmpzJztcclxuXHJcbmV4cG9ydCBkZWZhdWx0IHtcclxuXHJcblxyXG4gICAgcGFyYW1zOiB7XHJcbiAgICAgICAgdGl0bGU6IFwiXCIsXHJcbiAgICAgICAgdGltZTogXCJcIixcclxuICAgICAgICB2ZW51ZTogXCJcIixcclxuICAgICAgICBkZXNjcmlwdGlvbjogXCJcIixcclxuICAgICAgICBjYXRlZ29yeTogXCJcIlxyXG5cclxuICAgIH0sXHJcbiAgICBldmVudHM6IFtdLFxyXG4gICAgZmluZChpZCkge1xyXG4gICAgICAgIGxldCByZXNwb25zZSA9IHt9O1xyXG4gICAgICAgIHRoaXMuZXZlbnRzLm1hcChmdW5jdGlvbih4KSB7XHJcbiAgICAgICAgICAgIGlmICh4LmlkID09IGlkKSB7XHJcbiAgICAgICAgICAgICAgICBPYmplY3QuYXNzaWduKHJlc3BvbnNlLCB4KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIHJldHVybiByZXNwb25zZTtcclxuXHJcbiAgICB9LFxyXG4gICAgYWxsKCkge1xyXG4gICAgICAgIHJldHVybiBuZXcgUHJvbWlzZSgocmVzLCByZWopID0+IHtcclxuICAgICAgICAgICAgYXhpb3MuZ2V0KCcvY20tdXNlci92aWV3LWV2ZW50cycpLnRoZW4oKHJlc3BvbnNlKSA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmV2ZW50cyA9IHJlc3BvbnNlLmRhdGE7XHJcbiAgICAgICAgICAgICAgICByZXMocmVzcG9uc2UpO1xyXG4gICAgICAgICAgICB9KS5jYXRjaCgoZXJyb3IpID0+IHtcclxuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKCd0aGVyZSB3YXMgYW4gZXJyb3IgZ2V0dGluZyB0aGUgZmlsZScpO1xyXG4gICAgICAgICAgICAgICAgcmVqKGVycm9yKTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSlcclxuICAgIH0sXHJcbiAgICBEZWxldG9yKGV2ZW50SUQpIHtcclxuICAgICAgICBheGlvcy5wb3N0KCcvY20tdXNlci9kZWxldGUvdXNlcnMnLCB7ICdpZCc6IGV2ZW50SUQgfSkudGhlbigoKSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuYWxsKCk7XHJcbiAgICAgICAgfSkuY2F0Y2goKGVycm9yKSA9PiB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKCd0aGVyZSB3YXMgZXJyb3IgZGVsZXRpbmcgdGhlIGV2ZW50Jyk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9LFxyXG4gICAgVXBkYXRvcihkYXRhKSB7XHJcbiAgICAgICAgJHRoaXMudmFsaWRhdGUoKS50aGVuKGZ1bmN0aW9uKHN1Y2Nlc3MpIHtcclxuICAgICAgICAgICAgaWYgKHN1Y2Nlc3MpIHtcclxuICAgICAgICAgICAgICAgIGF4aW9zLnBvc3QoJy9jbS11c2VyL2V2ZW50cy91cGRhdGUnLCBkYXRhKS50aGVuKChyZXMpID0+IHtcclxuXHJcbiAgICAgICAgICAgICAgICB9KS5jYXRjaCgoZXJyKSA9PiB7XHJcblxyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgIH0sXHJcbiAgICBDcmVhdG9yKGRhdGEpIHtcclxuXHJcblxyXG4gICAgfSxcclxuXHJcblxyXG59XG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9Nb2RlbHMvRXZlbnQuanMiLCJsZXQgaW1hZ2UgPSBudWxsO1xyXG5cclxuY2xhc3MgRm9ybSB7XHJcblx0XHJcblx0Y29uc3RydWN0b3IoZGF0YSl7XHJcblx0XHR0aGlzLm9yaWdpbmFsRGF0YSA9IF8uZXh0ZW5kKHt9LCBkYXRhKTtcclxuXHRcdF8ubWFwKCh2YWx1ZSwgZmllbGQpID0+IHRoaXNbZmllbGRdID0gdmFsdWUpO1xyXG5cdFx0dGhpcy5lcnJvcnMgPSBuZXcgRm9ybUVycm9ycygpO1xyXG5cdH1cclxuXHJcblx0ZGF0YSgpIHtcclxuXHRcdGxldCBkYXRhID0gXy5leHRlbmQoe30sIHRoaXMpO1xyXG5cdFx0bGV0IGZvcm1EYXRhID0gbmV3IEZvcm1EYXRhKCdjbGllbnQtZm9ybScpO1xyXG5cdFx0XHRkZWxldGUgZGF0YS5vcmlnaW5hbERhdGE7XHJcblx0XHRcdGRlbGV0ZSBkYXRhLmVycm9ycztcclxuXHRcdF8ubWFwKGRhdGEsICh2YWx1ZSwga2V5KSA9PiBmb3JtRGF0YS5hcHBlbmQoa2V5LCB2YWx1ZSkpO1xyXG5cdFx0cmV0dXJuIGZvcm1EYXRhO1xyXG5cdH1cclxuXHJcblx0cGVyc2lzdChpbWFnZSkge1xyXG5cdFx0dGhpcy5pbWFnZSA9IGltYWdlO1xyXG5cdFx0bGV0IHJlYWxJbWFnZSA9IHRoaXMuaW1hZ2U7XHJcblx0XHRjb25zb2xlLmxvZyh0aGlzLmRhdGEoKSk7XHJcblx0fVxyXG5cclxuXHRyZXNldCgpIHtcclxuXHJcblx0XHRmb3IobGV0IGZpZWxkIGluIHRoaXMub3JpZ2luYWxEYXRhKXtcclxuXHRcdFx0dGhpc1tmaWVsZF0gPSAnJztcclxuXHRcdH1cclxuXHRcdHRoaXMuZXJyb3JzLmNsZWFyKCk7XHJcblx0fVxyXG5cclxuXHRwb3N0KHVybCkgeyB0aGlzLnN1Ym1pdCgncG9zdCcsIHVybCkgfVxyXG5cclxuXHRwYXRjaCh1cmwpIHsgdGhpcy5zdWJtaXQoJ3BhdGNoJywgdXJsKSB9XHJcblxyXG5cdGRlbGV0ZSh1cmwpIHsgdGhpcy5zdWJtaXQoJ2RlbGV0ZScsIHVybCkgfVxyXG5cclxuXHRzdWJtaXQocmVxdWVzdFR5cGUsIHVybCkge1xyXG5cdFx0cmV0dXJuIG5ldyBQcm9taXNlKChyZXNvbHZlLHJlamVjdCkgPT4ge1xyXG5cdFx0XHRheGlvc1tyZXF1ZXN0VHlwZV0odXJsLCB0aGlzLmRhdGEoKSlcclxuXHRcdFx0XHQudGhlbihyZXNwb25zZSA9PiB7XHJcblx0XHRcdFx0XHR0aGlzLm9uU3VjY2VzcyhyZXNwb25zZS5kYXRhKTtcclxuXHRcdFx0XHRcdHJlc29sdmUocmVzcG9uc2UpO1xyXG5cdFx0XHRcdH0pXHJcblx0XHRcdFx0LmNhdGNoKGVycm9yID0+IHtcclxuXHRcdFx0XHRcdHRoaXMub25GYWlsKGVycm9yLnJlc3BvbnNlLmRhdGEpO1xyXG5cdFx0XHRcdFx0cmVqZWN0KGVycm9yKTtcclxuXHRcdFx0XHR9KVxyXG5cdFx0fSk7XHJcblx0fVxyXG5cclxuXHRvblN1Y2Nlc3MoZGF0YSkge1xyXG5cdFx0XHJcblx0XHR0aGlzLnJlc2V0KCk7XHJcblx0XHRcclxuXHR9XHJcblxyXG5cdG9uRmFpbChlcnJvcnMpIHtcclxuXHRcdHRoaXMuZXJyb3JzLnJlY29yZChlcnJvcnMpO1xyXG5cclxuXHR9XHJcbn1cclxuXHJcbmNsYXNzIEZvcm1FcnJvcnMge1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKCkge1xyXG5cdFx0dGhpcy5lcnJvcnMgPSB7fTtcclxuICAgIH1cclxuXHJcbiAgICBnZXQoZmllbGQpIHtcclxuICAgICAgICBpZih0aGlzLmVycm9yc1tmaWVsZF0pe1xyXG4gICAgICAgICAgICByZXR1cm4gdGhpcy5lcnJvcnNbZmllbGRdWzBdO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBoYXMoZmllbGQpIHsgcmV0dXJuIHRoaXMuZXJyb3JzLmhhc093blByb3BlcnR5KGZpZWxkKSB9XHJcblxyXG4gICAgYW55KCkgeyByZXR1cm4gT2JqZWN0LmtleXModGhpcy5lcnJvcnMpLmxlbmd0aCA+IDA7IH1cclxuXHJcbiAgICByZWNvcmQoZXJyb3JzKSB7IHRoaXMuZXJyb3JzID0gZXJyb3JzIH1cclxuXHJcbiAgICBjbGVhcihmaWVsZCkge1xyXG4gICAgICAgaWYoZmllbGQpe1xyXG4gICAgICAgIGRlbGV0ZSB0aGlzLmVycm9yc1tmaWVsZF07XHJcbiAgICAgICAgcmV0dXJuXHJcbiAgICAgICB9IFxyXG4gICAgICAgdGhpcy5lcnJvcnMgPSB7fTtcclxuICAgIH1cclxufVxyXG5cclxuZXhwb3J0IHsgRm9ybSwgRm9ybUVycm9ycyB9XG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9Nb2RlbHMvRm9ybS5qcyIsImltcG9ydCB7IEZvcm0sIEVycm9ycyB9IGZyb20gJy4vRm9ybS5qcyc7XHJcblxyXG5jb25zdCBkZWZhdWx0cyA9IHtcclxuICAgIG5hbWU6IG51bGwsXHJcbiAgICBkZXNjcmlwdGlvbjogbnVsbCxcclxuICAgIHByaWNlOiAwLFxyXG4gICAgY2F0ZWdvcnk6IG51bGwsXHJcbiAgICBpbWFnZTogbnVsbCxcclxuICAgIHF1YW50aXR5OiAwLFxyXG59XHJcblxyXG5leHBvcnQgZGVmYXVsdCB7XHJcbiAgICBmaW5kKGlkKSB7XHJcbiAgICAgICAgcmV0dXJuIF8uZXh0ZW5kKHRoaXMuZm9ybSwgdGhpcy5tZXJjaHNbXy5maW5kSW5kZXgodGhpcy5tZXJjaHMsIG8gPT4gby5pZCA9PSBpZCldKTtcclxuICAgIH0sXHJcbiAgICBtZXJjaHM6IFtdLFxyXG4gICAgZm9ybTogbmV3IEZvcm0oZGVmYXVsdHMpLFxyXG4gICAgYWxsKCkge1xyXG4gICAgICAgIHJldHVybiBuZXcgUHJvbWlzZSgocmVzLCByZWopID0+IHtcclxuICAgICAgICAgICAgdGhpcy5mb3JtLnN1Ym1pdCgnZ2V0JywgJy9hcGkvdjEvbWVyY2hhbmRpc2UnKVxyXG4gICAgICAgICAgICAgICAgLnRoZW4ocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMubWVyY2hzID0gW10uc2xpY2UuY2FsbChyZXNwb25zZS5kYXRhKTtcclxuICAgICAgICAgICAgICAgICAgICByZXMocmVzcG9uc2UpO1xyXG4gICAgICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgICAgIC5jYXRjaChlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coJ3RoZXJlIHdhcyBhbiBlcnJvcicpO1xyXG4gICAgICAgICAgICAgICAgICAgIHJlaihlcnJvci5yZXNwb25zZSk7XHJcbiAgICAgICAgICAgICAgICB9KVxyXG4gICAgICAgIH0pO1xyXG4gICAgfSxcclxuICAgIHVwZGF0ZShpZCkge1xyXG4gICAgICAgIGNvbnNvbGUubG9nKGlkKTtcclxuICAgIH0sXHJcbiAgICByZW1vdmUobWVyY2hJZCkge1xyXG4gICAgICAgIHJldHVybiBheGlvcy5kZWxldGUoYC9hcGkvdjEvbWVyY2hhbmRpc2UvJHttZXJjaElkfWApO1xyXG4gICAgICAgIHJldHVybiBheGlvcy5kZWxldGUoYC9hcGkvdjEvbWVyY2hhbmRpc2UvJHttZXJjaElkfWApO1xyXG4gICAgfVxyXG59XG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9Nb2RlbHMvTWVyY2hhbmRpc2UuanMiLCJjbGFzcyBjdXN0b21WYWxpZGF0b3Ige1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKGxhYmVsa2V5LCBjaGVja1ZhbGlpZGl0aWVzKSB7XHJcbiAgICAgICAgdGhpcy5sYWJlbCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCJsYWJlbFtmb3I9XCIgKyBsYWJlbGtleSArIFwiXVwiKTtcclxuICAgICAgICB0aGlzLmlucHV0Tm9kZSA9IHRoaXMubGFiZWwucXVlcnlTZWxlY3RvcihcImlucHV0XCIpO1xyXG4gICAgICAgIHRoaXMuZWxlbV9saXN0ID0gdGhpcy5sYWJlbC5xdWVyeVNlbGVjdG9yKFwiLmlucHV0LXJlcXVpcmVtZW50IGxpOm50aC1jaGlsZCgxKVwiKTtcclxuICAgICAgICB0aGlzLnZhbGlkaXRpZXMgPSBbXTtcclxuICAgICAgICB0aGlzLmNoZWNrVmFsaWlkaXRpZXMgPSBjaGVja1ZhbGlpZGl0aWVzO1xyXG4gICAgICAgIHRoaXMucmVnaXN0ZXJzTGlzdGVuZXJzKCk7XHJcbiAgICB9XHJcbiAgICBhZGRWYWxpZGl0aWVzKG1lc3NhZ2UpIHtcclxuICAgICAgICB0aGlzLnZhbGlkaXRpZXMucHVzaChtZXNzYWdlKTtcclxuICAgIH1cclxuICAgIGdldFZhbGlpZGl0aWVzKCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLnZhbGlpZGl0aWVzO1xyXG4gICAgfVxyXG4gICAgY2hlY2tWYWxpaWRpdGllcygpIHtcclxuICAgICAgICBmb3IgKHZhciBpID0gMDsgaSA8IHRoaXMuY2hlY2tWYWxpaWRpdGllcy5sZW5ndGg7IGkrKykge1xyXG4gICAgICAgICAgICB2YXIgaXNJbnZhbGlkID0gdGhpcy5jaGVja1ZhbGlpZGl0aWVzW2ldLmlzRW1wdHkodGhpcy5pbnB1dE5vZGUpO1xyXG4gICAgICAgICAgICBpZiAoaXNJbnZhbGlkKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmFkZFZhbGlkaXRpZXModGhpcy5jaGVja1ZhbGlpZGl0aWVzW2ldLmludmFsaWRpdHlNZXNzYWdlKTtcclxuICAgICAgICAgICAgICAgIHRoaXMuZWxlbV9saXN0LmNsYXNzTGlzdC5yZW1vdmUoJ3ZhbGlkJyk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmVsZW1fbGlzdC5jbGFzc0xpc3QuYWRkKCdpbnZhbGlkJyk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmVsZW1fbGlzdC5jbGFzc0xpc3QucmVtb3ZlKCdpbnZhbGlkJyk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmVsZW1fbGlzdC5jbGFzc0xpc3QuYWRkKCd2YWxpZCcpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgY2hlY2tJbnB1dCgpIHtcclxuICAgICAgICBpZiAodGhpcy5pbnB1dE5vZGUudmFsdWUgIT0gJycpIHtcclxuICAgICAgICAgICAgdGhpcy5pbnB1dE5vZGUuc2V0Q3VzdG9tVmFsaWRpdHkoJycpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHZhciBtc2cgPSB0aGlzLmdldFZhbGlpZGl0aWVzKCk7XHJcbiAgICAgICAgICAgIHRoaXMuaW5wdXROb2RlLnNldEN1c3RvbVZhbGlkaXR5KG1zZyk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMuY2hlY2tWYWxpaWRpdGllcygpO1xyXG4gICAgfVxyXG4gICAgcmVnaXN0ZXJzTGlzdGVuZXJzKCkge1xyXG4gICAgICAgIHZhciBjdXN0b21WYWxpZGF0b3IgPSB0aGlzO1xyXG4gICAgICAgIHRoaXMuaW5wdXROb2RlLmFkZEV2ZW50TGlzdGVuZXIoJ2tleXVwJywgZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICAgIGN1c3RvbVZhbGlkYXRvci5jaGVja0lucHV0KCk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcbn1cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9yZXNvdXJjZXMvYXNzZXRzL2pzL01vZGVscy9WYWxpZGF0b3IuanMiLCJpbXBvcnQgRXZlbnRzUGFnZSBmcm9tICcuL2NvbXBvbmVudHMvY2xpZW50L0V2ZW50c1BhZ2UudnVlJztcclxuaW1wb3J0IE1lcmNoYW5kaXNlUGFnZSBmcm9tICcuL2NvbXBvbmVudHMvY2xpZW50L01lcmNoYW5kaXNlUGFnZS52dWUnO1xyXG5cclxud2luZG93LmNhdCA9IHtcclxuICAgIHN0YXRlOiAzXHJcbn1cclxuY29uc3QgQXBwID0gbmV3IFZ1ZSh7XHJcbiAgICBlbDogXCIjY2xpZW50XCIsXHJcbiAgICBjb21wb25lbnRzOiB7RXZlbnRzUGFnZSwgTWVyY2hhbmRpc2VQYWdlfSxcclxuICAgIGRhdGEgOiB7XHJcbiAgICAgICAgc2xpZGVTdGF0ZTogMCxcclxuICAgICAgICBzaG93QWRkRm9ybTogZmFsc2UsXHJcbiAgICAgICAgc2hvcENhdGVnb3JpZXM6IF8udG9BcnJheSh3aW5kb3cuU2hvcENhdGVnb3JpZXMpXHJcbiAgICB9LFxyXG4gICAgbWV0aG9kczoge1xyXG4gICAgfVxyXG59KTtcclxuXHJcbmNvbnN0IEFwcENvbmZpZyA9IHtcclxuICAgIEhvc3Q6IG51bGxcclxufTtcclxuKGZ1bmN0aW9uICgkKSB7XHJcbiAgICBBcHBDb25maWcuSG9zdCA9IFwiLy9cIiArIHdpbmRvdy5sb2NhdGlvbi5ob3N0ICsgXCIvY20tdXNlci9cIjtcclxuICAgIGxvYWRfZGVwZW5kZW5jaWVzKCk7XHJcbn0pKGpRdWVyeSk7XHJcblxyXG5mdW5jdGlvbiBsb2FkX2RlcGVuZGVuY2llcygpIHtcclxuICAgIGNvbnNvbGUubG9nKCdsb2FkZWQnKTtcclxuICAgIGlmKCQoJy5jaG9zZW4tc2VsZWN0JylbMF0pIHtcclxuICAgICAgICAkKCcuY2hvc2VuLXNlbGVjdCcpLmNob3Nlbih7fSk7XHJcbiAgICB9XHJcbn1cclxuXHJcbi8vbmF2YmFyLXRvcCBtZW51IGJ1dHRvbiBldmVudFxyXG52YXIgbWVudUJ1dHRvbiA9IHtcclxuICAgIGluaXQ6IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICB2YXIgX21lbnVCYXIgPSB0aGlzLnNlbGYgPSAkKCcjbWVudS1idXR0b24nKS5maXJzdCgpLFxyXG4gICAgICAgICAgICBfc2lkZU5hdiA9IE5hdkJhci5zZWxmID0gJCgnI2NsaWVudC1zaWRlYmFyJyksIF9wYXJlbnRDb250ID0gJCgnI21haW4tY29sJyksIF9wYXJlbnQgPSB0aGlzO1xyXG4gICAgICAgIHZhciBzdGF0ZSA9IF9wYXJlbnQuc3RhdGU7XHJcbiAgICAgICAgc3RhdGUuYWN0aXZlID0gcGFyc2VJbnQoQ29va2llLmdldENvb2tpZSgnbWVudVN0YXRlJykpO1xyXG4gICAgICAgIF9tZW51QmFyLm9uKCdjbGljaycsIGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgc3RhdGUuYWN0aXZlID0gKHN0YXRlLmFjdGl2ZSA9PT0gMSA/IDAgOiAxKTtcclxuICAgICAgICAgICAgaWYgKHN0YXRlLmFjdGl2ZSA9PT0gMSkge1xyXG4gICAgICAgICAgICBcdGNvbnNvbGUudHJhY2UoX3NpZGVOYXYpO1xyXG4gICAgICAgICAgICAgICAgX21lbnVCYXIuYWRkQ2xhc3MoJ2FjdGl2ZScpO1xyXG4gICAgICAgICAgICAgICAgX3NpZGVOYXYuYWRkQ2xhc3MoJ2NvbC1zbGlkZS1oaWRlJyk7XHJcbiAgICAgICAgICAgICAgICBfcGFyZW50Q29udC5yZW1vdmVDbGFzcygnY29sLWxnLW9mZnNldC0yJyk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgZWxzZSB7XHJcbiAgICAgICAgICAgICAgICBfbWVudUJhci5yZW1vdmVDbGFzcygnYWN0aXZlJyk7XHJcbiAgICAgICAgICAgICAgICBfc2lkZU5hdi5yZW1vdmVDbGFzcygnY29sLXNsaWRlLWhpZGUnKTtcclxuICAgICAgICAgICAgICAgIF9wYXJlbnRDb250LmFkZENsYXNzKCdjb2wtbGctb2Zmc2V0LTInKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhDb29raWUuZ2V0Q29va2llKCdtZW51U3RhdGUnKSwgc3RhdGUuYWN0aXZlKTtcclxuICAgICAgICAgICAgJCh0aGlzKS50b2dnbGVDbGFzcygnYWN0aXZlJyk7XHJcbiAgICAgICAgICAgIGRvY3VtZW50LmNvb2tpZSA9IFwibWVudVN0YXRlPVwiICsgc3RhdGUuYWN0aXZlICsgXCJcIjtcclxuICAgICAgICB9KTtcclxuICAgIH0sXHJcbiAgICBzdGF0ZTogeyBhY3RpdmU6IGZhbHNlIH1cclxufTtcclxuLy9cclxudmFyIE5hdkJhciA9IHtcclxuICAgIHNlbGY6IG51bGwsXHJcbiAgICBpbml0OiBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgdmFyIE5hdmlnYXRpb24gPSB0aGlzLnNlbGY7XHJcbiAgICAgICAgTmF2aWdhdGlvbi5jaGlsZHJlbigndWwnKS5kZWxlZ2F0ZSgnbGknLCAnY2xpY2snLCBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgIHZhciBfc3ViTmF2ID0gJCh0aGlzKS5uZXh0KCk7XHJcbiAgICAgICAgICAgIGlmIChfc3ViTmF2WzBdLnRhZ05hbWUgPT09IFwiVUxcIikge1xyXG4gICAgICAgICAgICAgICAgX3N1Yk5hdi50b2dnbGVDbGFzcygnY29sbGFwc2UnKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG59O1xyXG5cclxudmFyIENvb2tpZSA9IHtcclxuICAgIGxvYWRDb29raWU6IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICB2YXIgRG9jQ29va2llID0gZG9jdW1lbnQuY29va2llLCBDb29raWVBcnJheSA9IFtdO1xyXG4gICAgICAgIHRoaXMuY29va2llcyA9IERvY0Nvb2tpZS5zcGxpdCgvXFxzL2cpO1xyXG4gICAgICAgIGZvciAodmFyIF9pID0gMCwgX2EgPSB0aGlzLmNvb2tpZXM7IF9pIDwgX2EubGVuZ3RoOyBfaSsrKSB7XHJcbiAgICAgICAgICAgIHZhciBjb29raWUgPSBfYVtfaV07XHJcbiAgICAgICAgICAgIHZhciBfYiA9IGNvb2tpZS5zcGxpdCgvXFw9LyksIG5hbWUgPSBfYlswXSwgdmFsdWUgPSBfYlsxXTtcclxuICAgICAgICAgICAgQ29va2llQXJyYXkucHVzaCh7IFwibmFtZVwiOiBuYW1lLCBcInZhbHVlXCI6IHZhbHVlLnNwbGl0KC9cXDskLylbMF0gfSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBDb29raWVBcnJheTtcclxuICAgIH0sXHJcbiAgICBzZXRDb29raWU6IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICBjb25zb2xlLmxvZygnc2V0dGluZyBjb29raWUnKTtcclxuICAgIH0sXHJcbiAgICBnZXRDb29raWU6IGZ1bmN0aW9uIChjb29raWVTdHJpbmcpIHtcclxuICAgICAgICB2YXIgY29va2llcyA9IHRoaXMubG9hZENvb2tpZSgpO1xyXG4gICAgICAgIGZvciAodmFyIF9pID0gMCwgY29va2llc18xID0gY29va2llczsgX2kgPCBjb29raWVzXzEubGVuZ3RoOyBfaSsrKSB7XHJcbiAgICAgICAgICAgIHZhciBtaSA9IGNvb2tpZXNfMVtfaV07XHJcbiAgICAgICAgICAgIGlmIChtaS5uYW1lID09IGNvb2tpZVN0cmluZykge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIG1pLnZhbHVlO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgIH1cclxufTtcclxudmFyIFNlcnZpY2VzID0ge1xyXG4gICAgaW5pdDogZnVuY3Rpb24gKCkge1xyXG4gICAgICAgIHRoaXMuY29uc3RydWN0KCk7XHJcbiAgICAgICAgdmFyIF9hID0gdGhpcy5wcm9wcywgbGlzdCA9IF9hLmxpc3QsIEVkaXRCb3ggPSBfYS5FZGl0Qm94LCBTZXJ2aWNlRm9ybSA9IF9hLlNlcnZpY2VGb3JtLCBkZWxMaW5rID0gX2EuZGVsTGluaztcclxuICAgICAgICB2YXIgX2hpZGUgPSBcImNvbGxhcHNlXCI7XHJcbiAgICAgICAgdmFyIGxpc3RfaXRlbSA9IGxpc3QuZmluZChcIi5saXN0LWdyb3VwLWl0ZW1cIik7XHJcbiAgICAgICAgZnVuY3Rpb24gZ2V0UGFyZW50KHgpIHtcclxuICAgICAgICAgICAgcmV0dXJuICQoeCkucGFyZW50cygnLmxpc3QtZ3JvdXAtaXRlbScpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBsaXN0X2l0ZW0ubWFwKGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgJCh0aGlzKS5yZW1vdmVDbGFzcyhfaGlkZSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgbGlzdC5kZWxlZ2F0ZShcImJ1dHRvbltkYXRhLWVkaXRdXCIsIFwiY2xpY2tcIiwgZnVuY3Rpb24gKGV2dCkge1xyXG4gICAgICAgICAgICB2YXIgX3BhcmVudCA9IGdldFBhcmVudCh0aGlzKSwgX3hlcm94ID0gX3BhcmVudC5hdHRyKFwiZGF0YS14ZXJveFwiKTtcclxuICAgICAgICAgICAgc2hvd0xvYWRlcihldnQpO1xyXG4gICAgICAgICAgICBFZGl0Qm94LmxvYWQoQXBwQ29uZmlnLkhvc3QgKyBcInNlcnZpY2VzL1wiICsgX3hlcm94LCBmdW5jdGlvbiAoc29uY3QsIHhocikge1xyXG4gICAgICAgICAgICAgICAgcmVtb3ZlTG9hZGVyKCk7XHJcbiAgICAgICAgICAgICAgICB2YXIgaW1hZ2UgPSBuZXcgSW1hZ2VFZGl0KCdpbnB1dFtuYW1lPWltYWdlQ29sbF0nLCAnLmRyYWctZHJvcCcpO1xyXG4gICAgICAgICAgICAgICAgU2VydmljZUZvcm0uYWRkQ2xhc3MoX2hpZGUpO1xyXG4gICAgICAgICAgICAgICAgRWRpdEJveC5zY3JvbGxUbygxMDApO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgICAgICBsaXN0LmRlbGVnYXRlKFwiYnV0dG9uW2RhdGEtZGVsXVwiLCBcImNsaWNrXCIsIGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgdmFyIHBhcmVudExpc3QgPSBnZXRQYXJlbnQodGhpcyksIF9zZXJ2aWNlWGVyb3ggPSBwYXJlbnRMaXN0LmF0dHIoXCJkYXRhLXhlcm94XCIpLCBfY29uZmlybSA9IGNvbmZpcm0oXCJEZWxldGUgP1wiKTtcclxuICAgICAgICAgICAgaWYgKF9jb25maXJtKVxyXG4gICAgICAgICAgICAgICAgJC5wb3N0KGRlbExpbmssIHsgX3Rva2VuOiBMYXJhdmVsLmNzcmZUb2tlbiwgYWN0aXZpdHk6IFwic2VydmljZXNcIiwgc2VydmljZVhlcm94OiBfc2VydmljZVhlcm94IH0sIGZ1bmN0aW9uICh4aHIpIHtcclxuICAgICAgICAgICAgICAgICAgICBpZiAoeGhyLmNvZGUgPT09IHRydWUpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgYWxlcnQoXCJEZWxldGVkXCIpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBwYXJlbnRMaXN0LmFkZENsYXNzKF9oaWRlKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9LCBcImpzb25cIik7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9LFxyXG4gICAgY29uc3RydWN0OiBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgdGhpcy5wcm9wcyA9IHtcclxuICAgICAgICAgICAgbGlzdDogJChcIiNzZXJ2aWNlLWxpc3RcIiksXHJcbiAgICAgICAgICAgIEVkaXRCb3g6ICQoJyNlZGl0LXNlcnZpY2VzJyksXHJcbiAgICAgICAgICAgIFNlcnZpY2VGb3JtOiAkKFwiI3JlZ2lzdGVyX19zZXJ2aWNlXCIpLFxyXG4gICAgICAgICAgICBkZWxMaW5rOiBBcHBDb25maWcuSG9zdCArIFwic2VydmljZXMvZGVsZXRlXCJcclxuICAgICAgICB9O1xyXG4gICAgfVxyXG59O1xyXG52YXIgQWR2ZXJ0c1BhZ2UgPSB7XHJcbiAgICBpbml0OiBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgdGhpcy5jb25zdHJ1Y3RvcigpO1xyXG4gICAgICAgIHZhciBfaGlkZSA9IFwiY29sbGFwc2VcIjtcclxuICAgICAgICB2YXIgX2EgPSB0aGlzLnByb3BzLCBGb3JtID0gX2EuRm9ybSwgRWRpdFBhbmVsID0gX2EuRWRpdFBhbmVsLCBFZGl0UGFnZSA9IF9hLkVkaXRQYWdlLCBBZHZlcnRCb3ggPSBfYS5BZHZlcnRCb3gsIHBhbmVsQm9keSA9IF9hLnBhbmVsQm9keSwgQWR2ZXJ0Qm94ZXMgPSBfYS5BZHZlcnRCb3hlcztcclxuICAgICAgICBBZHZlcnRCb3guZmluZChcImJ1dHRvbltkYXRhLWNvbGxhcHNlXVwiKS5jbGljayhmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgIHZhciBpID0gJCh0aGlzKS5maW5kKFwiaVwiKSwgcm0gPSBpLmF0dHIoXCJkYXRhLXRvZ2dsZVwiKTtcclxuICAgICAgICAgICAgaWYgKEZvcm0uaGFzQ2xhc3MoX2hpZGUpKSB7XHJcbiAgICAgICAgICAgICAgICBpLnJlbW92ZUNsYXNzKHJtKTtcclxuICAgICAgICAgICAgICAgIEZvcm0ucmVtb3ZlQ2xhc3MoX2hpZGUpO1xyXG4gICAgICAgICAgICAgICAgaWYgKHdpbmRvdy5pbm5lcldpZHRoID4gMTIwMCkge1xyXG4gICAgICAgICAgICAgICAgICAgIEFkdmVydEJveC5yZW1vdmVDbGFzcyhcImV4cGFuZFwiKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBlbHNlIHtcclxuICAgICAgICAgICAgICAgIGkuYWRkQ2xhc3Mocm0pO1xyXG4gICAgICAgICAgICAgICAgRm9ybS5hZGRDbGFzcyhfaGlkZSk7XHJcbiAgICAgICAgICAgICAgICBpZiAod2luZG93LmlubmVyV2lkdGggPiAxMjAwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgQWR2ZXJ0Qm94LmFkZENsYXNzKFwiZXhwYW5kXCIpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgQWR2ZXJ0Qm94ZXMuZGVsZWdhdGUoJ1tkYXRhLWVkaXRdJywgJ2NsaWNrJywgZnVuY3Rpb24gKGV2dCkge1xyXG4gICAgICAgICAgICBpZiAoZXZ0LmN0cmxLZXkgPT0gdHJ1ZSkge1xyXG4gICAgICAgICAgICAgICAgZXZ0LnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICAgICAgICAgICAgICBldnQuc3RvcFByb3BhZ2F0aW9uKCk7XHJcbiAgICAgICAgICAgICAgICB2YXIgeGVyb3ggPSAkKHRoaXMpLmF0dHIoXCJkYXRhLWVkaXRcIik7XHJcbiAgICAgICAgICAgICAgICB2YXIgbGluayA9ICQodGhpcykuYXR0cihcImhyZWZcIik7XHJcbiAgICAgICAgICAgICAgICBFZGl0UGFuZWwucmVtb3ZlQ2xhc3MoX2hpZGUpO1xyXG4gICAgICAgICAgICAgICAgRWRpdFBhbmVsLmxvYWQobGluaywgc2hvd0xvYWRlcik7XHJcbiAgICAgICAgICAgICAgICBFZGl0UGFuZWwuc2Nyb2xsVG8oODkpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgQWR2ZXJ0Qm94LmRlbGVnYXRlKCdbZGF0YS1kZWxdJywgJ2NsaWNrJywgZnVuY3Rpb24gKGV2dCkge1xyXG4gICAgICAgICAgICB2YXIgX3NlbGYgPSAkKHRoaXMpLCBfeGVyb3ggPSBfc2VsZi5hdHRyKFwiZGF0YS1kZWxcIiksIF9hZHZlcnRQcmV2aWV3ID0gX3NlbGYucGFyZW50cyhcIi5hZHZlcnRzLXByZXZpZXdcIik7XHJcbiAgICAgICAgICAgIGlmIChjb25maXJtKFwiRGVsZXRlP1wiKSkge1xyXG4gICAgICAgICAgICAgICAgc2hvd0xvYWRlcihldnQpO1xyXG4gICAgICAgICAgICAgICAgJC5wb3N0KEVkaXRQYWdlICsgXCIvZGVsZXRlXCIsIHsgYWR2ZXJ0WGVyb3g6IF94ZXJveCwgZGVsZXRlU3VibWl0OiAxMCwgX3Rva2VuOiB3aW5kb3cuTGFyYXZlbC5jc3JmVG9rZW4gfSwgZnVuY3Rpb24gKHhocikge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmICh4aHIuY29kZSA9PT0gMjAwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIF9hZHZlcnRQcmV2aWV3LmFkZENsYXNzKFwiY29sbGFwc2VcIik7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlbW92ZUxvYWRlcigpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBFZGl0UGFuZWwuYWRkQ2xhc3MoJ19oaWRlJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgICAgICBFZGl0UGFuZWwuZGVsZWdhdGUoXCJzcGFuW2RhdGEtY29sbGFwc2VdXCIsIFwiY2xpY2tcIiwgZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICBFZGl0UGFuZWwuYWRkQ2xhc3MoX2hpZGUpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfSxcclxuICAgIGNvbnN0cnVjdG9yOiBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgdmFyIEZvcm0gPSAkKFwiI2FkZF9hZHZlcnRcIiksIEFkdmVydEJveCA9ICQoXCIjYWR2ZXJ0LWJveFwiKSwgZWRpdFBhbmVsID0gJChcIiNlZGl0LWFkdmVydFwiKSwgX2EgPSB3aW5kb3cubG9jYXRpb24sIHBhdGhuYW1lID0gX2EucGF0aG5hbWUsIGhhc2ggPSBfYS5oYXNoO1xyXG4gICAgICAgIHRoaXMucHJvcHMgPSB7XHJcbiAgICAgICAgICAgIEZvcm06IEZvcm0sXHJcbiAgICAgICAgICAgIEVkaXRQYWdlOiBwYXRobmFtZSxcclxuICAgICAgICAgICAgQWR2ZXJ0Qm94OiBBZHZlcnRCb3gsXHJcbiAgICAgICAgICAgIEVkaXRQYW5lbDogZWRpdFBhbmVsLFxyXG4gICAgICAgICAgICBBZHZlcnRCb3hlczogQWR2ZXJ0Qm94LmZpbmQoXCIucGFuZWxcIiksXHJcbiAgICAgICAgfTtcclxuICAgIH0sXHJcbn07XHJcbnZhciBBY2NvdW50ID0ge1xyXG4gICAgaW5pdDogZnVuY3Rpb24gKCkge1xyXG4gICAgICAgIHRoaXMuY29uc3RydWN0KCk7XHJcbiAgICAgICAgdmFyIF9hID0gdGhpcy5wcm9wcywgQmFzaWNQYW5lbCA9IF9hLkJhc2ljUGFuZWwsIFBQYW5lbCA9IF9hLlBQYW5lbCwgVXBkYXRlUGFuZWwgPSBfYS5VcGRhdGVQYW5lbCwgTW9kaWZ5QnRuID0gX2EuTW9kaWZ5QnRuLCBDaGFuZ2VQYXNzID0gX2EuQ2hhbmdlUGFzcztcclxuICAgICAgICBpZiAoQmFzaWNQYW5lbC5sZW5ndGggIT09IDApIHtcclxuICAgICAgICAgICAgTW9kaWZ5QnRuLmNsaWNrKGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgICAgIFVwZGF0ZVBhbmVsLnRvZ2dsZUNsYXNzKFwiY29sbGFwc2VcIikuc2Nyb2xsVG8oMTAwKTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIENoYW5nZVBhc3MuY2xpY2soZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICAgICAgUFBhbmVsLnRvZ2dsZUNsYXNzKFwiY29sbGFwc2VcIikuc2Nyb2xsVG8oMTAwKTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGNvbnNvbGUubG9nKCd3b3JraW5nIG9uIHRoZSBBY2NvdW50IHBhbmVsJyk7XHJcbiAgICB9LFxyXG4gICAgY29uc3RydWN0OiBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgdGhpcy5wcm9wcyA9IHtcclxuICAgICAgICAgICAgQmFzaWNQYW5lbDogJCgnI2FjY291bnRfX3Byb2ZpbGUnKSxcclxuICAgICAgICAgICAgUFBhbmVsOiAkKFwiI2NoYW5nZV9fcGFzc3dvcmRcIiksXHJcbiAgICAgICAgICAgIFVwZGF0ZVBhbmVsOiAkKFwiI2FjY291bnRfX3VwZGF0ZVwiKSxcclxuICAgICAgICAgICAgTW9kaWZ5QnRuOiAkKFwiI21vZGlmeVwiKSxcclxuICAgICAgICAgICAgQ2hhbmdlUGFzczogJChcIiNjaC1wd2RcIiksXHJcbiAgICAgICAgfTtcclxuICAgIH1cclxufTtcclxuXHJcbmZ1bmN0aW9uIHNob3dMb2FkZXIoZSwgYikge1xyXG4gICAgaWYgKGIgPT09IHZvaWQgMCkgeyBiID0gXCJmYWlsZWRcIjsgfVxyXG4gICAgaWYgKGFyZ3VtZW50cy5sZW5ndGggPCAyKSB7XHJcbiAgICAgICAgY29uc29sZS5sb2coJ2p1c3QgYSBzaW1wbGUgbG9hZGVyJyk7XHJcbiAgICB9XHJcbiAgICBlbHNlIHtcclxuICAgICAgICBjb25zb2xlLmxvZygnaGlkZSBsb2FkZXInKTtcclxuICAgICAgICBpZiAoYiA9PSBcInN1Y2Nlc3NcIikge1xyXG4gICAgICAgICAgICBjb25zb2xlLmxvZygnaGlkaW5nIHdpdGggc3VjY2VzcycpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBlbHNlIHtcclxuICAgICAgICAgICAgY29uc29sZS53YXJuKCdoaWRpbmcgd2l0aCBlcnJvcicpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG5cclxuZnVuY3Rpb24gcmVtb3ZlTG9hZGVyKCkge1xyXG4gICAgY29uc29sZS5sb2coXCJyZW1vdmluZyBsb2FkZXJcIik7XHJcbn1cclxuXHJcbiQoZnVuY3Rpb24gKCkge1xyXG4gICAgbWVudUJ1dHRvbi5pbml0KCk7XHJcbiAgICBTZXJ2aWNlcy5pbml0KCk7XHJcbiAgICBBZHZlcnRzUGFnZS5pbml0KCk7XHJcbiAgICBOYXZCYXIuaW5pdCgpO1xyXG4gICAgQWNjb3VudC5pbml0KCk7XHJcbn0pO1xyXG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NtLWRhc2hib2FyZC5qcyIsInZhciBkaXNwb3NlZCA9IGZhbHNlXG52YXIgbm9ybWFsaXplQ29tcG9uZW50ID0gcmVxdWlyZShcIiEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvY29tcG9uZW50LW5vcm1hbGl6ZXJcIilcbi8qIHNjcmlwdCAqL1xudmFyIF9fdnVlX3NjcmlwdF9fID0gcmVxdWlyZShcIiEhYmFiZWwtbG9hZGVyP3tcXFwiY2FjaGVEaXJlY3RvcnlcXFwiOnRydWUsXFxcInByZXNldHNcXFwiOltbXFxcImVudlxcXCIse1xcXCJtb2R1bGVzXFxcIjpmYWxzZSxcXFwidGFyZ2V0c1xcXCI6e1xcXCJicm93c2Vyc1xcXCI6W1xcXCI+IDIlXFxcIl0sXFxcInVnbGlmeVxcXCI6dHJ1ZX19XV19IS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvcj90eXBlPXNjcmlwdCZpbmRleD0wIS4vTW9kYWwudnVlXCIpXG4vKiB0ZW1wbGF0ZSAqL1xudmFyIF9fdnVlX3RlbXBsYXRlX18gPSByZXF1aXJlKFwiISEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvdGVtcGxhdGUtY29tcGlsZXIvaW5kZXg/e1xcXCJpZFxcXCI6XFxcImRhdGEtdi0wNzhiMDJmMVxcXCIsXFxcImhhc1Njb3BlZFxcXCI6ZmFsc2V9IS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvcj90eXBlPXRlbXBsYXRlJmluZGV4PTAhLi9Nb2RhbC52dWVcIilcbi8qIHN0eWxlcyAqL1xudmFyIF9fdnVlX3N0eWxlc19fID0gbnVsbFxuLyogc2NvcGVJZCAqL1xudmFyIF9fdnVlX3Njb3BlSWRfXyA9IG51bGxcbi8qIG1vZHVsZUlkZW50aWZpZXIgKHNlcnZlciBvbmx5KSAqL1xudmFyIF9fdnVlX21vZHVsZV9pZGVudGlmaWVyX18gPSBudWxsXG52YXIgQ29tcG9uZW50ID0gbm9ybWFsaXplQ29tcG9uZW50KFxuICBfX3Z1ZV9zY3JpcHRfXyxcbiAgX192dWVfdGVtcGxhdGVfXyxcbiAgX192dWVfc3R5bGVzX18sXG4gIF9fdnVlX3Njb3BlSWRfXyxcbiAgX192dWVfbW9kdWxlX2lkZW50aWZpZXJfX1xuKVxuQ29tcG9uZW50Lm9wdGlvbnMuX19maWxlID0gXCJyZXNvdXJjZXNcXFxcYXNzZXRzXFxcXGpzXFxcXGNvbXBvbmVudHNcXFxcTW9kYWwudnVlXCJcbmlmIChDb21wb25lbnQuZXNNb2R1bGUgJiYgT2JqZWN0LmtleXMoQ29tcG9uZW50LmVzTW9kdWxlKS5zb21lKGZ1bmN0aW9uIChrZXkpIHtyZXR1cm4ga2V5ICE9PSBcImRlZmF1bHRcIiAmJiBrZXkuc3Vic3RyKDAsIDIpICE9PSBcIl9fXCJ9KSkge2NvbnNvbGUuZXJyb3IoXCJuYW1lZCBleHBvcnRzIGFyZSBub3Qgc3VwcG9ydGVkIGluICoudnVlIGZpbGVzLlwiKX1cbmlmIChDb21wb25lbnQub3B0aW9ucy5mdW5jdGlvbmFsKSB7Y29uc29sZS5lcnJvcihcIlt2dWUtbG9hZGVyXSBNb2RhbC52dWU6IGZ1bmN0aW9uYWwgY29tcG9uZW50cyBhcmUgbm90IHN1cHBvcnRlZCB3aXRoIHRlbXBsYXRlcywgdGhleSBzaG91bGQgdXNlIHJlbmRlciBmdW5jdGlvbnMuXCIpfVxuXG4vKiBob3QgcmVsb2FkICovXG5pZiAobW9kdWxlLmhvdCkgeyhmdW5jdGlvbiAoKSB7XG4gIHZhciBob3RBUEkgPSByZXF1aXJlKFwidnVlLWhvdC1yZWxvYWQtYXBpXCIpXG4gIGhvdEFQSS5pbnN0YWxsKHJlcXVpcmUoXCJ2dWVcIiksIGZhbHNlKVxuICBpZiAoIWhvdEFQSS5jb21wYXRpYmxlKSByZXR1cm5cbiAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICBpZiAoIW1vZHVsZS5ob3QuZGF0YSkge1xuICAgIGhvdEFQSS5jcmVhdGVSZWNvcmQoXCJkYXRhLXYtMDc4YjAyZjFcIiwgQ29tcG9uZW50Lm9wdGlvbnMpXG4gIH0gZWxzZSB7XG4gICAgaG90QVBJLnJlbG9hZChcImRhdGEtdi0wNzhiMDJmMVwiLCBDb21wb25lbnQub3B0aW9ucylcbiAgfVxuICBtb2R1bGUuaG90LmRpc3Bvc2UoZnVuY3Rpb24gKGRhdGEpIHtcbiAgICBkaXNwb3NlZCA9IHRydWVcbiAgfSlcbn0pKCl9XG5cbm1vZHVsZS5leHBvcnRzID0gQ29tcG9uZW50LmV4cG9ydHNcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jb21wb25lbnRzL01vZGFsLnZ1ZVxuLy8gbW9kdWxlIGlkID0gLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NvbXBvbmVudHMvTW9kYWwudnVlXG4vLyBtb2R1bGUgY2h1bmtzID0gMSIsInZhciBkaXNwb3NlZCA9IGZhbHNlXG52YXIgbm9ybWFsaXplQ29tcG9uZW50ID0gcmVxdWlyZShcIiEuLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvY29tcG9uZW50LW5vcm1hbGl6ZXJcIilcbi8qIHNjcmlwdCAqL1xudmFyIF9fdnVlX3NjcmlwdF9fID0gcmVxdWlyZShcIiEhYmFiZWwtbG9hZGVyP3tcXFwiY2FjaGVEaXJlY3RvcnlcXFwiOnRydWUsXFxcInByZXNldHNcXFwiOltbXFxcImVudlxcXCIse1xcXCJtb2R1bGVzXFxcIjpmYWxzZSxcXFwidGFyZ2V0c1xcXCI6e1xcXCJicm93c2Vyc1xcXCI6W1xcXCI+IDIlXFxcIl0sXFxcInVnbGlmeVxcXCI6dHJ1ZX19XV19IS4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvcj90eXBlPXNjcmlwdCZpbmRleD0wIS4vRXZlbnRzUGFnZS52dWVcIilcbi8qIHRlbXBsYXRlICovXG52YXIgX192dWVfdGVtcGxhdGVfXyA9IG51bGxcbi8qIHN0eWxlcyAqL1xudmFyIF9fdnVlX3N0eWxlc19fID0gbnVsbFxuLyogc2NvcGVJZCAqL1xudmFyIF9fdnVlX3Njb3BlSWRfXyA9IG51bGxcbi8qIG1vZHVsZUlkZW50aWZpZXIgKHNlcnZlciBvbmx5KSAqL1xudmFyIF9fdnVlX21vZHVsZV9pZGVudGlmaWVyX18gPSBudWxsXG52YXIgQ29tcG9uZW50ID0gbm9ybWFsaXplQ29tcG9uZW50KFxuICBfX3Z1ZV9zY3JpcHRfXyxcbiAgX192dWVfdGVtcGxhdGVfXyxcbiAgX192dWVfc3R5bGVzX18sXG4gIF9fdnVlX3Njb3BlSWRfXyxcbiAgX192dWVfbW9kdWxlX2lkZW50aWZpZXJfX1xuKVxuQ29tcG9uZW50Lm9wdGlvbnMuX19maWxlID0gXCJyZXNvdXJjZXNcXFxcYXNzZXRzXFxcXGpzXFxcXGNvbXBvbmVudHNcXFxcY2xpZW50XFxcXEV2ZW50c1BhZ2UudnVlXCJcbmlmIChDb21wb25lbnQuZXNNb2R1bGUgJiYgT2JqZWN0LmtleXMoQ29tcG9uZW50LmVzTW9kdWxlKS5zb21lKGZ1bmN0aW9uIChrZXkpIHtyZXR1cm4ga2V5ICE9PSBcImRlZmF1bHRcIiAmJiBrZXkuc3Vic3RyKDAsIDIpICE9PSBcIl9fXCJ9KSkge2NvbnNvbGUuZXJyb3IoXCJuYW1lZCBleHBvcnRzIGFyZSBub3Qgc3VwcG9ydGVkIGluICoudnVlIGZpbGVzLlwiKX1cblxuLyogaG90IHJlbG9hZCAqL1xuaWYgKG1vZHVsZS5ob3QpIHsoZnVuY3Rpb24gKCkge1xuICB2YXIgaG90QVBJID0gcmVxdWlyZShcInZ1ZS1ob3QtcmVsb2FkLWFwaVwiKVxuICBob3RBUEkuaW5zdGFsbChyZXF1aXJlKFwidnVlXCIpLCBmYWxzZSlcbiAgaWYgKCFob3RBUEkuY29tcGF0aWJsZSkgcmV0dXJuXG4gIG1vZHVsZS5ob3QuYWNjZXB0KClcbiAgaWYgKCFtb2R1bGUuaG90LmRhdGEpIHtcbiAgICBob3RBUEkuY3JlYXRlUmVjb3JkKFwiZGF0YS12LTdhMjk3ZjEwXCIsIENvbXBvbmVudC5vcHRpb25zKVxuICB9IGVsc2Uge1xuICAgIGhvdEFQSS5yZWxvYWQoXCJkYXRhLXYtN2EyOTdmMTBcIiwgQ29tcG9uZW50Lm9wdGlvbnMpXG4gIH1cbiAgbW9kdWxlLmhvdC5kaXNwb3NlKGZ1bmN0aW9uIChkYXRhKSB7XG4gICAgZGlzcG9zZWQgPSB0cnVlXG4gIH0pXG59KSgpfVxuXG5tb2R1bGUuZXhwb3J0cyA9IENvbXBvbmVudC5leHBvcnRzXG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL3Jlc291cmNlcy9hc3NldHMvanMvY29tcG9uZW50cy9jbGllbnQvRXZlbnRzUGFnZS52dWVcbi8vIG1vZHVsZSBpZCA9IC4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jb21wb25lbnRzL2NsaWVudC9FdmVudHNQYWdlLnZ1ZVxuLy8gbW9kdWxlIGNodW5rcyA9IDEiLCJ2YXIgZGlzcG9zZWQgPSBmYWxzZVxuZnVuY3Rpb24gaW5qZWN0U3R5bGUgKHNzckNvbnRleHQpIHtcbiAgaWYgKGRpc3Bvc2VkKSByZXR1cm5cbiAgcmVxdWlyZShcIiEhdnVlLXN0eWxlLWxvYWRlciFjc3MtbG9hZGVyP3NvdXJjZU1hcCEuLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc3R5bGUtY29tcGlsZXIvaW5kZXg/e1xcXCJ2dWVcXFwiOnRydWUsXFxcImlkXFxcIjpcXFwiZGF0YS12LTQwMWVlOTRjXFxcIixcXFwic2NvcGVkXFxcIjpmYWxzZSxcXFwiaGFzSW5saW5lQ29uZmlnXFxcIjp0cnVlfSEuLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT1zdHlsZXMmaW5kZXg9MCEuL01lcmNoYW5kaXNlUGFnZS52dWVcIilcbn1cbnZhciBub3JtYWxpemVDb21wb25lbnQgPSByZXF1aXJlKFwiIS4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9jb21wb25lbnQtbm9ybWFsaXplclwiKVxuLyogc2NyaXB0ICovXG52YXIgX192dWVfc2NyaXB0X18gPSByZXF1aXJlKFwiISFiYWJlbC1sb2FkZXI/e1xcXCJjYWNoZURpcmVjdG9yeVxcXCI6dHJ1ZSxcXFwicHJlc2V0c1xcXCI6W1tcXFwiZW52XFxcIix7XFxcIm1vZHVsZXNcXFwiOmZhbHNlLFxcXCJ0YXJnZXRzXFxcIjp7XFxcImJyb3dzZXJzXFxcIjpbXFxcIj4gMiVcXFwiXSxcXFwidWdsaWZ5XFxcIjp0cnVlfX1dXX0hLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yP3R5cGU9c2NyaXB0JmluZGV4PTAhLi9NZXJjaGFuZGlzZVBhZ2UudnVlXCIpXG4vKiB0ZW1wbGF0ZSAqL1xudmFyIF9fdnVlX3RlbXBsYXRlX18gPSBudWxsXG4vKiBzdHlsZXMgKi9cbnZhciBfX3Z1ZV9zdHlsZXNfXyA9IGluamVjdFN0eWxlXG4vKiBzY29wZUlkICovXG52YXIgX192dWVfc2NvcGVJZF9fID0gbnVsbFxuLyogbW9kdWxlSWRlbnRpZmllciAoc2VydmVyIG9ubHkpICovXG52YXIgX192dWVfbW9kdWxlX2lkZW50aWZpZXJfXyA9IG51bGxcbnZhciBDb21wb25lbnQgPSBub3JtYWxpemVDb21wb25lbnQoXG4gIF9fdnVlX3NjcmlwdF9fLFxuICBfX3Z1ZV90ZW1wbGF0ZV9fLFxuICBfX3Z1ZV9zdHlsZXNfXyxcbiAgX192dWVfc2NvcGVJZF9fLFxuICBfX3Z1ZV9tb2R1bGVfaWRlbnRpZmllcl9fXG4pXG5Db21wb25lbnQub3B0aW9ucy5fX2ZpbGUgPSBcInJlc291cmNlc1xcXFxhc3NldHNcXFxcanNcXFxcY29tcG9uZW50c1xcXFxjbGllbnRcXFxcTWVyY2hhbmRpc2VQYWdlLnZ1ZVwiXG5pZiAoQ29tcG9uZW50LmVzTW9kdWxlICYmIE9iamVjdC5rZXlzKENvbXBvbmVudC5lc01vZHVsZSkuc29tZShmdW5jdGlvbiAoa2V5KSB7cmV0dXJuIGtleSAhPT0gXCJkZWZhdWx0XCIgJiYga2V5LnN1YnN0cigwLCAyKSAhPT0gXCJfX1wifSkpIHtjb25zb2xlLmVycm9yKFwibmFtZWQgZXhwb3J0cyBhcmUgbm90IHN1cHBvcnRlZCBpbiAqLnZ1ZSBmaWxlcy5cIil9XG5cbi8qIGhvdCByZWxvYWQgKi9cbmlmIChtb2R1bGUuaG90KSB7KGZ1bmN0aW9uICgpIHtcbiAgdmFyIGhvdEFQSSA9IHJlcXVpcmUoXCJ2dWUtaG90LXJlbG9hZC1hcGlcIilcbiAgaG90QVBJLmluc3RhbGwocmVxdWlyZShcInZ1ZVwiKSwgZmFsc2UpXG4gIGlmICghaG90QVBJLmNvbXBhdGlibGUpIHJldHVyblxuICBtb2R1bGUuaG90LmFjY2VwdCgpXG4gIGlmICghbW9kdWxlLmhvdC5kYXRhKSB7XG4gICAgaG90QVBJLmNyZWF0ZVJlY29yZChcImRhdGEtdi00MDFlZTk0Y1wiLCBDb21wb25lbnQub3B0aW9ucylcbiAgfSBlbHNlIHtcbiAgICBob3RBUEkucmVsb2FkKFwiZGF0YS12LTQwMWVlOTRjXCIsIENvbXBvbmVudC5vcHRpb25zKVxuICB9XG4gIG1vZHVsZS5ob3QuZGlzcG9zZShmdW5jdGlvbiAoZGF0YSkge1xuICAgIGRpc3Bvc2VkID0gdHJ1ZVxuICB9KVxufSkoKX1cblxubW9kdWxlLmV4cG9ydHMgPSBDb21wb25lbnQuZXhwb3J0c1xuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2NvbXBvbmVudHMvY2xpZW50L01lcmNoYW5kaXNlUGFnZS52dWVcbi8vIG1vZHVsZSBpZCA9IC4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9jb21wb25lbnRzL2NsaWVudC9NZXJjaGFuZGlzZVBhZ2UudnVlXG4vLyBtb2R1bGUgY2h1bmtzID0gMSJdLCJzb3VyY2VSb290IjoiIn0=