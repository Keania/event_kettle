var ServicePage = {
    init: function () {
        this.construct();
        this.alignImage();
    },
    alignImage: alignImage,
    construct: function () {
        this.props = {
            CoverImage: $("#cover-photo")
        };
    }
};
$(function () {
    ServicePage.init();
});
