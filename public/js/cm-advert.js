var _this = this;
/**
 * Created by Joseph on 30/12/2016.
 */
var UserAdverts = {
    start: function () {
        var _loadBox = _this.loadBox = $('#advert-box');
        _loadBox.delegate('.adverts-preview .panel-heading a[href] ', 'click', _this.justClick);
    },
    justClick: function () {
        console.log(_this);
        var _fx = $(_this).attr('href');
        switch (_fx) {
            case '#edit':
                UserAdverts.edit();
                break;
            case '#visible':
                UserAdverts.toggleVisibility(_this);
                break;
            default:
                alert('  NO(fx) ');
                break;
        }
    },
    edit: function () {
        //brings up modal box
        //and editing options
        //
    },
    toggleVisibility: function (_anchor) {
        //gets the toogle state via AJAX
        //and toogle it if active
        //toggle the eye icon
        var _icon = $(_anchor).find('i'), _open = 'glyphicon-eye-close', _close = 'glyphicon-eye-open';
        if (_icon.hasClass(_open)) {
            _icon.removeClass(_open).addClass(_close);
        }
        else {
            _icon.removeClass(_close).addClass(_open);
        }
        //makes the changes
        $.ajax({
            url: 'hdr_p',
            type: 'POST',
            data: 'ad=visible',
            dataType: 'xml',
            success: function (xhr) {
                console.log(xhr);
            }
        });
    }
};
this.onload = function () { return UserAdverts.start(); };
