/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 1);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/assets/js/blog.js":
/***/ (function(module, exports) {


var Blog = new Vue({
    el: '#blog-app',
    data: {}
});

var BlogImage = function () {
    function BlogImage(x) {
        this.props = {
            width: 0,
            height: 0
        };
        this.className = "img-shorten";
        try {
            this.self = $(x);
            this.child = this.self.find('img');
            this.props = JSON.parse(this.self.attr(BlogImage.imageAttr));
            this.props = { width: this.self.width(), height: this.self.height() };
            this.startUp();
            console.log(this);
        } catch (b) {
            console.error('Image Error Occurred');
        }
    }
    BlogImage.prototype.startUp = function () {
        var _a = this,
            self = _a.self,
            props = _a.props,
            className = _a.className;
        if (props.height > BlogImage.maxHeight) {
            self.addClass(className);
            this.activateClick();
        }
    };
    BlogImage.prototype.activateClick = function () {
        var _a = this,
            self = _a.self,
            className = _a.className,
            child = _a.child;
        self.click(function () {
            if (self.hasClass(className)) {
                var height = child.height() + "px";
                self.animate({ height: height }, 400, 'swing', function () {});
                self.removeClass(className);
            } else {
                self.addClass(className);
                self.attr('style', '');
            }
        });
    };
    return BlogImage;
}();

BlogImage.maxHeight = 350;
BlogImage.imageAttr = "data-attr";
$(function () {
    configureImages();
    activeNavBarScrolling();
    var backTopButton = $('#back-2-top');
    backTopButton.click(function () {
        $('body, html').animate({ scrollTop: "5px" }, 700, 'swing');
    });
});
/**
 * configures the post images with the width/height size
 * @return null
 */
function configureImages() {
    var figures = $("figure[data-attr]");
    figures.each(function (x) {
        var figure = new BlogImage(this);
    });
}
/**
 * manipulates the blog page header-nav when scroll pass a limit.
 * @return void
 */
function activeNavBarScrolling() {
    var top = $(this).scrollTop();
    var navbrand = $('.navbar-brand');
    var mainNav = $('body > .navbar-default');
    var className = 'shift';
    if (top > 100) {
        if ($(this).width() > 768) mainNav.addClass('hideMenu');
        navbrand.addClass(className);
    } else {
        if ($(this).width() > 768) mainNav.removeClass('hideMenu');
        navbrand.removeClass(className);
    }
}
/**
 *shows or adds the back 2 top button on the page
 *
 *@return void
 */
function back2Top() {
    var win = $(window);
    var button = $('#back-2-top');
    if (win.scrollTop() > 300) {
        button.css({ right: "30px" });
        setTimeout(function () {
            button.removeClass('invisible');
        });
    } else {
        button.css({ right: "-100px" }, function () {
            button.addClass('invisible');
        });
    }
}

$(window).scroll(function () {
    back2Top.call(this);
    activeNavBarScrolling.call(this);
});
//# sourceMappingURL=blog.js.map

/***/ }),

/***/ 1:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("./resources/assets/js/blog.js");


/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAgOWYwMDI3ZmM4YzZjNGZkMDUyZjgiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9ibG9nLmpzIl0sIm5hbWVzIjpbIkJsb2ciLCJWdWUiLCJlbCIsImRhdGEiLCJCbG9nSW1hZ2UiLCJ4IiwicHJvcHMiLCJ3aWR0aCIsImhlaWdodCIsImNsYXNzTmFtZSIsInNlbGYiLCIkIiwiY2hpbGQiLCJmaW5kIiwiSlNPTiIsInBhcnNlIiwiYXR0ciIsImltYWdlQXR0ciIsInN0YXJ0VXAiLCJjb25zb2xlIiwibG9nIiwiYiIsImVycm9yIiwicHJvdG90eXBlIiwiX2EiLCJtYXhIZWlnaHQiLCJhZGRDbGFzcyIsImFjdGl2YXRlQ2xpY2siLCJjbGljayIsImhhc0NsYXNzIiwiYW5pbWF0ZSIsInJlbW92ZUNsYXNzIiwiY29uZmlndXJlSW1hZ2VzIiwiYWN0aXZlTmF2QmFyU2Nyb2xsaW5nIiwiYmFja1RvcEJ1dHRvbiIsInNjcm9sbFRvcCIsImZpZ3VyZXMiLCJlYWNoIiwiZmlndXJlIiwidG9wIiwibmF2YnJhbmQiLCJtYWluTmF2IiwiYmFjazJUb3AiLCJ3aW4iLCJ3aW5kb3ciLCJidXR0b24iLCJjc3MiLCJyaWdodCIsInNldFRpbWVvdXQiLCJzY3JvbGwiLCJjYWxsIl0sIm1hcHBpbmdzIjoiO0FBQUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQUs7QUFDTDtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLG1DQUEyQiwwQkFBMEIsRUFBRTtBQUN2RCx5Q0FBaUMsZUFBZTtBQUNoRDtBQUNBO0FBQ0E7O0FBRUE7QUFDQSw4REFBc0QsK0RBQStEOztBQUVySDtBQUNBOztBQUVBO0FBQ0E7Ozs7Ozs7OztBQzVEQSxJQUFNQSxPQUFPLElBQUlDLEdBQUosQ0FBUTtBQUNwQkMsUUFBSSxXQURnQjtBQUVwQkMsVUFBTTtBQUZjLENBQVIsQ0FBYjs7QUFPQSxJQUFJQyxZQUFhLFlBQVk7QUFDekIsYUFBU0EsU0FBVCxDQUFtQkMsQ0FBbkIsRUFBc0I7QUFDbEIsYUFBS0MsS0FBTCxHQUFhO0FBQ1RDLG1CQUFPLENBREU7QUFFVEMsb0JBQVE7QUFGQyxTQUFiO0FBSUEsYUFBS0MsU0FBTCxHQUFpQixhQUFqQjtBQUNBLFlBQUk7QUFDQSxpQkFBS0MsSUFBTCxHQUFZQyxFQUFFTixDQUFGLENBQVo7QUFDQSxpQkFBS08sS0FBTCxHQUFhLEtBQUtGLElBQUwsQ0FBVUcsSUFBVixDQUFlLEtBQWYsQ0FBYjtBQUNBLGlCQUFLUCxLQUFMLEdBQWFRLEtBQUtDLEtBQUwsQ0FBVyxLQUFLTCxJQUFMLENBQVVNLElBQVYsQ0FBZVosVUFBVWEsU0FBekIsQ0FBWCxDQUFiO0FBQ0EsaUJBQUtYLEtBQUwsR0FBYSxFQUFFQyxPQUFPLEtBQUtHLElBQUwsQ0FBVUgsS0FBVixFQUFULEVBQTRCQyxRQUFRLEtBQUtFLElBQUwsQ0FBVUYsTUFBVixFQUFwQyxFQUFiO0FBQ0EsaUJBQUtVLE9BQUw7QUFDQUMsb0JBQVFDLEdBQVIsQ0FBWSxJQUFaO0FBQ0gsU0FQRCxDQVFBLE9BQU9DLENBQVAsRUFBVTtBQUNORixvQkFBUUcsS0FBUixDQUFjLHNCQUFkO0FBQ0g7QUFDSjtBQUNEbEIsY0FBVW1CLFNBQVYsQ0FBb0JMLE9BQXBCLEdBQThCLFlBQVk7QUFDdEMsWUFBSU0sS0FBSyxJQUFUO0FBQUEsWUFBZWQsT0FBT2MsR0FBR2QsSUFBekI7QUFBQSxZQUErQkosUUFBUWtCLEdBQUdsQixLQUExQztBQUFBLFlBQWlERyxZQUFZZSxHQUFHZixTQUFoRTtBQUNBLFlBQUlILE1BQU1FLE1BQU4sR0FBZUosVUFBVXFCLFNBQTdCLEVBQXdDO0FBQ3BDZixpQkFBS2dCLFFBQUwsQ0FBY2pCLFNBQWQ7QUFDQSxpQkFBS2tCLGFBQUw7QUFDSDtBQUNKLEtBTkQ7QUFPQXZCLGNBQVVtQixTQUFWLENBQW9CSSxhQUFwQixHQUFvQyxZQUFZO0FBQzVDLFlBQUlILEtBQUssSUFBVDtBQUFBLFlBQWVkLE9BQU9jLEdBQUdkLElBQXpCO0FBQUEsWUFBK0JELFlBQVllLEdBQUdmLFNBQTlDO0FBQUEsWUFBeURHLFFBQVFZLEdBQUdaLEtBQXBFO0FBQ0FGLGFBQUtrQixLQUFMLENBQVcsWUFBWTtBQUNuQixnQkFBSWxCLEtBQUttQixRQUFMLENBQWNwQixTQUFkLENBQUosRUFBOEI7QUFDMUIsb0JBQUlELFNBQVNJLE1BQU1KLE1BQU4sS0FBaUIsSUFBOUI7QUFDQUUscUJBQUtvQixPQUFMLENBQWEsRUFBRXRCLFFBQVFBLE1BQVYsRUFBYixFQUFpQyxHQUFqQyxFQUFzQyxPQUF0QyxFQUErQyxZQUFZLENBQzFELENBREQ7QUFFQUUscUJBQUtxQixXQUFMLENBQWlCdEIsU0FBakI7QUFDSCxhQUxELE1BTUs7QUFDREMscUJBQUtnQixRQUFMLENBQWNqQixTQUFkO0FBQ0FDLHFCQUFLTSxJQUFMLENBQVUsT0FBVixFQUFtQixFQUFuQjtBQUNIO0FBQ0osU0FYRDtBQVlILEtBZEQ7QUFlQSxXQUFPWixTQUFQO0FBQ0gsQ0ExQ2dCLEVBQWpCOztBQTRDQUEsVUFBVXFCLFNBQVYsR0FBc0IsR0FBdEI7QUFDQXJCLFVBQVVhLFNBQVYsR0FBc0IsV0FBdEI7QUFDQU4sRUFBRSxZQUFZO0FBQ1ZxQjtBQUNBQztBQUNBLFFBQUlDLGdCQUFnQnZCLEVBQUUsYUFBRixDQUFwQjtBQUNBdUIsa0JBQWNOLEtBQWQsQ0FBb0IsWUFBWTtBQUM1QmpCLFVBQUUsWUFBRixFQUFnQm1CLE9BQWhCLENBQXdCLEVBQUVLLFdBQVcsS0FBYixFQUF4QixFQUE4QyxHQUE5QyxFQUFtRCxPQUFuRDtBQUNILEtBRkQ7QUFHSCxDQVBEO0FBUUE7Ozs7QUFJQSxTQUFTSCxlQUFULEdBQTJCO0FBQ3ZCLFFBQUlJLFVBQVV6QixFQUFFLG1CQUFGLENBQWQ7QUFDQXlCLFlBQVFDLElBQVIsQ0FBYSxVQUFVaEMsQ0FBVixFQUFhO0FBQ3RCLFlBQUlpQyxTQUFTLElBQUlsQyxTQUFKLENBQWMsSUFBZCxDQUFiO0FBQ0gsS0FGRDtBQUdIO0FBQ0Q7Ozs7QUFJQSxTQUFTNkIscUJBQVQsR0FBaUM7QUFDN0IsUUFBSU0sTUFBTTVCLEVBQUUsSUFBRixFQUFRd0IsU0FBUixFQUFWO0FBQ0EsUUFBSUssV0FBVzdCLEVBQUUsZUFBRixDQUFmO0FBQ0EsUUFBSThCLFVBQVU5QixFQUFFLHdCQUFGLENBQWQ7QUFDQSxRQUFJRixZQUFZLE9BQWhCO0FBQ0EsUUFBSThCLE1BQU0sR0FBVixFQUFlO0FBQ1gsWUFBSTVCLEVBQUUsSUFBRixFQUFRSixLQUFSLEtBQWtCLEdBQXRCLEVBQ0lrQyxRQUFRZixRQUFSLENBQWlCLFVBQWpCO0FBQ0pjLGlCQUFTZCxRQUFULENBQWtCakIsU0FBbEI7QUFDSCxLQUpELE1BS0s7QUFDRCxZQUFJRSxFQUFFLElBQUYsRUFBUUosS0FBUixLQUFrQixHQUF0QixFQUNJa0MsUUFBUVYsV0FBUixDQUFvQixVQUFwQjtBQUNKUyxpQkFBU1QsV0FBVCxDQUFxQnRCLFNBQXJCO0FBQ0g7QUFDSjtBQUNEOzs7OztBQUtBLFNBQVNpQyxRQUFULEdBQW9CO0FBQ2hCLFFBQUlDLE1BQU1oQyxFQUFFaUMsTUFBRixDQUFWO0FBQ0EsUUFBSUMsU0FBU2xDLEVBQUUsYUFBRixDQUFiO0FBQ0EsUUFBSWdDLElBQUlSLFNBQUosS0FBa0IsR0FBdEIsRUFBMkI7QUFDdkJVLGVBQU9DLEdBQVAsQ0FBVyxFQUFFQyxPQUFPLE1BQVQsRUFBWDtBQUNBQyxtQkFBVyxZQUFZO0FBQ25CSCxtQkFBT2QsV0FBUCxDQUFtQixXQUFuQjtBQUNILFNBRkQ7QUFHSCxLQUxELE1BTUs7QUFDRGMsZUFBT0MsR0FBUCxDQUFXLEVBQUVDLE9BQU8sUUFBVCxFQUFYLEVBQWdDLFlBQVk7QUFDeENGLG1CQUFPbkIsUUFBUCxDQUFnQixXQUFoQjtBQUNILFNBRkQ7QUFHSDtBQUNKOztBQUVEZixFQUFFaUMsTUFBRixFQUFVSyxNQUFWLENBQWlCLFlBQVk7QUFDekJQLGFBQVNRLElBQVQsQ0FBYyxJQUFkO0FBQ0FqQiwwQkFBc0JpQixJQUF0QixDQUEyQixJQUEzQjtBQUNILENBSEQ7QUFJQSxnQyIsImZpbGUiOiJcXGpzXFxibG9nLmpzIiwic291cmNlc0NvbnRlbnQiOlsiIFx0Ly8gVGhlIG1vZHVsZSBjYWNoZVxuIFx0dmFyIGluc3RhbGxlZE1vZHVsZXMgPSB7fTtcblxuIFx0Ly8gVGhlIHJlcXVpcmUgZnVuY3Rpb25cbiBcdGZ1bmN0aW9uIF9fd2VicGFja19yZXF1aXJlX18obW9kdWxlSWQpIHtcblxuIFx0XHQvLyBDaGVjayBpZiBtb2R1bGUgaXMgaW4gY2FjaGVcbiBcdFx0aWYoaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0pIHtcbiBcdFx0XHRyZXR1cm4gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0uZXhwb3J0cztcbiBcdFx0fVxuIFx0XHQvLyBDcmVhdGUgYSBuZXcgbW9kdWxlIChhbmQgcHV0IGl0IGludG8gdGhlIGNhY2hlKVxuIFx0XHR2YXIgbW9kdWxlID0gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0gPSB7XG4gXHRcdFx0aTogbW9kdWxlSWQsXG4gXHRcdFx0bDogZmFsc2UsXG4gXHRcdFx0ZXhwb3J0czoge31cbiBcdFx0fTtcblxuIFx0XHQvLyBFeGVjdXRlIHRoZSBtb2R1bGUgZnVuY3Rpb25cbiBcdFx0bW9kdWxlc1ttb2R1bGVJZF0uY2FsbChtb2R1bGUuZXhwb3J0cywgbW9kdWxlLCBtb2R1bGUuZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXyk7XG5cbiBcdFx0Ly8gRmxhZyB0aGUgbW9kdWxlIGFzIGxvYWRlZFxuIFx0XHRtb2R1bGUubCA9IHRydWU7XG5cbiBcdFx0Ly8gUmV0dXJuIHRoZSBleHBvcnRzIG9mIHRoZSBtb2R1bGVcbiBcdFx0cmV0dXJuIG1vZHVsZS5leHBvcnRzO1xuIFx0fVxuXG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlcyBvYmplY3QgKF9fd2VicGFja19tb2R1bGVzX18pXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm0gPSBtb2R1bGVzO1xuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZSBjYWNoZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5jID0gaW5zdGFsbGVkTW9kdWxlcztcblxuIFx0Ly8gZGVmaW5lIGdldHRlciBmdW5jdGlvbiBmb3IgaGFybW9ueSBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQgPSBmdW5jdGlvbihleHBvcnRzLCBuYW1lLCBnZXR0ZXIpIHtcbiBcdFx0aWYoIV9fd2VicGFja19yZXF1aXJlX18ubyhleHBvcnRzLCBuYW1lKSkge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBuYW1lLCB7XG4gXHRcdFx0XHRjb25maWd1cmFibGU6IGZhbHNlLFxuIFx0XHRcdFx0ZW51bWVyYWJsZTogdHJ1ZSxcbiBcdFx0XHRcdGdldDogZ2V0dGVyXG4gXHRcdFx0fSk7XG4gXHRcdH1cbiBcdH07XG5cbiBcdC8vIGdldERlZmF1bHRFeHBvcnQgZnVuY3Rpb24gZm9yIGNvbXBhdGliaWxpdHkgd2l0aCBub24taGFybW9ueSBtb2R1bGVzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm4gPSBmdW5jdGlvbihtb2R1bGUpIHtcbiBcdFx0dmFyIGdldHRlciA9IG1vZHVsZSAmJiBtb2R1bGUuX19lc01vZHVsZSA/XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0RGVmYXVsdCgpIHsgcmV0dXJuIG1vZHVsZVsnZGVmYXVsdCddOyB9IDpcbiBcdFx0XHRmdW5jdGlvbiBnZXRNb2R1bGVFeHBvcnRzKCkgeyByZXR1cm4gbW9kdWxlOyB9O1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQoZ2V0dGVyLCAnYScsIGdldHRlcik7XG4gXHRcdHJldHVybiBnZXR0ZXI7XG4gXHR9O1xuXG4gXHQvLyBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGxcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubyA9IGZ1bmN0aW9uKG9iamVjdCwgcHJvcGVydHkpIHsgcmV0dXJuIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmplY3QsIHByb3BlcnR5KTsgfTtcblxuIFx0Ly8gX193ZWJwYWNrX3B1YmxpY19wYXRoX19cbiBcdF9fd2VicGFja19yZXF1aXJlX18ucCA9IFwiXCI7XG5cbiBcdC8vIExvYWQgZW50cnkgbW9kdWxlIGFuZCByZXR1cm4gZXhwb3J0c1xuIFx0cmV0dXJuIF9fd2VicGFja19yZXF1aXJlX18oX193ZWJwYWNrX3JlcXVpcmVfXy5zID0gMSk7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gd2VicGFjay9ib290c3RyYXAgOWYwMDI3ZmM4YzZjNGZkMDUyZjgiLCJcclxuY29uc3QgQmxvZyA9IG5ldyBWdWUoe1xyXG5cdGVsOiAnI2Jsb2ctYXBwJyxcclxuXHRkYXRhOiB7XHJcblxyXG5cdH1cclxufSk7XHJcblxyXG52YXIgQmxvZ0ltYWdlID0gKGZ1bmN0aW9uICgpIHtcclxuICAgIGZ1bmN0aW9uIEJsb2dJbWFnZSh4KSB7XHJcbiAgICAgICAgdGhpcy5wcm9wcyA9IHtcclxuICAgICAgICAgICAgd2lkdGg6IDAsXHJcbiAgICAgICAgICAgIGhlaWdodDogMCxcclxuICAgICAgICB9O1xyXG4gICAgICAgIHRoaXMuY2xhc3NOYW1lID0gXCJpbWctc2hvcnRlblwiO1xyXG4gICAgICAgIHRyeSB7XHJcbiAgICAgICAgICAgIHRoaXMuc2VsZiA9ICQoeCk7XHJcbiAgICAgICAgICAgIHRoaXMuY2hpbGQgPSB0aGlzLnNlbGYuZmluZCgnaW1nJyk7XHJcbiAgICAgICAgICAgIHRoaXMucHJvcHMgPSBKU09OLnBhcnNlKHRoaXMuc2VsZi5hdHRyKEJsb2dJbWFnZS5pbWFnZUF0dHIpKTtcclxuICAgICAgICAgICAgdGhpcy5wcm9wcyA9IHsgd2lkdGg6IHRoaXMuc2VsZi53aWR0aCgpLCBoZWlnaHQ6IHRoaXMuc2VsZi5oZWlnaHQoKSB9O1xyXG4gICAgICAgICAgICB0aGlzLnN0YXJ0VXAoKTtcclxuICAgICAgICAgICAgY29uc29sZS5sb2codGhpcyk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGNhdGNoIChiKSB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoJ0ltYWdlIEVycm9yIE9jY3VycmVkJyk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgQmxvZ0ltYWdlLnByb3RvdHlwZS5zdGFydFVwID0gZnVuY3Rpb24gKCkge1xyXG4gICAgICAgIHZhciBfYSA9IHRoaXMsIHNlbGYgPSBfYS5zZWxmLCBwcm9wcyA9IF9hLnByb3BzLCBjbGFzc05hbWUgPSBfYS5jbGFzc05hbWU7XHJcbiAgICAgICAgaWYgKHByb3BzLmhlaWdodCA+IEJsb2dJbWFnZS5tYXhIZWlnaHQpIHtcclxuICAgICAgICAgICAgc2VsZi5hZGRDbGFzcyhjbGFzc05hbWUpO1xyXG4gICAgICAgICAgICB0aGlzLmFjdGl2YXRlQ2xpY2soKTtcclxuICAgICAgICB9XHJcbiAgICB9O1xyXG4gICAgQmxvZ0ltYWdlLnByb3RvdHlwZS5hY3RpdmF0ZUNsaWNrID0gZnVuY3Rpb24gKCkge1xyXG4gICAgICAgIHZhciBfYSA9IHRoaXMsIHNlbGYgPSBfYS5zZWxmLCBjbGFzc05hbWUgPSBfYS5jbGFzc05hbWUsIGNoaWxkID0gX2EuY2hpbGQ7XHJcbiAgICAgICAgc2VsZi5jbGljayhmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgIGlmIChzZWxmLmhhc0NsYXNzKGNsYXNzTmFtZSkpIHtcclxuICAgICAgICAgICAgICAgIHZhciBoZWlnaHQgPSBjaGlsZC5oZWlnaHQoKSArIFwicHhcIjtcclxuICAgICAgICAgICAgICAgIHNlbGYuYW5pbWF0ZSh7IGhlaWdodDogaGVpZ2h0IH0sIDQwMCwgJ3N3aW5nJywgZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICBzZWxmLnJlbW92ZUNsYXNzKGNsYXNzTmFtZSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgZWxzZSB7XHJcbiAgICAgICAgICAgICAgICBzZWxmLmFkZENsYXNzKGNsYXNzTmFtZSk7XHJcbiAgICAgICAgICAgICAgICBzZWxmLmF0dHIoJ3N0eWxlJywgJycpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICB9O1xyXG4gICAgcmV0dXJuIEJsb2dJbWFnZTtcclxufSgpKTtcclxuXHJcbkJsb2dJbWFnZS5tYXhIZWlnaHQgPSAzNTA7XHJcbkJsb2dJbWFnZS5pbWFnZUF0dHIgPSBcImRhdGEtYXR0clwiO1xyXG4kKGZ1bmN0aW9uICgpIHtcclxuICAgIGNvbmZpZ3VyZUltYWdlcygpO1xyXG4gICAgYWN0aXZlTmF2QmFyU2Nyb2xsaW5nKCk7XHJcbiAgICB2YXIgYmFja1RvcEJ1dHRvbiA9ICQoJyNiYWNrLTItdG9wJyk7XHJcbiAgICBiYWNrVG9wQnV0dG9uLmNsaWNrKGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAkKCdib2R5LCBodG1sJykuYW5pbWF0ZSh7IHNjcm9sbFRvcDogXCI1cHhcIiB9LCA3MDAsICdzd2luZycpO1xyXG4gICAgfSk7XHJcbn0pO1xyXG4vKipcclxuICogY29uZmlndXJlcyB0aGUgcG9zdCBpbWFnZXMgd2l0aCB0aGUgd2lkdGgvaGVpZ2h0IHNpemVcclxuICogQHJldHVybiBudWxsXHJcbiAqL1xyXG5mdW5jdGlvbiBjb25maWd1cmVJbWFnZXMoKSB7XHJcbiAgICB2YXIgZmlndXJlcyA9ICQoXCJmaWd1cmVbZGF0YS1hdHRyXVwiKTtcclxuICAgIGZpZ3VyZXMuZWFjaChmdW5jdGlvbiAoeCkge1xyXG4gICAgICAgIHZhciBmaWd1cmUgPSBuZXcgQmxvZ0ltYWdlKHRoaXMpO1xyXG4gICAgfSk7XHJcbn1cclxuLyoqXHJcbiAqIG1hbmlwdWxhdGVzIHRoZSBibG9nIHBhZ2UgaGVhZGVyLW5hdiB3aGVuIHNjcm9sbCBwYXNzIGEgbGltaXQuXHJcbiAqIEByZXR1cm4gdm9pZFxyXG4gKi9cclxuZnVuY3Rpb24gYWN0aXZlTmF2QmFyU2Nyb2xsaW5nKCkge1xyXG4gICAgdmFyIHRvcCA9ICQodGhpcykuc2Nyb2xsVG9wKCk7XHJcbiAgICB2YXIgbmF2YnJhbmQgPSAkKCcubmF2YmFyLWJyYW5kJyk7XHJcbiAgICB2YXIgbWFpbk5hdiA9ICQoJ2JvZHkgPiAubmF2YmFyLWRlZmF1bHQnKTtcclxuICAgIHZhciBjbGFzc05hbWUgPSAnc2hpZnQnO1xyXG4gICAgaWYgKHRvcCA+IDEwMCkge1xyXG4gICAgICAgIGlmICgkKHRoaXMpLndpZHRoKCkgPiA3NjgpXHJcbiAgICAgICAgICAgIG1haW5OYXYuYWRkQ2xhc3MoJ2hpZGVNZW51Jyk7XHJcbiAgICAgICAgbmF2YnJhbmQuYWRkQ2xhc3MoY2xhc3NOYW1lKTtcclxuICAgIH1cclxuICAgIGVsc2Uge1xyXG4gICAgICAgIGlmICgkKHRoaXMpLndpZHRoKCkgPiA3NjgpXHJcbiAgICAgICAgICAgIG1haW5OYXYucmVtb3ZlQ2xhc3MoJ2hpZGVNZW51Jyk7XHJcbiAgICAgICAgbmF2YnJhbmQucmVtb3ZlQ2xhc3MoY2xhc3NOYW1lKTtcclxuICAgIH1cclxufVxyXG4vKipcclxuICpzaG93cyBvciBhZGRzIHRoZSBiYWNrIDIgdG9wIGJ1dHRvbiBvbiB0aGUgcGFnZVxyXG4gKlxyXG4gKkByZXR1cm4gdm9pZFxyXG4gKi9cclxuZnVuY3Rpb24gYmFjazJUb3AoKSB7XHJcbiAgICB2YXIgd2luID0gJCh3aW5kb3cpO1xyXG4gICAgdmFyIGJ1dHRvbiA9ICQoJyNiYWNrLTItdG9wJyk7XHJcbiAgICBpZiAod2luLnNjcm9sbFRvcCgpID4gMzAwKSB7XHJcbiAgICAgICAgYnV0dG9uLmNzcyh7IHJpZ2h0OiBcIjMwcHhcIiB9KTtcclxuICAgICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgYnV0dG9uLnJlbW92ZUNsYXNzKCdpbnZpc2libGUnKTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuICAgIGVsc2Uge1xyXG4gICAgICAgIGJ1dHRvbi5jc3MoeyByaWdodDogXCItMTAwcHhcIiB9LCBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgIGJ1dHRvbi5hZGRDbGFzcygnaW52aXNpYmxlJyk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcbn1cclxuXHJcbiQod2luZG93KS5zY3JvbGwoZnVuY3Rpb24gKCkge1xyXG4gICAgYmFjazJUb3AuY2FsbCh0aGlzKTtcclxuICAgIGFjdGl2ZU5hdkJhclNjcm9sbGluZy5jYWxsKHRoaXMpO1xyXG59KTtcclxuLy8jIHNvdXJjZU1hcHBpbmdVUkw9YmxvZy5qcy5tYXBcblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2Jsb2cuanMiXSwic291cmNlUm9vdCI6IiJ9