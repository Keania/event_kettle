let jQuery, $, FB : any;

// if('serviceWorker' in navigator){
// 	window.addEventListener('load',()=>{
// 		navigator.serviceWorker.register('./js/sw.js').then(() => {
// 			console.log("Worker was added Successfully");
// 		}).catch(() => {
// 			console.log("Worker extension failed .");
// 		});
// 	});
// };

function alignImage(){

		const {CoverImage} = this.props;
		let _height = CoverImage.find('img').height();
		// let _width = CoverImage.find('img').width();

		let _ratio = _height / CoverImage.height();
		if(_ratio > 2){
			let _nHeight = _ratio * Math.abs(CoverImage.height() / 2);
			_nHeight = _nHeight / 2;

			CoverImage.find("img").css({marginTop: "-"+_nHeight + "px" });
		}
		// console.log(_ratio,CoverImage.width());
}

let caps = function(string: String) {

	let lower = string.toLowerCase(),
		first = lower[0].toUpperCase();

	return first + lower.substring(1); 
}

String.prototype.caps = function() {
	return caps(this);
}

$(function(){

	try{
		let lightbox;
		lightbox.option({
			alwaysShowNavOnTouchDevices: true,
			resizeDuration : 300,
		});
	}catch(x){}

	// $.ajaxSetup({ cache: true });
	// $.getScript('//connect.facebook.net/en_US/sdk.js', function(){
	// 	FB.init({
	// 		appId: '495470100787197',
	// 		version: 'v2.7' // or v2.1, v2.2, v2.3, ...
	// 	});     
	// 	$('#loginbutton,#feedbutton').removeAttr('disabled');
	// 	FB.getLoginStatus(updateStatusCallback);
	// });

});

(function($){
	$.fn.scrollTo = jumpTo;
	return jumpTo;
})(jQuery);


class ImageEdit {	
	private input : any;
	private imageArray = [];
	private targetElement : any ;
	private images = [];

	constructor(input : string , elementName : string ){
			let inputElement = this.input = $(input),
				element = this.targetElement = $(elementName);


		if(inputElement.length > 0 && element.length > 0)
		{
			try{
				let mob = JSON.parse(inputElement.val());
				console.log(mob);
				if(mob.length > 0 ){
					this.imageArray = mob;
				}
			}catch(x){
				console.log(x.name);
			}

			this.images = element.find('figure.sr-image-tb');
			this.getButtons();
			this.showButton();
		}
	}

	getButtons(){
		this.images.map(function(e){
			$(this).append(ImageEdit.removeButton());
		});
	}

	static removeButton(){
		let button = $("<button>");
		button.addClass('sr-rb fa fa-remove');
		return button;
	}

	showButton(){
		let _buttons = this.targetElement,
			_parent = this,
			{imageArray} = _parent ;	

		_buttons.delegate('.sr-rb','click',function(e){
			e.preventDefault();
			e.stopImmediatePropagation();

			if(confirm('Are You Sure ?')){
				try{

					let _imageName = $(this).siblings('img').attr('src');
					_imageName = _imageName.match(/[a-zA-z0-9.]{20}\.(?:jpg|jpeg|png|gif|svg)/)[0];
					console.log(_imageName);

					if(_imageName !== null){
						let position; 

						for( let i in imageArray ) { 

							// console.log({ 
							// 	searchCurrentName : imageArray[i].name,
							// 	imageName :  _imageName,
							// 	matched : imageArray[i].name === _imageName ,
							// 	index : i,
							// });

							if (imageArray[i].name === _imageName ){
								position = i;

								break;
							}else{
								position = -1;
							}
						}

						if(position > -1){
							delete _parent.imageArray[position];
							_parent.updateValue();
						}
					}
				}catch(x){
					console.warn(x.name);
				}
			}
		});
	}

	updateValue(){
		this.input.val(JSON.stringify(this.imageArray));
		console.log(this.input.val());
	}

}

const Jump : any = {
	lastScrollPosition : 0
};

function jumpTo(offset){

	if(this !== window){

		let _object : any = $(this),
			_off : number  = (arguments.length == 0) ? 0 : offset,
			_selector : string = "body,html",
			_top : number = 0;


		if(0 !=_object.length) {
			Jump.lastScrollPosition = $(window).scrollTop();
			let _top = Math.ceil(_object.offset().top - _off)+"px";
			$(_selector).animate({scrollTop:_top }, 1000, "swing");
		}
	}
};


class NumberFormat {

	public element : any;

	private prefix : string = "+234";

	constructor(element : string, any ) {

		this.element = 
		(typeof(element) == "string" ? $(element) : element);
		this.setup();

	}

	setup() {

		this.element.on('keyup', function(){
			console.log($(this).val());
		});
		
	}
}