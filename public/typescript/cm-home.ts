const Adverts = {
    construct(){
        this.props = 
        {
            Holder : $('.advert-list '),
            Modal : $(' #advert-preview '),
            PhoneButton : $('button#phone')
        }
    },
    init() {
        this.construct();
        let {Modal,Holder} = this.props;
        if(window.innerWidth > 768 && window.innerHeight > 300  ){
            //adverts constructor
            Holder.delegate('.thumb-box a', 'click',this.showAdvertInfo);
            //adverts modal
            Modal.find('a[data-close]')
                .click(function() {
                    Modal.removeClass('show');
            });

        }else{
            console.warn("Modal Not Found.");
        }
        //advert single page slide button
        if(this.props.PhoneButton.length !== 0){
            this.slideButton();
        }

    },
    showAdvertInfo(evt) {

        if(evt.ctrlKey !== true){
            window.location.replace($(this).attr("href"));
            return false;

        }else{
            evt.stopPropagation();
            evt.preventDefault();
        }

        let _box = $(this),
             _a = _box.attr('data-xerox'),
             _b = Adverts;

        $.ajax({
            url: load.basket,
            dataType: 'json',
            data: 'activity=adverts&xerox=' + _a,

            success: function(xhr) {
                _b.process(xhr);
            }
        });
    },
    process(ni) {
        const {Modal} = this.props;

        for( let i in ni){
            let g : any = Modal.find("#"+i),
                f : string = ni[i];

            if(i == "ad_image"){
                g.attr("src",f);
            }else if( i == "userLink"){
                g.attr("href",f);
            }else if(i=="phone"){
                g.text(f);
            }else{
                g.text(f);
            }
            console.log("#"+i+'\t\t || ',ni[i]);
        }

        Modal.addClass('show');
    },
    slideButton(){
        
        this.props.PhoneButton.click(function(){

            let numberBadge = $(this).next('.badge');
            if(numberBadge.length === 0){ console.warn('Badge is not next to button')}
            // if(!numberBadge.hasClass('slide-out'))
            console.log(numberBadge);
            numberBadge.toggleClass('slide-out');
        });

    },
    update: '',
    NOA: 0
};

const load = {
    basket :  '/fetch',
};

const Menu = {

    init : function() {

        let _sideBarButton = $(".pd-menu-button");

        _sideBarButton.click(function(){

            let _sideBarMenu   = $(this).parents('.panel').find('ul.navbar-collapse');
            if(_sideBarMenu.hasClass("collapse")){
                _sideBarMenu.removeClass("collapse");
            }else{
                _sideBarMenu.addClass("collapse");
            }
            
        });
    }
}

$(function() {

    Adverts.init();
    Menu.init();
    
});
