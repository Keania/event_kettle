const AppConfig = {
    Host : null
};

(function($){
    AppConfig.Host = "//" + window.location.host + "/cm-user/";
})(jQuery);


//navbar-top menu button event
const menuButton = {
    init() {
        const _menuBar = this.self = $('.navbar-default button ').first(),
            _sideNav = NavBar.self = $('#logged__in__menu'),
            _parentCont = $('#pt-container'),
            _parent = this;
            let {state} = _parent;
            state.active = parseInt(Cookie.getCookie('menuState')) ;

        _menuBar.on('click', function(){
            state.active = ( state.active === 1 ? 0 : 1);

            if(state.active === 1){
                _menuBar.addClass('active');
                _sideNav.addClass('col-slide-hide');
                _parentCont.removeClass('col-lg-offset-2');

            }else{
                _menuBar.removeClass('active');
                _sideNav.removeClass('col-slide-hide');
                _parentCont.addClass('col-lg-offset-2');
            }

            console.log(Cookie.getCookie('menuState'),state.active);
            $(this).toggleClass('active');

            document.cookie = "menuState="+state.active+"";
        });
    },

    state : { active : false }
};

//
const NavBar = {
    self : null,
    init(){
        let Navigation = this.self;
        Navigation.children('ul').delegate('li','click',function(){

           const _subNav = $(this).next();

           if(_subNav[0].tagName === "UL"){           
               _subNav.toggleClass('collapse');
           }

        });
    }
};


const Cookie = {

     loadCookie(){
         const DocCookie = document.cookie,
               CookieArray = [];

         this.cookies = DocCookie.split(/\s/g);
         for (let cookie of this.cookies) {
             var [name, value,] = cookie.split(/\=/);
             CookieArray.push({"name": name, "value": value.split(/\;$/)[0]});
         }
         return CookieArray;
     },

    setCookie(){
        console.log('setting cookie');
    },

    getCookie(cookieString: string ){
        let cookies = this.loadCookie();
        for( let mi of cookies){
            if(mi.name == cookieString ){
                return mi.value;
            }
        }
        return false;
    }
}

const Services = {

    init(){
        this.construct();
        let {list,EditBox,ServiceForm,delLink} = this.props;
        let _hide = "collapse";
        let list_item = list.find(".list-group-item");

        function getParent(x){
            return $(x).parents('.list-group-item');
        }

        list_item.map(function () {
            $(this).removeClass(_hide);
        });

        list.delegate("button[data-edit]","click",function(evt){
            let _parent = getParent(this),
                _xerox  = _parent.attr("data-xerox");
                showLoader(evt);

                EditBox.load( AppConfig.Host + "services/" + _xerox ,function(sonct,xhr){
                removeLoader();
                let image = new ImageEdit('input[name=imageColl]','.drag-drop');

                ServiceForm.addClass(_hide);
                EditBox.scrollTo(100);
            });
        });

        list.delegate("button[data-del]","click",function(){
            let parentList = getParent(this),
                _serviceXerox = parentList.attr("data-xerox"),
                _confirm = confirm("Delete ?");

                if(_confirm)
                    $.post(delLink,{_token: Laravel.csrfToken , activity: "services",serviceXerox:_serviceXerox},(xhr)=>{
                        if( xhr.code === true ){
                            alert("Deleted");
                            parentList.addClass(_hide);
                        }
                    },"json");
        });
    },
    construct(){
        this.props = {
            list : $("#service-list"),
            EditBox : $('#edit-services'),
            ServiceForm : $("#register__service"),
            delLink : AppConfig.Host + "services/delete"
        }
    }
}

const AdvertsPage = {

    init (){
        this.constructor();
        const _hide = "collapse";
        const {Form,EditPanel,EditPage,AdvertBox,panelBody,AdvertBoxes} = this.props;

        AdvertBox.find("button[data-collapse]").click(function(){
            let i = $(this).find("i"),
                rm = i.attr("data-toggle");

            if(Form.hasClass(_hide)){
                i.removeClass(rm);
                Form.removeClass(_hide);

                if(window.innerWidth > 1200 ){
                    AdvertBox.removeClass("expand");
                }

            }else {
                i.addClass(rm);
                Form.addClass(_hide);
                if(window.innerWidth > 1200){
                    AdvertBox.addClass("expand");
                }
            }
        });
        AdvertBoxes.delegate('[data-edit]', 'click', function(evt){
            
            if ( evt.ctrlKey == true ){

                evt.preventDefault();
                evt.stopPropagation();

                let xerox = $(this).attr("data-edit");
                let link = $(this).attr("href");

                EditPanel.removeClass(_hide);
                EditPanel.load( link , showLoader );
                EditPanel.scrollTo(89);

            }
            
        });

        AdvertBox.delegate('[data-del]','click',function (evt) {
            let _self = $(this),
                _xerox  = _self.attr("data-del"),
                _advertPreview = _self.parents(".adverts-preview");

            if(confirm("Delete?")) {

                showLoader(evt);

                $.post(EditPage + "/delete" , {advertXerox: _xerox, deleteSubmit: 10, _token : window.Laravel.csrfToken }, function (xhr) {
                    if (xhr.code === 200) {
                        _advertPreview.addClass("collapse");
                        removeLoader();
                        EditPanel.addClass('_hide');
                    }
                });
            }
        });
        EditPanel.delegate("span[data-collapse]","click",function(){
            EditPanel.addClass(_hide);
        });

    },
    constructor(){
        let Form  =  $("#add_advert"),
            AdvertBox = $("#advert-box"),
            editPanel = $("#edit-advert"),
            {pathname,hash} = window.location;

        this.props = {
            Form : Form,
            EditPage : pathname  ,
            AdvertBox : AdvertBox,
            EditPanel  : editPanel,
            AdvertBoxes : AdvertBox.find(".panel"),
        }
    },
}

const Account = {
    init(){
        this.construct();
        const { BasicPanel,PPanel,UpdatePanel,ModifyBtn,ChangePass} = this.props;
        if(BasicPanel.length !== 0){
            ModifyBtn.click(function(){
                UpdatePanel.toggleClass("collapse").scrollTo(100);
            });
            ChangePass.click(function(){
                PPanel.toggleClass("collapse").scrollTo(100);
            });
        }
        console.log('working on the Account panel');
    },
    construct(){
        this.props = {
            BasicPanel : $('#account__profile'),
            PPanel : $("#change__password"),
            UpdatePanel : $("#account__update"),
            ModifyBtn : $("#modify"),
            ChangePass : $("#ch-pwd"),
        }
    }
}

function showLoader(e : Event, b : string = "failed") {
    if(arguments.length < 2 ){

        console.log('just a simple loader');

    }else{

        console.log('hide loader');
        if(b == "success"){
            console.log('hiding with success');
        }else{
            console.warn('hiding with error');
        }
    }
}

function removeLoader(){
    console.log("removing loader");
}

$(function () {
    menuButton.init();
    Services.init();
    AdvertsPage.init();
    NavBar.init();
    Account.init();
});