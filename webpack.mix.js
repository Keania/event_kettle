const mix = require('laravel-mix');
let source = "resources/assets/sass/";

mix.sass(source + 'events.scss', 'public/css').sourceMaps();
mix.js('resources/assets/js/app.js', 'public/js').version();
// mix.js('resources/assets/js/blog.js', 'public/js').version();
mix.js('resources/assets/js/cm-dashboard.js', 'public/js').version();
mix.sass(source + 'blog.scss', 'public/css');
mix.sass(source + 'cm-account.scss', 'public/css');

// mix.sass(source + 'cm-register.scss', 'public/css');
// mix.sass(source + 'cm-admin.scss','public/css');
mix.sass(source + 'cm.scss', 'public/css');
mix.sass(source + 'cm-services.scss', 'public/css');

// mix.sourceMaps();
// mix.disableNotifications()

//compiles javascript
// mix.js('resources/assets/js/admin.js', 'public/js').sourceMaps();
//
//.scripts /* Vanilla JS */
//.react  /* React JS */
mix.browserSync({ proxy: "http://localhost:70" }) /* Live Preview */
    //.sourceMaps() /* Attach Source codes */
    //,disableNotifications() /* Disable Notifications */