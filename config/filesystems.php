<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Here you may specify the default filesystem disk that should be used
    | by the framework. The "local" disk, as well as a variety of cloud
    | based disks are available to your application. Just store away!
    |
    */

    'default' => 'local',

    /*
    |--------------------------------------------------------------------------
    | Default Cloud Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Many applications store files both locally and in the cloud. For this
    | reason, you may specify a default "cloud" driver here. This driver
    | will be bound as the Cloud disk implementation in the container.
    |
    */

    'cloud' => 's3',

    /*
    |--------------------------------------------------------------------------
    | Filesystem Disks
    |--------------------------------------------------------------------------
    |
    | Here you may configure as many filesystem "disks" as you wish, and you
    | may even configure multiple disks of the same driver. Defaults have
    | been setup for each driver as an example of the required options.
    |
    | Supported Drivers: "local", "ftp", "s3", "rackspace"
    |
    */

    'disks' => [

        'local' => [
            'driver' => 'local',
            'root' => storage_path('app'),
        ],

        'public' => [
            'driver' => 'local',
            'root' => storage_path('app/public'),
            'url' => env('APP_URL').'/storage',
            'visibility' => 'public',
        ],

        'event_images' => [
            'driver' => 'local',
            'root' => storage_path('app/public'),
            'url' => env('APP_URL').'/storage/events/',
            'visibility' => 'public',
        ],

        'cover_images' => [
            'driver' => 'local',
            'root' => storage_path('app/public/covers'),
            'url' => env('APP_URL').'/storage/covers/',
            'visibility' => 'public',
        ],
        
        'admin_photos' => [
            'driver' => 'local',
            'root' => storage_path('app/public/uploads/admin'),
            'url' => env('APP_URL') . '/storage/uploads/admin',
            'visibility' => 'public',
        ],
        'graphics' => [
            'driver' => 'local',
            'root' => storage_path('app/public/uploads/graphics-portfolio'),
            'url' => env('APP_URL') . '/storage/uploads/graphics-portfolio',
            'visibility' => 'public',
        ],
        'postUploads' => [
            'driver' => 'local',
            'root' => storage_path('app/public/uploads'),
            'url' => env('APP_URL') . '/storage/uploads',
            'visibility' => 'public',
        ],

        'service_images' => [
            'driver' => 'local',
            'root' => storage_path('app/public/services'),
            'url' => env('APP_URL').'/storage/services/',
            'visibility' => 'public',
        ],

        'user_images' => [
            'driver' => 'local',
            'root' => storage_path('app/public/users'),
            'url' => env('APP_URL').'/storage/users/',
            'visibility' => 'public',
        ],

        's3' => [
            'driver' => 's3',
            'key' => env('AWS_KEY'),
            'secret' => env('AWS_SECRET'),
            'region' => env('AWS_REGION'),
            'bucket' => env('AWS_BUCKET'),
        ],

    ],

];
