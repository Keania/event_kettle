<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;
    $firstname =  $faker->firstName;
    $lastname =  $faker->lastName;
    $username = strtolower($firstname ." ". $lastname);

    return [
    	'username' => $username,
        'fname' => $firstname,
        'lname' => $lastname,
        'phone' => $faker->phone,
        'gender' => $faker->gender,
        'active' => $faker->boolean(),
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});
