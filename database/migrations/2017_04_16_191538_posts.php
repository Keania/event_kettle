<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Posts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('label',100)->unique();
            $table->string('slug',100)->unique();
            $table->longText('content');
            $table->tinyInteger('cat_id',false,true);
            // $table->foreign('cat_id')->references('id')->on('category')->onDelete('cascade');
             
            $table->integer('author');
            // $table->foreign('author')->references('id')->on('admins');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('post');
    }
}
