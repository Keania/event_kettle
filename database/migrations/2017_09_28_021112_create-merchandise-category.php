<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMerchandiseCategory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
            Schema::create('shop_category', function (Blueprint $table) {
                $table->increments('id');
                $table->string('name');
                $table->string('slug',30);
                $table->string('icon');
                $table->boolean('cat_parent');
                $table->integer('sub_cat',false,true);
                $table->tinyInteger('position',false, true)->default(NULL);
                $table->timestamps();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
