<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEvent extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title',77);
            $table->string('slug');
            $table->string('cover_image')->nullable();
            $table->string('short_desc', 70);
            $table->dateTime('time');
            $table->integer('owner', false);
            $table->string('venue', 255);
            $table->integer('category', false)->default(0);
            $table->longText('description');
            $table->boolean('show');
            $table->timestampsTz();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
