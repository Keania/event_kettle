<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BlogCategory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blog_category', function (Blueprint $table) {
            $table->tinyIncrements('id');
            $table->string('name',20)->unique();
            $table->string('icon',70);
            $table->boolean('position')->default(NULL);
            $table->boolean('inferior');
            $table->tinyInteger('superior')->unsigned()->nullable();
            $table->foreign('superior')->references('id')->on('blog_category')->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('category');
    }
}
