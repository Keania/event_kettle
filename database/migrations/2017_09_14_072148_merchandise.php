<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Merchandise extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('merchandise', function (BluePrint $table) {

            $table->increments('id');
            $table->string('name', 255);
            $table->string('image');
            $table->integer('owner', false, true);
            $table->tinyInteger('category')->default('0');
            $table->double('price', 20, 2)->default('0.00');
            $table->integer('quantity')->default(1);
            $table->timestamps();

            $table->tinyInteger('visible')->default(0);
            $table->longText('description');
            $table->string('taxonomy')->nullable();
            // KEY `adverts_user_fk` (`owner`),
            // CONSTRAINT `adverts_user_fk` FOREIGN KEY (`owner`) REFERENCES `users` (`id`)
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
