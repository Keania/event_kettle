<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Comments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('email',100);
            $table->longText('comment');
            $table->bigInteger('post',false,true);
            $table->boolean('reply',1);
            $table->bigInteger('reply_target',false,true)->nullable();
            $table->foreign('post')->references('id')->on('posts');
            $table->foreign('reply_target')->references('id')->on('comments');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments');
    }
}
