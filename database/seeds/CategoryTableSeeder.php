<?php

use Illuminate\Database\Seeder;
use App\Blog\Category;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Category::truncate();
        //
        $faker = Faker\Factory::create();
        $categories = [
            'Fashion' => 'zmdi zmdi-collection-item',
            'Gossip' => 'zmdi zmdi-thumb-down' ,
            'Top Events' => 'zmdi zmdi-cake',
            'Fun Stuff' => 'zmdi zmdi-triangle-down'
        ];
        $x = 0;
        foreach( $categories as $key => $icon ):
            ++$x;
            Category::create([
                'name' => $key,
                'icon' => $icon,
                'inferior' => false,
                'position' => $x,
                'superior' => null,
                'created_at' => $faker->dateTime('now'),
                'updated_at' => $faker->dateTime('now'),
            ]);

        endforeach;
    }   
}
