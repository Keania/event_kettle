<?php

use Illuminate\Database\Seeder;

class CommentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $faker = Faker\Factory::create();
        $limit = 25;
        $posts = DB::table('posts')->pluck('id')->toArray();
        $reply = false;
        $oncomment = null;
        $comments = DB::table('comments')->pluck('id')->toArray();

        for($i=0; $i < $limit; $i++){

            if( sizeof($comments) > 10 || $i > 10) {

                $comments = DB::table('comments')->pluck('id')->toArray();

                $reply = $faker->randomElement([true,false,false]);

                if($reply === true) {

                    $oncomment = $faker->randomElement($comments);

                }

            }

        	DB::table('comments')->insert([
        		 'post'=>$faker->randomElement($posts),
        		 'email'=> $faker->unique()->freeEmail,
                 'comment'=>$faker->realText(200, 2),
                 'reply' => $reply,
                 'reply_target' => $oncomment,
                 'created_at' => $faker->dateTime($max = 'now'),
                 'updated_at' => $faker->dateTime($max = 'now')
        	]);

        }
    }
}
