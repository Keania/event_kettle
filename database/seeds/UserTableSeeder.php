<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\User::truncate();

        $faker = Faker\Factory::create();

        foreach(range(0,30) as $user) {

            $firstname =  $faker->firstName;
            $lastname =  $faker->lastName;
            $username = strtolower($firstname .".". $lastname);

            App\User::create([
                'username' => $username,
                'phone' => $faker->phoneNumber,
                'active' => $faker->boolean(),
                'email' => $faker->unique()->safeEmail,
                'password' => @$password ?: $password = bcrypt('secret'),
                'remember_token' => str_random(10),
            ]);
        }

    }
}
