<?php

use Illuminate\Database\Seeder;
use App\Shop\ServiceCategory;

class ServiceCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ServiceCategory::truncate();
        $values = [
            'Disc Jocky',
            'Catering',
            'Stage Lighting',
            'Make ups',
            'Stage Designing',
            'Media'
        ];
        $faker = Faker\Factory::create();

        foreach ($values as $label => $value) {
            ServiceCategory::create([
                'label' => $value,
                'slug' => str_slug($value),
                'description' => $faker->sentence(13),
            ]);
        }
    }
}
