<?php

use Illuminate\Database\Seeder;

class EventsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Clients\Events::truncate();

        $faker = Faker\Factory::create();

        foreach(range(0,30) as $user) {
            $title = $faker->unique()->sentence(4);
            $slug= str_slug($title);
            $desc = $faker->paragraph(30,3);
            $time = new \Carbon\Carbon('next month');
            $short_desc = \Illuminate\Support\Str::words($desc, 5);
            $users = \App\User::all()->pluck('id')->toArray();
            App\Clients\Events::create([
                'title'  => $title,
                'slug' => $slug,
                'cover_image' =>null,
                'short_desc' => $short_desc,
                'description' => $desc,
                'owner' => $faker->randomElement($users),
                'time' => $time->toDateString(),
                'venue' => $faker->address,
                'show' => $faker->boolean
            ]);
        }

    }
}
