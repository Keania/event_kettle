<?php

use Illuminate\Database\Seeder;
use App\Blog\Post;

class PostTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Post::truncate();

        $faker = Faker\Factory::create();
        $limit = 15;

        $authors = DB::table('admins')->pluck('id')->toArray();
        $category = DB::table('category')->pluck('id')->toArray();

        foreach( range(1,40) as $index => $value ) {

            $postTitle = $faker->unique()->sentence;

        	DB::table('posts')->insert([
        		'author'=> $faker->randomElement($authors),
                'cat_id'=> $faker->randomElement($category),
        		'label'=> $postTitle,
                'content'=> $faker->paragraphs(4,12),
                'slug'=> str_slug($postTitle),
                'created_at' => $faker->dateTime($max = 'now'),
                'updated_at' => $faker->dateTime($max = 'now'),
    		]);

        }
    }
}
