<?php

use Illuminate\Database\Seeder;
use App\Shop\Category;

class ShopCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         Category::truncate();
        //
        $faker = Faker\Factory::create();
        $categories = [
            'Shirts' => 'zmdi zmdi-collection-item',
            'Accessories' => 'zmdi zmdi-thumb-down' ,
            'Gadgets' => 'zmdi zmdi-cake',
            'Cup Cakes' => 'zmdi zmdi-cake',
            'Caps' => 'zmdi zmdi-triangle-down'
        ];
        $x = 0;
        foreach( $categories as $key => $icon ):
            ++$x;
            Category::create([
                'name' => $key,
                'icon' => $icon,
                'slug' => str_slug($key),
                'cat_parent' => false,
                'position' => $x,
                'sub_cat' => 0,
                'created_at' => $faker->dateTime('now'),
                'updated_at' => $faker->dateTime('now'),
            ]);
        endforeach;
    }
}
