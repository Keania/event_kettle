<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       $this->call(CategoryTableSeeder::class);
       $this->call(ShopCategorySeeder::class);
       $this->call(ServiceCategorySeeder::class);
       $this->call(PostTableSeeder::class);

    }
}
