<?php

use Illuminate\Database\Seeder;

class AdvertSeederTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::raw("ALTER TABLE adverts ADD COLUMN quantity int(11) unsigned default 1 ");
    }
}
