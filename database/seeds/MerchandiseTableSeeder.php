<?php

use App\User;
use App\Shop\Category;
use App\Clients\Merchandise;
use Illuminate\Database\Seeder;

class MerchandiseTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Merchandise::truncate();

        $faker = Faker\Factory::create();
        $categories = Category::all()->pluck(['id'])->toArray();
        $users = User::all()->take(5)->pluck(['id'])->toArray();
        $taxo = ['shoes','hats','bands','wrist-wears','clothes','glasses','horns'];

        foreach (range(1, 40) as $index)
        {
            Merchandise::create([
                'name' => $faker->sentence(7),
                'image' => "20170313.Phlaq.jpg",
                'owner' => $faker->randomElement($users),
                'description' => $faker->paragraph,
                'price' => $faker->numberBetween(200, 5000),
                'visible' => $faker->boolean,
                'category' => $faker->randomElement($categories),
                'quantity' => $faker->numberBetween(1, 40),
                'taxonomy' => $faker->randomElement($taxo),
            ]);
        }
    }
}
