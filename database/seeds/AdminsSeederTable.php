<?php

use App\Admin;
use Illuminate\Database\Seeder;

class AdminsSeederTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Admin::truncate();
        $faker = Faker\Factory::create();
        $password = "Tomclancy12";

        $firstname =  $faker->firstName;
        $lastname =  $faker->lastName;
        $username = $firstname ." ". $lastname;

        foreach (range(0,10) as $index) {
            # code...
            DB::table('admins')->insert([
                'firstname' => $firstname,
                'lastname' => $lastname,
                'username' => $username,
                'password' => bcrypt($password),
                'privilege' => $faker->randomElement([1,2,3]),
                'active' => $faker->boolean(),
                'picture' => 'heoinagoin.jpg',
                'description' => $faker->text(100),
          ]);
        }
    }
}
