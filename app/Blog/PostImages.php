<?php

namespace App\Blog;

use Illuminate\Database\Eloquent\Model;

class PostImages extends Model
{
	public $table = 'post_images';

	public $fillable = ['name','post_id','mime_type'];

    public function post(){
    	return $this->belongsTo('App\Blog\Post','id');
    }
}
