<?php
namespace App\Blog;


use App\Blog\Category;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    //restrict columns for modifyin
    protected $fillable = ['slug','label','content','author'];
    protected $gauarded =[];
    //return all the comments on a post

    public function comments(){
    	return $this->hasMany('App\Blog\Comment','post');
    }

   //return the author of the post
    public function getAuthor() {
    	return $this->belongsTo('App\Admin','author');
    }

    public function category() {
    	return $this->belongsTo('App\Blog\Category','cat_id');
    }

    public function images(){
        return $this->hasMany('App\Blog\PostImages','post_id');
    }

    public static function getLink($post){

        $slug = $post->slug;
        $category = $post->category->name;
        return route('blog') . "/" . str_slug($category) . "/{$slug}";

    }
}
