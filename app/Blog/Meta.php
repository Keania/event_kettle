<?php
namespace App\Blog;

Trait Meta {

	public function metas(){
    	$meta = [
			['content' => is_null($result['post_editor']) ? 'Wale Princewill' : $user , 'name' => 'author'],
			['content' => $content, 'property' => 'og:description' ],
			['content' => 'News, Politics, Sports, Advertisement ', 'name' => 'keywords' ,],
			['content' => $category." News", 'name' => 'description' ],
			['content' => "http://247blogee.com/".imageOne($result['post_image']), 'property' => 'og:image'],
			['content' => '247blogee' ,'property' => 'og:site_name', ],
			['content' => $result['title'],'property' => 'og:title']
		];
    }

    protected function addMeta() {

		$handler = (isset($this->meta)) ? $this->meta : false;

		if(!$handler){
			return 'died';
		}
		
		!is_array($handler) ? die('not Array') : null;
		
		$metaRecord = '';
		foreach($handler as $eachMeta){
			$tag = '<meta ';
			foreach( $eachMeta as $attrib => $value){
				$tag.=$attrib."=\"".$value."\" ";
			}
			$tag.="/>\n\t";
			$metaRecord.=$tag;
		}
		return $metaRecord;

	}
}