<?php

namespace App\Blog;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{

    //restricted
    protected $guarded =[];

    protected $table = 'comments';

    //User who commented
    public function post() {
    	return $this->belongsTo('App\Blog\Post','post');
    }

    public function children() {
    	return $this->hasMany("App\Blog\Comment","id");
    }

    public function parent() {
    	return $this->belongsTo("App\Blog\Comment","reply_target");
    }

}