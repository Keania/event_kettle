<?php

namespace App\Blog;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
/**
*@param
*/
class Category extends Model
{

	use SoftDeletes;

    protected $table='blog_category';

	protected $dates = ['deleted_at'];

    public function posts(){
    	return $this->hasMany('App\Blog\Post','cat_id');
    }


}
