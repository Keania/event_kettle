<?php

namespace App\Clients;

use Illuminate\Database\Eloquent\Model;

class ServiceImages extends Model
{
    //
    protected $table = 'service_images';

    protected $fillable = ['name','serviceID'];

    protected $guarded = ['created'];

    const CREATED_AT = 'created';

    public function service(){
    	return $this->belongsTo('App\Clients\Service','id');
    }
}
