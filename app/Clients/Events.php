<?php

namespace App\Clients;

use App\Clients\UserProduct;
use Illuminate\Database\Eloquent\Model;

class Events extends Model implements UserProduct
{
//    protected $fillable = ['label','image','owner','description','price','taxanomy'];

    protected $guarded = ['created_at'];

    public function user(){
        return $this->belongsTo('App\User','owner');
    }

    public function cat(){
    	return $this->belongsTo("App\Shop\Category","category");
    }

    public static function getModel(){
        return 'AdvertModel';
    }

}
