<?php

namespace App\Clients;

use App\Clients\UserProduct;
use Illuminate\Database\Eloquent\Model;

class Service extends Model implements UserProduct
{
    protected $table = 'service_record';

    protected $fillable = ['label','category','cover_image','description','userID'];

    protected $guarded = ['created'];

    const CREATED_AT = 'created';

    public function owner()
    {
        return $this->belongsTo("App\User", "userID");
    }

    public function category()
    {
    	return $this->belongsTo("App\Shop\ServiceCategory", "category");
    }

    public function photos()
    {
        return $this->hasMany("App\Clients\ServiceImages","serviceID");
    }

    public static function getModel()
    {
        return 'ServiceModel';
    }
}
