<?php
namespace App\Clients;

use App\Shop\Category;
use Illuminate\Database\Eloquent\Model;

class Merchandise extends Model
{
    public $table = 'merchandise';

    public $fillable = ['name','quantity','price','owner','image','taxonomy','description'];
    public $guard = ['created_at','updated_at'];

    public function owner()
    {
        return $this->hasMany(App\User::class, 'owner');
    }

    public function cat()
    {
        return $this->belongsTo(Category::class, 'category');
    }
}
