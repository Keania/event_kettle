<?php

namespace App;

use App\Person;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements Person
{
    use Notifiable;
    /**
     * The attributes that are mass assignable.
     *
     * @var arra
     */
    protected $table = 'users';

    protected $fillable = [
        'phone','username',
        'email','password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected function events()
    {
        return $this->hasMany('App\Clients\Events', 'owner');
    }

    public function merchandise()
    {
        return $this->hasMany('App\Clients\Merchandise', 'owner');
    }

    public function services()
    {
        return $this->hasMany('App\Clients\Service', 'userID');
    }
}
