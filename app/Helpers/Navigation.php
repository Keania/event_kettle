<?php
/**
 * Created by PhpStorm.
 * User: Joseph
 * Date: 01/01/2017
 * Time: 12:40
 */
namespace App\Helpers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class Navigation {

	public static function layout($name,Array $info = []) {

	    $link = @$info[0];
	    $icon = @$info[1];
	    $id   = @$info['id'] ? "id='" . @$info['id'] . "'" : "";

	    $ic   = preg_match('/^fa/',$icon) ? 'fa' : 'zmdi';
	    $styleClass = ( $link == $path = request()->fullUrl() ) ? "class='active'" : "";

	    echo (
	        "<li {$id} {$styleClass} >
	        	<a title='{$name}' href='$link'>
		            <span class='{$ic} $icon icon'></span>
		            <span>$name</span>
	            </a>
	        </li>"
	    );

	}

}


