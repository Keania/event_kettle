<?php

    namespace App\Shop;

    use Illuminate\Support\Collection;

    class Meta
    {

        public $title = null;

        public $url = null;

        public $type = null;

        public $description = null;

        public $image = null;


        /** @var array */
        private static $collection;


        public function __construct($title, $url, $type = "", $description, $image = "")
        {

            $this->url = $url;
            $this->type = $type;
            $this->title = $title;
            $this->image = $image;
            $this->description = $description;

        }

        public static function push(Meta $value)
        {
            array_push(static::$collection,$value);
        }

        public function metaSample()
        {
            ?>
            <!-- You can use Open Graph tags to customize link previews.
                Learn more: https://developers.facebook.com/docs/sharing/webmasters -->
            <meta property="og:url" content="http://www.your-domain.com/your-page.html"/>
            <meta property="og:type" content="product"/>
            <meta property="og:title" content="Product"/>
            <meta property="og:description" content="Your description"/>
            <meta property="og:image" content="http://www.your-domain.com/path/image.jpg"/>
            </head>
            <?php
        }

        public function preview()
        {

        }

        protected function buildMeta()
        {

            $handler = $this->toArray();
            $metaRecord = '';

            foreach ($handler as $eachMeta) {
                $tag = '<meta ';
                foreach ($eachMeta as $attrib => $value) {
                    $tag .= $attrib . "=\"" . $value . "\" ";
                }
                $tag .= "/>\n\t";
                $metaRecord .= $tag;
            }

            return $metaRecord;

        }
    }
