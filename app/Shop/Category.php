<?php

namespace App\Shop;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\HtmlString;

class Category extends Model
{
    protected $table = "shop_category";

    protected $fillable = ['name','icon','cat_parent','sub_cat','position'];

    protected $guarded = ['created'];

    public function adverts()
    {
        return $this->hasMany('App\Clients\Events', 'category');
    }

    public static function pick_icon($iconName)
    {
        return new HtmlString("<i class='{$iconName}'></i>");
    }

    /**
     * Generates a select input for the advert categories
     *
     * @param $class string
     * @param $match string
     *
     * @return Illuminate\Support\HtmlString
     **/
    public static function selection($name = "cat_selection",$model="", $match = "", $class_name = "")
    {
        $model = !empty($model) ? "v-model='{$model}'" : '';

        $select = "<select required class='form-control {$class_name} ' name='{$name}' $model>";
        $select.= "<option disabled selected value='0'>Choose Category...</option>";
        foreach (self::all() as $category) {
            $attr = (((int) $match) === $category->id) ? "selected" : "";
            $select .= "<option {$attr} value=\"{$category->id}\">{$category->name}</option>";
        }
        $select .= "</select>";

        return new HtmlString($select);
    }
}
