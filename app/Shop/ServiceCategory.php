<?php

namespace App\Shop;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\HtmlString;

class ServiceCategory extends Model
{
    //
    protected $table = 'service_category';

    protected $primaryKey = 'id';
 
    protected $fillable = ['label','slug','description'];

    protected $guarded = ['created_at'];

    public function services(){
    	return $this->hasMany('App\Clients\Service','id');
    }

    /**
    *genearates a select input for the advert categories 
    *@param String $class 
    *@param Integer $id
    *@return Illuminate\Support\HtmlString 
    **/
    public static function selection($form_name = "service_selection",$class = "",$match = "") {

        $select = "<select class='{$class}' name='{$form_name}'>";
        $select.= "<option value='0'>Choose Category...</option>";
        foreach ( self::all() as $serviceCat ){

            $attr = ( $match  == $serviceCat->label ) ? "selected" : "";
            $select .= "<option {$attr} value='{$serviceCat->id}'>{$serviceCat->label}</option>";

        }
        $select .= "</select>";

        return new HtmlString($select);
    }

}
