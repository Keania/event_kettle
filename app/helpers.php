<?php

use Illuminate\Support\Str;
use Illuminate\Support\HtmlString;
use Illuminate\Container\Container;
use Illuminate\Support\Facades\Storage;
use Illuminate\Contracts\Bus\Dispatcher;
use Illuminate\Contracts\Auth\Access\Gate;
use Illuminate\Contracts\Routing\UrlGenerator;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\Auth\Factory as AuthFactory;
use Illuminate\Contracts\View\Factory as ViewFactory;
use Illuminate\Contracts\Cookie\Factory as CookieFactory;
use Illuminate\Database\Eloquent\Factory as EloquentFactory;
use Illuminate\Contracts\Validation\Factory as ValidationFactory;
use Illuminate\Contracts\Broadcasting\Factory as BroadcastFactory;
use App\Shop\Category as CatModel;
use App\Person;


function getImage($disk,$imageName){
	return Storage::disk($disk)->url($imageName);
}

function fullName($person){

	if($person == null) return "";
	switch (get_class($person)) {
		case 'App\User':
			return ucfirst(@$person->fname) ." " .ucfirst($person->lname) ;
			break;
		case 'App\Admin':
			return ucfirst(@$person->first_name) ." " .ucfirst($person->last_name) ;
			break;
		default:
			return "";
	}
}

function advert_selection($cls = "", $same = ""){
	return CatModel::selection($cls, $same);
}

function getAsset($filename){	
	return asset('images/assets/' . $filename );
}


function getUserImage($imageName){

	$file = Storage::disk('user_images')->url($imageName);
	$imageExist = Storage::disk('user_images')->exists($imageName);

	return $imageExist ? $file : getAsset('user.svg');

}

function getAdvertImage($imageName){

	$file = Storage::disk('advert_images')->url($imageName);
	$imageExist = Storage::disk('advert_images')->exists($imageName);

	return $imageExist ? $file : getAsset('no-advert.svg');

}

function icon($name,$tag = "i"){
	$iconname = ($name[0] === "f" ? "fa" : "glyphicon" ) . " {$name}";
	$content = "<{$tag} class='{$iconname}'></{$tag}>";
	return new HtmlString($content);
}