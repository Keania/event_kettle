<?php

namespace App\Transformers;

use Storage;
use App\Shop\Category;

class MerchandiseTransformer extends Transformer
{
    public function transform($item)
    {
        return [
            'id' => (int) $item['id'],
            'name' => $item['name'],
            'owner' => $item['owner'],
            'taxonomy' => $item['taxonomy'],
            'price' => (float) $item['price'],
            'visible' => (bool) $item['visible'],
            'description' => $item['description'],
            'quantity' => (int) $item['quantity'],
            'image' => getAdvertImage(Storage::disk('advert_images')->url($item['image'])),
            'category' => Category::find($item['category'])->first()->name
        ];
    }
}
