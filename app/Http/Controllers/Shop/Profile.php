<?php

namespace App\Http\Controllers\Shop;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class Profile extends Controller
{
    //

    /**
     * loads the user profile page 
     *
     * @return View
     * @author 
     **/
    public function index(Request $request, $username){
    	$owner = User::where('username',"=", $username)->first();
    	return view('shop.profile.index', [ 'owner' => $owner ]);
    }

    
}
