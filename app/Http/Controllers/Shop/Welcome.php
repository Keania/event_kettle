<?php

namespace App\Http\Controllers\Shop;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class Welcome extends Controller
{
    //

    public function index(){
    	return view('shop.welcome',['title' => 'Welcome to eCMARKET']);
    }

}
