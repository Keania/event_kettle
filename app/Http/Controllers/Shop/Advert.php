<?php

namespace App\Http\Controllers\Shop;

use App\Clients\Events as AdvertModel;
use App\Shop\Category;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class Advert extends Controller
{
    
    /**
    * fetches the data for the selected adver
    *@param Illuminate\Http\Request $request
    *@param (int) $id 
    */
    public function show(Request $request, $category, $id){

    	$advert = AdvertModel::find($id);

        if($advert->user === null){
            $advert->user = User::find($advert->owner);
        }

	    $advert->user->address = route('user.profile', [
           'id' => $advert->user->username ,
        ]);

    	return view('shop.advert.single-page', [
    	   'advert' => $advert,
    	   'owner' => $advert->user,
        ]);

    }

    public function category(Request $request){
    	$categories = Category::all();
    	$fetch = DB::table('adverts')->limit(12)->pluck('id')->toArray();
        $adverts = AdvertModel::find($fetch);

    	return view('shop.advert.category',[
            'title' => "Select a Category",
            'categories' => $categories,
            'adverts' => $adverts,
        ]);

    }

    public function single(Request $request, $categoryName ) {

    	foreach( Category::all(['id','name']) as $category ) {
    		if( strtolower(str_slug($category->name)) == $categoryName ){
    			$valid = (int) $category->id;
    		}
    	}

    	if(@$valid):
    	
	    	$category = Category::find($valid);
			$label = (string) ucfirst($category->name);
			$fetch = DB::table('merchandise')->where(['category' => $valid ]);
            $adverts = AdvertModel::find($fetch->pluck('id')->toArray());

	    	return view('shop.advert.category-page',[
                'category' => $category,
	            'adverts' => $adverts,
                'paginate' => $fetch->paginate(3),
	        ]);

        else:

        	abort(404);

	    endif;
    }

}
