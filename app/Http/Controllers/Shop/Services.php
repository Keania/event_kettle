<?php

namespace App\Http\Controllers\Shop;

use Illuminate\Http\Request;
use App\Clients\Service;
use App\Shop\ServiceCategory as SC;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;
use App\Http\Controllers\Controller;

class Services extends Controller
{
    //
    public function index(Request $request){

    	$serviceCat = SC::all();
    	$services = DB::table('service_record')->limit(15)->pluck('id');
    	$services = static::addLinks(Service::find($services->toArray()));

    	return view('shop.services.home',[
    	    'serviceCategories' => $serviceCat, //service categories
    	    'services' => $services , //top/sponsored service cards.
	    ]);

    }

    public function category(Request $request, $category){

    	$categories = static::addCategoryLink(SC::all(['label','id']));
    	foreach($categories as $cat){

    		if( str_slug($cat->label) === $category ){

    			$records = static::getServices($cat->label);
    			$paginate = $records[0];
    			$services = $records[1];
    		
		    	return view('shop.services.category',[
    	            'title' => $cat->label . ' - ' . config('APP','eCMarket') ,
    	            'serviceCat' => $cat , 
    	            'services' => $services,
    	            'categories' => $categories,
    	            'paginate' => $paginate
	            ]);

    		}
    	}

		abort(303);

    }

    /**
    * adds a link property to the services categories
    *@param  String $category 
    *@param  String $id 
    *@return Illumniate\Support\Facades\View
    */
   	public function single(Request $request, $category, $id){

   		$service = Service::find($id);

   		if($service):

   			$props = [
   				'title' => $service->label ." - ". config('APP','eCMARKET'),
				'service' => $service,
   			];

	   		return view('shop.services.single')->with($props);

   		else:

   			abort(404);

   		endif;
   	}

   	/**
    * adds a link property to the services
    *@param Illuminate\Support\Collection $services
    *@return Illuminate\Support\Collection
    */
   	public static function addLinks(Collection $services){

   		$services->map(function(Service $service){
			return $service->link = static::makeLinks($service);
   		});

   		return $services;
   	}

    /**
    *fetches all the services in a category
    *@param 
    *@return Array 
    */
    public static function getServices($cat_name = ""){
    
    	$paginate = DB::table('service_record')->where(['category' => $cat_name ])->paginate(25);
    	$identity = $paginate->pluck('id')->toArray();

    	return [ $paginate , static::addLinks(Service::find($identity)) ];

    }

    /**
    * adds a link property to the services categories
    *@param Illuminate\Support\Collection $category
    *@return Illuminate\Support\Collection
    */
    public static function addCategoryLink(Collection $categories){

    	$categories->map(function($category){

    		return $category->link = static::makeCategoryLink($category);

    	});

    	return $categories;

    }

    /**
    * generates the link for the service model once fetched
    *@param App\Clients\Service $service
    *@return String 
    */
    public static function makeLinks(Service $service){
    	return route('service.single',['category' => str_slug($service->category) , 'id' => $service->id ]);
    }

    /**
    * generates the link for the services category model once fetched
    *@param App\Shop\ServiceCategory $category
    *@return String 
    */
    public static function makeCategoryLink(SC $category){
    	return route('service.category',['category' => str_slug($category->label) ]);
    }
}

