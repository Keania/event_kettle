<?php

namespace App\Http\Controllers\Shop;

use App\Clients\Merchandise;
use App\Clients\Service;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class Home extends Controller
{
	/**
	*creates index/home page for adverts and services
	*@return
	*/
   	public function index() {

   		// $adverts = Merchandise::where('visible','=','1')->orderBy('id','desc')->take(25)->get();
         // $services = Service::where('id',"!=",null)->orderBy('id','desc')->take(25)->get();
         // $services->map(function($x){
         //    $x->link = Services::makeLinks($x);
         // });

   		return view('shop.home');

   	}

   	public function terms(){
   		return view('shop.terms');
   	}
}
