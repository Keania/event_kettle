<?php

namespace App\Http\Controllers\Blog;


use App\Blog\Post;
use App\Blog\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    //
    public function index(){
    	//page title
    	$categories = Category::all(['name','icon'])->sort();
        $posts = Post::orderByDesc('id')->paginate(6);

    	return view('blog.index')
    		->with([
    		    'posts' => $posts,
    		    'categories' => $categories
	       ]);
    }
}
