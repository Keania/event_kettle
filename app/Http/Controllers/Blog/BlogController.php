<?php
namespace App\Http\Controllers\Blog;

use App\Blog\Post;
use App\Blog\Meta;
use App\Blog\Category;
use App\Http\Controllers\Controller;

class BlogController extends Controller
{
	use Meta;
}
