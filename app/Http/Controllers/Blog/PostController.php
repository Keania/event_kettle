<?php

namespace App\Http\Controllers\Blog;


use App\Blog\Post;
use App\Blog\Comment;
use App\Blog\Category;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\HtmlString;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Blog\BlogController;
use Illuminate\Support\Facades\Storage;

class PostController extends BlogController
{
    //
    const diskName = 'postUploads';

    function __construct(){
    	$this->storage = Storage::disk(self::diskName);
    } 

    public function index(Request $request, $categori,$slug)
	{

		$catCollection = Category::all();

		foreach($catCollection as $key => $category):

			if($catCollection->isEmpty() || empty($slug)) return abort(404);

			$category = $category;
			if($categori == str_slug($category->name)):
				$slug_cat = $categori == str_slug($category->name);
				
				$post = Post::where(['slug' => $slug, 'cat_id' => $category->id])->first();
				if($post == null ) { return abort(404); }

				$post->content = static::findImages($post->content, $post->images);
		        $this->getMeta($post);
		        if(empty($post->images) == false) {
					$props = static::getImageProps($post->images->first());

					$this->meta[] = ['property' => 'og:image:width', 'content' => $props['width'] . "px" ];
					$this->meta[] = ['property' => 'og:image:height', 'content' => $props['height'] . "px" ];				
		        }

				return view('blog.post',[
			        'post' => $post,
				    'category'=> $category,
				    'metaData' => $this->addMeta(),
		        ]);

	        endif;	
        endforeach;

		return abort(404);
		
	}

	/**finds a post image placeholder 
	*@param $string
	*@param [Illuminate\Support\Collection] $images
	*@return [Illuminate\Support\HtmlString]  string 
	*/
	public static function findImages( $string, $images ){
		
		global $_string;
		$_string = $string;

		$images->map(function($subject,$count){
			global $_string, $mib;
			$disk = Storage::disk(self::diskName);
			$imageProps = self::getImageProps($subject);
			
			$count = $count + 1;
			$search = "{image{$count}}";
			$replace = '<p>
				<figure data-attr=\''.json_encode($imageProps).'\'>
					<img class="img-thumbnail" src="'.( $disk->url( $subject->name ) ).'"/>
				</figure>
			</p>';
			$_string = str_replace($search, $replace, $_string);
			
		});

		return new HtmlString($_string);
	}
    
    private static function getImageProps($image){
    	
    	if(@$image->name !== null){

	        $imageProps = getimagesize(storage_path("/app/public/uploads/".$image->name));
			return [
				'width' => $imageProps[0],
				'height' => $imageProps[1],
			];    
    	}
    	return [
			'width' => 0,
			'height' => 0,
		];  
			
    }
    
	private function getMeta(Post $post){

		$fullname = $post->getAuthor->firstname . ' ' . $post->getAuthor->lastname;
		$imageLink = $this->storage->exists(@$post->images->first()->name)
                    ? $this->storage->url(@$post->images->first()->name) 
                    : asset('images/assets/idea.svg');
        $description = Str::words(static::shortDesc($post->content), 30);

		$this->meta = [
			#general
			['content' => $fullname , 'property' => 'author'],
			['name' => 'referrer', 'content' => 'origin'],
			['name' => 'keywords' , 'content' => 'Technology, Design, Web Development, Graphics'],


			#article metas
			['property' => 'article:published_time', 'content' => @$post->created_at->toAtomString()],
			['property' => 'article:modified_time', 'content' => @$post->updated_at->toIso8601String()],
			['property' => 'article:tag', 'content' => $post->category->name ],

			#twitter metas
			['property' => 'twitter:card', 'content' => "blog" ],
			['property' => 'twitter:title', 'content' => $post->label ],
			['property' => 'twitter:url', 'content' => request()->url()  ],
			['property' => 'twitter:description', 'content' => $description ],

			#open graph metas
			['property' => 'og:url', 'content' =>  request()->url() ],
			['property' => 'og:type', 'content' => 'article'],
			['property' => 'og:title', 'content' =>  $post->label ],
			['property' => 'og:image', 'content' =>  $imageLink],
			['property' => 'og:site_name', 'content' =>  'Wavetech/Creative Blog' ],
			['property' => 'og:description', 'content' => trim($description) ],

		];
	}

	/**
	 * removes the image placeholders ({image1}) from the post content
	 *
	 * @param String $content
	 * @return string
	 * @author Joseph Julius Owonvwon
	 **/
	public static function shortDesc($content){
		
		$content = preg_replace("/{image\d+}/","", $content );
		return strip_tags($content);

	}
}