<?php

namespace App\Http\Controllers\Blog;

use App\Blog\Post;
use App\Blog\Comment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class CommentController extends Controller
{

    //adds a comment
    public function add(Request $request){

        // return $this->fake();

        try{

            $slug = $request->input("slug");
       
            $post = Post::where(['slug' => $slug ])->firstOrFail();
            $saved = $this->addComment($request, $post->id); //array

            if( $saved['success'] )                
                return response()->json($saved);

        }catch(ModelNotFoundException $x){
            return response()->json(['success' => false ]);
        }

    }

    public function fake(){
        return response()->json(['success' => true, 'id' => 12 ]);
    }

    protected function delete(Request $request){

        try {

            $id = (int) $request->id; //id
            $comment = Comment::findOrFail($id);
            $deleted = $comment->delete();
            return response()->json(['success' => $deleted ]);

        } catch( ModelNotFoundException $x ) {
            return response()->json(['success' => false ]);
        }

    }

    protected function addComment($request, $post_id ){

        $comment = new Comment();
        $comment->email = $request->email;
        $comment->comment = urldecode($request->comment);
        $comment->reply = false;
        $comment->reply_target = null;
        $comment->post = $post_id;
        $saved = $comment->save();

        return ["success" => $saved , "id" => $comment->id];
    }

    public function get(Request $request) {

    	var_dump($request->all());

    }


}
    ?>

  
    <?php
