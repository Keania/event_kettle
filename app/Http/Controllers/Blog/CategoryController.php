<?php

namespace App\Http\Controllers\Blog;

use App\Blog\Post;
use App\Blog\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    //
    public function index(Request $request, $category)
	{

		$cats = Category::pluck('name');
		foreach($cats as $key => $value){
			if($category == str_slug($value)){

				$category   = Category::where('name','=',$value)->first();
				$baseLink   = route('blog') ."/" . str_slug($category->name);

				$posts      = Post::where('cat_id','=',$category->id)->orderByDesc('id')->paginate(5,["*"],'page');				

				return view('blog.category',[

					'link'       => $baseLink,
					'posts'      => $posts,
					'category'   => $category,

				]);

			}
		}
		die();

	}

}
