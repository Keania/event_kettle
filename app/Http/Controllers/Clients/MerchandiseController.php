<?php

namespace App\Http\Controllers\Clients;

use App\Clients\Merchandise;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Transformers\MerchandiseTransformer;

class MerchandiseController extends Controller
{
    private $transformer;

    public function __construct(MerchandiseTransformer $transformer)
    {
        $this->transformer = $transformer;
    }


    public function index()
    {
        if (request()->ajax()) {
            $merch = Merchandise::all()->take(10);
            return response()->json($this->transformer->transformCollection($merch->toArray()));
        }
        return view('clients.merchandise');
    }
    /**
    * stores a new Merchanndise for shop
    */
    public function store(Request $request)
    {
        $this->validate($request, [
          'name' => 'string|required',
          'taxonomy' => 'string',
          "quantity" => 'required|integer',
          'category' => 'integer|required',
          'description' => 'string|required|min:20',
          'pricing' => 'required|integer',
        ]);
        dd($this->createMerchandise($request));
    }

    public function destroy($id) {
      if(request()->ajax()) {
        Merchandise::destroy($id);
      }
      return false;
    }

    protected function createMerchandise($request)
    {
        return  Merchandise::create([
              "name" => $request->name,
              "owner" => $request->user()->id,
              "taxonomy" => $request->taxonomy,
              "image" => '20170313.6h9LXVY7Fkb.jpg',
              "quantity" => $request->quantity,
              "category" => $request->category,
              "description" => $request->description,
              "price" => $request->pricing,
              "visible" => 0
            ]);
    }

    public function show($id)
    {
        if (request()->ajax()) {
            return response()->json([
                'data' => $this->transformer->transform(Merchandise::find($id))
            ]);
        }
        return false;
    }
}
