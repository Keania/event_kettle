<?php

namespace App\Http\Controllers\Clients;

use Illuminate\Http\Request;
use App\Clients\Events;
use App\Shop\Category;
use Illuminate\Support\Str as Str;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class EventsController extends Controller
{

	function __construct(){
		$this->storage = Storage::disk('event_images');
		$shopCategories = function($name = "event_category") {
			Category::selection($name);
		};
		view()->share('shopCategories', $shopCategories);
	}
	public function single($id){
	 $event = Events::find($id);
	 return $event;
	}
    public function index(){
		$event = Events::all();
		return $event;
	}
	public function show(Request $request, $id = 0 ) {
		return view('clients.events');
	}

	public function delete($id){

		$event = Events::find($id);
		$this->storage->delete($event->image);

		if( $event->delete() ){

			return response()->json([
				'code' => 200,
			]);
			
		}else{

			return resposne()->json([
            	'code' => 404 ,
        	]);
		}

	}

	public function update(Request $request){

       $event = Events::find($request->id);
        
        #updates the advert
        $event->title = $request->title;
		$event->slug = str_slug($event->title);
        $event->description = $request->description;
		$event->short_desc = Str::words($event->description,5);
        $event->time =$request->time;
        $event->owner = $request->user()->id;
		$event->category = (int) $request->category;
		$event->venue =$request->venue;
		$event->show = 1;
        $response = $event->save() 
        	? [ 'message' => 'Advert Added Successfully' ] 
        	: [ 'error' => 'Unknown Error, please try again later'];
        return back()->with($response);
	}

	public function add(Request $request){
		
	   $event = new Events();
        
        #updates the advert
        $event->title = $request->title;
		$event->slug = str_slug($event->title);
        $event->description = $request->description;
		$event->short_desc = Str::words($event->description,5);
        $event->time =$request->time;
        $event->owner = $request->user()->id;
		$event->category = (int) $request->category;
		$event->venue =$request->venue;
		$event->show = 1;
        $response = $event->save() 
        	? [ 'message' => 'Advert Added Successfully' ] 
        	: [ 'error' => 'Unknown Error, please try again later'];
        return back()->with($response);

	}

	public function ajax(Request $request, $id ){

		if(!( $advert = Events::find($id) ) ){

			return $request->ajax() ? response()->json(['not found']) : abort(404);

		} 

		$cat = CatModel::selection('advert__category',@$advert->cat->cat_id);

		$params = [
			'catSelection' => $cat,
			'xerox' => (int) $id,
			'advert' => Events::find($id),
		];

		return view('clients.edit-advert',$params);
	
	}
	public function images(Request $request){
		$event = Events::find($request->id);
		$this->storage->delete($event->cover_image);
		$path = $this->storage->putFile('images',$request->file);
		$event->cover_image = $path;
		$response = $event->save() ? 'Image added' :'Unknown error occured';

		return $response;

	}

}
