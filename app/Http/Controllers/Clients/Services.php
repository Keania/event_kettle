<?php

namespace App\Http\Controllers\Clients;

use Auth;
use App\Clients\Service;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\HtmlString;
use Illuminate\Support\Collection;
use App\Http\Controllers\Controller;
use App\Shop\ServiceCategory as SC;
use Illuminate\Support\Facades\Storage;

class Services extends Controller
{
    //

    function __construct(){
        $this->storage = Storage::disk('service_images');
        $this->cover_storage = Storage::disk('cover_images');
    }

    public function show(){
    	return view('clients.services');
    }
    
    /**
     * adds a service 
     * @param Request $request
     * @return View()
     * @author Joseph Owonvwon
     **/
    public function add(Request $request){

        $this->validate($request,$this->rules(),$this->messages());

        $title = $request->label;
        $description = $request->description;
        $category = $request->category;

        $coverImage = $this->addCoverImage($request->cover_image,null);
        if( $this->create($title,$description,$coverImage,$category) )
        {
            return redirect()->route('client.services');
        }

        $request->flash();
        return redirect()->back()->withErrors(['message' => 'Service was not created.']);
    }

    public function rules(){
        return [
            'cover_image' => 'file|required',
            'label' => 'string|required|max:30|unique:service_record',
            'description' => 'string|required|',
            'category' => 'numeric|min:1|',
        ];
    }

    public function messages(){
        return [
            'category.min' => 'must have a valid selection.'
        ];
    }

    /**
    *edits the processed date from the service form
    *@param Request $request
    *@return Redirect() redirect()->back()
    */
    public function edit(Request $request){

        global $images;

        if($request->input('put-service') == "delete") {

            if(static::delete($request->_xerox)){
                return redirect()->route('client.services')->with(['message' => 'Delete Successful' ]);
            }else{
                return redirect()->back()->with(['error' => 'Error Deleting Category']);
            }
        }

        $images = collect(json_decode($request->input('imageColl')));
        $service = Service::find($request->_xerox);

        //remove deleted images
        $service->photos->each(function($photo){
            global $images;
            $name = ($photo->name);

            if($images->contains('name','=',$name)){
                #in array
                
            }else{
                #not in array
                $photo->delete(); 
                $this->storage->delete($name);
            }
        });

        $coverImage = $this->addCoverImage($request->cover_image, $service->cover_image);

        if($service === null) return abort(302);
       
        try{

            //update the other data
            $service->label = $request->label;
            if( $coverImage ){            
                $service->cover_image = $coverImage;
            }
            $service->description = $request->description;
            $service->category = SC::find($request->input('service-category'))->label;

            if($service->update()){
                return redirect()->back()->with(['message' => 'update successful.']);
            }else{
                return redirect()->back()->with(['error' => 'There was an error updating this service. please try again later']);
            }

        }catch(QueryException $x){
            return redirect()->back()->with(['error' => $x->getMessage() ]);
        }

    }

    /**
     * ajax delete request
     *
     * @return Redirect redirect()->back()
     * @author Joseph Owonvwon 
     **/
    public function ajaxDelete(Request $request) {

        if($request->ajax()) {

            $state = static::delete($request->id);
            return response()->json(['code' => $state ]);

        }

        return response()->json(['message' => 404]);

    }

    /**
    *shows the edit service form
    *@param Request $request 
    *@param integer $id
    *@return View  
    */
    public function ajaxForm(Request $request,$id ) {
        
    	$service = Service::find($id);

        if( $request->ajax() ) {

	        return view('clients.edit-service',[
                'service' => $service,
                'images' => $this->getImages($service),
            ]);

	    }else{ 

            return view('clients.services',[
                'service' => $service,
                'images' => $this->getImages($service),
            ]);

        }
    }

    /**
     * adds a new cover image and deletes the old
     *
     * @return Boolean
     * @author 
     **/
    private function addCoverImage($newImage, $oldImage){

        if( ($newImage !== null) && $newImage->isValid() ) {

            $filename = date("Ymd") . "." . Str::random(11) . "." . $newImage->extension();
            $this->cover_storage->putFileAs('', $newImage , $filename ); 

            if (!empty($oldImage) ) 
                $this->cover_storage->delete($oldImage); //deletes the old cover photo
            return $filename;
        }
        return false;
    }

    private function create($title,$description,$cover_image,$category){

        $service = new Service();
         //update the other data
        $service->label = $title;
        if( $cover_image ){            

            $service->cover_image = $cover_image;
        }
        $service->userID = Auth::user()->id;
        $service->description = $description;
        $service->category = SC::find($category)->label;
        return $service->save();

    }

    public static function delete($id){

        $service = Service::findOrFail($id);
        
        $this->storage->delete($service->cover_image);
        $state = ( $service->delete() );

        return $state;

    }

    public function getImages( Service $service ){

    	$a = "";
	    foreach( $service->photos as $image ){

	        $imagelink = $this->storage->url( $image->name );
	        
			$a .= "
            <figure class='sr-image-tb thumbnail col-sm-12' 
                style='max-height: 100px;overflow: hidden;max-width: 100px;'>
	            <img src='{$imagelink}' />
	        </figure>";

	    }
	    return new HtmlString($a);
    }
}
