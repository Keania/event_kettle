<?php

namespace App\Http\Controllers\Clients;

use Auth;
use Storage;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class Profile extends Controller
{
    //
    public function index(Request $request ){
    	return view('clients.account',['user' => $request->user() ]);
    }

    /**
    *uploads a file to the server 
    *@param Illuminate|Http|Request $request
    *@return string | null 
    */
    private function changePhoto(Request $request){
        
        $file = $request->profile_pic;

        if( $file !== null ){

            $name = date("Ymd") . "." . Str::random(11) . "." . $file->extension();
            return Storage::disk('user_images')
                ->putFileAs("", $file, $name)  ? $name : null;
            
        }
         
        return null;
    }

    public function update(Request $request){

		$user = $request->user();
    	if(  $request->submit == "save"){

            //store the filename as the user image 
            //if the image was successfully uploaded
            if( null !== $filename = $this->changePhoto($request) ){
                $user->photo = $filename;
            }

    		$user->fname = $request->fname;
    		$user->lname = $request->lname;
    		$user->phone = $request->phone;
    		$user->email = $request->email;
    		$user->department = $request->department;
    		$user->save();

    		return back()->with(['message' => 'Profile update successful ']);
    		$this->confirmEmail();

    	}elseif($request->submit == "delete"){
            $this->deleteAccount();
        }
    }


    public function showDeletePage(){

		return view('clients.remove');

    }

    public function deleteAccount(Request $request){

        
        $this->validate($request,[
            'reason' => 'string',
        ]);

        // return to the user profile page
        if($request->delete == "true") return redirect()->route('client.profile');

        //content format
        $content = $this->now() . PHP_EOL . $request->reason . PHP_EOL . PHP_EOL ;

        if( $request->reason !== null )
            //log users message to the 
            //trucate.log file in the 
            // storage/app/logs/ directory 
            Storage::prepend('logs/truncation.log', $content );


        $request->user()->delete();
        $this->deleteUserAdverts($request->user());
        $this->deleteUserServices($request->user());


        Auth::guard()->logout();

        $request->session()->flush();

        $request->session()->regenerate();

        return redirect()->route('login');

    }

    private function now(){
        return (new Carbon('now'))->toDateTimeString();
    }

    private function deleteUserAdverts($user){

        //deletes all adverts
        foreach( $user->adverts as $advert ){

            $content = $this->now() . PHP_EOL . $advert->image . "\t\t" . $advert->label;
            Storage::prepend('logs/adverts_truncated.log',$content);
            Storage::disk('advert_images')->delete($advert->image);
            $advert->delete();
                        
        }

    }

    private function deleteUserServices($user){

         //deletes all services
        foreach($user->services as $service){

            $content = $this->now() . PHP_EOL . "\t\t" . $service->label;
            Storage::prepend('logs/adverts_truncated.log', $content);
            Storage::disk('cover_images')->delete($service->cover_image);
            
            foreach($service->photos as $image) {

                Storage::disk('service_images')->delete($image);

            }

            $service->delete();

        }

    }

    public function changePassword(Request $request){

        if( app('hash')->check( $request->cpass, $request->user()->password ) ) {

            $hashed = bcrypt( $request->pass );

            if(app('hash')->check($request->vpass, $hashed )){

                $message = "Password Updated";
                DB::table('users')->where(['id' => $request->user()->id ])->update(['password' => bcrypt($request->pass) ]);

            }else{
                
                $message = "Passwords do not match";

            }


        }else{

            $message = 'Passwords do not match';

        }

        $request->session()->regenerateToken();

        return redirect()->back()->with(['passMessage' => $message ]);
    }

    public function confirmEmail(){

    }

}
