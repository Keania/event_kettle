<?php
namespace App\Http\Controllers\Events;

use App\Http\Controllers\Controller;

class EventHome extends Controller
{

    public function index() {
        return view('events.homepage');
    }

}