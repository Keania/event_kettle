<?php
namespace App\Http\Controllers\Events;

use App\Http\Controllers\Controller;

class EventPage extends Controller
{
	public function index() {
		return view('events.eventpage');
	}
}