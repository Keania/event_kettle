<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Controller;
use App\Clients\Events;
use App\Clients\Service;
use App\User;

class AjaxController extends Controller
{
    //
    public function show(){
    	return view('admin.clients');
    }

    public function fetch(Request $request){

		if($request->ajax()){

			$id = $request->xerox;

			switch($request->get('activity')){


				//loads the adverts
				case 'adverts':

					return $this->advert($id);
					break;
 
				case 'services':
					$service = Service::find($request->xerox);
					return $this->service($service);
					break;

				default:
					return response()->json(['success' => false ]);
			}
		}
	}

	public function advert($id){

		$advert = Events::find($id);
        $user = $advert->user;
        $keys = ['label','xerox','ad_image','phone','price','desc','owner','userLink','time'];
        $values = [
            @$advert->label,
            @$advert->id,
            getAdvertImage($advert->image),
            @$user->phone ? : "Not Avialable",
            number_format($advert->price),
            @$advert->description,
            fullName($user),
            route('user.profile',['username' => @$user->username ]),
            @$advert->created->toFormattedDateString(),
        ];
        $options = array_combine($keys,$values);

		return response()->json($options);
	}

	private function service(UserProduct $product){

		return response()
		->view('admin.clients.services',[
	      	'fullname' => "{$advert->fname} {$advert->lname}",
	        'services' => $advert->services,
	        'profilePic' => $advert->photo,
		])->header('Content-Type','text/xml');

	}
 
	#truncate methods block start
	public function truncate(Request $request){

		if($request->ajax()){
			$id = $request->input('advertXerox');

			$state = DB::table('adverts')->where(['id' => $id ])->delete();
			$state = $state ? true : false;

			return response()->json(['success' => $state , 'Id' => $id ]);

		}
	}

	#truncate methods block end

	/**
	*input search method for ajax search requests
	*/
	public function search(Request $request){

		if ( $request->ajax() ){

			if( $request->person == true ){

				//search for username first
				$columns = ['id','fname','lname'];
				$advertname = $request->q;
				$searchString = DB::table('users')->select($columns)->where(['username' => $advertname ])->get();
				
				if(sizeof($searchString) < 1 ) {

					//search for first name and lastname
					$fname = @@(explode(' ',$request->q)[0]);
					$lname = @@(explode(' ',$request->q)[1]);

					$searchString = DB::table('users')
						->select($columns)
						->where([
					        ['fname',"LIKE", $fname ]
				        ])->orWhere(['lname' => $lname ])->get();

				}

				$searchString->map(function ($i) {
					$i->fname = ucfirst($i->fname);
					$i->lname = ucfirst($i->lname);
					return $i;
				});

				return response()->json($searchString);

			}

		}
	}

}
