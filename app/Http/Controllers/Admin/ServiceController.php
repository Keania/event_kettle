<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateServiceCategory;
use App\Shop\ServiceCategory as Service;
use Carbon\Carbon;

class ServiceController extends Controller
{
    //
    public function show(){
    	return view('admin.services');
    }

    public function create(CreateServiceCategory $request ){
        die();
        $done = DB::table('servicecategory')->insert(
        [
            'label' => $request->service_name,
            'description' => $request->service_desc,
            'created' => (new Carbon(strtotime('now')))->toDateTimeString()
        ]);
        // $service = new Service();
        // $service->label = $request->service_name;
        // $service->description = $request->service_desc;
        if($done){
            return redirect()->back()->with(['message' => 'Service Created']);
        }else{
            return redirect()->back()->withErrors(['error' => 'Error Creating Service ']);
        }
    }

    public function process(Request $request){

    	if($request->ajax()){

	    	$service = DB::table('servicecategory')->where(['id' => $request->xerox ]);

	    	if($service){

	    		$processed = $service->update([
		    		"label" => $request->name,
		    		"description" => $request->desc,
                 ]);

	    		$success =  $processed ? 200 : 300 ;

	    		return response()->json(['code' => $success ]);

	    	}else{
	    		return response()->json(['code' => 400 ]);
	    	}
    		
    	}

    }
    
}
