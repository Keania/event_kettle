<?php

namespace App\Http\Controllers\Admin;

use DB;
use Auth;
use App\Admin as AdminModel;
use Illuminate\Http\Request;
use App\Http\Requests\CreateAdmin;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{
    //
	public function show() {
		return view('admin.admins.list-admins',['admins' => AdminModel::all(), 'selected' => null ]);
	}

	public function showForm() {
		return view('admin.admins.form');
	}

	public function create(CreateAdmin $request){

		$okay = DB::table('admins')
				->insert([
					'first_name' => $request->firstname,
					'last_name' => $request->lastname,
					'username' => $request->username,
					'password' => bcrypt($request->password),
					'privilege' => $request->privilege,
				]);

		if ($okay) {
			return redirect()->back()->with(['message' => 'Administrator Created!']);
		}else{
			return redirect()->back()->with(['message' => 'Error Adding Administrator.']);
		}

	}

	public function update(CreateAdmin $request){

		$adminPass = Auth::guard('admin')->user()->password;

		//check admins password
		if ( app('hash')->check($request->password, $adminPass ) )
		{
			$okay = DB::table('admins')
			->where(['id' => $request->xerox ])->update([
				'username' => $request->username,
				'privilege' => $request->privilege,
			]);

			if ($okay) {
				return redirect()->back()->with(['message' => 'Changes Made!']);
			}else{
				return redirect()->back()->with(['error' => 'Error Adding Administrator.']);
			}

		}else{
			return redirect()->back()->withErrors(['updateError' => 'Your Password is incorrect.' ]);
		}


	}

	/*
	* modifies the staff data
	*
	*/
	public function manage(Request $request){

		return view('admin.admins.manage');

	}

	public function getAdmin(Request $request,$id){

		$params = [
			'admins' => AdminModel::all(),
			'selected' => AdminModel::find($id),
		];

		return view('admin.admins.list-admins', $params);
	}

}
