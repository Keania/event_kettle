<?php

namespace App\Http\Controllers\Admin;

use DB;
use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AccountController extends Controller
{
    //
    public function show(Request $request){
    	return view('admin.account',[ 'admin' => Auth::guard('admin')->user() ]);
    }

    public function update(Request $request){

    	$this->validate($request,[
            "old_password" => "string|required",
            "password" => "string|min:8|required|confirmed",
        ]);

        if (app('hash')->check($request->old_password,Auth::user()->password) )
        {
    		$query = DB::table('admins')->where(['id' => Auth::user()->id ])
    			->update(['password' => bcrypt($request->password)]);
    		if($query){
    			return redirect()->back()->with(['passMessage' => 'Password Changed']);
    		}else{
    			return redirect()->back()->withErrors(['passMessage' => 'Error Changing Password']);
    		}
        }else{
        	return redirect()->back()->withErrors(['old_password' => 'Current Password is Incorrect']);
        }
    }
}
