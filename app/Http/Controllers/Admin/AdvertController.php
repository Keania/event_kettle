<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Shop\Category;
use App\Http\Controllers\Controller;
use Illuminate\Database\QueryException;

class AdvertController extends Controller
{
    //
    public function show(){
    	return view('admin.category');
    }

    public function update(Request $request){

    	if($request->ajax()):

	    	if($request->action == 2){

	    		try{

		    		$category = Category::find($request->xerox);
		    		$success = DB::table('category')->where(['cat_id' => $request->xerox ])->update([
			    		'name' => $request->name,
			    		'icon' => $request->icon,
			    		'position' => (int) $category->position, 
					]);
		    		$oak = $success ? true : false;

	    		}catch( QueryException $x ){
	    			$oak = $x->getCode() ;
	    		}
	    		return response()->json(['success' => $oak ]);
	    	}
    	endif;
    }
}
