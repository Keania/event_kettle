<?php

namespace App\Http\Controllers\Admin;

use App\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;

class LoginController extends Controller
{

	use AuthenticatesUsers;

	public function __construct(){
		$this->middleware('guest',['except' => 'logout']);
	}

	public function showLogin(){
		return view('admin.login');
	}

	public function redirectPath(){
		return route('admin.dashboard');
	}

	public function username(){
		return 'username';
	}

	public function guard(){
		return Auth::guard('admin');
	}

	public function authenticated(Request $request, $user){

		if($user->privilege < 1){


			$this->guard()->logout();

	        $request->session()->flush();
	        $request->session()->regenerate();

			$request->flash();

	        return redirect()->back()
	        	->withErrors(['privilege' => 'Am sorry, but your account has been disabled.']);
		}

	}

}


