<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Controller;
use App\User;

class ClientController extends Controller
{
    //
    public function show(){
    	return view('admin.clients');
    }

    public function fetch(Request $request){
		if($request->ajax()){

			$user = User::find($request->xerox);

			switch($request->get('activity')){

				case 'adverts':
					return $this->getAdverts($user);
					break;

				case 'services':
					return $this->getServices($user);
					break;

				case 'manage':
					return $this->getInformation($user);
					break;

				default:
					return response()->json(['success' => false ]);
			
			}
		}
	}

	public function getAdverts(User $user) {
		
		$imageLink = Storage::disk("user_images")->exists($user->photo)
			? Storage::disk("user_images")->url($user->photo)
			: asset("images/assets/user.svg");

		$props = [
			'fullname' => $user->fname." ".$user->lname,
			'adverts' => $user->adverts,
			'username' => $user->username,
			'photo' => $imageLink
		];

		return response()->json($props); //('admin.clients.events',compact('user'))->header("Content-Type","text/xml");

	}

	public function getInformation(User $user){
		return view('admin.clients.manage',['user' => $user ]);
	}

	private function getServices(User $user){

		return response()
		->view('admin.clients.services',[
	      	'fullname' => "{$user->fname} {$user->lname}",
	        'services' => $user->services,
	        'profilePic' => $user->photo,
		])->header('Content-Type','text/xml');

	}
 
	#truncate methods block start
	public function truncate(Request $request){

		if($request->ajax()){
			$id = $request->input('advertXerox');

			$state = DB::table('adverts')->where(['id' => $id ])->delete();
			$state = $state ? true : false;

			return response()->json(['success' => $state , 'Id' => $id ]);

		}
	}

	#truncate methods block end

	/**
	*input search method for ajax search requests
	*/
	public function search(Request $request){

		if ( $request->ajax() ){

			if( $request->input('q')[0] === "@" ){

				//search for username first
				$columns = ['id','fname','lname'];
				$username = $request->q;
				$searchString = DB::table('users')->select($columns)->where(['username' => substr($username, 1, -1) ])->get();
				
				if(sizeof($searchString) < 1 ) {

					//search for first name and lastname
					$fname = @@(explode(' ',$request->q)[0]);
					$lname = @@(explode(' ',$request->q)[1]);

					$searchString = DB::table('users')
						->select($columns)
						->where([
					        ['fname',"LIKE", $fname ]
				        ])->orWhere(['lname' => $lname ])->get();

				}

				$searchString->map(function ($i) {
					$i->fname = ucfirst($i->fname);
					$i->lname = ucfirst($i->lname);
					return $i;
				});

				return response()->json($searchString);

			}

		}
	}

}
