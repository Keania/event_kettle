<?php

namespace App\Http\Requests;

use Illuminate\Http\Request;
use Illuminate\Validation\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Http\FormRequest;

class AdvertUpdate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if(Auth::user() !== null){
            return true;
        }
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'advert__subject' => 'string|required|max:50',
            'product__pricing' => 'numeric|required|min:0',
            'advert__description' => 'required|min:40',
            'quantity' => 'numeric|required|min:1',
        ];
    }

    public function messages(){
        return [
            'advert__subject.string' => 'Advert Label must be a string', 
            'quantity.required' => 'Let us know the quantity of the product.',
            'quantity.numeric' => 'This quantity input only accept numbers.',
            'advert__subject.required' => 'pls give your advert a label', 
            'advert__description.min' => 'please add more details to your description',
            'product__pricing.required' => 'You don\'t add a fixed price',
            'product__pricing.min' => 'Please add a price tag to your advert',
        ];
    }

    public function withValidator(Validator $validator){
        
        if( $validator->fails() ) {

            request()->flash();
            return redirect()->back()->withErrors($validator);

        }

    }
}
