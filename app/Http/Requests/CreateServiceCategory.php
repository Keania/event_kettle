<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Auth;

class CreateServiceCategory extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if(Auth::guard('admin')->user()){
            return true;
        }
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'service_name' => "string|required|max:20",
            'service_desc' => "string|required|min:20",
        ];
    }

    public function message(){
        return
        [
            'service_name.max' => 'Service Name is not to exceed :max.',
            'service_name.required' => 'Service Name is empty, please fill.',
            'service_desc.min' => 'I can bearly understand your description',
        ];
    }
}
