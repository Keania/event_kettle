<?php

namespace App\Http\Requests;

use Illuminate\Http\Request;
use Illuminate\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Auth;

class CreateAdmin extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if( Auth::guard("admin")->user() !== null ){
            return true;
        }
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $except = [
            'firstname' => 'string|required|max:30',
            'lastname' => 'string|required|max:30'
        ];
        $main = [
            'username' => 'required|max:30',
            'password' => 'required',
            'privilege' => "numeric|required"
        ];

        if(request()->method('post')){
            return array_merge($except,$main);
        }else{
            return $main;
        }
    }

    public function withValidator(Validator $validator){
        
        if( $validator->fails() ) {

            return redirect()->back();

        }

    }
}
